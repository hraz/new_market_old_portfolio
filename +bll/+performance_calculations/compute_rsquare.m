function [rsquare rmse] = compute_rsquare(portfolioValue,marketReturn)

%   COMPUTE_RSQUARE compute the rsquare and rmse of the fit between the portfolio returns and the
%   market returns.
%
%   [rsquare rmse] = compute_rsquare(portfolioValue,marketReturn)
%   receives the portfolio values over time, the market return series,
%   and returns rsquare and rmse between the portfolio and market returns
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days.
%       marketReturn - a vector of market returns, given in the same
%       times frequency as portfolioValue.
%   Output:
%       rsquare =  1-SSerr/SStot, SSerr = sum(portfolioReturn - marketReturn)^2; SStot = sum(portfolioReturn- portfolioMeanReturn)^2;
%       rmse = sqrt(SSerr/length(portfolioReturn)), i.e., estimation of the std of
%       the random component in the data (portfolioReturn) with respect to the
%       model (marketReturn);
%   Sample:
%		Some example of the function using.
%
%		See Also:
%		compure_alpha, compute_beta, compute_omega, compute_moments,
%		compute_ratios, compute_downside_volatility, compute_jensen_alpha
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
s2=size(marketReturn);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif ~isa(marketReturn,'numeric')
    error('compute_omega:wrongInputType','marketReturn is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0
    error('compute_omega:wrongInputSize','portfolioValue is expected to be a vector.')
elseif sum(s2>1)==2 || sum(s2>1) == 0
    error('compute_omega:wrongInputSize','marketReturn is expected to be a vector.')
elseif s1(1)>s1(2) %a column vector
    if s1(1)~=s2(1)+1 %number of rows in the portfolioValue vector must be greater than the number of rows in the marketReturn vector
        error('compute_omega:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
elseif s1(1)<s1(2) %same for the case of row vector
    if s1(2)~=s2(2)+1
        error('compute_omega:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
end

%% compute rsquare
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
%[~,gof] = fit(portfolioReturnSeries,marketReturn,'poly1');
rsquare = 1; %gof.rsquare;
rmse = 1; %gof.rmse;


