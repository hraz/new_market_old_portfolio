function [ marginal_value_at_risk component_value_at_risk] = mVaR( percentile_risk, cumulative_returns, hist_length )
%function computes marginal VaR for all assets in portfolio.  The
%computation is done by computing the VaR for the portfolio without a given
%asset and dividing this quantity by the portfolio VaR.
%
%
% input: cumulative_returns - N-ASSETS X asset_count X time matrix containing list of cumulative_income (1),
%                            asset count (2), for time_periods of time (3)
%
%        percentile_risk - double - scalar between 1-10% saying the percentile wanted
%
%        hist_length - double - scalar telling the number of days back for the function to check, default is 60
%
%
% output: marginal_value_at_risk - N-ASSETS X 1 vector with marginal value at risk for each bond
%
%         component_value_at_risk - N-ASSETS X 1 vector with component
%         value at risk for each bond
% written by Hillel Raz, January 2013, BondIT

import simulations.*;

num_bonds = size(cumulative_returns, 1);
hist_length_actual = size(cumulative_returns, 3);

marginal_value_at_risk = zeros(1, num_bonds); %to store marginal value at risk per bond
component_value_at_risk = zeros(1, num_bonds); %to store component value at risk per bond
cumulative_returns_all = zeros(1, hist_length_actual);

cumulative_returns_all(:) =sum( cumulative_returns(:, 1, :).* cumulative_returns(:, 2, :)); %calculate return of portfolio for every given time unit

[value_at_risk] = VaR(percentile_risk, cumulative_returns_all, hist_length); %get VaR

for i = 1:num_bonds
    cumulative_returns_matrix_without_i = cumulative_returns;
    cumulative_returns_matrix_without_i(i, 1, :) = 0; %remove i-th returns
    cumulative_returns_without_i = sum( cumulative_returns_matrix_without_i(:, 1, :).* cumulative_returns_matrix_without_i(:, 2, :));
    %calculate the returns without bond i
    
    [value_at_risk_i] = VaR(percentile_risk, cumulative_returns_without_i, hist_length);%get VaR for portfolio without asset i
    marginal_value_at_risk(i) = value_at_risk_i/value_at_risk; %marginal VaR for i-th asset
end

component_value_at_risk = marginal_value_at_risk.*cumulative_returns(:, 1, end)';

end

