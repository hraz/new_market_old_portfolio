clc; clear;
dbstop if error
vecNsin = [ 1105659
     1106913
     1111327
     1940121
     1940188
     1980218
     2260099
     2260198
     2810216
     4110102
     4250155
     7150204
     7410053
     7410178
     7460207];
vecWeight = [    0.0848
    0.0943
    0.0132
    0.0950
    0.0658
    0.0102
    0.0290
    0.0569
    0.0996
    0.1004
    0.0164
    0.1010
    0.0996
    0.0505
    0.0833];
allocation=dataset(vecNsin,vecWeight,'VarNames', {'nsin', 'weight'});
 reqDate = 732859;
 calc_expected_return([], allocation, [], reqDate)