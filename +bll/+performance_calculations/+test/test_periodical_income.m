clear; clc;

%% Import external packages:
import dal.market.filter.static.*;
import bll.simulation.*;

%% find value of bonds at time t_f starting from t_0
start_date = '2005-10-13';
end_date =  '2008-10-13';

param.coupon_type = {'Fixed Interest'};

nsin = intersect(bond_static_complex( start_date, param ), bond_static_complex( end_date, param ));

[varargout] = periodical_income( nsin, start_date, end_date );