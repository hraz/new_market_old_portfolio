function varargout = calc_expected_risk(varargin)
%CALC_EXPECTED_RISK calculates expected risk
%
%    varargout = calc_expected_risk(varargin) calculates expected risk from
%    the clean price of the bonds in the portfolio. The number of
%    observations entered via ftsPrices should be the number wanted for
%    caculating the risk.
%
%    Input:
%        Possibility 1: (mtxCovariance, allocation)
%                       ([], allocation, hisLength,reqDate, idModel, idBenchmark)
%                                 (risk using Covariance matrix)
%        =============
%        ftsPrices - NDates X NBonds fts, historical prices on which to do the calculation
%         NOTE: if this value is entered empty (=[]) then the function fetches the data itself
%             and needs in addition histLength input
%        mtxCovariance - nASSETS X mDAYS DOUBLE - matrix of covariance
%           between different clean price yield values of bonds.
%        allocation - dataset with at least the following fields:
%         nsin - nASSETS X 1 - sorted
%         weight - nASSETS X 1 - same order as nsins
%        idModel - INTEGER - 1 = Markowitz
%                            2 = Single Index
%                            3 = Omega
%                            4 = Multi Index
%                            5 = Spread
%                            6 = Risk Parity
%        idBenchmark - INTEGER - the nsin of the benchmark versus which to
%           compare - only valid for idModel = 2.
%        histLength - Needed only in the case ftsPrices=[]. States the amount of observarion
%           to use in calculation.
%        reqDate - DATENUM - needed only in the case ftsPrices = [].
%
%        Possibility 2: (?)
%        =============
%
%
%    Output:
%        expRisk - DOUBLE - the expected risk of the portfolio based on the
%            covariance matrix
%
%
% Written by Hillel Raz, Jan 2013
% Updated by Amit Godel, Apr 2013
%     changed varargin{2} variable type to dataset, instead of original
%     struct (needed for correspondenceto rest of the code)
%     also changed the isfield(..) to ismember(fieldnames(..)
% Copyright, BondIT Ltd

 global NBUSINESSDAYS
import dal.market.get.dynamic.*;
import bll.data_manipulations.*;
import bll.solver_calculations.*;
import utility.*;

%% Check input
if ~isempty(varargin{1}) &&isa(varargin{1}, 'double') && isa(varargin{2}, 'dataset') && ismember('weight',fieldnames(varargin{2}))
    mtxCovariance = varargin{1};
    allocation = varargin{2};
    weightsBonds = allocation.weight;
elseif  isa(varargin{2}, 'dataset') && ismember('nsin',fieldnames(varargin{2})) && ismember('count',fieldnames(varargin{2}))
    allocation = varargin{2};   
    histLength = varargin{3};
    reqDate = varargin{4};
    idModel = varargin{5};
    if nargin == 6
        idBenchmark = varargin{6};
    end
    tbl_name = 'bond';
    data_name =  {'price_clean_yld'};

    %% Remove 0's from list of nsins for DB pulls
    nsins = allocation.nsin;
    nsins = setdiff(nsins, 0);
    [ftsPrice] = get_multi_dyn_between_dates( nsins , data_name , tbl_name, reqDate-histLength, reqDate);
    mtxPricesYld = fts2mat(ftsPrice.price_clean_yld);
    strLiveNsins=fieldnames(ftsPrice.price_clean_yld);
    liveNsins=strid2numid(strLiveNsins(4:end));
    liveNsinsIndx=find_multi_indx(liveNsins,allocation.nsin);
    weightsBonds = allocation.weight(liveNsinsIndx);  

    %% Type of risk to calculate - based on model
    flagMarkowitz = 0;
    if idModel == 1
        flagMarkowitz = 1;
        flagRiskCalc = 1;
    elseif idModel == 2 % Single Index
        flagRiskCalc = 2;
    else
        flagRiskCalc = 1; % Other models
    end

    %% Remove NaNs from mtxPrices
    dates_spots_erased = [];
    nsins2throw = [];
    NumNanAllowed = 0; % Can't allow any NaNs in this data
    if sum(sum(isnan(mtxPricesYld))) % If there are NaN's in the data
        [mtxPricesYld, nsins2throw, dates_spots_erased] = GetRidOfNan(mtxPricesYld, flagMarkowitz, NumNanAllowed);
    end   
    if any(nsins2throw)
        weightsBonds(nsins2throw) = []; % Erase bonds that may have NaN's in them.
        weightsBonds = weightsBonds/sum(weightsBonds); % Renormalize weights
    end
    if flagRiskCalc == 1
        [~,  mtxCovariance] = ewstats(mtxPricesYld);
    elseif flagRiskCalc == 2
        flagSigma_eiMethod = 1; % Use Sigma_ei Method
        histLength = size(mtxPricesYld, 1);
        [vecIndexPriceYld vecIndexYTM]=GetMktIndex(reqDate,histLength,idBenchmark);
        [~,~,~,mtxCovariance]= calc_single_index_params(mtxPricesYld,vecIndexPriceYld,vecIndexYTM,flagSigma_eiMethod,histLength);
    end
else
    error('calc_expected_risk:wrongInput', ...
        'One or more input arguments is not of the right type');
end

%% Calculate Risk
expRisk = sqrt(NBUSINESSDAYS*weightsBonds'*mtxCovariance*weightsBonds);
varargout{1} = expRisk;
