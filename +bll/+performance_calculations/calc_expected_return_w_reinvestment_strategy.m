function [expReturn] = calc_expected_return_w_reinvestment_strategy(reinvestmentStrategy, PortWts, YTM, varargin)
%CALC_EXPECTED_RETURN_W_REINVESTMENT_STRATEGY takes into account the reinvestment strategy in calculating expected return.
%
% [expReturn] = calc_expected_return_w_reinvestment_strategy(reinvestmentStrategy, PortWts, YTM, varargin)
% Calculates the expected return of a portfolio for the entire portfolio duration taking into account the reinvestment strategy for bonds that ended.
% Options for input:
%   calc_expected_return_w_reinvestment_strategy('Specific-Bond-Reinvestment',
%       PortWts, YTM) - 'Specific-Bond-Reinvestment' (also
%       'real-bond-reinvestment')
%   calc_expected_return_w_reinvestment_strategy('risk-free',
%       weight, vecYTM, portTTM, TTM, reqDate) - 'risk-free' - need
%       to pull risk free data.
%   calc_expected_return_w_reinvestment_strategy('risk-free',
%       weight, vecYTM, portTTM, TTM, riskfreeRates, riskfreeTTM) -
%       'risk-free', no need to pull data
%   calc_expected_return_w_reinvestment_strategy('benchmark', weight, YTM,
%       portTTM, TTM, benchmarkID, reqDate) - benchmark, no need to
%       pull data
%    Input:  PortWts - N X 1 double - containing weights of bonds in portfolio
%            Duration - N X 1 double - vector containing durations of each bond
%            YTM - N X 1 double - vector containing the yearly YTM's of the bonds
%            reinvestmentStrategy - string - what type of reinvestment strategy to calculate, possibilities:
%               1. 'Specific-Bond-Reinvestment': we invest in the same
%                   bond from which we got a coupon.
%               2. 'risk-free': we invest in a risk free asset (government bond).
%               3. 'portfolio': we invest in the entire portfolio.
%               4. 'benchmark': we invest in the appropriate benchmark.
%               5. 'bank': we put the money in the bank to earn interest.
%               6. 'none':
%               7. 'real-specific-bond-reinvestment': seek the best bond
%                   based on closest YTM to original bond's YTM
%           varargin:    riskfreeRates - 7 X 1 double - yields of risk free rates
%                        riskfreettm - 1 X 7 double - ttms of riskfree bonds
%                        portTTM - double - portfolio weighted ttm.
%                        TTM - double - Nassets X 1 of bonds' ttm
%                        reqDate - double - required date
%                        benchmarkID - Double - benchmark id used for DB
%                           calls
%    Output: expReturn - Double - expected annual return calculated for entire duration of portfolio.
%
%    See also: calc_expected_return
%
%    Hillel Raz, June 2013
%    Copyright 2013, BondIT LTD.
%    Updated by Hillel Raz, July 2013


%% Check input arguments type:
flagType(1) = isa(PortWts, 'double');
flagType(2) = isa(reinvestmentStrategy, 'char');
flagType(3) = isa(YTM, 'double');

if strcmp(reinvestmentStrategy, 'risk-free') & nargin == 6
    portTTM = varargin{1};
    TTM = varargin{2};
    reqDate = varargin{3};
    flagType(4) = isa(portTTM, 'double');
    flagType(5) = isa(TTM, 'double');
    flagType(6) = isa(reqDate, 'double');
end

if strcmp(reinvestmentStrategy, 'risk-free') | strcmp(reinvestmentStrategy, 'benchmark')
    if nargin == 7
        portTTM = varargin{1};
        TTM = varargin{2};
        flagType(4) = isa(portTTM, 'double');
        flagType(5) = isa(TTM, 'double');
        if strcmp(reinvestmentStrategy, 'risk-free')
            riskfreeRates = varargin{3};
            riskfreettm = varargin{4};
            flagType(6) = isa(riskfreeRates, 'double');
            flagType(7) = isa(riskfreettm, 'double');
        elseif strcmp(reinvestmentStrategy, 'benchmark')
            benchmarkID = varargin{3};
            reqDate = varargin{4};
            flagType(6) = isa(benchmarkID, 'double');
            flagType(7) = isa(reqDate, 'double');
        end
    end
end
if sum(flagType) ~= length(flagType)
    error('calc_expected_return_w_reinvestment_strategy:wrongInput', ...
        'One or more input arguments is not of the right type');
end

if size(PortWts, 1) < size(PortWts, 2)
    PortWts = PortWts';
end
if size(YTM, 1) < size(YTM, 2)
    YTM = YTM';
end

if exist( 'TTM')
    if size(TTM, 1) < size(TTM, 2)
        TTM = TTM';
    end
end
%% Check Reinvestment strategy and calculate
expReturn= [];
if strcmp(reinvestmentStrategy, 'risk-free')
    % Calculation here uses the appropriate risk free rate, according to the
    % time left, and calculates the expected return this way.
    
    durationsLessThanPort = min(TTM, portTTM); % Get times less than portfolio duration
    riskfreeDurations = portTTM - durationsLessThanPort; % Necessary riskfree durations, 0 is ok
    if nargin == 6 %Need to pull riskfree data
        interest_period = unique(ceil(riskfreeDurations));
        if interest_period(1) == 0
            interest_period = interest_period(2:end);
        end
        import dal.market.get.base_data.*;
        
        nominal_yield = get_nominal_yield_with_param(interest_period, reqDate, reqDate);
        riskfreeRates = fts2mat(nominal_yield.yield);
        riskfreettm = fts2mat(nominal_yield.ttm);
    end
    if size(riskfreeRates, 2) > size(riskfreeRates, 1)
        riskfreeRates = riskfreeRates';
    end
    if size(riskfreettm, 2) < size(riskfreettm, 1)
        riskfreettm = riskfreettm';
    end
    riskfreeRates = [ 0; riskfreeRates];
    %% Find closest riskfree range, which is larger than riskfreeDurations
    
    possibleRiskfreeDurations = unique(ceil(riskfreeDurations));
    numRiskfreeDurations = length(possibleRiskfreeDurations);
    numBonds = length(riskfreeDurations);
    riskfreeIndices = zeros(numBonds, 1);
    for iter1 = 1:numBonds
        for iter2 = 1:numRiskfreeDurations
            if ceil(riskfreeDurations(iter1)) == possibleRiskfreeDurations(iter2)
                riskfreeIndices(iter1) = iter2;
            end
        end
    end
    expReturn = (sum( (PortWts.*(1+YTM).^durationsLessThanPort).*(1+riskfreeRates(riskfreeIndices)).^riskfreeDurations))^(1/portTTM) - 1;
elseif strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment')|| strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment')
    expReturn = PortWts'*YTM;
    
elseif strcmp(reinvestmentStrategy, 'benchmark')
    import dal.market.get.dynamic.*; % For get_multi_dyn_between_dates
    
    ftsBenchmark = get_multi_dyn_between_dates(benchmarkID, {'ytm'}, 'index', reqDate, reqDate);
    ytmBenchmark = fts2mat(ftsBenchmark.ytm);
    
    durationsLessThanPort = min(TTM, portTTM); % Get times less than portfolio duration
    benchmarkInvestedDurations = portTTM - durationsLessThanPort; % Necessary riskfree durations, 0 is ok
    if portTTM ~= 0
        expReturn = ( sum( (PortWts.*(1+YTM).^durationsLessThanPort).*(1+ytmBenchmark).^benchmarkInvestedDurations))^(1/portTTM) - 1;
    else
        expReturn = 0; % Last day of portfolio, expected return has no meaning at this point, set to 0. 
    end
    
end