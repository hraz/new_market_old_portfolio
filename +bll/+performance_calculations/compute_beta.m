function beta = compute_beta(portfolioValue,marketReturn)


%   COMPUTE_BETA computes the beta of the portfolio based on the portfolio returns and the
%   market index returns.
%
%   beta = compute_beta(portfolioValue,marketReturn)
%   receives the portfolio values over time, the market returns vector,
%   and returns the portfolio beta with the input market.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days.
%       marketReturn - a vector of market returns, given in the same
%       times frequency as portfolioValue.
%   Output:
%   beta  - a measure of correlation between the portfolio and the market
%   index. beta is defined as the covariance of the portfolio with the
%   market, divided by the market variance.
%   Sample:
%		Some example of the function using.
%
%		See Also:
%		compute_alpha, compute_omega, compute_moments,
%		compute_ratios, compute_downside_volatility, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
s2=size(marketReturn);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif ~isa(marketReturn,'numeric')
    error('compute_omega:wrongInputType','marketReturn is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0
    error('compute_omega:wrongInputSize','portfolioValue is expected to be a vector.')
elseif sum(s2>1)==2 || sum(s2>1) == 0
    error('compute_omega:wrongInputSize','marketReturn is expected to be a vector.')
elseif s1(2)~=1 || s2(2)~=1
    error('compute_omega:wrongInputSize','portfolioValue and marketReturn are expected to be column vectors.')
elseif s1(1)>s1(2) %a column vector
    if s1(1)~=s2(1)+1 %number of rows in the portfolioValue vector must be greater than the number of rows in the marketReturn vector
        error('compute_omega:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
elseif s1(1)<s1(2) %same for the case of row vector
    if s1(2)~=s2(2)+1
        error('compute_omega:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
end


%% portfolio returns
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
portfolioMeanDailyReturn = mean(portfolioReturnSeries);                            % Average daily return over the investment period
centeredPortfolioReturnSeries = portfolioReturnSeries - portfolioMeanDailyReturn;  % Subtruct the expectation of the portfolio returns from its return series

%% market index returns
marketMeanDailyReturn = mean(marketReturn);                                        % Average daily return of the market index over the investment period
centeredMarketReturnSeries = marketReturn - marketMeanDailyReturn;                 % Substruct the mean as the formula for expectation requires
numerator=centeredPortfolioReturnSeries'*centeredMarketReturnSeries;           % Calc the inner product of the centerd portfolio return with the marke's centerd return.
denominator=centeredMarketReturnSeries'*centeredMarketReturnSeries;            % Inner product between the market returns to itself.
beta=numerator/denominator;                                                    % Estimation of beta by definition: cov(portfolio,market)/var(market)

