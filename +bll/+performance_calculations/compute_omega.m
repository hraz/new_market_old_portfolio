function omega = compute_omega(portfolioValue, mar)


%compute_omega computes the omega ratio of the portfolio.
%			
%   omega = compute_omega(portfolioValue, mar)
%   receives the portfolio values over time, and the minimum
%   acceptable return, and returns the omega ratio of the portfolio.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days. 
%       mar �  minimum acceptable return, i.e., the minimal return which is
%       acceptable by the investor. Any return which lower than mar is
%       considered a failaur, while any return above mar is considered a
%       success. The investor's mar should be in annual terms, which is
%       then converted to daily terms in order to be used with the daily
%       portfolioValue vector.
%   Output:
%       omega � the ratio beween the sum of probabilities of obtaining a return
%       greater than mar to the sum of probabilities of obtaining a return that is lower
%       than mar. A higher omega is always considered better than a lower
%       omega.
%   Sample:
%		Some example of the function using.
% 	
%		See Also:
%		compure_alpha, compute_beta, compute_moments,
%		compute_ratios, compute_downside_volatility, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
s2=size(mar);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif ~isa(mar,'numeric')
    error('compute_omega:wrongInputType','mar is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0                                              
   error('compute_omega:wrongInputSize','portfolioValue is expected to be a vector.')
elseif sum(s2)~=2 
    error('compute_omega:wrongInputSize','mar is expected to be a scaler')
elseif mar<0
    error('compute_omega:wrongInputSize','mar is expected to be a positive scaler')
% elseif mar>1
%     error('compute_omega:wrongInputSize','mar is expected to be a fractional scaler, e.g., for mar=6 percent, enter 0.06 instead of 6');
end


%% computation of omega
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
marDaily = Year2Day(mar);                                                      % Daily minimum acceptable return
omega = lpm(-portfolioReturnSeries, -marDaily , 1) / lpm(portfolioReturnSeries, marDaily ,1); % Compute Omega according to definition.
