clear; clc;

import bll.unsmoothing.*;
import dal.market.get.dynamic.*;

 nsin = [ 1060052 1098656];
 rdate = '2008-01-02';
data_name = 'ytm';
alpha1 = 0.5;
alpha2 = 0.4;


 [ source ] = da_bond_dyn( nsin , data_name , rdate );
 
 foarf_fts = foarf(source, alpha1);
 ar2_fts = ar2(source, alpha1, alpha2);
 fivi_fts = fivi(source, alpha1);
 states_fts = states(source, alpha1);