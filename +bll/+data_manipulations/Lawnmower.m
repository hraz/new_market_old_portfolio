function [solution solver_props nsin_data] = Lawnmower(solver_props, solution, nsin_data)
% 
% function that removes bonds with low weight from solution and returns their index
% 
% input: filtered - data structure containing NASSETS
%        solver_props - data structure containing properties needed for solver
%        solution - data structure containing the solution of the optimization, including list of M-ASSETS of solutin, their weights, expected return, risk, duration
%        
% output: solution - data structure containing updated solution of the optimization, including list of M-ASSETS of solution, their weights, expected return, risk, duration
%        
% 
% written by Hillel Raz, January 2013, BondIT
% 

import bll.data_manipulations.*; 

%% remove bonds with not enough weight
[flag_bonds_removed, solution.selected_indx] =  keep_only_nsin_with_large_weights(solution, solver_props.MinNumOfBonds, solver_props.MaxWeight2NonAllocBonds); 


if flag_bonds_removed
    solution.nsin_removed_due_to_small_weights = setdiff(solution.nsin_sol, solution.nsin_sol(solution.selected_indx));
    solution.nsin_sol = solution.nsin_sol(solution.selected_indx);
   
    solution.NumBondsSol = length(solution.nsin_sol);
    solution.PortWts = solution.PortWts(solution.selected_indx);
    
    [solver_props] = raise_max_bond_wgt(solver_props, solution.NumBondsSol);
    
    if (solution.NumBondsSol >= 1/solver_props.MaxBondWeight) && flag_bonds_removed
        if size(solution.selected_indx, 1)>size(solution.selected_indx, 2)
            solution.selected_indx = solution.selected_indx';
        end
        solver_props.NewConSet = solver_props.conSet(:,[solution.selected_indx end]);
        
        solver_props.solved = 1; %flag to solver that solution has been found once and there is no need to pull data again.
        
        %%%%%%%%%%%%%% TO DO: add check that if began with enough bonds and
        %%%%%%%%%%%%%% threw away so that now have not enough bonds, still
        %%%%%%%%%%%%%% should find a solution
        
    elseif (solution.NumBondsSol < 1/solver_props.MaxBondWeight)
        solution.error = ['NoSolution-NotEnoughBondsWithSubstantialWeightOptSh' num2str(solver_props.port_duration_sought)];
        solution.PortReturn = NaN;
        solution.PortRisk = NaN;
        
    end
    
end