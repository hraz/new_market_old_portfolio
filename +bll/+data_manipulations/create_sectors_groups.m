function [SectorGroups]=create_sectors_groups(Sector,N)
% Creat a matrix of groups of bonds with identical sectors.
% each row is one group, and the 1's indicate indices of bonds that
% are members of the same group.

SectorScope=unique(Sector); %FindDiffSectorsFunc(Sector); %find all kinds of different sectors that exist in among the bonds that passed the filters
%[SectorScope SectorLocations]=FilterUnconstSectorsSim(SectorScope,MaxSectorWeight,MinSectorWeight,AllSectors); % there is no need to add a line in the constraint matrix for sectors that appear as one of the sectors of bonds that passed the filters, but are not constraintd by the user
SectorGroups=[];
if ~isempty(SectorScope)
    SectorGroups=zeros(length(SectorScope),N); %preallocate
    for i=1:length(SectorScope) %for each desired group create a line with ones and zeros in the correct places
        SectorGroups(i,:)=strcmp(Sector,SectorScope(i)); %the indices with 1's, are bonds with sectors identical to SectorScope(1)
    end
end

%     SectorGroups=zeros(length(SectorScope),N); %preallocate
%     for bond_num = 1:N
%         SectorGroups(bond_num, Sector) = 1;
%     end