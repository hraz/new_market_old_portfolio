function [solver_props] = additional_fields_for_debug_solver_props(solver_props)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a = find(solver_props.UserBondTypes>0);
b = ones(1, 7); c= zeros(1,7);
solver_props.MaxTypeWeight = b(a);
solver_props.MinTypeWeight = c(a);


solver_props.AllSectors={  'FASHION & CLOTHING'
    'FOREIGN SHARES AND OPTIONS'
    'INDEX PRODUCTS';
    'BIOMED'
    'CHEMICAL RUBBER & PLASTIC '
    'COMMERCE'
    'COMMERCIAL BANKS'
    'COMMUNICATIONS & MEDIA '
    'COMPUTERS'
    'ELECTRONICS'
    'FINANCIAL SERVICES'
    'FOOD'
    'GOVERNMENT'
    'HOTELS & TOURISM '
    'INSURANCE COMPANIES'
    'INVESTMENT & HOLDINGS'
    'ISSUANCES'
    'METAL'
    'MISCELLANEOUS INDUSTRY'
    'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS'
    'OIL & GAS EXPLORATION'
    'REAL-ESTATE AND CONSTRUCTION'
    'SERVICES'
    'STRUCTURED BONDS '
    'UNKNOWN'
    'WOOD & PAPER'};

solver_props.SectorsToInclude = solver_props.AllSectors;

solver_props.TypeFilterParams{1}.bond_type= {'Corporate Bond'};
solver_props.TypeFilterParams{1}.linkage_index= {'NON LINKED'};
solver_props.TypeFilterParams{1}.coupon_type= {'Fixed Interest'}; %TODO: add 'Unknown'
solver_props.TypeFilterParams{1}.sector= solver_props.SectorsToInclude;

solver_props.TypeFilterParams{2}.bond_type= {'Corporate Bond'};
solver_props.TypeFilterParams{2}.linkage_index= {'CPI'};
solver_props.TypeFilterParams{2}.coupon_type= {'Fixed Interest'};
solver_props.TypeFilterParams{2}.sector= solver_props.SectorsToInclude;

solver_props.TypeFilterParams{3}.bond_type= {'Corporate Bond'};
solver_props.TypeFilterParams{3}.linkage_index= {'NON LINKED'};
solver_props.TypeFilterParams{3}.coupon_type= {'Variable Interest'};
solver_props.TypeFilterParams{3}.sector= solver_props.SectorsToInclude;

solver_props.TypeFilterParams{4}.bond_type= {'Corporate Bond'};
solver_props.TypeFilterParams{4}.linkage_index= {'USD' , 'UKP', 'EURO'};
solver_props.TypeFilterParams{4}.coupon_type= {'Fixed Interest' , 'Variable Interest'}; %TODO: add 'Special'
solver_props.TypeFilterParams{4}.sector= solver_props.SectorsToInclude;

solver_props.TypeFilterParams{5}.bond_type= {'Government Bond Shachar' 'Short Term Treasury Bill'};
solver_props.TypeFilterParams{5}.linkage_index= {'NON LINKED'};
solver_props.TypeFilterParams{5}.coupon_type= {'Fixed Interest' 'Zero Coupon'};
solver_props.TypeFilterParams{5}.sector= solver_props.SectorsToInclude;

solver_props.TypeFilterParams{6}.bond_type= {'Government Bond Galil' 'Government Bond Improved Galil'};
solver_props.TypeFilterParams{6}.linkage_index= {'CPI'};
solver_props.TypeFilterParams{6}.coupon_type= {'Fixed Interest'};
solver_props.TypeFilterParams{6}.sector= solver_props.SectorsToInclude;

solver_props.TypeFilterParams{7}.bond_type= {'Government Bond New Gilon' 'Government Bond Gilon'};
solver_props.TypeFilterParams{7}.linkage_index= {'NON LINKED'};
solver_props.TypeFilterParams{7}.coupon_type= {'Variable Interest'};
solver_props.TypeFilterParams{7}.sector= solver_props.SectorsToInclude;

%%%%%%%%%%%%%%%%
