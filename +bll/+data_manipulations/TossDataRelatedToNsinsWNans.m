function [nsin_data] = TossDataRelatedToNsinsWNans(nsin_data, solver_props, index_of_bad_nsins )
% 
% function erases the columns or rows related to nsins that have nans in their data.
% 
% input:  nsin_data - structure containing the nsins' data
% 
%         index_of_bad_nsins - 1 X N vector containing index of nsins whose data needs to be erased
%         
%         written by Hillel Raz, BondIT 2013

nsin_data.Dataset(index_of_bad_nsins, : ) = [];

nsin_data.RankDataSet(index_of_bad_nsins,  : ) = [];

% nsin_data.name(index_of_bad_nsins,  : ) = [];
% nsin_data.bond_type(index_of_bad_nsins,  : ) = [];
nsin_data.linkage_index(index_of_bad_nsins,  : ) = [];
%nsin_data.InterestType(index_of_bad_nsins,  : ) = [];
nsin_data.Duration(index_of_bad_nsins,  : ) = [];

% nsin_data.sector(index_of_bad_nsins,  : ) = [];

nsin_data.YTM(index_of_bad_nsins, :) = [];
nsin_data.price_clean_yld(index_of_bad_nsins, :) = [];

nsin_data.IndexYTM = nsin_data.IndexYTM(solver_props.dates_kept_vector);
nsin_data.IndexPriceYld = nsin_data.IndexPriceYld(solver_props.dates_kept_vector);

nsin_data.Sharpe = sharpe(nsin_data.ReturnMat, solver_props.RisklessRateDay)'; %calc the Sharpe ratio of each bond
nsin_data.InfoRatio = inforatio(nsin_data.ReturnMat, nsin_data.IndexPriceYld).';
