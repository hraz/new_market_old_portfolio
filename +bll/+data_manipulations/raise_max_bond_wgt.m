function [solver_props] = raise_max_bond_wgt(solver_props, NumBonds)
%function checks if number of bonds is minimum and if so raises the maximum
%allowed weight to the next level (as if minimum number of allowed bonds
%was one less) so to allow for optimization in solution.  

        if  NumBonds == solver_props.MinNumOfBonds %if we hit exactly lowest number of bonds, increase min weight as if one less bond is allowed so to allow for some optimization
            solver_props.MaxBondWeight = 1/(solver_props.MinNumOfBonds - 1);
            solver_props.max_weight_changed_flag = 1; % For conset
        end
        
end