function [handles solver_props]=filter_fun(handles, solver_props, benchmark_id)
% this function performs the filter as is done in the gui but without any of the graphics assigned to the handles or the screen
% 
% input: handles
% 
% output: handles with list of nsins.  


import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.market_indx.*;
import simulations.*; 

%instead of FindCurrLivingBonds
% handles.all_nsin= get_lived_assets(handles.DateNum);         %find all bonds that are a live in the date: handles.DateNum date
%     handles_all_nsin = handles.all_nsin;
%     
%instead of FiliterBondsAccording2TypeNoGui
flag_markowitz = 0.5;
while flag_markowitz <1
[handles solver_props error2 error3] = FilterBondsAccording2TypeNoGuiBench(handles, solver_props, benchmark_id);

% instead of CombineTypeNsins

[handles solver_props] = CombineTypeNsinsNoGuiSim(handles, solver_props);

handles.nsin = setdiff(handles.nsin, [1100791]);%bad nsin, bad, bad.


%instead of UpdateNumOfBonds


if (handles.Markowitz ==1) && (length(handles.nsin)>handles.HistLen)
    temp_ds = get_bond_history_length(  handles.nsin,  handles.DateNum );
    
    histlen_of_nsins=temp_ds.history_length;
   
    min_histlen=find((arrayfun(@(x) sum(histlen_of_nsins>=x+1)-x,1:800))<=0,1,'first')+1;

    handles.HistLen = min_histlen;
    handles.DynFilterParams.history_length = {min_histlen 10000};

        flag_markowitz = 0.2;
end
flag_markowitz = flag_markowitz + 0.6;
handles.NumBonds = length(handles.nsin);
end
save handles;
