function [A b]=create_durations_constraints(Duration,MaxPortDuration,MinPortDuration)
%Creat the YTM constrains, according to matlab custom standart of inequalities:
%Aw'<=b. If you don't want an upper bound, put a big number in MaxYTM
A(1,:)=Duration;        %put the duration in the first row of A. This is for the maximum constraint
A(2,:)=-Duration;        %put the duration in the first row of A. This is for the minimum constraint
b(1,1)=MaxPortDuration; %upper bound
b(2,1)=-MinPortDuration; %lower bound. minus is needed to be compatiable with Ax'<=b, but this -Aw'>=-b...



