function [handles solver_props] = CombineTypeNsins(handles, solver_props)
% perform union and intersect operation to find the government nsins that passed the filters, 
% the corporate nsins that passed the filters, and the total nsins that passed the filters.
   % handles.BondsPerType = zeros(length(handles.UserBondTypes),1);
    handles.nsin  = [];
    handles.corp_nsin = [];
    handles.gov_nsin = [];       
    for i=1:sum(handles.UserBondTypes(1:4)) %for one of the corporate bonds, intersect with the nsin that passed the rank filter        
            eval(sprintf(' handles.corp_nsin = union(handles.corp_nsin, handles.type_nsin.filter_%d);',i));
    end   
    for i=1:sum(handles.UserBondTypes(5:7)) %for one of the corporate bonds, intersect with the nsin that passed the rank filter        
            eval(sprintf(' handles.gov_nsin = union(handles.gov_nsin, handles.type_nsin.filter_%d);',i+sum(handles.UserBondTypes(1:4))));
    end   
    
    %%%%%%%%%%%%%%%%%%%%
    solver_props.corp_nsin = handles.corp_nsin;
    solver_props.gov_nsin = handles.gov_nsin;
    %%%%%%%%%%%%%%%%%%%%%%%
    
    handles.nsin = union(handles.gov_nsin,handles.corp_nsin);
    
    handles.nsin0 = handles.nsin;                               %This will be used to reset the nsin list before the specific bonds constraint are merged into the nsin. If we would have used only nsin, we would have had to take off bonds that were removed from the specific bonds constraint list from the nsin list...and it is prefered to construct the final nsin list from the beginning by taking nsin0 and adding to it the specific bond constraints
    handles.gov_nsin0 = handles.gov_nsin;
    handles.corp_nsin0 = handles.corp_nsin;    
    %save local copy in order to save time of filtering with same params
    handles_nsin = handles.nsin;
    handles_nsin0 = handles.nsin0;    
    handles_gov_nsin = handles.gov_nsin;
    handles_corp_nsin = handles.corp_nsin;
    handles_gov_nsin0 = handles.gov_nsin0;
    handles_corp_nsin0 = handles.corp_nsin0;
   
    %save '+gui\handles_misc_nsin.mat' handles_nsin handles_corp_nsin handles_gov_nsin handles_nsin0 handles_gov_nsin0 handles_corp_nsin0;
   
end