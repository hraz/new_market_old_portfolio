function [handles] = UpdateNumOfBonds(handles)
%update the number of bonds that passed the filters, as well as the GUI
%which represents this number to the user.
error =[];
handles.BondsPerType0= handles.BondsPerType;                %this is used in the conset creation, when taking only the user limits for the original bonds, before additiong the user specific constraints
handles.NumBonds=length(handles.nsin);
if handles.NumBonds<handles.MinNumOfBonds
    handles.NumBonds=handles.NumBonds0;                 %reset the number of bond to the initial number of bonds (all bonds who are a live today, with no filtering)
%     set(handles.NumBondsG,'String',handles.NumBonds);
%     msgbox(sprintf('Not enough bonds were found for optimization! \nModify the parameters such that there will be at least %d candidate bonds.',handles.MinNumOfBonds),'Not Enough Bonds','error');
    error = ['Not Enough Bonds'];
end
% set(handles.NumBondsG,'String',handles.NumBonds);
% set(handles.SolOutMsg,'String','Done Filtering!')