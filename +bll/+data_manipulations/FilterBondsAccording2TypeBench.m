function [handles solver_props error2 error3] = FilterBondsAccording2TypeBench(handles, solver_props, benchmark_id)
%filter bonds according to their static and dynamic parameters. 
%Then, intersect the resutls to find the find nsins list for each type
%import dal.market.filter.static.*;
import dal.market.filter.total.*;
import dal.market.filter.risk.*;
 import utility.*;
  import utility.dal.*;
  import dal.market.get.market_indx.*;
%     tic
error2 =[]; error3 = [];
    handles.rank_nsin = bond_rank_complex( handles.DynFilterParams.maalot{1} , handles.DynFilterParams.maalot{2},...
        handles.DynFilterParams.midroog{1}, handles.DynFilterParams.midroog{2}, handles.DynFilterParams.log_opr, handles.DateNum);        
    if size(handles.rank_nsin,2)>1
        error2 = ['Correcting error - handles.rank_nsin is not a column vector'];
        handles.rank_nsin=handles.rank_nsin';
    end
    

%     toc
    ds=dataset();
        ind=0;
    handles.TypesIncluded = 0;       
     for i=1:4
        if handles.UserBondTypes(i)
            ind=ind+1;
            handles.TypesIncluded(ind) = i;         %if UserBondTypes=[1 1 1 0 1 1 0] then TypesIncluded=[1 2 3 5 6]       
                handles.TypeFilterParams{i}.class_name = {};
                handles.TypeFilterParams{i}.sector = handles.SectorsToInclude'; %update the field of sector with the most updated list of sectors to include                
                ds = add2dsforfilterttl( handles.TypeFilterParams{i}.class_name,  handles.TypeFilterParams{i}.coupon_type,...
                     handles.TypeFilterParams{i}.linkage_index,  handles.TypeFilterParams{i}.sector,...
                      handles.TypeFilterParams{i}.bond_type,ds);
        end
     end
     for i=5:7
        if handles.UserBondTypes(i)
            ind=ind+1;
            handles.TypesIncluded(ind) = i;
            handles.TypeFilterParams{i}.class_name = {};
            handles.TypeFilterParams{i}.sector = {}; %for government we prefer sectors to be empty
            ds = add2dsforfilterttl( handles.TypeFilterParams{i}.class_name,  handles.TypeFilterParams{i}.coupon_type,...
                handles.TypeFilterParams{i}.linkage_index,  handles.TypeFilterParams{i}.sector,...
                handles.TypeFilterParams{i}.bond_type, ds);
        end
     end
         handles.DynFilterParams.history_length = {60 1000};
        handles.type_nsin = bond_ttl_multi(handles.DateNum, ds, handles.DynFilterParams ); %since bond_ttl doesn't filter ranks, we can remove maalot, midroog, etc.... since keeping them doesn't change the result, we'll keep them although they are not used
    
        %% get bonds in index
        if ~isempty(benchmark_id) %if no benchmark_id specified, filter as usual.
            [components] = get_market_indx_components(benchmark_id, handles.DateNum);
            if ~isempty(components)
                bonds_index = components.id;
            else
                bonds_index = [];
            end
            if ~isempty(bonds_index)
                for i = 1:1:7
                  eval(sprintf('sol =intersect(handles.type_nsin.filter_%d, bonds_index);', i));
                eval(sprintf('handles.type_nsin.filter_%d = sol;', i));% = intersect(handles.type_nsin, bonds_index);
          
                end
            end
        end
        
        for i=1:sum(handles.UserBondTypes(1:4)) %for one of the corporate bonds, intersect with the nsin that passed the rank filter
            eval(sprintf('temp_filtered_nsin =handles.type_nsin.filter_%d;',i));
            if size(temp_filtered_nsin,2)>1
                error3 = ['Correcting error - temp_filtered_nsin is not a column vector'];
                temp_filtered_nsin=temp_filtered_nsin';
            end
            eval(sprintf('handles.type_nsin.filter_%d = intersect (temp_filtered_nsin,  handles.rank_nsin,''rows'');',i));
        end        
handles.BondsPerType = zeros(length(handles.UserBondTypes),1);                          %find the number of bonds in each type. Used in thc Conset (for one example - maybe there are more).
for i=1:length(handles.TypesIncluded)        
        eval(sprintf('handles.BondsPerType(handles.TypesIncluded(%d)) = length(handles.type_nsin.filter_%d);',i,i));    
end

    handles.type_nsin0 = handles.type_nsin;                 %this reset value of type_nsin will be used in specific bonds constraint, where type_nsin will be resetted to type_nsin0, in order to add the specific bonds to the initial type_nsin, and not to the result from the last run
    handles_type_nsin = handles.type_nsin;
    handles_type_nsin0  = handles.type_nsin0;        
   handles.BondsPerType0 = handles.BondsPerType;
        %save '+gui\handles_type_nsin.mat' handles_type_nsin handles_type_nsin0;
       
end