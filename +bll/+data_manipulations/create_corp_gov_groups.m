function CorpGovGroups=create_corp_gov_groups(CorpGovTypes,Dataset,N)
%creat groups of only corp bonds and/or only gov bonds
CorpGovGroups=zeros(sum(CorpGovTypes),N); %preallocate memory for either 1 or 2 groups, depending if the user want constraints on Corp,Gov, or both
start=1;%the first line
Corp_bond_ind = logical(strcmp(Dataset.bond_type,'Corporate Bond')); % find all the indices of corporate bonds
Gov_bond_ind  = not(Corp_bond_ind);                                  % if it is not a corp bond, it is one of the shahar, gilon, galil...
if CorpGovTypes(1)==1    %if the user want to set a constraint on corp bonds (in genereal)
    CorpGovGroups(start,Corp_bond_ind)=1;
    start=start+1;
end
if CorpGovTypes(2)==1    %if the user want to set a constraint on gov bonds (in genereal)
    CorpGovGroups(start,Gov_bond_ind)=1;    
end
    