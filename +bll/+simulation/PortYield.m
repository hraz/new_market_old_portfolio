function [ FinYield ] = PortYield( V_0, V_F )
%calculates yield of portfolio based on initial value V_0 and final value
%V_F

FinYield = V_F/V_0 -1;


end

