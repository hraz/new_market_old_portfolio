function  par_fun( file_name, sim, t_0, sim_len, handles, compounding_interest_rate_lower_bound, compounding_interest_rate_upper_bound )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

      import gui.*
import bll.simulation.*;
import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;


load(fullfile(file_name))
file_name1 = 'Results1.mat';
file_name2 = 'Results2.mat';
file_name3 = 'Results3.mat';
file_name4 = 'Results4.mat';


handles.DateNum =t_0 + 7*(sim-1);




[handles errors]=FindBondsNoGui(handles);

Results(sim, 1) = handles.DateNum;

t_f1 = handles.DateNum + sim_len*365;
handles.NumBonds = length(handles.nsin);
Results(sim, 2) = handles.NumBonds;
%load 'D:\core\handles.mat';

[errorsa2 errorsa3 handles] = SolverNoGui(handles);
  fprintf('%i\n', sim)

 handles.Wts(handles.Wts<=handles.MaxWeight2NonAllocBonds*1.5)=0; 
%  print zeros where the weighs are smaller than MaxWeight2NonAllocBonds -- the maximum weight that a bond can have 
%  while stil be considered as a bond which was not included in the portfolio
 nsin_i = handles.nsin;
 
  if isempty(handles.PortReturn)
       handles.PortReturn = NaN;
   end
   if isempty(handles.PortRisk)
       handles.PortRisk = NaN;
    Results(sim, 3) = NaN;
     Results(sim, 4)= NaN;
      Results(sim, 5)= NaN;
       Results(sim, 6)= NaN;
        Results(sim, 7)= NaN;
         Results(sim, 8)= NaN;
         Results(sim, 9)= NaN;
   else
   wts = handles.PortWts(1,:)';

   wts0 = (wts==0);
   nsin = nsin_i(~wts0);
   wts1 = wts(~wts0);
   
 Results(sim, 3) = handles.PortReturn;
 Results(sim, 4) = handles.PortRisk;
 
   Sh = handles.Sharpe;
   Sh1 = Sh(~wts0);
   Results(sim, 5) = Sh1'*wts1;
 
allocation = sortrows(dataset(nsin, wts1),1);
   BondList = [nsin wts1];
 
     [ cumulative_holding_ret, holding_ret1, stop_point  ] = get_bond_holding_return(allocation, handles.DateNum, t_f1, -0.05, 0.3); 
  
     ytm_port1 = holding_ret1( end, 2:end, end)*wts1;
                   
                                 
   Results(sim, 6) = ytm_port1;
   
    [ cov1 covar1 covar] = hist_risk_calc( handles.DateNum, t_f1, BondList  ); %calculate covariance of portfolio
    Results(sim, 7) = covar;
      
  [ synth_indx_change1 all_bonds_change1] = synth_indx( BondList,  handles.DateNum, t_f1 ); %synthe index
  Results(sim, 8) = all_bonds_change1/100;
  Results(sim, 9) = synth_indx_change1;
% 
%   

   end
   
    if mod(sim, 4) == 1
         load 'Results1.mat';
   Result_val1(sim, :) =   Results(sim, :);

save(file_name1, 'Result_val1');
    elseif mod(sim, 4) ==2
        load 'Results2.mat';

        Result_val2(sim, :) = Results(sim, :) ;

        save(file_name2, 'Result_val2');
    elseif mod(sim, 4) == 3
        load 'Results3.mat';

         Result_val3(sim, :) = Results(sim, :);

         save(file_name3, 'Result_val3');
    elseif mod(sim, 4) == 0
        load 'Results4.mat';

    Result_val4(sim, :) =  Results(sim, :);

    save(file_name4, 'Result_val4');
     end

clear handles;


end

