function [ synth_indx_change all_bonds_change ] = synth_indx( BondList, t_0, t_f )
%this function calculates the sytnthetic index for a given portfolio based
%on the weights that its bonds hold in it
% 
% input:  BondList - n x 2 matrix made of list of nsins of bonds in portfolio and weights for each bond
% 
%                 t_0 - starting time for portfolio
%                 
%                 t_f - finishing time for portfolio
% 
% t_0 = time at which investment is to begin
%
% t_f - final date for which the calculation is to be made
% 
% 
% output: 
% 
% syn_indx_yld - scalar giving yield of synthetic index
% 
% all_bonds_change - scalar giving yield of all bonds index (601)
% 
% 


import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;
import bll.simulation.*;

t_0 = datenum(t_0); %initial date to which compare
t_f = datenum(t_f); % final date on which info is wanted

bond_nsin_in = BondList( :, 1);
bond_weights_in = BondList(:, 2);

[bond_nsin Ind_n] = sort(bond_nsin_in);%sort bonds as they may come in 'out of order'
bond_weights = bond_weights_in(Ind_n);

%% get index weights according to constituting bonds

[ indx_uniq indx_wts ] = bond_dist_by_index( bond_nsin, bond_weights, t_0 );

%% Get values of indices on t_0 and t_f

data_name = {'value'};
 tbl_name = 'index';

indx_price_t_0 =   get_multi_dyn(indx_uniq, data_name, tbl_name, t_0 );  %value of indices on t_0
indx_price_t_f =   get_multi_dyn(indx_uniq, data_name, tbl_name, t_f );  %value of indices on t_f


    
indx_price_beg = fts2mat(indx_price_t_0.value( end));
indx_price_finish = fts2mat(indx_price_t_f.value( end));

%in case one of the indices died before period ended, reconfigure by not
%taking it into account



if  sum(isnan(indx_price_finish))
    
    indx_not_alive =   isnan(indx_price_finish); %catch the NaN
    
    indx_wts = indx_wts(~indx_not_alive);
    indx_wts = 1/sum(indx_wts)*indx_wts;
    indx_price_beg = indx_price_beg(~indx_not_alive);
    indx_price_finish = indx_price_finish(~indx_not_alive);

end


%% Get value change for each index and calculate synthetic index yield for time passed
indx_tot_wts = sum(indx_wts);
indx_wts = 1/indx_tot_wts * indx_wts;

indx_val_beg = indx_price_beg * indx_wts; %initial value of synthetic index according to breakdown of bonds
indx_val_finish = indx_price_finish*indx_wts; %final value of synthetic index according to breakdown of bonds

time = (t_f - t_0)/365;% time that portfolio has lived in years

synth_indx_change = ( (indx_val_finish/indx_val_beg)^(1/time) - 1)*100; %yield of synthetic index by year


%% Get values of All Bonds index (601) to compare as well

data_name = {'value'};
 tbl_name = 'index';


AB_beg = get_multi_dyn(601, data_name, tbl_name, t_0 );  %value of indices on t_0
AB_val_t_0 =fts2mat(AB_beg.value( end));


AB_fin = get_multi_dyn(601, data_name, tbl_name, t_f );  %value of indices on t_0
AB_val_t_f =fts2mat(AB_fin.value( end));


all_bonds_change = ( (AB_val_t_f/AB_val_t_0)^(1/time) - 1)*100;

end

