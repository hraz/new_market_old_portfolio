function [ Skew, Kurt ] = PortSkew( PortVal )

%provides skewness and kurtosis for a portfolio given portfolio returns for a time
%period [t_0, t_f]

Skew = skewnewss(PortVal);

Kurt = kurtosis(PortRet);

end

