function [ rm_ds ] = get_rm(port_alloc, end_date, mar, start_date, rnrr )
%GET_RM calculates portfolio risk measures.
%
%   [ rm_ds ] = get_rm(port_alloc, rnrr, rdate ) receives a portfolio, risk neutral rate of return,
%   minimum accepted return, required date, and creates the risk measures object. 
%
%   Inputs:
%      port_alloc - is a portfolio allocation table.
%
%       rdate - is a scalar value specifying the required date.
%
%       mar - is a scalar value specifying the minimum accepted return.
%
%   Optional input:
%       rnrr - is a scalar value specifying the risk neutral rate of return.
%
%   Outputs:
%       rm_ds - is a risk measures dataset.
%

% Yigal Ben Tal
% Copyright 2011-2012.

    %% Import external packages:
    import dml.*;
    import bll.simulation.*;
    import dal.market.filter.market_indx.*;
    import dal.market.get.dynamic.*;
    import dal.market.get.market_indx.*;
    import utility.statistics.*;
    import utility.rm.*;

    %% Input validation:
    error(nargchk(3, 5, nargin));
    if (~isa(port_alloc, 'dataset'))
        error('portfolio:rm:wrongInputs', 'Not valid object.');
    end
    if nargin < 5 || isempty(rnrr)
        rnrr = 0;
    end
    
    %% Step #1 (Collecting yields of all assets in the given portfolio):
    if nargin < 4 || isempty(start_date)
        [ tmp ] = get_multi_dyn( port_alloc.nsin, {'ytm'}, 'bond', end_date );
        yield = fts2mat(tmp);
    else
        [ tmp ] = get_multi_dyn_between_dates( port_alloc.nsin, {'ytm'}, 'bond', start_date, end_date );
        yield = fts2mat(tmp.ytm);
    end
    
    %% Step #2 (Collecting yields of benchmark needed information):
    if nargin < 4 || isempty(start_date)
        [ tmp2 ] = get_multi_dyn( port_alloc.nsin, {'ytm'}, 'index', end_date );
        bench = fts2mat(tmp);
    else
        [ tmp ] = get_multi_dyn_between_dates( 601, {'ytm'}, 'index', start_date, end_date );
        bench = fts2mat(tmp.ytm);
    end

    %% Step #3 (Calculation statistical risk measures values):
    [ asset_exp_ret, ~, cov_mtx ] = nanstat(yield);
    exp_return = asset_exp_ret * port_alloc.weight;
    volatility = sqrt(port_alloc.weight' * cov_mtx * port_alloc.weight);

    port_ret = yield  * port_alloc.weight;%%check
    skew = skewness( port_ret );
    kurt = kurtosis( port_ret );
    rm_ds = dataset(exp_return, volatility, skew, kurt);

    %% Step #4 (Calculation CAPM model parameters):
    [ alpha, beta, nonsysrisk, bench_expret ] = nanab( port_ret, bench);
    rm_ds = [rm_ds, dataset(alpha, beta, nonsysrisk)];

    %% Step #5 (Calculation financial ratios):
    sharpe = sharpe_ratio( exp_return, volatility, rnrr );
    jensen = jensen_ratio( exp_return, beta, bench_expret, rnrr );
    omega = omega_ratio(yield, mar);
    inf_ratio = inforatio(port_ret, bench);
    rm_ds = [rm_ds, dataset(sharpe, jensen, omega, inf_ratio)];
    
    
end