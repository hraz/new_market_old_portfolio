close all
clear
clc
% dbstop if error

addpath('C:\Users\Hilel\Desktop\Sync Before Yohai Meeting')
%% Define params
NumPorts = 20;
RisklessRate=0.0225;
RisklessRateWeek=Year2Week(RisklessRate);
ExpInflation=0.03;
HistLen=100; %in weeks
MaxBondDuration=6; %planning simulation for 3 years
MinBondDuration=2;
MaxMaalotRank='AA-';
MinMaalotRank='A-';
MaxMidrugRank='Aaa';
MinMidrugRank='NR';
AndOr='And';
MaxAvgMon=1000e6;
MinAvgMon=0.1e6;
MaxYTMallowed=0.5;
UserBondTypes = {'Corporate Bond', 'Government Bond  Galil',...
    'Government Bond  New Gilon','Government Bond  Shachar' };
% MaxTypeWeight=MaxTypeWeight;
% MinTypeWeight=MinTypeWeight;
% CorpGovTypes=CorpGovTypes;
% MaxCorpGovWeight=MaxCorpGovWeight;
% MinCorpGovWeight=MinCorpGovWeight;
UserSectors = { 'COMPUTERS','CHEMICAL', 'INSURANCE COMPANIES','FINANCIAL SERVICES',...
    'INSURANCE', 'COMMERCE','INSURANCE', 'COMMERCIAL BANKS', 'REAL-ESTATE AND DEVELOPMENT'};

% SectorsNotToInclude=SectorsNotToInclude;
% MaxSectorWeight=MaxSectorWeight;
% MinSectorWeight=MinSectorWeight;
% MaxPortDuration=MaxPortDuration;
% MinPortDuration=MinPortDuration;
% MaxBondWeight=MaxBondWeight;
% MinBondWeight=MinBondWeight;
% Model=Model; 
% Algorithm=Algorithm;
%Filters


%% PullOutData

import dal.market.filter.*;
import dal.market.get.*;

linkage = {};
% linkage = {'NIS' , 'UKP'};
% linkage = {'UKP', 'USD', 'EURO'}

coupon_type = '';
% coupon_type = 'VAR';

param.rdate = 734757;%uint32(datenum('2006-01-01'));
param.length = {HistLen []};
param.ytm_bruto = {nan MaxYTMallowed};
param.mod_duration = {MinBondDuration MaxBondDuration};
param.turnover = {MaxAvgMon, MinAvgMon }; %AvgMonetary

param.maalot = {MaxMaalotRank, MinMaalotRank};
param.midroog = {MaxMidrugRank, MinMidrugRank};
param.log_opr = AndOr;


tic
%bond_cmn(type, sector, linkage, coupon_type)
[ isin ] = bond_ttl( UserBondTypes, UserSectors, linkage, coupon_type, param)
toc





