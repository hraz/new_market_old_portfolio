function [ asset_pincome, port_pincome, port_pret, port_pincome_volatility ] = periodical_income( nsin, start_date, end_date )
%PERIODICAL_INCOME calculates periodical asset income.
%
%   [ asset_pincome, port_pincome, port_pret, port_pincome_volatility ] = periodical_income( nsin, start_date, end_date )
%   receives the list of nsin, start_date, end_date and returns periodical income for each asset.
%
%   Income:
%       allocation - is an NASSETSx3 dataset specifying the given portfolio allocation table.
%
%       start_date - is a date specifying the first date of required period.
%
%       end_date - is a date specifying the last date of required period.
%
%   Output:
%       asset_pincome - is a 
%       per_income - is a scalar specifying the periodical income for the given allocation table
%                            over the required period.
%
%       port_pincome_volatility - is a scalar specifying periodical income volatility over the period.
%
% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import dal.market.get.cash_flow.*;
    import dal.market.get.dynamic.*;
    
    %% Input validation:
    error(nargchk(3,3,nargin));
    
    %% Get periodical daily income return:
    [ data ] = get_multi_dyn_between_dates( nsin, {'price_dirty'}, 'bond', start_date, end_date );
    dates = data.price_dirty.dates;
    
    asset_periodical_data = cell(numel(nsin),2); 
    asset_income = [];
    for d  = 2: length(dates)
        tmp= da_bond_periodical_income( data.price_dirty.dates(d-1), data.price_dirty.dates(d), nsin );
        asset_income = [asset_income; tmp ];
    end

    
    
    
    port_pincome = zeros(length(dates), 1);
    asset_pincome_yield = zeros(length(dates)-1, 1);
    
    for d  = 1: length(dates) - 1
        asset_pincome = da_bond_periodical_income( dates(d), dates(d+1), allocation.nsin );
        port_pincome(d) = asset_pincome.periodical_income' * allocation.holdings;
        
        asset_pincome_yield(d) = asset_pincome(d) / (price(d) * allocation.holdings);
    end

    %% Calculation risk of the periodical daily return:
    port_pincome_volatility = nanstd(asset_pincome_yield);
    
    %% Calculation of the periodical portfolio income and return:
    asset_income = da_bond_periodical_income( start_date, end_date, allocation.nsin );
    port_pincome = double(asset_income(1,:)) * allocation.holdings;
    port_pret = port_pincome / price(1) * allocation.holdings;
    
end

