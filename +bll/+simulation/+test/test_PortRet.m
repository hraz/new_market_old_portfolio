clear; clc;

import bll.simulation.*

%%choose portfolio makeup

 nsin =[  1089978
     1091859
     1940121
     1940188
     2260099
     4110060
     7480015
     9266636
     9267030]
w = [.6; .4; .1; .2; .4; .5; .12; .3; .4]; %doesn't necessarily need to add up to 1 but does need to correspond to number of bonds

t_0 = '2006-01-02';

t_f =  '2011-03-24';

BondList = [nsin w];

%% calculate total value of portfolio and returns of it

[ PortVal PercRet ytm ret] = PortRet( BondList, t_0, t_f )
