%clear; clc;

%% Import external packages:
import bll.simulation.*

%% find value of bonds at time t_f starting from t_0
nsin =[ 1095157; 1105329; 1110931];
wts = [0.3; 0.6; 0.1];
 
BondList = [nsin wts];
 
t_0 = '2009-01-02';

t_f =  '2011-12-31';

[cov_mat covar covar2] =hist_risk_calc( t_0, t_f, BondList);