clear; clc;
% dbstop if error

import bll.simulation.*

%%choose portfolio makeup

nsin = [ 1104405; 1105329];
%1110931];
w = [.5; .5];%choose weights.  they don't necessarily need to add up to 1 but need to correspond to number of bonds
%i.e. vector needs to be same size

t_0 = '2009-01-02'; %starting date

t_f =  '2011-03-31';%end date

BondList = [nsin w];

riskless_rate = .03;
%%calculate Sharpe Ratio

[SharpeRatB SharpeRatP] = PortSharpe( BondList, riskless_rate, t_0, t_f )

