function  [PortReturn loc ClosestErrorFlag]=FindClosestTarRet(PortReturn,TarRetDay,NumPorts)
% Finds the closest return in the PortReturn vector (which was
% found in matlab's optimization function) to the TarRetDay.
% Return also the index of the place where we inserted the target return
Def=PortReturn-TarRetDay; %find the differnce between the target and all the returns vector
AbsDef=abs(Def); %calc the abs of ht diff
[val loc]=min(AbsDef); %find the one with lowest abs, which means it is the colosest

%find the distance between the user target return and the closest return
%available (recall that the PortReturn vector is a modified vector, whose
%NaN numbers (if any) were taken out). If this distance is greater than the
%distances that portopt uses for his minimization, i.e.,
%(MaxReturn-MinReturn)/NumPorts, this means that the closest return to the user request which
%could been obtained from the optimization was unavailable, so we need to
%raise a flag and tell the user that we are givving him the closest
%available solution.
ClosestErrorFlag=0;
if size(PortReturn,1)>=2
    if length(PortReturn)<NumPorts %if there were points with NaNs, and we have eliminated them, this statement will be true, and then we can carry on with this procedure...otherwise we dont have to.
        portoptDistance=(PortReturn(end)-PortReturn(1))/NumPorts; %find the distance which is used in portopt function
        if loc~=1 %find the min distance between the user tar return and closest available return. since we dont know if the closest point has a lower or higher return than the user target return, look at both the previous and the followin points, i.e., (loc-1) and (loc+1), respectively
            if loc+1>length(PortReturn)
                MinUserDistance=min([abs(TarRetDay-PortReturn(loc)) abs(TarRetDay-PortReturn(loc-1))]);
            else
            MinUserDistance=min([abs(TarRetDay-PortReturn(loc)) abs(TarRetDay-PortReturn(loc+1)) abs(TarRetDay-PortReturn(loc-1))]);
            end
        else %just look at this point and the next one
            MinUserDistance=min(abs(TarRetDay-PortReturn(loc),TarRetDay-PortReturn(loc+1)));
        end
        if MinUserDistance>portoptDistance %the theoretical closest point was unfeasiable, so raise a flag
            ClosestErrorFlag=1;
        end
    end
else
    loc=1;
end