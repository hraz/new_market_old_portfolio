function [ytmOutput] = calc_ytm_from_efficient_frontier_and_risk(PortReturn, PortRisk, riskInput)
%CALC_YTM_FROM_EFFICIENT_FRONTIER_AND_RISK calculates ytm on curve for
%given risk
%
% [ytmOutput] = calc_ytm_from_efficient_frontier_and_risk(PortReturn,
% PortRisk, riskInput) - function calculates the expected ytm given a
% wanted risk based on linear interpolation.
%
%   Input:  PortReturn - Double - array of expected portfolio returns
%
%           PortRisk - Double - array of expected portfolio risks
%
%           riskInput - Double - benchmark risk
%
%   Output: ytmOutput - Double - expected ytm of a portfolio on efficient
%      frontier with the benchmark risk
%
%   See also:   benchmark_solutions
%
% Hillel Raz, June 19th, Day after amit.baby.bris
% Copyright 2013, Bond IT Ltd

global NBUSINESSDAYS;
import utility.data_conversions.*;
%% Find closest two ytm points

numReturns = length(PortReturn);
returnsYearly = Day2Year(PortReturn);
riskYearly = PortRisk;
spotBigger = find(riskYearly > riskInput, 1, 'first');
spotSmaller = find(riskYearly < riskInput, 1, 'last');


%% Find via linear interpolation the ytm
if isempty(spotBigger)&&spotSmaller==length(riskYearly)
    ytmOutput = PortReturn(end);
elseif isempty(spotSmaller)&&spotBigger==1
    ytmOutput = PortReturn(1);
else
    slope = (returnsYearly(spotBigger) - returnsYearly(spotSmaller))/(riskYearly(spotBigger) - riskYearly(spotSmaller));
    ytmOutput = slope*(riskInput - riskYearly(spotBigger)) + returnsYearly(spotBigger);
end