function [PortDuration PortReturnMV PortRiskMV PortWts Wts PortReturnYear PortRiskYear MarkerPosition error warning]=MinVarPortError(YTM,ExpCovMat,DurationVector, NumPorts,ConSet,FminConFlag,handles)
% Solves the minimum variance portfolio optimization probelm,
% and plots the efficient frontier. it returns the allocated weights for all bonds
% that passed the filters, which are needed in order to obtain the minimum
% variance point.
import gui.*;
Daily_YTM=Year2Day(YTM);                                    %since the data is daily, and so does the covariance matrix, the YTM should also be daily
if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
    tmp = handles.MyBonds;                                  %MyBonds are the bonds in where am i mode
    if ~isempty(tmp)                                        %if we are in where am i mode, init the algorithm with the users weights
        for i=1:length(handles.MyBonds)
            ind(i)=find(extractfield(bond(BondScope),'ID')==handles.MyBonds(i).ID);
        end
        InitWeight(ind) = handles.MyWeights;                %init of weights according to users bonds
    else                                                    %if not in where am i mode
        InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N
    end   
        AddTCConstraint = 0;

    [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat,NumPorts, [], ConSet, InitWeight, AddTCConstraint); %Optimization
else                                                        %if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    [PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFmincon(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds);                        %Optimization
end

if isempty(PortRisk)                                        %if there was no solution
    PortWts=[];
    Wts=[];
    PortRiskYear=[];
    PortReturnYear=[];
    MarkerPosition = 1;
    PortReturnMV = NaN;
    PortRiskMV = NaN;
    PortDuration = NaN;
    return
end
%% Find out if there were unfeasible points, which are charachterizes by a NaN in their PortRisk, and a PortReturn or one. exclude these points in order to plot what was succesfful, and tell the user that we are showing the closest point to his requst
SolutionPoints = (~isnan(PortRisk));                        %indices of points which had a solution
PortReturn=PortReturn(SolutionPoints);                      %take only the returns of points with a solution
PortRisk=PortRisk(SolutionPoints);                          %take only the risk of points with a solution
PortWts=PortWts(SolutionPoints,:);                          %take only the weights of points with a solution
PortReturnYear=Day2Year(PortReturn);                        %tranlate returns back to yearly returns, in order to plot in yearly terms
PortRiskYear=PortRisk*sqrt(365);                            %translate the risk to annual terms
hold on
plot(PortRiskYear,PortReturnYear*100,'LineWidth',2);        %plot the efficient frontier
hold on
plot([0 PortRiskYear(1)],PortReturnYear(1)*100*ones(1,2),'r--','LineWidth',2)               %plot horizontal red dotted line
plot(PortRiskYear(1),PortReturnYear(1)*100,'r*','markersize',16);                           %plot a red star
xlim([PortRiskYear(1)*0.8 PortRiskYear(end)*1.2])                                           %set limits
%DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope
PortDuration=PortWts(1,:)*DurationVector;                       %calculate the weighted average of the years to maturity
tit=sprintf('Return: %1.2f%%, Risk: %1.3f, Duration: %1.2f years',PortReturnYear(1)*100,PortRiskYear(1),PortDuration);  %set title
PortReturnMV = PortReturnYear(1);
PortRiskMV = PortRiskYear(1);
AddFigNames(tit,'\sigma','Annual Return [%]',1)
Wts=PortWts(1,:)';                                          %output weights are those of the minimal variance, i.e., the first point in the solution
MarkerPosition = 1;                                         %this is the position of the marker bar in the upper left corner of the efficient frontier plot. Since its the minimum variance portfolio, it is the first point, and thus, the marker should be in his first position
