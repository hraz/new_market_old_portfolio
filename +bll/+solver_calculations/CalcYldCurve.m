function handles=CalcYldCurve(handles)
%This functin calcs the yield curve according to the gov bonds data. for
%each type of gov bonds (NIS,CPI,VAR), it findes all the bonds in this
%type, extracts their duration and YTM, and extrapulates the results using
%the LS estimation to find a smooth linear connectin between duration and
%YTM.
import dal.market.filter.static.*;
import dml.*;
import dal.market.get.dynamic.*;
LoadData = 0;
if LoadData ==0
    handles.gov_nsin=cell(3,1);
    handles.gov_ytm = cell(3,1);
    handles.gov_mod_dur = cell(3,1);
    handles.gov_mod_dur_high_res = cell(3,1);
    handles.gov_ytm_high_res  = cell(3,1);
    for CorpBondType=1:3 %the current corp bond is either NIS, CPI, or VARY
        handles.gov_nsin{CorpBondType} = bond_static_complex( handles.DateNum, handles.TypeFilterParams{CorpBondType+4});            %use only static filter in order to filter gov bonds from all types (shahar, galil, gilon) with no dynamic parameters involved (this is why we use _complex filter and not _total). The argument is handles.TypeFilterParams{CorpBondType+4} since the first 4 types are corporate, so in order to find the matching gov type, we need to add 4
        handles.gov_ytm{CorpBondType} = fts2mat(get_bond_dyn_single_date( handles.gov_nsin{CorpBondType} , 'ytm' , handles.DateNum ));
        handles.gov_mod_dur{CorpBondType} = fts2mat(get_bond_dyn_single_date( handles.gov_nsin{CorpBondType} , 'mod_dur', handles.DateNum ));
        lin_coeffs = polyfit(handles.gov_mod_dur{CorpBondType},handles.gov_ytm{CorpBondType},1);                    %find a linear equiation that best suits the observations of YTM and duration
        handles.gov_mod_dur_high_res{CorpBondType}=0:0.05:max(handles.gov_mod_dur{CorpBondType})+1;                  %creat a new high resolution x axis
        handles.gov_ytm_high_res{CorpBondType}=polyval(lin_coeffs,handles.gov_mod_dur_high_res{CorpBondType});      %evalate the linear equaiton from polyfit in the high resolution points, such that every durartion can be matched with an extrapulated ytm value
    end
    
    handles_gov_mod_dur_high_res = handles.gov_mod_dur_high_res;
    handles_gov_ytm_high_res = handles.gov_ytm_high_res;
%     save 'gov_data.mat' handles_gov_mod_dur_high_res handles_gov_ytm_high_res
else
    load 'gov_data.mat'
    handles.gov_mod_dur_high_res = handles_gov_mod_dur_high_res;
    handles.gov_ytm_high_res = handles_gov_ytm_high_res;
end
