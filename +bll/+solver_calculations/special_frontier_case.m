function [solution nsin_data ] = special_frontier_case(solver_props, solution, nsin_data)
%
% function designed to solve for special case of particular frontier points; optimal Sharpe, target return optimal Sharpe and higher moments
%
% input: solver_props - structure containing fields dealing with solver
%              properties
%
%          solution - structure containing initial solution
%
%          nsin_data - structure containing data pulled about bonds
%
%
% output:   solution - structure with updated solution according to the
%               special case specified in solver_props.frontier_point
%
%           nsin_data - structure containing data pulled about bonds,
%             redemption type updated possibly with new risk_free bond
%             and/or cut down to list according to selected index
%
% written by Hillel Raz, January 2013, BondIT
%
dbstop if error
import bll.solver_calculations.*;
import utility.data_conversions.*;
import dal.market.get.static.*;

if sum(~isempty(solution.PortReturn))&& sum(~isnan(solution.PortReturn))
    point = solver_props.frontier_point;
    if strcmp(solver_props.frontier_point, 'TarOS') && ~solver_props.solved
        point = 'OS'; % To ensure that first time solved, just solves for OS
    end
    switch point
        
        case 'OS'
            
            %After call to portoptError call portallocError to get Sharpe point
            
            [solution.PortReturn, solution.PortRisk,solution.PortWts, solution.error] =...
                portallocError(solution.PortRisk, solution.PortReturn, solution.PortWts, Day2Year(solver_props.RisklessRateDay)); %find the weights that maximize the Sharpe ratio
            
            if solution.selected_indx
                solution.PortDuration = solution.PortWts*nsin_data.Duration(solution.selected_indx);
                %nsin_data.redemption_type = [nsin_data.redemption_type(solution.selected_indx)];
            else
                solution.PortDuration = solution.PortWts*nsin_data.Duration; % Specific point was chosen so we calculate the PortDuration for that point only
            end
            
        case 'TarOS'
            
            %after call to portoptError call portallocError to get Sharpe point
            
            [solution.PortReturn, solution.PortRisk,solution.PortWts, solution.error] =...
                portallocError(solution.PortRisk, solution.PortReturn, solution.PortWts, Day2Year(solver_props.RisklessRateDay)); %find the weights that maximize the Sharpe ratio
            if solution.selected_indx
                solution.PortDuration = solution.PortWts*nsin_data.Duration(solution.selected_indx);
                %nsin_data.redemption_type = [nsin_data.redemption_type(solution.selected_indx)];
            else
                solution.PortDuration = solution.PortWts*nsin_data.Duration; % Specific point was chosen so we calculate the PortDuration for that point only
            end            
            if solver_props.solved %if this is last run of solver, then we want to add in the free_weight_bond
                
                %using formula x *RiskFree + (1-x) * PortRet = TarOS
                riskFreeWeight =  (solver_props.TarRetOS - solution.PortReturn)/(Day2Year(solver_props.RisklessRateDay) - solution.PortReturn);
                riskFreeWeight = min(riskFreeWeight, 1); %in case its larger than one, which means that the request is smaller than the riskfree.  Give riskfree.
                riskFreeWeight = max(riskFreeWeight, 0); %No short for now. 
                if solution.selected_indx
                    solution.PortDuration = solution.PortWts*nsin_data.Duration(solution.selected_indx);
                else
                    solution.PortDuration = solution.PortWts*nsin_data.Duration; % Specific point was chosen so we calculate the PortDuration for that point only
                end
                %Keep solution for portduration as it was.
                riskfreeInNsinList = solver_props.risk_free_bond(solver_props.port_duration_sought)==solution.nsin_sol;
                if ~any(riskfreeInNsinList)
                    solution.nsin_sol = [solution.nsin_sol; solver_props.risk_free_bond(solver_props.port_duration_sought)] ;
                    nsin_data.Duration = [nsin_data.Duration; (riskFreeWeight>0)*solver_props.risk_free_bond_ttm(solver_props.port_duration_sought)];
                    solution.PortWts = [(1-riskFreeWeight)*solution.PortWts riskFreeWeight];
                    solution.NumBondsSol = length(solution.nsin_sol); %Update in case bond was added
                    [solution.nsin_sol, indexNsins] = sort(solution.nsin_sol);
                    % Update redemption_type to be set only for those bonds
                    % that were chosen, and add the redemption type for the
                    % risk free
                    solution.selected_indx = []; % Empty out index as it is irrelevant now
                    solution.PortWts = solution.PortWts(indexNsins);
                    typeRedemptionRiskFree = get_bond_stat(solver_props.risk_free_bond(solver_props.port_duration_sought),...
                        solution.DateNum, 'redemption_type');
                    nsin_data.redemption_type = [nsin_data.redemption_type; typeRedemptionRiskFree.redemption_type];
                    nsin_data.redemption_type = nsin_data.redemption_type(indexNsins);
                    nsin_data.YTM = [nsin_data.YTM; solver_props.risk_free_interests(solver_props.port_duration_sought)];
                    nsin_data.YTM = nsin_data.YTM(indexNsins); % Added the YTM for riskfree bond to list
                else
                    solution.PortWts = (1-riskFreeWeight)*solution.PortWts+riskFreeWeight*riskfreeInNsinList';
                end
                %Calculate the new portfolio expected return and risk using
                %linear interpolation
                slopeLine = (solution.PortReturn - solver_props.risk_free_interests(solver_props.port_duration_sought))/solution.PortRisk;
                solution.PortReturn = (1-riskFreeWeight)*solution.PortReturn + riskFreeWeight*solver_props.risk_free_interests(solver_props.port_duration_sought);
                solution.PortRisk = (solution.PortReturn - solver_props.risk_free_interests(solver_props.port_duration_sought))/slopeLine;
                
                %in case we are hedging and risk_free_weight is negative.
            end
            %             if solution.selected_indx
            %                 solution.PortDuration = solution.PortWts*nsin_data.Duration([solution.selected_indx end]);
            %             else
            %                 solution.PortDuration = solution.PortWts*nsin_data.Duration;% Specific point was chosen so we calculate the PortDuration for that point only
            %             end
        case 'HighMom'
            
            %     case 'Om'
            
            
    end
end