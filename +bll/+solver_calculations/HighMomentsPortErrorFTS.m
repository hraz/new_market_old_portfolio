function [PortWts Wts PortReturnYear PortRiskYear MarkerPosition error warning]  = HighMomentsPortErrorFTS(YTM,ExpCovMat,NumPorts,ConSet,FminConFlag,handles)
% Solves the minimum variance portfolio optimization probelm,
% and plots the efficient frontier. it returns the allocated weights for all bonds
% that passed the filters, which are needed in order to obtain the minimum
% variance point.
global NBUSINESSDAYS;
import gui.*;

%M3 = building_third_moment_matrix_from_return_matrix(handles.ReturnMat);
%M4 = building_fourth_moment_matrix_from_return_matrix(handles.ReturnMat);
T = handles.HistLen;

Daily_YTM=Year2Day(YTM);                                    %since the data is daily, and so does the covariance matrix, the YTM should also be daily
if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
    tmp = handles.MyBonds;                                  %MyBonds are the bonds in where am i mode
    if ~isempty(tmp)                                        %if we are in where am i mode, init the algorithm with the users weights
        for i=1:length(handles.MyBonds)
            ind(i)=find(extractfield(bond(BondScope),'ID')==handles.MyBonds(i).ID);
        end
        InitWeight(ind) = handles.MyWeights;                %init of weights according to users bonds
    else                                                    %if not in where am i mode
        InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N
    end
    [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat,NumPorts, [], ConSet, InitWeight); %Optimization
else                                                        %if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    [PortRisk, PortReturn, PortWts, FunctionValue, error, warning] = portoptErrorFminconHighMoments(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds, handles.ReturnMat);                        %Optimization
end

if isempty(PortRisk)                                        %if there was no solution
    PortWts=[];
    Wts=[];
    PortRiskYear=[];
    PortReturnYear=[];
    MarkerPosition = 1;
    return
end
%% Find out if there were unfeasible points, which are charachterizes by a NaN in their PortRisk, and a PortReturn or one. exclude these points in order to plot what was succesfful, and tell the user that we are showing the closest point to his requst
SolutionPoints = (~isnan(PortRisk));                        %indices of points which had a solution
PortReturn=PortReturn(SolutionPoints);                      %take only the returns of points with a solution
PortRisk=PortRisk(SolutionPoints);                          %take only the risk of points with a solution
PortWts=PortWts(SolutionPoints,:);                          %take only the weights of points with a solution
PortReturnYear=Day2Year(PortReturn);                        %tranlate returns back to yearly returns, in order to plot in yearly terms
PortRiskYear=PortRisk*sqrt(NBUSINESSDAYS);  %translate the risk to annual terms

hold on
plot(FunctionValue,PortReturnYear*100, 'k', 'LineWidth',2);        %plot the efficient frontier
hold on
plot([0 FunctionValue(1)],PortReturnYear(1)*100*ones(1,2),'r--','LineWidth',2)               %plot horizontal red dotted line
plot(FunctionValue(1),PortReturnYear(1)*100,'r*','markersize',16);                           %plot a red star
xlim([FunctionValue(1)*0.8 FunctionValue(end)*1.2])                                           %set limits
ylim([PortReturnYear(1)*99 PortReturnYear(end)*101])                                           %set limits
DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope
Duration=PortWts(1,:)*DurationVector;                       %calculate the weighted average of the years to maturity
tit=sprintf('Return: %1.2f%%, Effective Risk: %1.3f, Duration: %1.2f years',PortReturnYear(1)*100,FunctionValue(1),Duration);  %set title
AddFigNames(tit,'Effective Risk','Annual Return [%]',1)

%hold on
%plot(PortRiskYear,PortReturnYear*100, 'k', 'LineWidth',2);        %plot the efficient frontier
%hold on
%plot([0 PortRiskYear(1)],PortReturnYear(1)*100*ones(1,2),'r--','LineWidth',2)               %plot horizontal red dotted line
%plot(PortRiskYear(1),PortReturnYear(1)*100,'r*','markersize',16);                           %plot a red star
%xlim([PortRiskYear(1)*0.8 PortRiskYear(end)*1.2])                                           %set limits
%DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope
%Duration=PortWts(1,:)*DurationVector;                       %calculate the weighted average of the years to maturity
%tit=sprintf('Return: %1.2f%%, Risk: %1.3f, Duration: %1.2f years',PortReturnYear(1)*100,PortRiskYear(1),Duration);  %set title
%AddFigNames(tit,'\sigma','Annual Return [%]',1)
Wts=PortWts(1,:)';                                          %output weights are those of the minimal variance, i.e., the first point in the solution
MarkerPosition = 1;                                         %this is the position of the marker bar in the upper left corner of the efficient frontier plot. Since its the minimum variance portfolio, it is the first point, and thus, the marker should be in his first position

x = PortWts;
PortSkewness = [];
for a = 1:size(x, 1);
   % PortSkewness(a) =  (T/(T-1))^(3/2)*x(a,:)*M3*(kron(x(a,:),x(a,:)))'/((x(a,:)*ExpCovMat*x(a,:)')^(3/2));
    %PortKurtosis(a) = (T/(T-1))^2*x(a,:)*M4*kron(x(a,:),kron(x(a,:),x(a,:)))'/((x(a,:)*ExpCovMat*x(a,:)')^2);
    r = handles.ReturnMat*PortWts(a, :)';
    
    PortKurtosis(a) = kurtosis(r);
    PortSkewness(a) = skewness(r);
end
%figure;
%plot(PortRiskYear, PortSkewness', 'g');
%figure
%plot(PortRiskYear, PortKurtosis');

PortSkewness
PortKurtosis