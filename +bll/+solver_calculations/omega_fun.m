function omega = omega_fun(ReturnMat, Mar, w)

%%Symbolic function used to optimize the allocations for bonds in order to
%%optimize the omega_ratio
%%Notice that this is in fact -1*Omega and hence we want to minimize this
%%function!!

%% ReturnMat is the T*N matrix of asset returns where T is the number of observations and N is the number of assets.
%%Mar is the Minimum acceptable return
%% w is the vector of allocations (weights)

import gui.*;


if(size(w,1)==1)
    w = w';
end

Data = ReturnMat*w; %expected return of current portfolio according to weights allocated
MinusData = -1*Data;
MinusMar = -1*Mar;
%omega = lpm(-Data, -Mar, 1)/lpm(Data, Mar,1); 
omega = -lpm(MinusData, MinusMar, 1)/(lpm(Data, Mar,1)+lpm(MinusData, MinusMar, 1)); 
