function [solution nsin_data solver_props] = solver_ultimate(constraints, filtered, solver_props, DateNum, varargin)
%SOLVER_ULTIMATE solves the for the best possible portfolio under the constraints given
%
%   [solution nsin_data solver_props] = solver_ultimate(constraints, filtered, solver_props, DateNum, varargin)
%   Solves the optimization problem using the bonds found through the
%   filter, and according to the algorithm chosen, calculates the
%   covariance matrix and finds the optimal allocation of the bonds in a
%   portoflio.
%
%   Input:
%       Required:
%       constraints - data structure - required, contains constraints input by user - check bll/filtering_functions/filter_by_constraints
%
%       filtered - data structure - required, contains nsins list, and
%          their breakdown into cells for coset - see filter_by_constraints
%
%       solver_props - data structure - required, contains various fields
%          required for the solver_utlimate which determine the algorithm to
%          be used for solution and the final make-up of the portfolio.
%
%       DateNum - DATENUM - required, datenum of date for which the
%          portfolio is solved
%
%       Optional:
%       nsin_data - data structure - structure containing data pulled on
%          nsins filtered, including YTM, price_dirty, etc. and various
%          calculated measures - such as InfoRatio and Sharpe.
%
%       solution - data structure - structure containing solved portfolio,
%          portfolio weights breakdown, expected portfolio return, risk and
%          duration, allocation, fields for nsins removed and for those kept.
%
%   Output:
%       solution - data structure, see above
%
%       nsin_data - data structure, see above
%
%       solver_props - data structure ,see above
%
%   See also:
%       filter_by_constraints, simulation_function, various functions used
%           below
%
% Hillel Raz, February 14th 2013, Valentine's day
% Copyright 2013, Bond IT Ltd.


import bll.*;
import dal.market.get.risk.*;
import dal.market.filter.total.*;
import bll.data_manipulations.*;
import bll.solver_calculations.*;
import utilities.data_conversions.*;
import bll.filtering_functions.*;
import utility.data_conversions.*;
solver_props.port_duration_sought = constraints.dynamic.port_dur;%set port duration to the one sought in the constraints

solution.DateNum = DateNum;

if nargin == 6 %case in which solution is received as well, no need to pull data again
    nsin_data = varargin{1};
    solution = varargin{2};
else %If no solution was passed need to solve...
    solution.nsin_removed = [];%field that will hold list of nsins removed if they had nan's in their data
    solution.nsin_removed_due_to_small_weights = []; %Field that will hold list of nsins removed from solution due to low weight
    
    if  (constraints.dynamic.hist_len  <= filtered.NumBonds) && (solver_props.Markowitz)% if the number of bonds is greater than the number of observations, in the case of the Markowitz model, the solver cannot be called since the estimated covariance matrix won't be positive definite. Thus, the single index model is chosen instead
        constraints.dynamic.hist_len  = filtered.min_histlen;
    end
    
    nsin_data = get_bond_data(filtered, solver_props, constraints, DateNum);
    
    solver_props.MaxPortDuration = solver_props.port_duration_sought + 0.5;
    solver_props.MinPortDuration = solver_props.port_duration_sought - 0.5;
    
    sortedDurations=sort(nsin_data.Duration,'ascend');
    if mean(sortedDurations(1:solver_props.MinNumOfBonds)) > solver_props.MaxPortDuration 
        solution=[];
        error('solver_ultimate:Bond durations too large, duration constraints not met, no solution possible');        
    end
    if mean(sortedDurations(end-solver_props.MinNumOfBonds - 1:end)) < solver_props.MinPortDuration
        solution=[];
        error('solver_ultimate:Bond durations too small, duration constraints not met, no solution possible');
    end
    [nsin_data.IndexPriceYld nsin_data.IndexYTM]=GetMktIndex(DateNum,constraints.dynamic.hist_len, constraints.benchmark_id); %return the market index which corresponds to the types of bonds the user requested
    
    %% get riskfree rate for investment horizon
    
    solver_props.RisklessRate = solver_props.risk_free_interests(solver_props.port_duration_sought);
    solver_props.risklessrate_bond = solver_props.risk_free_bond(solver_props.port_duration_sought);
    solver_props.RisklessRateDay=Year2Day(solver_props.RisklessRate);
    solver_props.BorrowRate=solver_props.RisklessRate;
    solver_props.BorrowRateDay=Year2Day(solver_props.BorrowRate);
    
    %%
    [nsin_data.Sharpe nsin_data.InfoRatio nsin_data.ReturnMat]=CalcSharpeInfoReturn(nsin_data.price_clean_yld, constraints.dynamic.hist_len, solver_props.RisklessRate,nsin_data.IndexPriceYld); %also calc the return matrix
    
    [nsin_data.ReturnMat, nsins2throw, dates_spots_erased] = GetRidOfNan(nsin_data.ReturnMat, solver_props.Markowitz, solver_props.number_nans_allowed);
    
    solver_props.dates_kept_vector = setdiff([1:constraints.dynamic.hist_len], dates_spots_erased);
    
    if ~isempty(nsins2throw)
        solution.nsin_removed = filtered.nsin_list(nsins2throw);
        filtered.nsin_list(nsins2throw) = [];
        [nsin_data] = TossDataRelatedToNsinsWNans(nsin_data, solver_props, nsins2throw );%instead of pulling data again, update pulled data
    end
    [nsin_data.alpha,nsin_data.beta,nsin_data.Sigma_ei,nsin_data.SingIndExpCovMat]=calc_single_index_params(nsin_data.ReturnMat,nsin_data.IndexPriceYld,nsin_data.IndexYTM,solver_props.Sigma_eiMethod,constraints.dynamic.hist_len); %beta is already adjusted
    
    if solver_props.benchmarkSolutionFlag %If building portfolios directly related to index, get index cov mat
        [nsin_data.IndexRisk] = index_covariance_mat(nsin_data.IndexPriceYld, constraints.dynamic.hist_len);
    end
    
    filtered.NumBonds = length(filtered.nsin_list);
    solution.nsin_sol = filtered.nsin_list;
    
    %% choose algorithm (type of optimization problem)
    
    %calc the spread of each bond from the Gov bond with the same type and with the same duration (as close as possible)
    solver_props.Duration= nsin_data.Duration;
    
    [solver_props] = raise_max_bond_wgt(solver_props, filtered.NumBonds);
    
    [solver_props.conSet] = build_conSet(filtered,constraints,DateNum,solver_props);
    
    if isempty(solver_props.ConSeterror)                               %choose model
        if solver_props.Markowitz
            nsin_data = EstCovMat_Marko(nsin_data);
        elseif solver_props.SingleIndex || solver_props.MaxOmega
            nsin_data = EstCovMat_SingInd(nsin_data);
            %     elseif solver_props.MultiIndex
            %         handles = EstCovMat_MultiInd(handles);
            %     elseif solver_props.Spread
            %         handles = EstCovMat_Spread(handles);
        elseif solver_props.MinDownsideRisk
            [solver_props nsin_data] = EstSemiCovMat_MinDownsideRisk(solver_props, nsin_data);
        end
        solver_props.CleanCovarianceMatrixFlag =0;
        if solver_props.CleanCovarianceMatrixFlag
            [CovMat nsin_data] = PortfolioOfCovarianceMatrices(CovMat, constraints, nsin_data, solver_props);
        end
        
        solver_props.RiskLessWt=0; %this is zero for all algorithms except 4, where it can be something other than 0
        % solver_props.ClosestErrorFlag=0; %a flag that indicates whether or not there was a warning during the optimization which is relavent to the user. if it turns 1 - this means that the target return that the user entered wasn't feasible (and also its neighbourhood wasn't feasiable) so we need to let him know that. sometimes there are warnings which are relvanet to the user, (if there was some target return which wasn't feasiable) since the target return that the user wanted was feasible
        
    end
    
end

%% Solve for best portfolio
if isempty(solver_props.ConSeterror)
    [solution, nsin_data solver_props] = OptimizeCalculation(nsin_data, solver_props, filtered, solution);
end


%     if handles.HighMom  %Higher Moments Optimization
%         handles.FminConFlag = 1;
%         handles.NumPorts = 2;
%         [handles.PortWts handles.Wts handles.PortReturnYear handles.PortRiskYear handles.MarkerPosition handles.PortDuration handles.error handles.warning] = HighMomentsPortErrorFTS(handles.YTM, handles.ExpCovMat,...
%             handles.NumPorts, handles.ConSet, handles.FminConFlag, handles);
%     end
