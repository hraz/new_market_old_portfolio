function [IndexPriceYld IndexYTM]=GetMktIndex(DateNum, HistLen, idBenchmark)
% Get the market index data: value yield and ytm.
% in the future: find the best index that match the user bonds
import dal.market.get.dynamic.*;
import dml.*;

if isempty(idBenchmark)
    idBenchmark = 601; % In case no benchmark was chosen, take AllBonds
end
nsin = [idBenchmark]; %general bond
tbl_name = 'index';
    data_name = {'value_yld','ytm'};
IndexFts = get_multi_dyn( nsin , data_name , tbl_name, DateNum, HistLen);
IndexPriceYld=fts2mat(IndexFts.value_yld);
IndexYTM=fts2mat(IndexFts.ytm);

end