function [A b]=AddTarBetaCon(A,b,beta,TarBeta)
%Add two more lines to the custom constraints, as specified in A and b. The first line
%makes sure that the Beta of the portfolio (Which is equal to the weighted
%betas) is less or equal to TarBeta (by some threshold). The second line does that same with
%negative values, in order to make sure that the portfolio beta is larger
%than TarBeta (by some threshold). This will guarantee that the portfolio beta will be equal to
%TarBeta
A = [A ; beta' ; -beta'];
b = [b ; TarBeta+0.1 ; -(TarBeta-0.1)];


