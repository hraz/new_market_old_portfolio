function [solver_props nsin_data] = EstSemiCovMat_MinDownsideRisk(solver_props, nsin_data)
% Estimate the semi-covariance matrix in its symmetric approximation
import gui.*;
solver_props.Mar =Year2Day(solver_props.TarRet);
nsin_data.ExpCovMat = (lpm(nsin_data.ReturnMat, solver_props.Mar, 2)'*lpm(nsin_data.ReturnMat, solver_props.Mar, 2)).^(1/2).*corrcoef(nsin_data.ReturnMat);
