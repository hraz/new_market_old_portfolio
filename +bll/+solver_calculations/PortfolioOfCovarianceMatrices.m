function [CovMat nsin_data] = PortfolioOfCovarianceMatrices(CovMat, constraints, nsin_data, solver_props)

%%This Function 'cleans up' the historical covariance matrix (the so called
%%sample covariance matrix). This is accomplished by adding different
%%estimators with a lot of structure to the sample covariance matrix. This
%%way we get less estimation error for the price of increased specification
%%error. However, the resulting covariance matrix is better suited for
%%portfolio management and predicts the future better than the sample
%%covariance matrix.

import gui.*;

N = size(CovMat, 1);

%%Variance Estimator
VarianceEstimator = diag(diag(CovMat)); 

%%Constant Correlation Estimator
TriuCorrelationMatrix = zeros(N);
ConstantCorrelationMatrix = zeros(N);
for a = 1:N
    for b = a+1:N
TriuCorrelationMatrix(a, b) = CovMat(a, b)/sqrt(CovMat(a, a)*CovMat(b, b));
    end
end

ConstantCorrelation = sum(sum(TriuCorrelationMatrix))*2/N/(N-1);
for a = 1:N
    for b = a+1:N
        ConstantCorrelationMatrix(a, b) = ConstantCorrelation*sqrt(CovMat(a, a)*CovMat(b, b));
    end
end
ConstantCorrelationMatrix = ConstantCorrelationMatrix+ConstantCorrelationMatrix'+VarianceEstimator;

%%Single Index Estimator
[nsin_data.IndexPriceYld nsin_data.IndexYTM]=GetMktIndex(DateNum,constraints.HistLen);            %return the market index which corresponds to the types of bonds the user requested\
IndexPriceYld = nsin_data.IndexPriceYld;

[nsin_data.Sharpe nsin_data.InfoRatio nsin_data.ReturnMat]=CalcSharpeInfoReturnMatNoGui(nsin_data.price_clean_yld, constraints.HistLen, solver_props.RisklessRate,nsin_data.IndexPriceYld); %also calc the return matrix

ReturnMat = nsin_data.ReturnMat;
YTM = nsin_data.YTM;
IndexYTM = nsin_data.IndexYTM;
Rm=mean(IndexPriceYld);                            %market expected return
NormRm=IndexPriceYld-Rm;                        %substruct the mean as the formula for expectation requires
Beta = FindBeta(nsin_data.ReturnMat, nsin_data.IndexPriceYld, Rm, constraints.HistLen);
Beta = AdjustBeta(Beta);                          %adjust betas
alpha=FindAlpha(ReturnMat, YTM, Beta, Rm, IndexYTM);            %calc alphas
Sigma_ei = FindSigma_ei(ReturnMat, IndexPriceYld, alpha, Beta, Rm, constraints.HistLen, solver_props.Sigma_eiMethod);  %calc unsystematic risk, which can be diversified away. Try to estimate using variance_i-beta_i^2*variance_market
nsin_data.MarketVariance = NormRm'*NormRm;
SingleIndexEstimator = nsin_data.MarketVariance*(Beta*Beta')+diag(Sigma_ei);

%%Finally We make some weighted combination of the sample covariance matrix
%%with the estimators. Several options can be made.

CovMat = 1/3*(CovMat+VarianceEstimator+SingleIndexEstimator);
%CovMat = 1/4*(CovMat+VarianceEstimator+ConstantCorrelationMatrix+SingleIndexEstimator);

