function [solution, nsin_data, solver_props] = OptimizeCalculation(nsin_data, solver_props, filtered, solution)
%
% finds optimal portfolio according to model and point on frontier chosen by user
%
% input: nsin_data structure containing data for nsins filtered
%          solver_props - structure containing information on how to find solution
%
%
% output: solution - structure containing solution made of list of nsins,
% their respective weights, expected portfolio risk, expected portfolio
% return and expected portfolio duration
%
%
% written by Hillel Raz, December BondIT 2012

import bll.solver_calculations.*;
if solver_props.MaxOmega %If solving for Omega - return PortOmega as well
    if isempty(solution.nsin_removed_due_to_small_weights) % If this is the first time the solver is called
        solution.error = []; % Stores possible errors from solver
        solution.selected_indx = []; % Holds the index of the bonds selected for the solution
        
        if solver_props.benchmarkSolutionFlag && ~isfield(solver_props, 'WantedPortReturns')
            %If comparing to benchmarkSolutionFlag then solve for wanted
            %returns as well, if already done, proceed...
            [solution.PortDuration solution.PortReturn solution.PortRisk solution.PortWts solution.error warning solution.solverOutputDs solver_props.WantedPortReturns solution.PortOmega]= port_error(solver_props, nsin_data);
        else
            [solution.PortDuration solution.PortReturn solution.PortRisk solution.PortWts solution.error warning solution.solverOutputDs, ~, solution.PortOmega]= port_error(solver_props, nsin_data);
        end
    else
        [solution.PortDuration solution.PortReturn solution.PortRisk solution.PortWts solution.error warning solution.solverOutputDs, ~, solution.PortOmega]= port_error(solver_props, nsin_data, solution);
    end
else
    if isempty(solution.nsin_removed_due_to_small_weights) % If this is the first time the solver is called
        solution.error = []; % Stores possible errors from solver
        solution.selected_indx = []; % Holds the index of the bonds selected for the solution
        
        if solver_props.benchmarkSolutionFlag && ~isfield(solver_props, 'WantedPortReturns')
            %If comparing to benchmarkSolutionFlag then solve for wanted
            %returns as well, if already done, proceed...
            [solution.PortDuration solution.PortReturn solution.PortRisk solution.PortWts solution.error warning solution.solverOutputDs solver_props.WantedPortReturns]= port_error(solver_props, nsin_data);
        else
            [solution.PortDuration solution.PortReturn solution.PortRisk solution.PortWts solution.error warning solution.solverOutputDs]= port_error(solver_props, nsin_data);
        end
    else
        [solution.PortDuration solution.PortReturn solution.PortRisk solution.PortWts solution.error warning solution.solverOutputDs]= port_error(solver_props, nsin_data, solution);
    end
    
    if strcmp(solver_props.frontier_point, 'OS') ||  strcmp(solver_props.frontier_point, 'TarOS') ||  strcmp(solver_props.frontier_point, 'HighMom')
        [solution nsin_data ] = special_frontier_case(solver_props, solution, nsin_data);
    end
end




