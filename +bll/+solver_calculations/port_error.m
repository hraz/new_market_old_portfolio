function [PortDuration PortReturn PortRisk PortWts error warning solverOutputDs WantedPortReturns varargout]=port_error(solver_props, nsin_data, varargin)
% Solves the minimum variance portfolio optimization probelm,
% and plots the efficient frontier. it returns the allocated weights for all bonds
% that passed the filters, which are needed in order to obtain the minimum
% variance point.%
% input:  solver_props - data structure containing the solver properties necessary for optimization
%
%         nsin_data - data structure containing the nsin's data necessary for optimization
%
%         varargin - solution - data structure containing the already solved solution that will be re-optimizied
%
% output: PortDuration - scalar equaling the expected portfolio duration
%
%         PortReturn - scalar equaling the expected portfolio return
%
%         PortRisk - scalar equaling the expected portfolio's risk
%
%         PortWts - list of NASSETS x 1 of the weights for the optimal portfolio
%
%         error - string depicting possible error
%
%         warning - string with description of warning gotten back from algorithm
%
%         WantedPortReturns - Double - array of target returns
%           based on benchamrk.
%
%         varargout - PortOmega - Double - omega of calculated portfolio
%
% written by Hillel Raz, Based on Yoav the Machine's code, BondIT January 2013
%
%
global NBUSINESSDAYS;

import utility.data_conversions.*;
import bll.solver_calculations.*;

if nargin == 2
    ConSet = solver_props.conSet;
    YTM = nsin_data.YTM;
    ExpCovMat = nsin_data.ExpCovMat;
    DurationVector = nsin_data.Duration;
    ReturnMat = nsin_data.ReturnMat;
end

if nargin == 3
    solution = varargin{1};
    selected_indx = solution.selected_indx;
    ConSet = solver_props.NewConSet;
    YTM = nsin_data.YTM(selected_indx);
    ExpCovMat = nsin_data.ExpCovMat(selected_indx,selected_indx);
    DurationVector = nsin_data.Duration(selected_indx);
    ReturnMat = nsin_data.ReturnMat(:,selected_indx);
end

WantedPortReturns= [];
%% data needed
solverOutputDs = [];
NumPorts = solver_props.NumPorts;

FminConFlag = solver_props.FminConFlag;
AddTCConstraint = solver_props.AddTCConstraint;
TarRet = solver_props.TarRet;
%% begin optimization process

Daily_YTM=Year2Day(YTM);                                    %since the data is daily, and so does the covariance matrix, the YTM should also be daily
TarRetDay=Year2Day(TarRet);
InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N

if solver_props.AddTCConstraint
    [Daily_YTM ExpCovMat ConSet InitWeight] = UpdateConsetWithTCconstraints(solver_props.buy_cost, solver_props.sell_cost, InitWeight, Daily_YTM, ExpCovMat, ConSet, solver_props.MaxBondWeight);
end

benchmark = [];
if solver_props.benchmarkSolutionFlag % If we are solving versus the benchmark
    benchmark.YTM = nsin_data.IndexYTM(end);
    benchmark.Risk = nsin_data.IndexRisk;
end


if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
    %if not in where am i mode
    
    [PortRisk, PortReturn, PortWts, error, warning, solverOutputDs] = portfolio_optimization(Daily_YTM,ExpCovMat,NumPorts, TarRetDay, ConSet, InitWeight, AddTCConstraint, benchmark); %Optimization
    
    if solver_props.benchmarkSolutionFlag && ~isfield(solver_props, 'WantedPortReturns')
        [WantedPortReturns] = benchmark_solutions(PortReturn,PortRisk, benchmark);
    end
    
elseif FminConFlag==1
    if solver_props.benchmarkSolutionFlag && ~isfield(solver_props, 'WantedPortReturns')
        solver_props.Mar =  Year2Day(benchmark.YTM);
        WantedPortReturns = zeros(6,1);
        WantedPortReturns(1) = benchmark.YTM - 0.01;
        WantedPortReturns(2) = benchmark.YTM + 0.01;
        WantedPortReturns(3) = benchmark.YTM + 0.02;
        WantedPortReturns(4) = 2*benchmark.YTM;
        WantedPortReturns(5) = 3*benchmark.YTM;
        WantedPortReturns(6) = 4*benchmark.YTM;
    else
        solver_props.Mar = Year2Day(solver_props.TarRet); %minimum acceptable return
    end
    [PortMar, PortOmega PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFminconOmega(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        solver_props, nsin_data, ReturnMat);
    varargout{1} = PortOmega;
    
else%if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    [PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFmincon(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds);   % Fix error text here if used                     %Optimization
end

if isempty(PortRisk)                                        %if there was no solution
    PortWts=[];
    Wts=[];
    PortRiskYear=[];
    PortReturnYear=[];
    MarkerPosition = 1;
    PortReturn = NaN;
    PortRisk = NaN;
    PortDuration = NaN;
    return
end

NumBonds = length(nsin_data.nsin);

%if TC are active, save the buy weights and sell weight, and override
%PortWts such that it will hold only the final weights of the optimization
if  solver_props.AddTCConstraint                                        %before overriding PortWts in 3 lines from this line, save the weights to buy, to sell and the t values that were obtained
    WeightsToBuy = PortWts(:,NumBonds+1:2*NumBonds);
    WeightsToSell = PortWts(:,2*NumBonds+1:3*NumBonds);
    t = PortWts(:,3*NumBonds+1);                                %this factor scales the optimal weights, so in order to find the true optimal weight, this factor should be removed by dividing the optimal portfolio weight with it
    PortWts = PortWts(:,1:NumBonds)./repmat(t,1,NumBonds) ;
end

%% Find out if there were unfeasible points, which are charachterizes by a NaN in their PortRisk, and a PortReturn or one. exclude these points in order to plot what was succesfful, and tell the user that we are showing the closest point to his requst
SolutionPoints = (~isnan(PortRisk));  %indices of points which had a solution
if strcmp(solver_props.frontier_point, 'MV')
    SolutionPoints = SolutionPoints(1);         %in case of MeanVariance take first solved point
end
PortRisk=PortRisk(SolutionPoints)*sqrt(NBUSINESSDAYS);                          %take only the risk of points with a solution
PortWts=PortWts(SolutionPoints,:);                          %take only the weights of points with a solution
PortReturn=Day2Year(PortReturn(SolutionPoints));                        %tranlate returns back to yearly returns, in order to plot in yearly terms

%DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope
PortDuration=PortWts*DurationVector;                       %calculate the weighted average of the years to maturity
