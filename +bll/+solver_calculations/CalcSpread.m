function [handles]=CalcSpread(handles)
%This function calcs the spread between each corp bond and its gov
%equivalend bond, i.e., the gov bonds with the same linkage, interestType,
%and as close as possible duration.
import gui.*;
import dal.market.filter.static.*;
import dml.*;
import dal.market.get.dynamic.*;
handles=CalcYldCurve(handles); %calc the data which is necessary for plotting the yield curves. This function is also used when plotting yield curve. for now it is used in order to find the relation between duration and YTM for each gov bond type.
handles.corp_nsin = cell(3,1);
handles.corp_ytm = cell(3,1);
handles.corp_mod_dur = cell(3,1);
handles.Spread = zeros(length(handles.nsin),1);
%The ouput is a cell array with 4 rows and 3 columns. Each column is a different type of gov bonds. The first row is a vector of the original existing durations, second row is the existing YTM's. Row 3 is a high resolution duration vector, and row 4 is the corresponding YTM vector, to the durations of the bond type
if handles.LoadSavedData


    for CorpBondType=1:3 %for each corp bond type (NIS,CPI,VARY) - only corp bonds linked to USD/UKP etc are not calculated in this function, and their spread will be zero (just like gov bonds which are not filtered next, and thus, their spread will remain at zero)
        handles.corp_nsin{CorpBondType} = bond_static_complex( handles.DateNum, handles.TypeFilterParams{CorpBondType});    %fiter the corp bonds with static parameters only, such that only type, linkage, interest type are affecting the results. They will be used to extract the current corp bonds (for each type, linkage and interset type), since in FindBonds the rank filter is applied seperately, and is not taken into account in the total filter
        handles.corp_nsin{CorpBondType} = intersect ( handles.corp_nsin{CorpBondType},  handles.nsin) ; %take only the corp nsin's that passed the total filter (dynamic + static), since there is no point calculating the spread for bonds that didn't pass the filter
        if ~isempty(handles.corp_nsin{CorpBondType})            %if there are bonds that passed the filters in this group
            handles.corp_ytm{CorpBondType} = fts2mat(get_bond_dyn_single_date( handles.corp_nsin{CorpBondType} , 'ytm', handles.DateNum ));
            handles.corp_mod_dur{CorpBondType} = fts2mat(get_bond_dyn_single_date( handles.corp_nsin{CorpBondType} , 'mod_dur', handles.DateNum ));
            curr_corp_mod_dur=handles.corp_mod_dur{CorpBondType};
            curr_corp_ytm=handles.corp_ytm{CorpBondType};
            for j=1:length(handles.corp_nsin{CorpBondType}) %go over all the bonds in this group
                [val loc]=min(abs(curr_corp_mod_dur(j)-handles.gov_mod_dur_high_res{CorpBondType})); %find the location of the gov bond which has the closest duration to the duration of the current bond
                curr_gov_ytm=handles.gov_ytm_high_res{CorpBondType}(loc); %get the YTM of the bond
                tmp = handles.corp_nsin{CorpBondType};                      %use a temp vector to find the location of the current corp bond among the bond's vector
                ind= find(tmp(j) == handles.nsin);                          %ind is the index of the current corp bond in the handles.nsin vector(which is the vector of all bonds that passed the filters and are taking part in the optimization)
                handles.Spread(ind) = curr_corp_ytm(j)-curr_gov_ytm;        %calc the spread between the corp and gov bonds
            end
        end
    end
    handles_corp_nsin = handles.corp_nsin;
    handles_corp_ytm = handles.corp_ytm;
    handles_corp_mod_dur = handles.corp_mod_dur;
    handles_Spread = handles.Spread;
    tic;
%     save '+gui\SpraedData.mat' handles_corp_nsin handles_corp_ytm handles_corp_mod_dur handles_Spread
    fprintf('saving the spread file took %g secs\n',toc);

end
