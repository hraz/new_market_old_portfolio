function  [alpha,beta,Sigma_ei,MultiIndExpCovMat]=CalcMultiIndexParams(ReturnMat,YTM,IndexReturns, IndexYTM, Sigma_eiMethod,HistLen)
%Calc the multi index model params, in order to eventually find the
%covariance matrix which is derived using this model. 
import gui.*;
Rm=mean(IndexReturns);                            %market expected return
beta=zeros(length(YTM),length(Rm));
for i=1:length(Rm)
beta(:,i)=FindBeta(ReturnMat,IndexReturns(:,i),Rm(i),HistLen);      %calc betas for each of the multi indices
end
AdjBeta=AdjustBeta(beta);                          %adjust betas
alpha=FindAlpha(ReturnMat,YTM,AdjBeta,Rm,IndexYTM);            %calc alphas
 Sigma_ei=FindSigma_ei(ReturnMat,IndexReturns,alpha,AdjBeta,Rm,HistLen,Sigma_eiMethod);  %calc unsystematic risk, which can be diversified away. Try to estimate using variance_i-beta_i^2*variance_market
MultiIndExpCovMat=Beta2Cov(beta,IndexReturns,Sigma_ei,Rm,HistLen);                     %convert the betas to covariances, using the assumption that cov(i,j)=beta_i*beta_j*VarMar
