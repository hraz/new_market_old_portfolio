function conSet = build_conSet(filtered,constraints,reqDate,solver_props)
% Builds the conSet for the solver

% this function builds the matrix of size NConstraints X NBonds+1 which is
% used by the solver (for linear constratints only)
%   Input:
%   =====
%       reqDate    - ( YYYY-MM-DD )caculation date
%       filtered  - struct with at least the following field
%                               nsin_list - (NAssest X 1 DOUBLE) the securities of the 
%                               nsin_cell_lists - (NTypes X 1 CELLARRAY) 
%       constraints - struct with at least the following field:
%           sectors.sectors_to_include - sectors names (after Igal's change
%                                                                                                         it'd be sector Id's)                                                                                                      
%                                                                                                         
%   optional:
%       filtered - with the field:
%                           nsin_cell_lists - (NTypes X 1 CELLARRAY) 
%           constraints - holds the constraints according to which the
%           filteration was made. note that the Minimal\maximal weight can
%           be specific (to each constraint its own), global (same min\max
%           to all constraints) or default.
% 
%Output:
%       conSet - a matrix of NConstraints X (NAssets+1) holding the
%       'linear' representation of a the constraints (the way a constraint
%       is represented as the belonging of a security to the constraint
%       (using 0 and 1) and the limit constraint).

import dal.market.get.static.*;
import dal.market.filter.total.*;
import bll.solver_calculations.*;
import bll.filtering_functions.*;
import bll.*;

nsin_list=filtered.nsin_list;
if isfield(filtered,'nsin_cell_lists')
    nsin_cell_lists=filtered.nsin_cell_lists;
elseif isfield(constraints,'static') && isfield(constraints.static,'dataset')
    dyn_param.history_length = {NaN NaN};
    dyn_param.yield = {NaN NaN};
    dyn_param.mod_dur = {NaN NaN};
    dyn_param.turnover = {NaN NaN};
    dyn_param.ttm = {NaN NaN};
    staticDataset=constraints.static.dataset;
    nsin_list_multi = bond_ttl_multi( reqDate,  staticDataset, dyn_param );
    nsin_cell_lists=cell(size(staticDataset,2),1);
    for mFilters=1:size(staticDataset,2)
        nsin_cell_lists{mFilters}=intersect(eval(sprintf('nsin_list_multi.filter_%i',mFilters)),nsin_list);        
    end
else
    nsin_cell_lists=[];
    constraints.static.minWeight=[];
    constraints.static.maxWeight=[];
end

sectorsToInclude=constraints.sectors.sectors_to_include;

conTypes=zeros(size(nsin_cell_lists,1),length(nsin_list));
for mConSetRows=1:size(nsin_cell_lists,1)
    temp_nsin_list=nsin_cell_lists{mConSetRows};
    for kNsin=1:length(temp_nsin_list)
        rindx=find(nsin_list==temp_nsin_list(kNsin));
        if ~isempty(rindx)   % ignoring the nsins that fell in bad_nsin_list
            conTypes(mConSetRows,rindx)=1;
        end
    end
end

 nsinStat =get_bond_stat( nsin_list, reqDate, 'sector' );
 conSector=zeros(length(sectorsToInclude),length(nsin_list));
 for mSector=1:length(sectorsToInclude)
     conSector(mSector,:)=(strcmp(nsinStat.sector,sectorsToInclude{mSector}))';     
 end

 conGroups=[conSector;conTypes];
 [sectorsMinWeight staticMinWeight sectorsMaxWeight staticMaxWeight] = get_min_max_constraints_weights(constraints,conSector,conTypes);
 minGroupWeights=[sectorsMinWeight;staticMinWeight];
 maxGroupWeights=[sectorsMaxWeight;staticMaxWeight];

 if isfield(constraints,'specificSecurities') && ...
        ~isempty(constraints.specificSecurities)&& ...
        ~isempty(constraints.specificSecurities.nsin)
    specNsin=constraints.specificSecurities.nsin;
    minBondWithSpecBondWeight=solver_props.MinBondWeight*ones(size(nsin_list));
    maxBondWithSpecBondWeight=solver_props.MaxBondWeight*ones(size(nsin_list));
    for mSpecSecurity=1:length(specNsin)
        rindx=find(nsin_list == specNsin(mSpecSecurity));
        minBondWithSpecBondWeight(rindx)=constraints.specificSecurities.min(mSpecSecurity);
        maxBondWithSpecBondWeight(rindx)=constraints.specificSecurities.max(mSpecSecurity);
    end
 else
      minBondWithSpecBondWeight=solver_props.MinBondWeight*ones(size(nsin_list));
      maxBondWithSpecBondWeight=solver_props.MaxBondWeight*ones(size(nsin_list));
 end
 
[A b]=CreatDurationConstraints(solver_props.Duration,solver_props.MaxPortDuration,solver_props.MinPortDuration); %creat a custom made constraint of Years To Maturity
[conSet error] = port_conset('Default', length(nsin_list), 'GroupLims',conGroups,minGroupWeights,maxGroupWeights,'AssetLims',minBondWithSpecBondWeight,maxBondWithSpecBondWeight,length(nsin_list),'Custom',A,b); %Create constraint matrix