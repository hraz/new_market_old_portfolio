function [tolcon TolConFlag options2]=UpdateTolCon(tolcon,output,ErrorFlag,options2)
% this function compares the constraint violation to the max tolcon. if the
% constratin violation is not that big, the tolcon is increased to the area
% of the constraint violation ( a little larger) so that durion the next
% run of the function, there will be a solution
MaxTolCon=5e-3;
ConstVio=output.constrviolation; %the violation of the constraints
if ErrorFlag>0 %if there was a solution
    TolConFlag=0;
elseif ConstVio>MaxTolCon %if the constraint violation is larger than the maximum contratin violation that we will allow
    TolConFlag=1;%raise the flag that sais there is no solution
else %set the tolcon to be higher than the constraint violation, but no larger than the maximum allowed tolcon
    TolConFlag=0;
    if ConstVio<1e-5
        tolcon=1e-5;
    elseif ConstVio<1e-4
        tolcon=1e-4;
    elseif ConstVio<1e-3
        tolcon=1e-3;
    else %this means that ConstVio<5e-3, since we already went over the case of ConstVio>MaxTolCon
        tolcon=5e-3;
    end
end
options2 = optimset(options2,'tolcon', tolcon); %update the tolcon          