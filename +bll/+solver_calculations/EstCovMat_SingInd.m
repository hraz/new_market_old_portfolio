function nsin_data = EstCovMat_SingInd(nsin_data)
% Estimate the covaraince matrix according to the single index model. This
% was actually already done in CalcSingleIndexParams, but now it is
% assigned to handles.ExpCovMat, which is what the solver uses to solve the
% problem
nsin_data.ExpCovMat=nsin_data.SingIndExpCovMat; %use the covariance matrix which was found using the single index model
nsin_data.SingIndVar=diag(nsin_data.SingIndExpCovMat); %variances of the bonds, according to single index estimation
