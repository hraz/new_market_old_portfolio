function beta=FindBeta(ReturnMat,MktIndexReturns,Rm,HistLen)           %calc betas
%estimate beta based on the last HistLen returns observations of the bonds
%and the market index
NormRm=MktIndexReturns-Rm;                        %substruct the mean as the formula for expectation requires
ExpReturnMat=mean(ReturnMat);                     %calc the expected returns
NormReturnMat=ReturnMat-repmat(ExpReturnMat,min(size(ReturnMat, 1), HistLen),1);   %subtract the mean of the returns in order to prepar for the next step of expectation calculation

%% Check if the history length of ReturnMat is the same as that of the index.
% If not, set the history length to be the minimum of these two values.
HistLenReturnMat = size(ReturnMat, 1);
HistLenIndex = size(NormRm, 1);
minimumHistoryLength = min(HistLenReturnMat, HistLenIndex);
if HistLenReturnMat > HistLenIndex
    Numerator=NormReturnMat(end-minimumHistoryLength+1:end,:)'*NormRm;
elseif HistLenReturnMat < HistLenIndex
    Numerator=NormReturnMat'*NormRm(end-minimumHistoryLength+1:end);
else
    Numerator=NormReturnMat'*NormRm; %calc the inner product of each bonds normalized return with the markets return. This is equal to N*cov(RiRm) - the covariance of bond i with the market
end
Denominator=NormRm'*NormRm;                   %inner product between the market returns to itself. equal to N times var(Rm).
beta=Numerator/Denominator;                   %estimation of beta by definition (page 140)

