function [PortDuration RiskyReturnYear RiskyRiskYear PortWts RiskyWts PortReturnYear PortRiskYear MarkerPosition TangPortRetYear TangPortRiskYear error warning ]=OptimalPortError(YTM,ExpCovMat,NumPorts,ConSet,RisklessRateDay, BorrowRateDay, RiskAversion,FminConFlag,DurationVector, handles)
% Solves the optimal portfolio optimization probelm,
% and plots the efficient frontier and the capital market line. Returns the
% optimal portfolio, which is the one with the maximal Sharpe ratio
import gui.*;
Daily_YTM=Year2Day(YTM); %extract the Bruto yields of the bonds in the scope. these are the internal rate of return.
if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
    tmp = handles.MyBonds;                                  %MyBonds are the bonds in where am i mode
    if ~isempty(tmp)                                        %if we are in where am i mode, init the algorithm with the users weights
        for i=1:length(handles.MyBonds)
            ind(i)=find(extractfield(bond(BondScope),'ID')==handles.MyBonds(i).ID);
        end
        InitWeight(ind) = handles.MyWeights;                %init of weights according to users bonds
    else                                                    %if not in where am i mode
        InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N
    end   
    AddTCConstraint = 0;
    [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat,NumPorts, [], ConSet, InitWeight, AddTCConstraint); %Optimization
else                                                        %if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    [PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFmincon(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds);                        %Optimization
end

if isempty(PortRisk)                                        %if there was no solution
    PortWts=[];
    RiskyWts=[];
    PortRiskYear=[];
    PortReturnYear=[];
    RiskyReturnYear =[];
    RiskyRiskYear =[];
    TangPortRetYear =[]; TangPortRiskYear=[];
    MarkerPosition = 1;
    PortDuration = [];
    return
end
%else

% The next function can not be found in minimun variance optimization and
% targer return, since they don't optimize the sharpe ratio
PortRisk2 = PortRisk;
PortReturn2 = PortReturn ;
PortWts2 =PortWts  ;
RisklessRateDay2 = RisklessRateDay;
BorrowRateDay2 = BorrowRateDay ;
RiskAversion2 = RiskAversion;
% save input2portalloc_parfor PortRisk2 PortReturn2 PortWts2 RisklessRateDay2  BorrowRateDay2  RiskAversion2 
[RiskyRisk, RiskyReturn, RiskyWts, RiskyFraction,OverallRisk, OverallReturn, error] =...
    portallocError(PortRisk, PortReturn,PortWts, RisklessRateDay, BorrowRateDay, RiskAversion); %find the weights that maximize the Sharpe ratio

%% Analyze the results, and plot the point in the efficient frontier that maximizes the Sharpe ratio
RiskyWts=RiskyWts';                                          %weights of the portfolio on the efficient
PortReturnYear=Day2Year(PortReturn);                         %tranlate back to yearly returns, in order to plot in yearly terms
PortRiskYear=PortRisk*sqrt(365);                             %translate to yearly s.t.d., in order to plot the return vs. s.t.d. in yearly tersm
hold on
plot(PortRiskYear,PortReturnYear*100,'b','LineWidth',2);     %Plot the efficient first
hold on
xlim([PortRiskYear(1)*0.8 PortRiskYear(end)*1.2])
%% Plot the capital marker line - tangent to the efficient frontier
TangPortRisk=linspace(0,RiskyRisk,10);                       %divide the tangent line into parts in the risk axis
slope=(RiskyReturn-RisklessRateDay)/RiskyRisk;               %calc slope of the tangent line 
TangPortRet=slope*TangPortRisk+RisklessRateDay;              %calc return according to slope
TangPortRetYear=Day2Year(TangPortRet);                       %translate back into years
TangPortRiskYear=TangPortRisk*sqrt(365);                     %same for risk
hold on
plot(TangPortRiskYear,TangPortRetYear*100,'g','LineWidth',2) %plot tangent line
RiskyReturnYear=Day2Year(RiskyReturn);                       %translate portfolio returns to annual return
RiskyRiskYear=RiskyRisk*sqrt(365);                           %translate to annual s.t.d.
plot([0 RiskyRiskYear],RiskyReturnYear*100*ones(1,2),'r--','LineWidth',2)
plot(RiskyRiskYear, RiskyReturnYear*100,'r*','markersize',16)%mark the optimal point
%DurationVector=handles.Duration;                             %extract the duration of all the bonds in the BondScope
PortDuration=DurationVector.'*RiskyWts;                          %calculate the weighted average of the years to maturity
tit=sprintf('Return: %1.2f%%, Risk: %1.3f, Duration: %1.2f years',RiskyReturnYear*100,RiskyRiskYear,PortDuration);
AddFigNames(tit,'\sigma','Annual Return [%]',1)
[~,MarkerPosition] = min(abs(RiskyReturn - PortReturn));    %the marker position in the index of the returns that is the closest to the optimal Sharpe return

%end






