function Sigma_ei = FindSigma_ei(ReturnMat,MktIndexReturns,alpha,beta,Rm,HistLen,method)

%% Yoav's original version

% if method==1
% ExpRetByModel=alpha+beta*Rm.';            %expected return by using the model
% ExpRetByModel = ExpRetByModel.';                    %turn into a row vector
% D=ReturnMat-repmat(ExpRetByModel,min(size(ReturnMat, 1), HistLen),1);      %differenc matrix between the return and the expected return using the model. ei=Ri-(alpha_i+beta_i*R_M)
% Sigma_ei=mean(D.*D);                       %variance is the mean of square distances 
% else
%    DesAssetsVar=mean(ReturnMat.*ReturnMat);       %variances of the all assets. 
%    MeanMktIndex=mean(MktIndexReturns);           %the mean of the market return. this is \bar{R_m}
%    NormMktIndex=MktIndexReturns-repmat(MeanMktIndex,length(MktIndexReturns(:,1)),1);  %normalized return in the sense that the mean was taken off. the mean of this is 0.
%    MarketVar=diag(NormMktIndex'*NormMktIndex/min(size(ReturnMat, 1), HistLen));        %variance of market. take only the var and not the cov terms
%    Sigma_ei=DesAssetsVar-(beta.^2*MarketVar).';            %according to: sigma_ei^2=sigma_i^2-beta_i^2*sigma_M^2
% end

%%
% Version written by Idan on 23/5/13. NEED TO CHECK THIS WITH YOAV!!

%FindSigma_ei calculates variances of the residuals in single index model
%for each bond.
%
%   Sigma_ei = FindSigma_ei(ReturnMat,MktIndexReturns,alpha,beta,Rm,HistLen,method)
%   recieves:
%   the returns matrix, alpha, beta and the market returns and
%   calculates the variance for the residuals of each bond.
%
%	Input:
%       ReturnMat - a matrix of dimensions (nObservations)*(nBonds). The
%       (i, j) entry is the return of asset (i) on observation (j).
%       MktIndexReturns - a vector of dimensions (nObservations)*(1) of
%       market returns.
%       alpha - a vector of dimensions (nBonds)*(1) of the alpha values for
%       each asset according to the single index model.
%       beta - a vector of dimensions (nBonds)*(1) of the beta values for
%       each asset according to the single index model.
%       Rm - perhaps not needed.
%       HistLen - perhaps not needed.
%       method - perhaps not needed.
%   Output:
%       Sigma_ei - a vector of dimensions (nBonds)*(1) of the variances of
%       residuls.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%		FINDBETA, FIND_ALPHA
%
% Idan Oren, 23/5/13
% Copyright 2013, Bond IT Ltd.
% Updated by Hillel Raz, 25/06/2013
% Added a default check testing that the size of the index and the returns
% mat are equal.
%%


%% Check input arguments type:
flagType(1) = isa(ReturnMat, 'double');
flagType(2) = isa(MktIndexReturns, 'double');
flagType(3) = isa(alpha, 'double');
flagType(4) = isa(beta, 'double');
flagType(5) = isa(Rm, 'double');
flagType(6) = isa(HistLen, 'double');
flagType(7) = isa(method, 'double');
if sum(flagType) ~= length(flagType)
    error('FindSigma_ei:wrongInput', ...
        'One or more input arguments is not of the right type');
end

nObservations = min(size(ReturnMat, 1), length(MktIndexReturns));
nBonds = size(ReturnMat, 2);
mtxAlpha = ones(nObservations, 1)*alpha'; %repmat(alpha, nObservations, nBonds);
mtxBeta = ones(nObservations, 1)*beta'; %repmat(beta, nObservations, nBonds);
mtxMarket = MktIndexReturns(1:nObservations)*ones(1, nBonds); %repmat(MktIndexReturns, nObservations, nBonds);
mtxExpectedReturnBySingleIndexModel = mtxAlpha + mtxBeta.*mtxMarket;
mtxResiduals = ReturnMat(1:nObservations, :) - mtxExpectedReturnBySingleIndexModel;
Sigma_ei = var(mtxResiduals)';