function [ yield, iterations ] = bnd_yield(coupon, cf, varargin)
%BND_YIELD calculates yield to maturity for a fixed income security.
%   Given coupon rate, cash flows and list of some additional information, this
%   function returns the yields to maturity.
%
%   [ yield, iterations ] = bnd_yield(coupon, cf)
%   [ yield, iterations ] = bnd_yield(coupon, cf, varargin)
%
%   Input:
%       coupon - is an NASSETSx2 dataset specifying the coupon rate in decimal form.
%
%       cf - is an NALLPAYMENTSxNASSETS fts specifying the cash flow of required assets.
%
%   Optional Input:
%       Basis - is a scalar or NASSETSx1 vector specifying the day-count basis.
%                     Possible values include:
%                     0 - actual/actual
%                     1 - 30/360 SIA
%                     2 - actual/360
%                     3 - actual/365
%                     4 - 30/360 PSA
%                     5 - 30/360 ISDA
%                     6 - 30/360 European
%                     7 - actual/365 Japanese
%                     8 - actual/actual ISMA
%                     9 - actual/360 ISMA
%                    10 - actual/365 ISMA (default)
%                    11 - 30/360 ISMA
%                    12 - actual/365 ISDA
%                    13 - bus/252
%
%       Compounding - is a scalar or an NASSETSx1 vector specifying the compounding frequency
%                           for yield calculation.  By default, SIA bases (0-7) and BUS/252 use a
%                           semi-annual compounding convention and ISMA bases (8-12) use an annual
%                           compounding convention. 
%
%       lCouponCompounding - is a string or NASSETS cell array specifying the compounding
%                           convention for computing the yield of a bond in the last coupon period,
%                           i.e.: with only the last coupon and the face value to be repaid.
%                           Choices are 'simple' or 'compound'.
%
%   Output:
%       yield - is an NUMBONDSx2 dataset of the yield to maturity with a required compounding.
%
% Note: 
%       -  The function based on the MATLAB bndyield function and all corrections are made on base of
%           the Israeli market conditions like time to maturity calculation for Short Term Treasury
%           Bills and the form of the data that we have. 
%
%       -  If lCouponCompound, Compounding or Basis are input, optional inputs must be specified as
%           fields of some structure. 

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import utility.calendar.*;

    %% Input validation:
    error(nargchk(2,3,nargin));
    
    %% Input parsing:
    p = inputParser;

    p.addRequired('coupon', @(x)(isa(x, 'dataset')));
    p.addRequired('cf', @(x)(isa(x, 'fints')));

    p.addParamValue('compounding', 1, @(x) all(ismember(x,[-1 1 2 3 4 6 12])));
    p.addParamValue('basis', 10, @(x) all(isvalidbasis(x)));
    p.addParamValue('lcouponcompounding',{'compound'}, @(x) all(ismember(x,{'compound','simple'})));

    % Parse method to parse and validate the inputs:
    p.StructExpand = true; % for input as a structure
    p.KeepUnmatched = true;

    try
        p.parse(coupon, cf, varargin{:})
    catch ME
        newME = MException('pricing:bnd_yieldoptionalInputError', 'Error in input arguments');
        newME = addCause(newME,ME);
        throw(newME)
    end
    Compounding = p.Results.compounding;
    Basis =  p.Results.basis;
    lCouponCompound = cellstr(p.Results.lcouponcompounding);
    
    nBonds = size(cf,2);
    
    %% Step #1 (Scalar expansion on non-bond inputs):
    if (length(Compounding) == 1)
        Compounding = Compounding(ones(nBonds,1));
    elseif (length(Compounding) ~= nBonds)
        error('pricing:bnd_yield:wrongInput', 'Wrong number of compounding values.');
    end
    
    if (length(Basis) == 1)
        Basis = Basis(ones(nBonds,1));
    elseif (length(Basis) ~= nBonds)
        error('pricing:bnd_yield:wrongInput', 'Wrong number of compounding values.');
    end
    
    if (length(lCouponCompound) == 1)
        lCouponCompound = lCouponCompound(ones(nBonds,1));
    elseif (length(lCouponCompound) ~= nBonds)
        error('pricing:bnd_yield:wrongInput', 'Wrong number of last coupon compounding values.');
    end
    
    %% Step #2 (Generate cash flows):
    zero_coup_idx = isnan(coupon.rate);
    cf_amount = fts2mat(cf)';
    
    settle = cf.dates(1);
    cf_dates = (cf.dates)';
    cf_dates = cf_dates(ones(nBonds, 1), :);
    cf_dates(isnan(cf_amount)) = NaN;
    
    t_factor = yearfrac(settle(ones(size(cf_dates))), cf_dates, Basis);
    t_factor(zero_coup_idx, 2:end) = t_factor(zero_coup_idx, 2:end) - 1/365; % the correction for Israely Short Term Treasury Bills - MAKAMs
    
    %% Step #3 (Initial Guess for the discount based on yield = coupon rate):
    coupon_rate = coupon.rate;
    coupon_rate(zero_coup_idx) = 0;
    x_guess = 1./(1 + coupon_rate ./ Compounding);

    %% Step #4 (Initialization start values of the parameters):
    fZeroOptions = optimset(optimset('fzero'), 'TolX', 1e-12,'Display', 'off');
    fSolveOptions = optimset(optimset('fsolve'), 'TolX', 1e-12,'TolFun', 1e-12, 'Display', 'off','LargeScale', 'off');

    yld = zeros(nBonds,1);
    iterations = NaN(nBonds,1);

    %% Step #5 (Calculate yield to maturity):
    for i = 1:nBonds
        if isnan(cf_amount(i,1))
            yld(i) = NaN;
        elseif sum(~isnan(cf_amount(i,:))) == 2 % The case when settlement in last coupon period:
            % Solve directly for period discount factors:
            switch lower(lCouponCompound{i})
                case 'compound'
                    idx = min(find(~isnan(cf_amount(i,2:end)))+1);
                    per_disc = (-cf_amount(i,1)/cf_amount(i,idx))^(1/t_factor(i,idx));
                    yld(i) = (1./per_disc - 1) .* Compounding(i);
                    flag = 1;
                case 'simple'
                    idx = min(find(~isnan(cf_amount(i,2:end)))+1);
                    yld(i) = (-cf_amount(i,idx)./cf_amount(i,1)-1)./t_factor(i,idx) .* Compounding(i);
                    flag = 1;
            end
        else % General case, settlement before the last coupon period:

            objfun = @(x) nansum(cf_amount(i,:) .* (x.^t_factor(i,:)));
            [per_disc,~, flag, output] = fzero(objfun,x_guess(i), fZeroOptions);

            % if fzero was unable to find a zero, try using fsolve
            if flag < 0
                [per_disc, ~, flag, output] = fsolve(objfun, x_guess(i), fSolveOptions);
            end

            yld(i) = (1./per_disc - 1) .* Compounding(i);
            iterations(i) = output.iterations-1;
        end
    end % end of loop over bonds

    if flag < 0
        warning(message('pricing:bnd_yield:solutionConvergenceFailure'));
    end
    
    %% Step #6 (Create output dataset):
    nsin = coupon.nsin;
    yield = dataset(nsin, yld);
    
end
