% clear; clc;

%% Import external packages:
import bll.pricing.*;
import dal.market.get.cash_flow.*;
import dal.market.get.dynamic.*;
import utility.*;
import utility.calendar.*;
import utility.fts.*;
import utility.cash_flow.*;
  import dal.mysql.connection.*;
    import utility.dal.*;    
    import utility.dataset.*;    

%% Build start parameters:
settle = datenum('2005-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.
settle_end = datenum('2012-02-01');

%% The sample of the optional parameters structure:
% param.compounding = 1; % annually
% param.basis = 10; % act/365
% param.lcouponcompounding = 'compound'; % default compounding

%% Short Term Treasury Bills cash flow build:
% [ tbill_cf ] = cfbuilder(settle, 0);
% [ nsin ] = strid2numid(tsnames(tbill_cf));
% [ rate ] = NaN(size(tbill_cf,2),1);
% [ tbill_cr ] = dataset(nsin, rate);

%% Calculating of yield to maturity of the Short Term Treasury Bills:
% [ tbill_yld ] = bnd_yield( tbill_cr, tbill_cf, param );
% 
 conn = mysql_conn();
% err = tbill_yld.yld - fts2mat(da_bond_dyn(nsin, 'ytm', settle))'
while(settle <= settle_end)
    
    %% Treasury Notes cash flow build:
    [ shahar_cf ] = cfbuilder(settle, 3, false);
    [ nsin ] = strid2numid(tsnames(shahar_cf));
    [ shahar_cr ] = get_cash_flow_temp( nsin  );
    rate = shahar_cr.coupon_rate;
    shahar_cr = dataset(nsin, rate);
    
% yield = bndyield(80.55,0.0575 ,'2005-01-03','2006-08-20',1,10,0,'1997-08-13','1998-08-2','2006-08-20','Face')

%% Calculating of yield to maturity of the Treasury Notes:
    [ shahar_yld ] =bnd_yield( shahar_cr, shahar_cf ) ;
   obs_date = {datestr(settle)};
   yield = shahar_yld.yld;
   security_id = shahar_yld.nsin
     obs_date = obs_date(ones(size(shahar_yld.yld)));
     ds = dataset(security_id,obs_date,yield)
     fastinsert(conn,'tmp_yield',{'security_id','obs_date','yield'},ds);
    settle = settle+1;
end;
% bndyield
err = shahar_yld.yld - fts2mat(da_bond_dyn(nsin, 'ytm', settle))'


