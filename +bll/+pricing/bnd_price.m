function [ price ] = bnd_price(cf, ir_curve, settle, varargin)
%BND_PRICE dirty price a fixed income security from yield to maturity.
%   Given NBONDS with date parameters and yields to
%   maturity, return the clean prices and the accrued interest due.
%
%   [price, ai] = bnd_price(Yield, CouponRate, Settle, Maturity)
%
%   [price, ai] = bnd_price( Yield, CouponRate, Settle, Maturity, varargin )
%
%   Optional Inputs: Period, Basis, EndMonthRule, IssueDate, FirstCouponDate,
%                    LastCouponDate, StartDate, Face, LastCouponInterest,
%                    CompoundingCompounding, DiscountBasis
%
%   Note:
%   - For SIA conventions, the price and Yield are related by the formula:
%     price + Accrued Interest = sum(Cash_Flow*(1+Yield/2)^(-Time))
%     where the sum is over the bond's cash flows and corresponding times in
%     units of semi-annual coupon periods.
%     For ISMA conventions, the price and Yield are related by the formula:
%     price + Accrued Interest = sum(Cash_Flow*(1+Yield)^(-Time))
%   - All non-scalar or empty matrix input arguments must be either NUMBONDSx1
%     or 1xNUMBONDS conforming vectors.
%   - Fill unspecified entries in input vectors with NaN.
%   - Dates can be serial date numbers or date strings.
%   - Optional inputs can be specified as parameter value pairs.  If
%     LastCouponInterest, CompoundingCompounding or DiscountBasis are input,
%     optional inputs must be specified as parameter value pairs.
%     Otherwise, optional inputs may be specified by order according to the
%     help.
%
%   Inputs:
%        Yield - Yield to maturity.
%
%   CouponRate - Coupon rate in decimal form.
%
%       Settle - Settlement date.
%
%     Maturity - Maturity date.
%
%   Optional Inputs:
%            Period - Number of coupons payments per year.
%                     Possible values include:
%                     0, 1, 2 (default), 3, 4, 6, 12
%
%             Basis - Day-count basis.
%                     Possible values include:
%                     0 - actual/actual (default)
%                     1 - 30/360 SIA
%                     2 - actual/360
%                     3 - actual/365
%                     4 - 30/360 PSA
%                     5 - 30/360 ISDA
%                     6 - 30/360 European
%                     7 - actual/365 Japanese
%                     8 - actual/actual ISMA
%                     9 - actual/360 ISMA
%                    10 - actual/365 ISMA
%                    11 - 30/360 ISMA
%                    12 - actual/365 ISDA
%                    13 - bus/252
%
%      EndMonthRule - End-of-month rule; default is 1 (in effect)
%                     0 - Rule is NOT in effect for the bond(s)
%                     1 - (default) Rule is in effect for the bond(s) (meaning
%                         that a security that pays coupon interest on the last
%                         day of the month will always make payment on the last
%                         day of the month)
%
%         IssueDate - Bond issue date.
%
%   FirstCouponDate - Irregular or normal first coupon date.
%
%    LastCouponDate - Irregular or normal last coupon date.
%
%         StartDate - Forward starting date of payments.
%
%              Face - Face value of the bond; default is 100.
%
%   LastCouponInterest - Compounding convention for computing the yield of
%                        a bond in the last coupon period, i.e.: with only
%                        the last coupon and the face value to be repaid.
%                        Choices are 'simple' or 'compound'.
%
%   Compounding - Compounding for yield calculation.  By
%                          default, SIA bases (0-7) and BUS/252 use a semi-annual
%                          compounding convention and ISMA bases (8-12) use
%                          an annual compounding convention.
%
%   DiscountBasis - Basis used to compute the discount factors for
%                   computing the yield.  The default behavior is for SIA
%                   bases to use the actual/actual day count to compute
%                   discount factors, and for ISMA day counts and BUS/252
%                   to use the specified basis.
%
%   Outputs:
%   price - [NUMBONDSx1 vector] Clean price of the bond.
%           The dirty price of the bond is the clean price plus the accrued
%           interest. It is equal to the present value of the bond cash flows.
%
%   ai - [NUMBONDSx1 vector] Accrued interest payable at settlement.
%
%   See also BNDYIELD, CFAMOUNTS

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import utility.calendar.*;
    
    %% Input validation:
    error(nargchk(3, 4, nargin));
    
    %% Input parsing:
    p = inputParser;

    p.addParamValue('basis',10,@(x) all(isvalidbasis(x)));
    p.addParamValue('compounding',1,@(x) all(ismember(x,[-1 1 2 3 4 6 12])));

    % Parse method to parse and validate the inputs:
    p.StructExpand = true; % for input as a structure
    p.KeepUnmatched = true;

    try
        p.parse( varargin{:} );
    catch ME
        newME = MException('pricing:bnd_price:optionalInputError', 'Error in input arguments');
        newME = addCause(newME,ME);
        throw(newME)
    end

    param.compounding = p.Results.compounding;
    param.basis =  p.Results.basis;

    %% Step #1 (Get dates for the discount factors and discount factors according to the given curve):
    if (isa(ir_curve, 'bll.term_structure.ir_data_curve'))
        df = ir_curve.get_disc_factors(cf.dates, settle, param);
    elseif (isa(ir_curve, 'bll.term_structure.ir_func_curve'))
        df = ir_curve.get_disc_factors(cf.dates, settle);
    else
        error('pricing:bnd_price:wrongInput', 'Wrong interest curve object class.');
    end
    df = fts2mat(df);
    
    %% Step #2 (Resize discount factors for all assets):
    df = df(:, ones(1,size(cf, 2)));
    
    %% Step #3 (Compute expected assets' prices):
    price = nansum(cf .* df);
    
end