function [ rp_alloc, gr_mc_weight ] = rp_opt( nsin, cov_mtx, group_fun, rdate, varargin )
%RP_OPT returns portfolio allocation according to equal risk between assets from different groups.
%
%	[ rp_alloc, gr_mc_weight ] = rp_opt( nsin, cov_mtx, group_fun, varargin )
%   receives list of assets, its covariance matrix, handle on the groupping function and, as
%   alternative - required target risk rate. The function returns risk-parity allocation and group
%   risk marginal contributions.
%
%   Input:
%       nsin - is an NASSETx1 vector specifying the given assets.
%
%       cov_mtx - is an NASSETSxNASSETS covariance matrix of the given assets.
%
%       group_fun - is a handle on the function that divide given assets to some groups according
%                           its properties (like asset type: goverment/non-goverment, or rating value).
%
%
%   Optional input:
%       target_volatility - is a scalar value specifying the required target portfolio volatility.
%
%   Output:
%       rp_alloc - is a NASSETSx2 dataset specifying the risk-parity allocation.
%
%       gr_mc_weight - is a 1xNGROUPS vector specifying the each group marginal contribution to the
%                           portfolio total risk value.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.

    %% Import external packages:
    import bll.rp_mdl.*;

    %% Input validation:
    error(nargchk(4, 5, nargin));
    if isempty(nsin)
        error('bll_rp_mdl:rp_opt:mismatchInput', 'NSIN list cann''t be an empty.');
    end
    
    if ~isempty(cov_mtx) 
        if size(cov_mtx,1) ~= size(cov_mtx,1)
            error('bll_rp_mdl:rp_opt:mismatchInput', 'Covariance matrix must be quadratic.');
        elseif size(cov_mtx,1) ~= length(nsin)
            error('bll_rp_mdl:rp_opt:mismatchInput', 'Size of Covariance matrix is not equal to number of assets.');
        end
    else
        error('bll_rp_mdl:rp_opt:mismatchInput', 'Covariance matrix cann''t be an empty.');
    end
    
    if ~isempty(group_fun) && ~isa(group_fun, 'function_handle')
        error('bll_rp_mdl:rp_opt:mismatchInput', 'Wrong groupping function handle.');
    end
    
    if (isempty(rdate))
        error('bll_rp_mdl:rp_opt:mismatchInput', 'Required date is empty.');
    end
        
    %% Step #1 (Grouping given assets):
    [ group_alloc ] = feval(group_fun, nsin, rdate);
    
    %% Step #2 (Definition of target function and additional model constraints):
    function [err] = rp_target_fun(asset_weight)
        if (length(asset_weight) == length(nsin))
            % Calculation portfolio variance:
            port_var = (asset_weight' * cov_mtx * asset_weight); % portfolio risk described there using std.
            
            % Calculation beta of the of each asset using covariance of its own return vs. portfolio
            % return.
            asset_beta = NaN(1,length(nsin));
            for i = 1: size(group_alloc, 2)
                asset_beta(group_alloc(:, i)) = (cov_mtx(group_alloc(:,i), :) * asset_weight)' / port_var;
            end
            
            % Calculation grouped margin contribution into portfolio risk:
            gr_mc_weight = zeros(1, size(group_alloc, 2));
            for i = 1: size(group_alloc, 2)
                gr_mc_weight(i) = asset_beta(group_alloc(:, i)) * asset_weight(group_alloc(:, i));
            end
            
            % Target expression:
            err = norm(gr_mc_weight - (1/ size(group_alloc, 2)));
        end
        
    end

    %% Step #3 (Define initial weights):
    init_weight = ones(length(nsin),1) / length(nsin); % default is equal weights for all given assets;
    
    %% Step #4 (Define optimization process parameters):
%     options = optimset('Algorithm', 'interior-point', 'Display', 'iter-detailed', 'MaxFunEvals', 10000);
    options = optimset('Algorithm', 'interior-point', 'Display', 'off', 'MaxFunEvals', 10000);
    
    %% Step #5 (Execution of the optimization process):
    [rp_weight] = fmincon(@rp_target_fun, init_weight, [], [], ones(1, length(nsin)), 1, zeros(length(nsin), 1), ones(1, zeros(length(nsin), 1)-eps), [], options);
    
    %% Step #6 (Build risk-parity allocation):
    rp_alloc = dataset(nsin, rp_weight, 'VarNames', {'nsin', 'weight'});
    
end

