function [ group_alloc ] = rp_group( nsin, rdate )
%RP_GROUP groups assets according to pre-specified criteria.
%
%   [ group_alloc ] = rp_group( nsin )
%   receives list of assets and returns its distribution by pre-defined groups.
%
%   Input:
%       nsin - is an NASSETSx1 vector specifying given assets list.
%
%       rdate - is a required date for filterring.
%
%   Output:
%       group_alloc - is a boolean NASSETSxKGROUPS matrix specifying boolean value of belonging to
%                               the group.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.

    %% Import external packages:
    import dal.market.filter.static.*;
    import dal.market.filter.risk.*;
    
    %% Input validation:
    error(nargchk(2,2,nargin));
    
    if (isempty(nsin))
        error('bll_rp_mdl:rp_group:mismatchInput', 'The nsin list is an empty');
    elseif (~isnumeric(nsin))
        error('bll_rp_mdl:rp_group:wrongInput', 'The nsin list is not numerical');
    end
       
    %% Step #1 (Memory allocation to group-asset matrix):
    group_alloc = false(length(nsin), 3);
    
    %% Step #2 (Grouping assets according to 3 pre-defined groups: Government, and two groups according asset rating score):
    % Risk Free group (Government assets):
    group_alloc(:,1) = ismember(nsin, bond_static_partial( 'class', upper('Government Bond'), rdate ));
    
    %% Step #3 (Get rating data about given assets):
    mlt_lb = {'BBB-'; 'NVR'};
    mlt_ub = {'AAA'; 'BB+'};
    mdg_lb = {''; ''}; % mdg_lb = {'Baa3'; 'Ba1'};
    mdg_ub = {''; ''};
    log_opr = {'OR'; 'OR'};
    ds = dataset( mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr);
    % Filter results are:
    [ nsin_rating ] = bond_rank_multi(  rdate, ds, nsin );
    
    % Middle Risk group (Corporate with ratings between BBB- and AAA (Maalot scale) or between Baa3 and
    % Aaa (Midroog scale):
    group_alloc(:,2) = ismember(nsin, nsin_rating.filter_1); %ismember(nsin, bond_rank_complex('BBB-', 'AAA', 'Baa3', 'Aaa', 'AND', rdate ));
    
    % High Risk group (Corporate with ratings from BB+ and down (Maalot scale), or Ba1 and down (Midroog scale)):
    group_alloc(:,3) = ismember(nsin, nsin_rating.filter_2); %ismember(nsin, bond_rank_complex('BB+', 'NVR', 'Ba1', 'NVR', 'AND', rdate ));

end

