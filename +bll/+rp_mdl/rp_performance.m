function [ cumulative_holding_ret, risk, sharpe_hist ] = rp_performance( allocation, first_date, last_date, ir_lb, ir_ub, sim_freq )
%% RP_PERFORMANCE returns cumulative holding return on initial investment and its sharpe ratio.
%
%   [ port_holding_ret, sharpe_hist ] = rp_performance( allocation, first_date, last_date, sim_date, risk_free )
%   receives portfolio allocation variations, start ane finish dates for performance calculus,
%   simulation dates and risk free interest rates for each simulation period. The function returns
%   portfolio holding returns 3D matrix and 2D matrix of sharpe ratio values according to portfolio
%   allocation types and number of simulations.
%
%   Input:
%       allocation - is a structure that specifying different portfolio asset allocation methods.
%                       Each field must include 2D matrix: [nsin, weight], where the first dimention
%                       is a nsin list.
%
%       first_date - is a scalar value specifying the start date for portfoli oallocation
%                       performance calculus.
%
%       last_date - is a scalar value specifying the last date for portfoli oallocation
%                       performance calculus.
%
%       sim_freq - is a positive integer scalar specifying required number of simulation in a year.
%
%   Output:
%       cumulative_holding_ret - is an NALLOCTYPE structure of NOBSx2xNSIM matrix specifying portfolio holding return
%                       by portfolio allocation type and simulation date.
%
%       sharpe_hist - is an NALLOCTYPE structure of NSIMx1 matrix specifying the portfolio holding sharpe ratios
%                       for each portfolio allocation type and for all simulation dates.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Import external packages:
    import bll.rp_mdl.*;
    import dal.market.get.dynamic.*;
    import dal.market.get.base_data.*;
    import utility.fts.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(6,6,nargin));
    
    %% Step #1 (Initialization of basic variables and constants):
    portfolio_type = fieldnames(allocation);
    nportfolio_type = length(portfolio_type);
    
    %% Step #2 (Collect historical cummulative income and its yield for all history length):
    for i = 1:nportfolio_type
        [ cumulative_holding_ret.(portfolio_type{i}), holding_ret.(portfolio_type{i}), ~, stop_point.(portfolio_type{i})  ] = get_bond_holding_return(allocation.(portfolio_type{i}), first_date, last_date, ir_lb, ir_ub, sim_freq); 
    end
    
    %% Step #3 (Defining simulation dates to determine the portfolio performance):
    sim_period_mnth = 12 / sim_freq; 
    dates = cumulative_holding_ret.(portfolio_type{i})(:,1,:);
    total_time_period = ceil(12 * etime( datevec(dates(end)), datevec(dates(1)) ) / (3600*24*365));  % in months
    if total_time_period < sim_period_mnth
        warning('bll_rp_mdl:rp_performance:wrongInput', ...
            'Given number of simulations per a year not appropriate to given history length.');
    end
    nsim = length(stop_point.(portfolio_type{1}) );
    
    %% Step #4 (Get risk free rates for the given stop points):
    rf_time_period = etime( datevec(stop_point.(portfolio_type{1})), datevec(first_date(ones(length(stop_point.(portfolio_type{1})),1),1))) / (3600*24*365);    
    [appropriate_rf_period, ~, prev_i] = unique(approp_rf_prd( rf_time_period' )); % Look for an appropriate risk free time periods for calculated ones:
    rf_period = fts2mat(da_nominal_yield_between_dates( appropriate_rf_period, first_date, first_date ))'; % Looking for risk free interest rate for the period between given last_trading_date and the end of period:
    risk_free = rf_period(prev_i) .* rf_time_period;
                
    %% Step #5 (Sharpe ratio calculation):
    sharpe_hist = zeros(nsim, nportfolio_type+1 );
    sharpe_hist(:,1) = stop_point.(portfolio_type{1}) ;
    for i = 1:nsim
        A = sharpe(holding_ret.(portfolio_type{i})(:, 2:end, i), risk_free(i))
    end
    
    %% Step #6 (Portfolio Risk calculation):
    
end