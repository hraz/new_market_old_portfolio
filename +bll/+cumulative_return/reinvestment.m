function [mtxAllocationOut, mtxTransactions, benchmarkFlag, cashPerBondOut, benchmarkFTS, ytmInterestPerBondOut] = reinvestment(mtxAllocation, reinvestmentStrategy, cashIn, timeToMaturity, lastCallDate, desiredDate, constraints, cashPerBond, dailyYtmInterestPerBond, benchmarkFTS, ytmInterestPerBondIn, riskFreeInfo)

%REINVESTMENT reinvests money according to a given strategy.
%
%    [mtxAllocationOut, benchmarkFlag] = reinvestment(mtxAllocation, reinvestmentStrategy, cashIn,...
%                                                                                      portfolioDuration, lastCallDate, desiredDate, benchmark_id)
%   recieves the current allocation, the reinvestment strategy, the available cash and several additional
%   variables. It provides the new allocation according to one of the following reinvestment strategies
%   (for now we only work with the third option - 'risk-free'):
%   1. 'Specific-Bond-Reinvestment' - we invest in the same bond from which
%                                                                           we got a coupon.
%   2. 'portfolio': we invest in the entire portfolio.
%   3. 'risk-free': we invest in a risk free asset (government bond).
%   4. 'benchmark': we invest in the appropriate benchmark.
%   5. 'bank': we put the money in the bank to earn interest.
%   6. 'none': we keep the money without investing it.
%	Input:
%       mtxAllocation �  matrix of dimensions (nSecurities)*(3).
%                                                                            The format is:
%                                                                            security_id's | units | price |
%                                                                            -------------------------|------------|-----------|
%       reinvestmentStrategy - a string specifying the reinvestment
%                                                            strategy. For now this is only 'risk-free'.
%        cashIn - For most strategies, this is a scalar. The exception: if the strategy is to invest in a
%                           specific bond ('Specific-Bond-Reinvestment')) then cashIn is a matrix
%                           of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of cash from each bond.
%       timeToMaturity - time to maturity of the portfolio calculated from current date.
%       lastCallDate - a datenum variable of the last date the function was called upon.
%       desiredDate - a datenum variable of the current date.
%       constraints - data structure: require fields: benchmark_id - nsin of benchmark if needed. At this point it should
%                                      be empty.
%                     also will contain required fields for filtering
%                     purposes.
%       CashPerBond - a matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of  PREVIOUS cash from each bond.
%       ytmInterestPerBondIn - a vector of size (Nnsin)which holds the ytm of each bond at the beginning of the portfolio.
%       benchmarkFTS - an fts holding the daily yield of the benchmark.
%   Output:
%       mtxAllocationOut � matrix similar to mtxAllocation but
%                                                   updated due to the reinvestment process.
%        mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%       benchmarkFlag - a flag which indicates if we invested in the
%                                           benchmark (1) or not (0). For now, this is always 0 (zero).
%        CashPerBondOut - a matrix similar to cashPerBond but updated due to
%        interest and to new cash flow.
%       benchmarkFTS - an fts holding the daily yield of the benchmark.
%       ytmInterestPerBondOut - similar to ytmInterestPerBondIn but updated
%       due to new bonds that were purchased.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%       MODIFY_ALLOCATION
%
%   Comments:
%       9 is the 'nsin' of cash in the bank.
%       99 is the 'nsin' of cash not in the bank.
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Idan on 19/6/13 in order to implement the new reinvestment strategy according
% to which any amount of money we get from a specific bond is reinvested to a risk-free-like asset
% with interest rate equal to that bond's ytm (at the beginning of the
% portfolio).
% Update by Hillel, 28/07/13 Added riskFreeInfo as the last input
% parameter, added riskFreeInfo as the last input parameter in the call of risk_free_reinvestment 


import bll.cumulative_return.*;
import dal.market.get.base_data.*; % Because of: get_nominal_yield_with_param
import dal.market.get.dynamic.*; % Because of: get_multi_dyn_between_dates


%% Check input arguments type:
flagType(1) = isa(mtxAllocation, 'double');
flagType(2) = isa(reinvestmentStrategy, 'char');
flagType(3) = isa(cashIn, 'double');
flagType(4) = isa(timeToMaturity, 'double');
flagType(5) = isa(lastCallDate, 'numeric');
flagType(6) = isa(desiredDate, 'numeric');
if ~isempty(constraints.benchmark_id)
    flagType(7) = isa(constraints.benchmark_id, 'double');
else
    flagType(7) = 1;
end
flagType(8) = isa(cashPerBond, 'double');
flagType(9) = isa(dailyYtmInterestPerBond, 'double');
if sum(flagType) ~= length(flagType)
    error('reinvestment:wrongInput', ...
        'One or more input arguments is not of the right type');
end
%% Check input arguments sizes:
nColumns1 = size(mtxAllocation, 2);
sizes = [nColumns1];
flagSizes = sizes == [3];
if sum(flagSizes) ~= length(flagSizes)
    error('reinvestment:wrongInput', ...
        'One or more input arguments is not of the right size');
end

securityIdCashInBank = 9;
securityIdCashNotInBank = 0;
cashPerBondOut = cashPerBond;
mtxTransactions = [];
mRowsOfTransactions = 1;
ytmInterestPerBondOut = ytmInterestPerBondIn;
y = find(mtxAllocation(:,1)==securityIdCashNotInBank);
if isempty(mtxAllocation(y, 2))
    oldCash = 0;
else
    oldCash = mtxAllocation(y, 3);
end
benchmarkFlag = 0;
switch reinvestmentStrategy
    
    case 'Specific-Bond-Reinvestment'   %% THIS IS ONLY A TEMPORARY PART! It takes the money which
        % came from a specific bond and reinvest it in a fictionary asset which is
        % risk-free but has a return equal to the same bond's ytm, as it was at the
        % beginning of the portfolio.
        [mtxTransactions, cashPerBondOut] = specific_bond_reinvestment(dailyYtmInterestPerBond,...
            cashPerBond, desiredDate, lastCallDate, cashIn, securityIdCashNotInBank);
        
    case 'real-specific-bond-reinvestment'
        indicesOfBondsWithCash = cashIn(:, 2) ~= 0;
        ytmSought = ytmInterestPerBondIn(indicesOfBondsWithCash);
        bondsWithCash = cashIn(indicesOfBondsWithCash, 1); %CHECK!!! ************
        if ~isempty(ytmSought)
            [bondWanted bondPrice bondYTM] = find_specific_bond_according_to_ytm(desiredDate, constraints, ytmSought);
            [mtxTransactions, ~] = real_specific_bond_reinvestment(desiredDate, cashIn(indicesOfBondsWithCash, 2),...
                oldCash, bondWanted, bondPrice, securityIdCashNotInBank, bondsWithCash);
            ytmInterestPerBondOut(end+1: end+length(ytmSought)) = bondYTM;
            
        end
        cashPerBondOut = cashIn; %CHECK!!! ************
        cashPerBondOut(:, 2) = 0;
        
    case 'portfolio'  %%%% FIX THIS
        [mtxTransactions] = portfolio_reinvestment(cashIn, securityIdCashInBank,...
            mtxAllocation, oldCash, desiredDate);
        
    case 'risk-free'
        [mtxTransactions] = risk_free_reinvestment(timeToMaturity, desiredDate,...
            cashIn, securityIdCashNotInBank, oldCash, riskFreeInfo);
        
    case 'benchmark'
        [mtxTransactions, benchmarkFTS, cashPerBondOut] = benchmark_reinvestment(constraints, benchmarkFTS,...
            desiredDate, lastCallDate, cashPerBond, timeToMaturity,...
            cashIn, securityIdCashNotInBank);
        
    case 'bank'
        cashAvailable = cashIn;
        mtxTransactions = zeros(1, 5);
        %nUnitsToBuy = floor(cash_available/priceOfOneUnitPortfolio);
        %nUnitsToBuy = floor(cash_available/price_of_bond);
        mtxTransactions(1, 1) = datenum(desiredDate);
        mtxTransactions(1, 2) = securityIdCashInBank;
        mtxTransactions(1, 3) = 1;
        mtxTransactions(1, 4) = 0; %% How do we find this?
        mtxTransactions(1, 5) = cashAvailable;
    otherwise %%% Cash without any investment
        cashAvailable = cashIn;
        mtxTransactions = zeros(1, 5);
        mtxTransactions(1, 1) = datenum(desiredDate);
        mtxTransactions(1, 2) = securityIdCashNotInBank;
        mtxTransactions(1, 3) = 1;
        mtxTransactions(1, 4) = 0; %% How do we find this?
        mtxTransactions(1, 5) = cashAvailable;
end

%[allocation_out, ~] = ModifyAllocation1(transactions, allocation, portfolio_duration, last_call_date, desired_date);
[mtxAllocationOut] = modify_allocation(mtxTransactions, mtxAllocation, timeToMaturity, lastCallDate, desiredDate, constraints.benchmark_id);


