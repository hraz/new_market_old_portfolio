function [mtxTransactions] = risk_free_reinvestment(timeToMaturity, desiredDate,...
    cashIn, securityIdCashNotInBank, oldCash, riskFreeInfo)

%   RISK_FREE_REINVESTMENT investS in a risk free asset (government bond).
%
%    [mtxTransactions] = risk_free_reinvestment(timeToMaturity, desiredDate,...
%    cashIn, securityIdCashNotInBank, oldCash)
%   recieves cash and reinvests the cash by buying a risk-free asset
%	Input:
%       timeToMaturity - time to maturity of the portfolio calculated from current date.
%       desiredDate - a datenum variable of the current date.
%        cashIn - A matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of cash from each bond.
%       securityIdCashNotInBank - nsin of money which is set aside without
%       accumulating any interest.
%       dailyYtmInterestPerBond - double which is equal to the daily
%       interest rate.
%       oldCash - cash which we were previously holding .
%   Output:
%        mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%   Sample:
%			Some example of the function using.
%
%		See Also:
%           REINVESTMENT, PORTFOLIO_REINVESTMENT,   SPECIFIC_BOND_REINVESTMENT,
%           BENCHMARK_REINVESTMENT
%
% created by Idan Oren, 23/7/13
% Copyright 2013, Bond IT Ltd.
% Update by Hillel Raz, 28/07/13
% Added riskFreeInfo as the last input parameter 


%import dal.market.get.base_data.*; % Because of: get_nominal_yield_with_param

interestPeriod = timeToMaturity;
if interestPeriod >=1
    indexPeriod = interestPeriod + 11;
else
    indexPeriod = round(interestPeriod*12);
end
indexDate = find(riskFreeInfo.id.dates <= desiredDate, 1, 'last');
bonds = fts2mat(riskFreeInfo.id(indexDate));
securityID = bonds(indexPeriod);
cashAvailable = cashIn;
%x = find(allocation(:, 1)==security_id);
mtxTransactions = zeros(1, 5);
%         priceOfBond = fts2mat(riskFreeBond.price_dirty);
prices = fts2mat(riskFreeInfo.price_dirty(indexDate));
priceOfBond = prices(indexPeriod);
if (cashAvailable+oldCash)>priceOfBond % If there is enough cash, then bond is purchased
    nUnitsToBuy = floor((cashAvailable+oldCash)/priceOfBond);
    mtxTransactions(1, 1) = datenum(desiredDate);
    mtxTransactions(1, 2) = securityID;
    mtxTransactions(1, 3) = nUnitsToBuy;
    mtxTransactions(1, 4) = 0; %% How do we find this?
    mtxTransactions(1, 5) = nUnitsToBuy*priceOfBond;
    remainder = mod(cashAvailable, priceOfBond);
    mtxTransactions(2, 1) = datenum(desiredDate);
    mtxTransactions(2, 2) = securityIdCashNotInBank;
    mtxTransactions(2, 3) = 1;
    mtxTransactions(2, 4) = 0;
    mtxTransactions(2, 5) = -oldCash + remainder; % updating the amount of cash:
    % subtracting the amount which we previously held and keeping the remainder.
    %             if remainder ~= 0 % If there is remainder, we want to keep it for later use
    %                 mtxTransactions(2, 1) = datenum(desiredDate);
    %                 mtxTransactions(2, 2) = securityIdCashNotInBank;
    %                 mtxTransactions(2, 3) = 1;
    %                 mtxTransactions(2, 4) = 0;
    %                 mtxTransactions(2, 5) = remainder;
    %             end
else
    mtxTransactions(1, 1) = datenum(desiredDate);
    mtxTransactions(1, 2) = securityIdCashNotInBank;
    mtxTransactions(1, 3) = 1;
    mtxTransactions(1, 4) = 0;
    mtxTransactions(1, 5) = cashAvailable;
end

% interestPeriod = timeToMaturity;
% riskFreeBond = get_nominal_yield_with_param(interestPeriod, desiredDate, desiredDate);
% securityID = fts2mat(riskFreeBond.id);
% cashAvailable = cashIn;
% mtxTransactions = zeros(1, 5);
% priceOfBond = fts2mat(riskFreeBond.price_dirty);
% if (cashAvailable+oldCash)>priceOfBond % If there is enough cash, then bond is purchased
%     nUnitsToBuy = floor((cashAvailable+oldCash)/priceOfBond);
%     mtxTransactions(1, 1) = datenum(desiredDate);
%     mtxTransactions(1, 2) = securityID;
%     mtxTransactions(1, 3) = nUnitsToBuy;
%     mtxTransactions(1, 4) = 0; %% How do we find this?
%     mtxTransactions(1, 5) = nUnitsToBuy*priceOfBond;
%     %% subtracting the amount which we previously held and keeping the remainder.
%     % If there is remainder, we want to keep it for later use
%     remainder = mod(cashAvailable, priceOfBond);
%     mtxTransactions(2, 1) = datenum(desiredDate);
%     mtxTransactions(2, 2) = securityIdCashNotInBank;
%     mtxTransactions(2, 3) = 1;
%     mtxTransactions(2, 4) = 0;
%     mtxTransactions(2, 5) = -oldCash + remainder; % updating the amount of cash:
% else
%     mtxTransactions(1, 1) = datenum(desiredDate);
%     mtxTransactions(1, 2) = securityIdCashNotInBank;
%     mtxTransactions(1, 3) = 1;
%     mtxTransactions(1, 4) = 0;
%     mtxTransactions(1, 5) = cashAvailable;
% end