function cashFlow = cash_flow_from_coupons_according_to_ex_dates(ftsCashFlow, ftsPayExDatesMapping, desiredDate,...
    mtxHistoricalAllocationIn, reinvestmentStrategy, mtxAllocation)



%CASH_FLOW_FROM_COUPONS_ACCORDING_TO_EX_DATES computes the cash we got from
%coupons at a given date.
%
%   cashFlow = cash_flow_from_coupons_according_to_ex_dates(ftsCashFlow, ftsPayExDatesMapping,...
%                                                 desiredDate, mtxHistoricalAllocation, reinvestmentStrategy, mtxAllocation)
%   receives the fts of coupons, the fts of mapping between pay dates and ex dates,
%   the matrix of historical allocation and several additional input
%   arguments. It computes the cash flow at the curretn date according to
%   the portfolio as it was at the corresponding ex date.
%	Input:
%       ftsCashFlow � fts of dimensions (nDates)*(nSecurities).
%                                                                          The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | cash_flow1       | cash_flow2     | cash_flow3       |
%       ftsPayExDatesMapping � fts of dimensions (nDates)*(nSecurities).
%                                                                          The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | ex_date1            | ex_date2          | ex_date3           |
%       desiredDate - a datenum variable of the current date.
%       mtxHistoricalAllocation � matrix of dimensions (nDates*securities at each date)*(4).
%                                                                          The format is:
%                                                                          date | security_id's | units | price |
%                                                                           --------|--------------------------|------------|-----------|
%       reinvestmentStrategy - a string specifying the reinvestment
%                                                            strategy. For now this is only 'risk-free'.
%       mtxAllocation �  matrix of dimensions (nSecurities)*(3).
%                                                                            The format is:
%                                                                            security_id's | units | price |
%                                                                            -------------------------|------------|-----------|
%   Output:
%       cashFlow - For most strategies, this is a scalar. The exception: if the strategy is to invest in a
%                              specific bond ('Specific-Bond-Reinvestment')) then cashIn is a matrix
%                              of size (Nnsin)X(2). The first column is a list of
%                              securities and the second column is the amount of cash from each bond.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%		UPDATE_COUPONS, UPDATE_FTS_PAY_EX_DATES_MAPPING, UPDATE_HISTORICAL_ALLOCATION
%
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Hillel Raz, 25/04/2013, changed procedure so taking nsins
% according to ex dates so that for each ex date only the relevant nsins
% are chosen and only their units updated.




import dal.market.get.cash_flow.*;
import simulations.*;
import utility.*;

%% Check input arguments type:
flagType(1) = isa(ftsCashFlow, 'fints');
flagType(2) = isa(ftsPayExDatesMapping, 'fints');
flagType(3) = isa(desiredDate, 'numeric');
flagType(4) = isa(mtxHistoricalAllocationIn, 'double');
flagType(5) = isa(reinvestmentStrategy, 'char');
if sum(flagType) ~= length(flagType)
    error('cash_flow_from_coupons_according_to_ex_dates:wrongInput', ...
        'One or more input arguments is not of the right type');
end
%% Check input arguments sizes:
if ~isempty(mtxHistoricalAllocationIn)
    nColumns1 = size(mtxHistoricalAllocationIn, 2);
else
    nColumns1 = 4;
end
sizes = [nColumns1];
flagSizes = sizes == [4];
if sum(flagSizes) ~= length(flagSizes)
    error('cash_flow_from_coupons_according_to_ex_dates:wrongInput', ...
        'One or more input arguments is not of the right size');
end

nBondsInAllocation = size(mtxAllocation, 1);
payDates =  ftsCashFlow.dates;
couponFields = fieldnames(ftsCashFlow);
if (strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment'))...
        || (strcmp(reinvestmentStrategy, 'benchmark'))...
        || (strcmp(reinvestmentStrategy, 'risk-free'))...
        || (strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment'))
    nsinList = setdiff(mtxAllocation(:, 1), 0);
else
    nsinList = strid2numid(couponFields(4:end));    
end
nanInNsinList = isnan(nsinList);
nsinList(nanInNsinList) = [];
nNsins = length(nsinList);
units = zeros(nNsins, 1);
if (strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment'))...
         || (strcmp(reinvestmentStrategy, 'benchmark'))...
         || (strcmp(reinvestmentStrategy, 'risk-free'))...
         || (strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment'))
     if isrow(nsinList)
        nsinList = nsinList';
    end
    cashFlow = zeros(length(nsinList), 2); %%% For Debugging 
    cashFlow(:, 1) = nsinList;
else
    cashFlow = 0;
end

couponIndices = find(payDates == desiredDate); %%% Should get only one index
mtxPayExDates = fts2mat(ftsPayExDatesMapping);
payExMappingAtDesiredDate = mtxPayExDates(couponIndices, :);  %%% Should get only one line
indicesOfSecuritiesWithoutCoupons = isnan(payExMappingAtDesiredDate);
securitiesWithCoupons = ~indicesOfSecuritiesWithoutCoupons;
payExMappingAtDesiredDate(indicesOfSecuritiesWithoutCoupons) = [];
payExMappingDatesUnique = unique(payExMappingAtDesiredDate); %Get only unique list of dates
nRelevantExDates = length(payExMappingDatesUnique);
relevantNsins = nsinList(securitiesWithCoupons);
nRelevantNsins = length(relevantNsins);
if nRelevantExDates~=0
    
    for iRelevantExDate = 1:nRelevantExDates
        loopDate = payExMappingDatesUnique(iRelevantExDate); %Here take the unique dates only
        nsinRelevantDate = find(payExMappingAtDesiredDate == loopDate); %Take only nsins with relevant paydate
        for iRelevantNsins = 1:length(nsinRelevantDate)
            loopNsin = relevantNsins(nsinRelevantDate(iRelevantNsins));
            if loopDate == desiredDate %If it's today, no need to look at historical allocation
                relevantLineInHistoricalAllocation = find(mtxAllocation(:, 1) == loopNsin);
            else
                relevantLineInHistoricalAllocation = intersect(find((mtxHistoricalAllocationIn(:, 1) <= loopDate), 2*nBondsInAllocation, 'last'), find(mtxHistoricalAllocationIn(:, 2) == loopNsin));
                if ~isempty(relevantLineInHistoricalAllocation)
                    relevantLineInHistoricalAllocation = relevantLineInHistoricalAllocation(end); %In case bonds were removed and the allocation was changed
                end
            end
            % relevantLineInHistoricalAllocation = intersect(find((mtxHistoricalAllocationIn(:, 1) == loopDate)), find(mtxHistoricalAllocationIn(:, 2) == loopNsin));
            indexOfNsin = find(nsinList == loopNsin);
            if ~isempty(relevantLineInHistoricalAllocation)
                if loopDate == desiredDate %If it's today, no need to look at historical allocation
                    units(indexOfNsin) = mtxAllocation(relevantLineInHistoricalAllocation, 2);
                else
                    units(indexOfNsin) = mtxHistoricalAllocationIn(relevantLineInHistoricalAllocation, 3);
                end
            else
                warning('No ex date matches this pay date: %d, or at ex date no units of bond %d were held',...
                    [desiredDate, loopNsin]);
            end
        end
    end
    
    mtxCoupon = fts2mat(ftsCashFlow);
    [mCouponsRows, ~] = size(mtxCoupon);
    mtxUnits = ones(mCouponsRows, 1)*units';
    mtxCoupon = mtxCoupon.*mtxUnits;
    x = isnan(mtxCoupon); % Changing NaN valus in the coupon matrix into zeros for convenience.
    mtxCoupon(x) = 0;
    
    if (strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment')) || ...
            (strcmp(reinvestmentStrategy, 'benchmark')) || ...
             (strcmp(reinvestmentStrategy, 'risk-free')) ||...
             (strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment'))%%% CHECKME %%%
        % If our strategy is to reinvest in the specific bond from which we got the
        % money, then we must keep track of the origin of each amount of money. In
        % this case:
        cashFlow = zeros(length(nsinList), 2);
        cashFlow(:, 1) = nsinList;
        cashFlow(:, 2) = mtxCoupon(couponIndices, :);
    else
        % If we put the money in the bank, buy benchmark ETF, reinvest in the
        % entire portfolio or in a risk free asset, then we can sum all the cash
        % together for reinvestment. Then:
        
        totalCashFlowByDates = zeros(mCouponsRows, 2);
        totalCashFlowByDates(:, 1) = payDates;
        totalCashFlowByDates(:, 2) = sum(mtxCoupon, 2);
        % cash_flow(1) = 99;
        % cash_flow(2) = total_cash_flow_by_dates(coupons_indices, 2);
        cashFlow = totalCashFlowByDates(couponIndices, 2);
    end
    
    
    
    %      mtxRedemption = fts2mat(ftsRedemption);
    %      redemptionDate = ftsRedemption.dates;
    %      indexOfDesiredDate = find(redemptionDate == desiredDate);
    %      relevantLineInRedemption = mtxRedemption(indexOfDesiredDate, :);
    %      relevantLineInRedemption = relevantLineInRedemption(multiRedemptionFlag);
    %      mtxTransactions = zeros(length(x), 5);
    %      nsinsWithMultiRedemption = nsinList(multiRedemptionFlag);
    %      nNsinsWithMultiRedemption = length(nsinsWithMultiRedemption);
    %      mRowsOfTransactions = 1;
    %      unitsOfMultiRedemptionBonds = units(multiRedemptionFlag);
    %      for iNsinsWithMultiRedemption = 1:nNsinsWithMultiRedemption
    %          securityID = nsinsWithMultiRedemption(iNsinsWithMultiRedemption);
    %          percentOfParPaid = relevantLineInRedemption(iNsinsWithMultiRedemption);
    %          nUnitsToReduce = ceil(unitsOfMultiRedemptionBonds*percentOfParPaid);
    %          x = find(mtxAllocation(:, 1) == securityID);
    %          priceOfBond = mtxAllocation(x, 3);
    %          mtxTransactions(mRowsOfTransactions, 1) = desiredDate;
    %          mtxTransactions(mRowsOfTransactions, 2) = securityID;
    %          mtxTransactions(mRowsOfTransactions, 3) = nUnitsToReduce;
    %          mtxTransactions(mRowsOfTransactions, 4) = 0;
    %          mtxTransactions(mRowsOfTransactions, 5) = nUnitsToReduce*priceOfBond;
    %          mRowsOfTransactions = mRowsOfTransactions+1;
    %      end
end


