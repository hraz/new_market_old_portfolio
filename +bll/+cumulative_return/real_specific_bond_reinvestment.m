function [mtxTransactions, cashPerBondOut] = real_specific_bond_reinvestment(desiredDate, cashIn,...
    oldCash, bondWanted, bondPrice, securityIdCashNotInBank, bondsWithCash)

%   REALISTIC_SPECIFIC_BOND_REINVESTMENT reinvests money with interest that is equal to the ytm of the bond
%   from which the cah came from (coupon or redemption).
%
%   [mtxTransactions, cashPerBondOut] = real_specific_bond_reinvestment(desiredDate, cashIn,...
%    oldCash, bondWanted, bondPrice, securityIdCashNotInBank, cashPerBond)


%   recieves cash which came from a specific bond. It then reinvests the
%   cash in a risk-free interest which is equal to that bond's ytm.
%	Input:
%       desiredDate - a datenum variable of the current date.
%        cashIn - A matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of cash from each bond.
%       oldCash - cash which we were previously holding .
%       bondWanted - Double (possibly a vector) - nsin of bond with closest YTM to ytmSought
%       bondPrice - Double (possibly a vector) - dirty price of bond with closest YTM to ytmSought
%       securityIdCashNotInBank - nsin of money which is set aside without
%       cashPerBond - a matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of  PREVIOUS cash from each bond.
%       bondsWithCash - list of nsins which paid cash.
%   Output:
%        mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%        cashPerBondOut - a matrix similar to cashPerBond but updated due to
%        interest and to new cash flow.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%           REINVESTMENT, PORTFOLIO_REINVESTMENT, RISK_FREE_REINVESTMENT,
%           BENCHMARK_REINVESTMENT
%
% created by Idan Oren, 23/7/13
% Copyright 2013, Bond IT Ltd.

% NEEDS FIXING - cashAvailable not the same size as bondsWanted etc. ***********


import bll.cumulative_return.*;

% previousCashAmount = sum(cashPerBond(:, 2));
% nDaysOfInterest = desiredDate - lastCallDate;
cashPerBondOut = cashIn; %% cashPerBond?? ****************** Check this
cashAvailable = cashIn;
nRows = size(cashAvailable, 1);
mtxTransactions = zeros(2, 5);
index = 2;
if desiredDate == 734684
    734684;
end
for iRow = 1:nRows
    if (cashAvailable(iRow)+oldCash)>bondPrice(iRow) % If there is enough cash, then bond is purchased
        nUnitsToBuy = floor((cashAvailable(iRow)+oldCash)/bondPrice(iRow));
        mtxTransactions(index, 1) = datenum(desiredDate);
        mtxTransactions(index, 2) = bondWanted(iRow);
        mtxTransactions(index, 3) = nUnitsToBuy;
        mtxTransactions(index, 4) = 0; %% How do we find this?
        mtxTransactions(index, 5) = nUnitsToBuy*bondPrice(iRow);
        index = index + 1;
        % cashPerBondOut = modify_cash_per_bond(cashPerBondOut, bondsWithCash(iRow));
        %% subtracting the amount which we previously held and keeping the remainder.
        % If there is remainder, we want to keep it for later use
        remainder = mod(cashAvailable(iRow), bondPrice(iRow));
        mtxTransactions(1, 1) = datenum(desiredDate);
        mtxTransactions(1, 2) = securityIdCashNotInBank;
        mtxTransactions(1, 3) = 1;
        mtxTransactions(1, 4) = 0;
        mtxTransactions(1, 5) = mtxTransactions(1, 5) + remainder; % updating the amount of cash:
    else
        mtxTransactions(1, 1) = datenum(desiredDate);
        mtxTransactions(1, 2) = securityIdCashNotInBank;
        mtxTransactions(1, 3) = 1;
        mtxTransactions(1, 4) = 0;
        mtxTransactions(1, 5) = mtxTransactions(1, 5) + cashAvailable(iRow);
    end
end
 mtxTransactions(1, 5) = mtxTransactions(1, 5) - oldCash;