function [ftsCashFlowOut, ftsRedemptionOut, ftsSumOfRedemptionOut] = update_redemption_and_sum_of_redemption_and_cash_flow(ftsRedemption, ftsSumOfRedemption, ftsCashFlow, newSecurityID, firstDate, lastDate)
%UPDATE_REDEMPTION_AND_CASH_FLOW updates tWO fts: one which holds the
%information of cash flow for each security (nsin) and the other holds the
%redemption sequence for each bond.
%			
%   [ftsCashFlowOut ftsRedemptionOut] = update_redemption_and_cash_flow(ftsRedemption,... 
%                                                                                     ftsCashFlow, newSecurityID, firstDate, lastDate)
%   receives the existing aformentioned fts: cash flow and redemption of existing securities. It
%   also recieves new security id's  and updates these two fts to
%   hold the information for all the securities.
%	Input:
%       ftsRedemption � fts of dimensions (nDates)*(nSecurities) which
%       holds redemptions of bonds given in percents. The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | redemption1    | redemption2  | redemption3    |
%       ftsSumOfRedemption - fts of the sum of redemptions of a bond
%       starting today until the bond dies. The format is similar to
%       ftsredumption.
%       ftsCashFlow � fts of dimensions (nDates)*(nSecurities)  which
%       holds cash flow of bonds given in agorot. The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | cash_flow1       | cash_flow2     | cash_flow3       |
%       newSecurityID �  vector of new security id's (nsins). If this is
%                                             non-empty, the variable ftsCoupon should be updated.
%                                             Otherwise the output argument ftsCouponOut will be
%                                             identiacl to ftsCoupon.
%
%       firstDate - a datenum variable of the date the portfolio was created.   
%       lastDate - a datenum variable of the date the portfolio ends.   
%   Output:
%       ftsCashFlowOut - fts similar to ftsCashFlow but
%       updated due to newSecurityID.
%       ftsRedemptionOut � fts similar to ftsPayExDatesMappingIn but
%       updated due to newSecurityID.
%       ftsSumOfRedemptionOut - fts of the sum of redemptions of a bond starting today until the bond dies.
%   Sample:
%			Some example of the function using.
% 	
%		See Also:
%		UPDATE_PRICES, UPDATE_HISTORICAL_ALLOCATION, UPDATE_FTS_PAY_EX_DATES_MAPPING
%	
% Idan Oren, 13/3/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

import simulations.*;
import dal.market.get.cash_flow.*; % Because of: get_cash_flow
import bll.cumulative_return.*;


%% Check input arguments type:
flagType(1) = isa(ftsRedemption, 'fints');
flagType(2) = isa(ftsCashFlow, 'fints');
flagType(3) = isa(newSecurityID, 'double');
flagType(4) = isa(firstDate, 'numeric');
flagType(5) = isa(lastDate, 'numeric');
if sum(flagType) ~= length(flagType)
     error('update_coupons:wrongInput', ...
                'One or more input arguments is not of the right type');
end

%[ftsCashflowNew ftsRedemptionNew] = build_cash_flow(firstDate, lastDate, newSecurityID, ftsCashFlow, ftsRedemption);
 [ftsCashFlowOut, ftsRedemptionOut, ftsSumOfRedemptionOut] = build_cash_flow(firstDate, lastDate, newSecurityID, ftsCashFlow, ftsRedemption, ftsSumOfRedemption);
ftsCashFlowOut.desc = 'cash_flow';
ftsRedemptionOut.desc = 'redemption';
ftsRedemptionOut.desc = 'sum_of_redemption';






