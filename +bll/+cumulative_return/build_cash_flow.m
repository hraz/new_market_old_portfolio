function [ftsCashflow ftsRedemption ftsSumOfRedemption] = build_cash_flow(dateBegPort, portfolioMaturityDate, listNsin, varargin)
%BUILD_CASH_FLOW function retrieves cash flow data and calculates full cash
%flow
%
%   [ftsCashflow ftsRedemption] = build_cash_flow(dateBegPort,
%   portfolioMaturityDate, listNsin, varargin) - function calculates the
%   cash flow of a given portfolio, and returns the cashflow as well as the
%   redemption values.  In the case a bond is added to a given portfolio,
%   function returns the info for the initial bonds along with the added
%   one.
%
%   Input:  dateBegPort - DATE_NUM - date at which portfolio is generated
%
%           portfolioMaturityDate - DATE_NUM - date at which portfolio is
%               supposed to end
%
%           listNsin - 1 X nASSETS - list of nsins
%
%           varargin{1} - ftsCashflow - mDAYS X nASSETS array of cashflow -
%               sum of coupons and redemption (decimal)
%
%           varargin{2} - ftsRedemption - mDAYS X nASSETS array of
%               redemption values (decimal)
%
%           varargin{3} - ftsSumOfRedemption - mDAYS X nASSETS array of sum
%               of redemptions from given point till end of bond's life
%
%   Output: ftsCashflow - mDAYS X nASSETS array of cashflow -
%               sum of coupons and redemption (decimal)
%
%           ftsRedemption - mDAYS X nASSETS array of
%               redemption values (decimal)
%
%           ftsSumOfRedemption - mDAYS X nASSETS array of sum
%               of redemptions from given point till end of bond's life
%
%   See also: calc_total_value, calc_total_value_envelope
%
% Written by Hillel Raz, Jan 2013
% Copyright, BondIT Ltd
% Updated by Idan on 20.3.2013:
%       If ftsPayments is empty, and if nargin is 6, instead of setting the fts variables to be
%       empty, I set them to be a column of NaN's.

import dal.market.get.cash_flow.*;
import utility.*;
import bll.data_manipulations.*;

if ~isa(listNsin, 'double') ||...
        ~isa(dateBegPort, 'double') || ~isa(portfolioMaturityDate, 'double')...
        || ~isa(dateBegPort, 'double')
    error('get_cash_flow:wrongInput', ...
        'One or more input arguments is not of the right type');
end
if nargin == 6
    ftsCashflow = varargin{1};
    ftsRedemption = varargin{2};
    ftsSumOfRedemption = varargin{3};
end

[ ftsPayments ] = get_amortization(dateBegPort, portfolioMaturityDate, listNsin);
if ~isempty(ftsPayments)
    % Not all nsins have all their payments
    if ~isfield(ftsPayments, 'redemption') %No redemptions returned, create fts of NaN's
        fieldNames = fieldnames(ftsPayments.adj_factor);
        nsins = fieldNames(4:end);
        ftsPayments.redemption = fints(ftsPayments.adj_factor.dates, NaN(length(ftsPayments.adj_factor.dates), size(ftsPayments.adj_factor, 2)), nsins);
        ftsPayments.total_redemption_left = ftsPayments.redemption;
    elseif size(ftsPayments.adj_factor, 2) ~= size(ftsPayments.redemption, 2)
        [ftsPayments.redemption] = add_nans_to_fts_due_to_num_nsins_not_equal(ftsPayments.adj_factor, ftsPayments.redemption);
        [ftsPayments.total_redemption_left] = add_nans_to_fts_due_to_num_nsins_not_equal(ftsPayments.adj_factor, ftsPayments.total_redemption_left);
    end
    if ~isfield(ftsPayments, 'coupon') %No coupons returned, create fts of NaN's
        fieldNames = fieldnames(ftsPayments.adj_factor);
        nsins = fieldNames(4:end);
        ftsPayments.coupon = fints(ftsPayments.adj_factor.dates, NaN(length(ftsPayments.adj_factor.dates), size(ftsPayments.adj_factor, 2)), nsins);
        
    elseif size(ftsPayments.adj_factor, 2) ~= size(ftsPayments.coupon, 2)
        [ftsPayments.coupon] = add_nans_to_fts_due_to_num_nsins_not_equal(ftsPayments.adj_factor, ftsPayments.coupon);
    end
    
    % Not all dates show up in coupons and redemptions
    if size(ftsPayments.adj_factor, 1) ~= size(ftsPayments.redemption, 1)
        [ftsPayments.redemption] = add_nans_to_fts_due_to_num_dates_not_equal(ftsPayments.adj_factor, ftsPayments.redemption);
        [ftsPayments.total_redemption_left] = add_nans_to_fts_due_to_num_dates_not_equal(ftsPayments.adj_factor, ftsPayments.total_redemption_left);
    end
    if size(ftsPayments.adj_factor, 1) ~= size(ftsPayments.coupon, 1)
        [ftsPayments.coupon] = add_nans_to_fts_due_to_num_dates_not_equal(ftsPayments.adj_factor, ftsPayments.coupon);
        
    end
    
    % Calculate full payments
    
    linkageFactor = fts2mat(ftsPayments.adj_factor);
    coupons = fts2mat(ftsPayments.coupon);
    nansCoupons = isnan(coupons);
    couponPayments = coupons.*linkageFactor;
    redemptions = fts2mat(ftsPayments.redemption);
    sumsRedemptions = fts2mat(ftsPayments.total_redemption_left);
    redemptionPayments = redemptions./sumsRedemptions.*linkageFactor;
    nansRedemptions = isnan(redemptions);
    couponPayments(nansCoupons)=0; redemptionPayments(nansRedemptions)=0; %Zero out the NaN's.
    
    fullPayments =100*(couponPayments+ redemptionPayments);
    
    datesCoupons = ftsPayments.coupon.dates;
    datesRedemptions = ftsPayments.redemption.dates;
    ftsSumOfRedemptionInitial = ftsPayments.total_redemption_left;
elseif nargin == 6
    ftsSumOfRedemptionInitial = fints(ftsSumOfRedemption.dates, NaN*ones(size(ftsSumOfRedemption, 1), length(listNsin)), numid2strid(listNsin));
    datesRedemptions = [];
    datesCoupons = [];
    fullPayments = [];
    redemptions = [];
    coupons = [];
    linkageFactor = [];
else
    ftsSumOfRedemptionInitial = fints();
    datesRedemptions = [];
    datesCoupons = [];
    fullPayments = [];
    redemptions = [];
    coupons = [];
    linkageFactor = [];
end
%% Arrange in FTS
if (nargin == 3)
    if isempty(datesCoupons) || isempty(fullPayments)
        ftsCashflow = fints();
        ftsRedemption = fints();
        ftsSumOfRedemption =fints();
    else
        ftsCashflow = fints(datesCoupons, fullPayments, numid2strid(listNsin));
        ftsRedemption = ftsPayments.redemption;
        ftsSumOfRedemption = ftsSumOfRedemptionInitial;
    end
elseif nargin == 6 % Case when we are adding bonds to the existing portfolio
    if isempty(datesCoupons) || isempty(fullPayments)
        numNsins = length(listNsin);
        ftsRiskFreePayments = fints(); ftsRiskFreeRedemption = fints();
        for num = 1:numNsins
            ftsRiskFreePaymentsUpdated = fints(ftsCashflow.dates, NaN*ones(size(ftsCashflow, 1), 1), numid2strid(listNsin(num)));
            ftsRiskFreeRedemptionUpdated = fints(ftsRedemption.dates, NaN*ones(size(ftsRedemption, 1), 1), numid2strid(listNsin(num)));
            ftsRiskFreePayments =merge(ftsRiskFreePaymentsUpdated, ftsRiskFreePayments);
            ftsRiskFreeRedemption = merge(ftsRiskFreeRedemptionUpdated, ftsRiskFreeRedemption);
        end
        ftsRiskFreeRedemption.desc = 'redemption';
        if any(strcmp(fieldnames(ftsRiskFreePayments), 'series1'))
            ftsRiskFreePayments = rmfield(ftsRiskFreePayments, 'series1');
        end
        if any(strcmp(fieldnames(ftsRiskFreeRedemption), 'series1'))
            ftsRiskFreeRedemption = rmfield(ftsRiskFreeRedemption, 'series1');
        end
        
    else
        ftsRiskFreePayments = fints(datesCoupons, fullPayments, numid2strid(listNsin));
        ftsRiskFreeRedemption = fints(datesRedemptions, redemptions, numid2strid(listNsin));
    end
    ftsCashflow = merge(ftsCashflow, ftsRiskFreePayments);
    ftsRedemption = merge(ftsRedemption, ftsRiskFreeRedemption);
    ftsSumOfRedemption = merge(ftsSumOfRedemption, ftsSumOfRedemptionInitial);
end
