function [vecFinalDatesPerBond] = get_first_date_of_nan_prices(ftsPrice, portfolio_maturity_date)
%GET_FIRST_DATE_OF_NAN_PRICES gets first date in which there are Nan's in the price  
% 
%   [vecFinalDatesPerBond] = get_first_date_of_nan_prices(ftsPrice, portfolio_maturity_date)
%   receives the fints of price dirty and finds for each bond the first
%   time it no longer has a price. If bond is alive during entire period
%   then function returns arbitrary date beyond portfolio maturity date.
%
%     input:  ftsPrice - fints of dirty price for the NASSETS given for
%     entire period of the life of the portfolio
%     
%             portfolio_maturity_date - VALUE - representing date of the
%             maturity of the portfolio
%
%     
%     output: vecFinalDatesPerBond - NASSETS X 1 vector of dates for each
%     asset of when the first Nan is in the price fts.
%     
%     
%Hillel Raz, January 2013     
%Copyright BondIT Ltd     

matPrice = fts2mat(ftsPrice.price_dirty);

matLocationNanInPriceMat = isnan(matPrice); % Mat with the locations of the Nan's

vecDates = ftsPrice.price_dirty.dates;
vecFinalDatesPerBond = zeros(1, size(matPrice, 2));
nMatPrice = size(matPrice, 2);

for nAssets = 1:nMatPrice
    
vecAssetExistenceDates = vecDates(matLocationNanInPriceMat(:, nAssets));
if ~isempty(vecAssetExistenceDates)
vecFinalDatesPerBond(nAssets) = vecAssetExistenceDates(1); % Get first date at which there is a NaN
else
    vecFinalDatesPerBond(nAssets) = portfolio_maturity_date + 365; % Arbitrary value, stating that bond has not died
end

end

    
    
