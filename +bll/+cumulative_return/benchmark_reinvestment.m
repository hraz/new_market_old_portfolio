function [mtxTransactions, benchmarkFTS, cashPerBondOut] = benchmark_reinvestment(constraints, benchmarkFTS,...
    desiredDate, lastCallDate, cashPerBond, timeToMaturity,...
    cashIn, securityIdCashNotInBank)

%   BENCHMARK_REINVESTMENT investS in a risk free asset (government bond).
%
%   [mtxTransactions, benchmarkFTS, cashPerBondOut] = benchmark_reinvestment(constraints, benchmarkFTS,...
%    desiredDate, lastCallDate, cashPerBond, timeToMaturity,...
%    cashIn, securityIdCashNotInBank)
%   recieves cash and reinvests the
%   cash in a risk-free interest which is equal to the daily benchmark yield.
%	Input:
%       constraints - data structure: require fields: benchmark_id - nsin of benchmark if needed. 
%       benchmarkFTS - an fts holding the daily yield of the benchmark.
%       desiredDate - a datenum variable of the current date.
%       lastCallDate - a datenum variable of the last date the function was called upon.
%       cashPerBond - a matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of  PREVIOUS cash from each bond.
%       timeToMaturity - time to maturity of the portfolio calculated from current date.
%        cashIn - A matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of cash from each bond.
%       securityIdCashNotInBank - nsin of money which is set aside without
%       accumulating any interest. 
%   Output:
%        mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%       benchmarkFTS - an fts holding the daily yield of the benchmark.
%        CashPerBondOut - a matrix similar to cashPerBond but updated due to
%        interest and to new cash flow.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%           REINVESTMENT, PORTFOLIO_REINVESTMENT,   SPECIFIC_BOND_REINVESTMENT,
%           RISK_FREE_REINVESTMENT
%
% created by Idan Oren, 23/7/13
% Copyright 2013, Bond IT Ltd.
% Updated by Hillel Raz, 28/07/13 
% Added import dal.market.get.dynamic.*; 

import dal.market.get.dynamic.*;

if isempty(constraints.benchmark_id)
    constraints.benchmark_id = 601;
end
%% Two options here - can pull data for entire life of portfolio, so only one pull from DB, and then must save the info somehow - i.e. pass it back n forth
% or pull it every day, greatly slow down simulation.
if (isempty(benchmarkFTS))
    benchmarkFTS = get_multi_dyn_between_dates(constraints.benchmark_id, {'value_yld'}, 'index', desiredDate, desiredDate + 365*(timeToMaturity + 1));
end
numDays = find(benchmarkFTS.value_yld.dates == desiredDate);
%Single day
% benchmarkFTS = get_bond_dyn_single_date(constraints.benchmark_id, 'value_yld', datestr(desiredDate));
previousCashAmount = sum(cashPerBond(:, 2));
nDaysOfInterest = desiredDate - lastCallDate;
if (~isempty(benchmarkFTS))
    dataBenchmark = fts2mat(benchmarkFTS.value_yld);
else
    dataBenchmark = [];
end
%dataBenchmark = (1 + dataBenchmark).^(1/365) - 1;

%in case of all data pulled, initial date pulled is also saved and
%there is an index for number of days. if not then numDays is = 1,
%and is unnecessary
if ~isempty(numDays)
    cashAvailable = sum(((1 + dataBenchmark(numDays))^nDaysOfInterest).*cashPerBond(:, 2)) + sum(cashIn(:, 2));
else % In case that date is not a business date, no interest.
    cashAvailable = sum(cashPerBond(:, 2)) + sum(cashIn(:, 2));
end
mtxTransactions = zeros(1, 5);
mtxTransactions(1, 1) = datenum(desiredDate);
mtxTransactions(1, 2) = securityIdCashNotInBank;
mtxTransactions(1, 3) =1;
mtxTransactions(1, 4) = 0; %% How do we find this?
mtxTransactions(1, 5) = cashAvailable - previousCashAmount;
%         cashPerBondOut = cashPerBond;
if ~isempty(numDays)
    cashPerBondOut(:, 2) = cashPerBond(:, 2).*(1 + dataBenchmark(numDays)) + cashIn(:, 2);
else
    cashPerBondOut(:, 2) = cashPerBond(:, 2) + cashIn(:, 2);
end