clc; clear;
dbstop if error;

import bll.cumulative_return.*;

listNsin=[ 
     1106913
     1111327
     1940121
     1940188
     1980218
     2260099
     2260198
     2810216
     4110102
     4250155
     7150204
     7410053
     7410178
     7460207];
 dateBegPort = 732859;
 portfolioMaturityDate = dateBegPort + 2*365;
 
 [ftsCashflow ftsRedemption ftsSumOfRedemption ] = get_cash_flow(dateBegPort, portfolioMaturityDate, listNsin);
 
 listNsin = [1105659
     2810216
     4110102
     4250155]
 
  [ftsCashflow ftsRedemption ftsSumOfRedemption ] = get_cash_flow(dateBegPort, portfolioMaturityDate, listNsin, ftsCashflow,ftsRedemption, ftsSumOfRedemption)
%   
%   ftsCashflow = 
%  
%     desc:   || 
%     freq:  Unknown (0)
% 
%     'dates:  (23)'    'id1105659:  (23)'    'id1106913:  (23)'    'id1111327:  (23)'
%     '07-Feb-2008'     [             NaN]    [          0.0640]    [             NaN]
%     '12-Jun-2008'     [          0.0436]    [             NaN]    [             NaN]
%     '21-Dec-2008'     [             NaN]    [             NaN]    [          0.0337]
%     '08-Feb-2009'     [             NaN]    [          0.0640]    [             NaN]
%     '12-Jun-2009'     [          0.0484]    [             NaN]    [             NaN]
%     '21-Jun-2009'     [             NaN]    [             NaN]    [          0.0380]
%     '20-Dec-2009'     [             NaN]    [             NaN]    [          0.0380]
%     '07-Feb-2010'     [             NaN]    [          0.3140]    [             NaN]
%     '13-Jun-2010'     [          0.3544]    [             NaN]    [             NaN]
%     '20-Jun-2010'     [             NaN]    [             NaN]    [          0.0380]
%     '20-Dec-2010'     [             NaN]    [             NaN]    [          0.0380]
%     '07-Feb-2011'     [             NaN]    [          0.3140]    [             NaN]
%     '12-Jun-2011'     [          0.3609]    [             NaN]    [             NaN]
%     '20-Jun-2011'     [             NaN]    [             NaN]    [          0.2380]
%     '20-Dec-2011'     [             NaN]    [             NaN]    [          0.2380]
%     '07-Feb-2012'     [             NaN]    [          0.3140]    [             NaN]
%     '12-Jun-2012'     [          0.3744]    [             NaN]    [             NaN]
%     '20-Jun-2012'     [             NaN]    [             NaN]    [          0.0380]
%     '20-Dec-2012'     [             NaN]    [             NaN]    [          0.0380]
%     '07-Feb-2013'     [             NaN]    [          0.3140]    [             NaN]
%     '20-Jun-2013'     [             NaN]    [             NaN]    [          0.2380]
%     '20-Dec-2013'     [             NaN]    [             NaN]    [          0.2380]
%     '20-Jun-2014'     [             NaN]    [             NaN]    [          0.2380]
%     
%      
% ftsRedemption = 
%  
%     desc:  redemption || 
%     freq:  Unknown (0)
% 
%     'dates:  (23)'    'id1105659:  (23)'    'id1106913:  (23)'    'id1111327:  (23)'
%     '07-Feb-2008'     [             NaN]    [               0]    [             NaN]
%     '12-Jun-2008'     [               0]    [             NaN]    [             NaN]
%     '21-Dec-2008'     [             NaN]    [             NaN]    [               0]
%     '08-Feb-2009'     [             NaN]    [               0]    [             NaN]
%     '12-Jun-2009'     [               0]    [             NaN]    [             NaN]
%     '21-Jun-2009'     [             NaN]    [             NaN]    [               0]
%     '20-Dec-2009'     [             NaN]    [             NaN]    [               0]
%     '07-Feb-2010'     [             NaN]    [          0.2500]    [             NaN]
%     '13-Jun-2010'     [          0.3333]    [             NaN]    [             NaN]
%     '20-Jun-2010'     [             NaN]    [             NaN]    [               0]
%     '20-Dec-2010'     [             NaN]    [             NaN]    [               0]
%     '07-Feb-2011'     [             NaN]    [          0.2500]    [             NaN]
%     '12-Jun-2011'     [          0.3333]    [             NaN]    [             NaN]
%     '20-Jun-2011'     [             NaN]    [             NaN]    [          0.2000]
%     '20-Dec-2011'     [             NaN]    [             NaN]    [          0.2000]
%     '07-Feb-2012'     [             NaN]    [          0.2500]    [             NaN]
%     '12-Jun-2012'     [          0.3333]    [             NaN]    [             NaN]
%     '20-Jun-2012'     [             NaN]    [             NaN]    [               0]
%     '20-Dec-2012'     [             NaN]    [             NaN]    [               0]
%     '07-Feb-2013'     [             NaN]    [          0.2500]    [             NaN]
%     '20-Jun-2013'     [             NaN]    [             NaN]    [          0.2000]
%     '20-Dec-2013'     [             NaN]    [             NaN]    [          0.2000]
%     '20-Jun-2014'     [             NaN]    [             NaN]    [          0.2000]
%     
%     
%     ftsSumOfRedemption = 
%  
%     desc:  total_redemption_left || total_redemption_left
%     freq:  Daily (1)
% 
%     'dates:  (23)'    'id1105659:  (23)'    'id1106913:  (23)'    'id1111327:  (23)'
%     '07-Feb-2008'     [             NaN]    [             NaN]    [             NaN]
%     '12-Jun-2008'     [             NaN]    [             NaN]    [             NaN]
%     '21-Dec-2008'     [             NaN]    [             NaN]    [             NaN]
%     '08-Feb-2009'     [             NaN]    [             NaN]    [             NaN]
%     '12-Jun-2009'     [             NaN]    [             NaN]    [             NaN]
%     '21-Jun-2009'     [             NaN]    [             NaN]    [             NaN]
%     '20-Dec-2009'     [             NaN]    [             NaN]    [             NaN]
%     '07-Feb-2010'     [             NaN]    [               1]    [             NaN]
%     '13-Jun-2010'     [          1.0000]    [             NaN]    [             NaN]
%     '20-Jun-2010'     [             NaN]    [             NaN]    [             NaN]
%     '20-Dec-2010'     [             NaN]    [             NaN]    [             NaN]
%     '07-Feb-2011'     [             NaN]    [          0.7500]    [             NaN]
%     '12-Jun-2011'     [          0.6667]    [             NaN]    [             NaN]
%     '20-Jun-2011'     [             NaN]    [             NaN]    [               1]
%     '20-Dec-2011'     [             NaN]    [             NaN]    [          0.8000]
%     '07-Feb-2012'     [             NaN]    [          0.5000]    [             NaN]
%     '12-Jun-2012'     [          0.3333]    [             NaN]    [             NaN]
%     '20-Jun-2012'     [             NaN]    [             NaN]    [             NaN]
%     '20-Dec-2012'     [             NaN]    [             NaN]    [             NaN]
%     '07-Feb-2013'     [             NaN]    [             NaN]    [             NaN]
%     '20-Jun-2013'     [             NaN]    [             NaN]    [          0.6000]
%     '20-Dec-2013'     [             NaN]    [             NaN]    [          0.4000]
%     '20-Jun-2014'     [             NaN]    [             NaN]    [          0.2000]

