clear; clc;
dbstop if error;

import simulations.*;
import bll.*;
import dal.market.get.dynamic.*;

date = '2006-12-11';
date_num = datenum(date);
benchmarkID = [];

solution.nsin_sol = [ 1099456
     1099738
     1260306
     1260397
     1320100
     1380047
     1410174
     1460070
     1940089
     1940105
     1940147
     1940287
     1940303
     1940329
     1940345
     1940360
     1940386
     1980119
     1980150
     2260115
     2300051
     2310043
     2600195
     2860088
     3230067
     3720034
     3720075
     3900099
     4150066
     4310074]';
 
 solution.count = round(100*rand(length(solution.nsin_sol), 1))';
solution.PortDuration = 4;

solution.initial_prices = fts2mat(get_bond_dyn_single_date( solution.nsin_sol, 'price_dirty', date));

DataForCalcTotalValue(solution, date_num, benchmarkID )
