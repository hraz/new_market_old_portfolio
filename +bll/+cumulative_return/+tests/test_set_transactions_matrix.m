clc; clear;
import bll.*;

vecFinalDatesPerBond = [ 734847; 733805; 733042; 734847; 734847; 734207; 734847; 733042; 734847]';

transactionDate = 733042;
mtxHistoricalAllocationOut = zeros(3, 9);
mtxHistoricalAllocationOut(1,:) =  [1940105; 1940147; 1940287; 1940303; 1940329; 1940345; 1940360; 1940386; 1980119];
mtxHistoricalAllocationOut(2, :) = ceil(100*rand(1, 9))';

[mtxTransactions] = set_transactions_matrix(vecFinalDatesPerBond, transactionDate, mtxHistoricalAllocationOut)