function [ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, nsin_data, dateBegPort, dateFinalData, constraints, vargin )
%CALC_TOTAL_VALUE_ENVELOPE sets up and calculates cumulative return
%
%   [ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, dateBegPort, benchmark_id, vargin )
%     function is the upper level envelope function for calc_total_value, and provides the data needed for the function
%
%
%    input: solution - data structure containing list of bonds, count of each,
%            sought portfolio duration, initial prices of bonds
%           required fields:
%               solution.nsin_sol - nNSINS X 1 DOUBLE - the nsins of the
%                   bonds in the portfolio
%               solution.count - nNSINS X 1 DOUBLE - the units of the bonds
%                   in the portfolio
%               solution.initial_prices - nNSINS X 1 DOUBLE - the prices of
%                   all bonds in the portfolio at 'creation_date'
%               solution.PortDuration - DATENUM - duration in years
%               solution.NumBondsSol - DOUBLE - number of assets
%               solution.flagMultiRedemption - nNSINS X 1 LOGICAL - '1' if
%                   the bond is 'multi-redemption', '0' if not.
%
%
%           dateBegPort - DATE_NUM - date at which portfolio is generated
%
%           dateFinalData - DATE_NUM - date at which there is no more data
%
%           constraints - data structure - required fields:
%               benchmark_id - VALUE - value representing ID of benchmark in which
%                   a possible reinvestment will be made
%
%           optional:
%               ftsPrice - fints - mDAYS X nASSETS fts of dirty prices of bonds
%               
%               in case of reinvestmentMethod - real specific bond -
%                   constraints are needed for filtering
%   output: ftsTotalHoldingValue - nOBSERVATIONS X 1 - fints containing net worth of portfolio at each date, inluding cash
%
%           ftsHoldingValuePerBond - nOBSERVATIONS X mASSETS fints - containing net worth of each bond and/or cash which are in the portfolio,
%             including reinvestments in the bonds or new investments added to th eportfolio.  Days when bond is not alive or not
%             in portfolio, worth is indicated as NaN
%
%   See also: calc_total_value, current_price_update_in_allocation
%
% Written by Hillel Raz, Jan 2013
% Copyright, BondIT Ltd
% Updated by Idan on 20/3/13:
%       If there are bonds without coupons then da_bond_cf_dates gives a
%       matrix without columns for these bonds. This causes problemslater
%       on. I added these missing columns (as columns of NaN's) to ftsPayExDatesMapping.

dbstop if error

import bll.cumulative_return.*;
import dal.market.get.dynamic.*;
import dal.market.get.cash_flow.*;
import bll.cumulative_return.*;
import utility.*;
import simulations.*;

%% Check input

if ~isa(solution, 'struct') || ~isa(solution.nsin_sol, 'double') ||...
        ~isa(solution.count, 'double') || ~isa(solution.initial_prices, 'double')...
        || ~isa(dateBegPort, 'double')|| ~isa(solution.flagMultiRedemption, 'logical')...
        || ~isa(nsin_data, 'struct') || ~isa(nsin_data.YTM, 'double') || ~isa(constraints.benchmark_id, 'double')
    error('calc_total_value_envelope:wrongInput', ...
        'One or more input arguments is not of the right type');
end

if nargin == 6
    ftsPrice = vargin(1);
    if ~isa(ftsPrice.price_dirty, 'fints')
        error('calc_total_value_envelope:wrongInput', ...
            'One or more input arguments is not of the right type');
    end
end

%% Needed data/information
portfolioMaturityDate = dateBegPort + ceil(365*solution.PortDuration);
flagMultiRedemption = solution.flagMultiRedemption;
dateEndSim = min(portfolioMaturityDate, dateFinalData);

if nargin<6
    [ftsPrice] = get_multi_dyn_between_dates(solution.nsin_sol, {'price_dirty'}, 'bond', dateBegPort, dateEndSim);
end
[ftsCashflow ftsRedemption ftsSumOfRedemption] = build_cash_flow(dateBegPort, dateEndSim, solution.nsin_sol);

%% Reinvestment strategy

reinvestmentStrategy = solution.ReinvestmentStrategy; %%%CHECK what are the options

%% Allocation - set up initial allocation matrix according to solution from solver
mtxInputAllocation=zeros(length(solution.nsin_sol),3);
mtxInputAllocation(:, 1) = solution.nsin_sol;
mtxInputAllocation(:, 2) = solution.count;
mtxInputAllocation(:, 3) = solution.initial_prices;

mtxAllocation = []; %For first calling of current_price_update_in_allocation

%% ex dates set up with payments' dates
[ftsPayExDatesMapping] = get_bond_cf_dates( dateBegPort, dateEndSim, solution.nsin_sol);
if solution.NumBondsSol > size(ftsPayExDatesMapping, 2)
    bondsInFtsPayExDatesMapping = fieldnames(ftsPayExDatesMapping);
    bondsInFtsPayExDatesMapping = strid2numid(bondsInFtsPayExDatesMapping(4:end));
    bondsWithoutExDate = setdiff(solution.nsin_sol, bondsInFtsPayExDatesMapping);
    nBondsWithoutExDate = length(bondsWithoutExDate);
    if ~isempty(ftsPayExDatesMapping)
        ftsToAddToftsPayExDatesMapping = fints(ftsPayExDatesMapping.dates, NaN*ones(size(ftsPayExDatesMapping, 1), nBondsWithoutExDate), numid2strid(bondsWithoutExDate));
    else
        ftsToAddToftsPayExDatesMapping = fints();
    end
    ftsPayExDatesMapping = merge(ftsPayExDatesMapping, ftsToAddToftsPayExDatesMapping);
    %mtxFtsPayExDatesMapping = fts2mat(ftsPayExDatesMapping);
    %fields = fieldnames(ftsPayExDatesMapping);
    %sortedFields = sort(strid2numid(fields(4: end)));
    %ftsPayExDatesMapping = fints(ftsPayExDatesMapping.dates, mtxFtsPayExDatesMapping, numid2strid(sortedFields));
end

priceDates = ftsPrice.price_dirty.dates;
paymentDates = ftsPayExDatesMapping.dates;
paymentDates= unique(sort([paymentDates; priceDates]));
paymentDates = [paymentDates; paymentDates(end)+1]; %Add additional day to break from while loop

nPayDates = 1; % Starting from first pay date, till last. Done in a while loop in case number of pay dates changes due to change in allocation
ftsTotalHoldingValue = fints();
ftsHoldingValuePerBond = fints();
firstDateVec = min(dateBegPort, paymentDates(1))*ones(size(mtxInputAllocation, 1), 1);
mtxHistoricalAllocationIn= [firstDateVec mtxInputAllocation];
mtxTransactions = [];
mtxTransactionsTotal = [];
cashHoldingTotal = [];
lastCallDate = paymentDates(1); % First call of function
cashPerBond = zeros(solution.NumBondsSol ,2);
cashPerBond(:, 1) = solution.nsin_sol;

benchmarkFTS = [];
ytmInterestPerBondIn = nsin_data.YTM(solution.selected_indx);

%% get cumulative return for each payment date
while nPayDates <= length(paymentDates) && paymentDates(nPayDates) <=dateEndSim && paymentDates(nPayDates) < dateFinalData
    
    if paymentDates(nPayDates) ==      734900
        2;
    end
    
    if paymentDates(nPayDates) ==   734712
        2;
    end
    if paymentDates(nPayDates) ==734899
        2;
    end
    
    
    %% add current prices for the next date that will be calculated.
    if ~isempty(mtxAllocation)
        [mtxInputAllocation]= current_price_update_in_allocation(mtxAllocation, ftsPriceOut, paymentDates(nPayDates));
    end
    
    %% get cumulative return
    [ftsPayExDatesMappingOut, totalHoldingValue, holdingValuePerBond, ftsCashFlowOut, ...
        ftsPriceOut,mtxAllocation , mtxHistoricalAllocationOut, mtxTransactionsOut, ...
        cashHoldingValue,  ftsRedemptionOut, ftsSumOfRedemptionOut, flagMultiRedemptionOut, cashPerBond, benchmarkFTS, ytmInterestPerBondOut] =...
        calc_total_value(mtxTransactions, ftsCashflow, mtxInputAllocation, ftsPrice, ...
        solution.PortDuration, reinvestmentStrategy, paymentDates(nPayDates), constraints, ...
        lastCallDate, paymentDates(1), paymentDates(end), ftsPayExDatesMapping, ...
        mtxHistoricalAllocationIn, ftsRedemption, ftsSumOfRedemption, ...
        flagMultiRedemption, ytmInterestPerBondIn, nsin_data.Duration(solution.selected_indx), cashPerBond, solution, nsin_data, benchmarkFTS);
    
    ftsTotalHoldingValueSingleDate = fints(paymentDates(nPayDates), totalHoldingValue);
    if any(paymentDates(nPayDates)== priceDates)
        ftsTotalHoldingValue = merge(ftsTotalHoldingValue, ftsTotalHoldingValueSingleDate);
        
        ftsHoldingValuePerBondSingleDate = fints(paymentDates(nPayDates), holdingValuePerBond(:, 2)', numid2strid(holdingValuePerBond(:,1))');
        ftsHoldingValuePerBond = merge(ftsHoldingValuePerBondSingleDate, ftsHoldingValuePerBond);
        
    end
    %% Update variables
    ftsPayExDatesMapping = ftsPayExDatesMappingOut ;
    ftsCashflow = ftsCashFlowOut;
    ftsPrice = ftsPriceOut;
    ftsRedemption =  ftsRedemptionOut;
    ftsSumOfRedemption = ftsSumOfRedemptionOut;
    flagMultiRedemption = flagMultiRedemptionOut;
    mtxHistoricalAllocationIn = mtxHistoricalAllocationOut;
    lastCallDate = paymentDates(nPayDates);
    mtxTransactionsTotal = [mtxTransactionsTotal; mtxTransactionsOut];
    cashHoldingTotal = [cashHoldingTotal; cashHoldingValue];
     ytmInterestPerBondIn = ytmInterestPerBondOut;
    
    
    %% if allocation has changed, reset final dates vector
    if size(mtxInputAllocation, 1) ~= size(mtxAllocation, 1)
        desiredDate = paymentDates(nPayDates);
        paymentDates = ftsPayExDatesMappingOut.dates;
        paymentDates= unique(sort([paymentDates; ftsPriceOut.price_dirty.dates]));
        paymentDates = [paymentDates; paymentDates(end)+1]; %Add additional day to break from while loop
        nPayDates = find(paymentDates <= desiredDate, 1, 'last'); % For the case that additional dates were
                                                                  % added into the total payment dates                                                                                                                                  
    end
    
    nPayDates = nPayDates + 1;
    
end

%% Remove empty column - series1.
% ftsTotalHoldingValue = rmfield(ftsTotalHoldingValue, 'series1');
ftsHoldingValuePerBond = rmfield(ftsHoldingValuePerBond, 'series1');

end
