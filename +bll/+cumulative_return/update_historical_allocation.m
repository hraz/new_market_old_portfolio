function mtxHistoricalAllocationOut = update_historical_allocation(mtxHistoricalAllocationIn, mtxAllocation, desiredDate)
%UPDATE_HISTORICAL_ALLOCATION updates the matrix of historical allocation.
%			
%   mtxHistoricalAllocationOut = update_historical_allocation(mtxHistoricalAllocationIn,...
%                                                                                                                                              mtxAllocation, desiredDate)
%   receives the current allocation matrix and updates the historical one
%   which holds the  securities (nsins) held at each point of time along with the
%   corresponding number of units and prices. 
%	Input:
%       mtxHistoricalAllocationIn � matrix of dimensions (nDates*securities at each date)*(4).
%                                                                          The format is:
%                                                                          date | security_id's | units | price |
%                                                                           --------|--------------------------|------------|-----------|
%       mtxAllocation �  matrix of dimensions (nSecurities)*(3).
%                                                                            The format is:
%                                                                            security_id's | units | price |
%                                                                            -------------------------|------------|-----------|
%       desiredDate - a datenum variable of the current date.   
%   Output:
%       mtxHistoricalAllocationIn � matrix of dimensions (nDates*securities at each date)*(4).
%                                                                          The format is:
%                                                                          date | security_id's | units | price |
%                                                                           --------|--------------------------|------------|-----------|
%   Sample:
%			Some example of the function using.
% 	
%		See Also:
%		UPDATE_PRICES, UPDATE_COUPONS, UPDATE_FTS_PAY_EX_DATES_MAPPING
%	
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

% Check input arguments type:
flagType(1) = isa(mtxHistoricalAllocationIn, 'double');
flagType(2) = isa(mtxAllocation, 'double');
flagType(3) = isa(desiredDate, 'numeric');
if sum(flagType) ~=  length(flagType)
     error('update_historical_allocation:wrongInput', ...
                'One or more input arguments is not of the right type');
end
%% Check input arguments sizes:
if ~isempty(mtxHistoricalAllocationIn)
    nColumns1 = size(mtxHistoricalAllocationIn, 2);
else
    nColumns1 = 4;
end
nColumns2 = size(mtxAllocation, 2);
sizes = [nColumns1 nColumns2];
flagSizes = sizes == [4 3];
if sum(flagSizes) ~=  length(flagSizes)
     error('update_historical_allocation:wrongInput', ...
                'One or more input arguments is not of the right size');
end

mtxHistoricalAllocationOut = mtxHistoricalAllocationIn;
mRowsHistoricalAllocation = size(mtxHistoricalAllocationIn, 1);
mRowsAllocation = size(mtxAllocation, 1);
datesVec = ones(mRowsAllocation, 1)*datenum(desiredDate);
mtxHistoricalAllocationOut(mRowsHistoricalAllocation+1:mRowsHistoricalAllocation+mRowsAllocation, 1) = datesVec;
mtxHistoricalAllocationOut(mRowsHistoricalAllocation+1:mRowsHistoricalAllocation+mRowsAllocation, 2) = mtxAllocation(:, 1); % second...
% column holds security id's.
mtxHistoricalAllocationOut(mRowsHistoricalAllocation+1:mRowsHistoricalAllocation+mRowsAllocation, 3) = mtxAllocation(:, 2); % third...
% column holds units.
mtxHistoricalAllocationOut(mRowsHistoricalAllocation+1:mRowsHistoricalAllocation+mRowsAllocation, 4) = mtxAllocation(:, 3); % fourth...
% column holds  prices.
