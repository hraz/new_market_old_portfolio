function [mtxTransactions, mtxAllocationOut] = modify_allocation_due_to_multiple_redemption_new_17_3_13(ftsRedemption, ftsSumOfRedemption, flagMultiRedemption, mtxAllocationIn, desiredDate,...
    ftsCashFlow, ftsPayExDatesMapping, mtxHistoricalAllocationIn)

%   MODIFY_ALLOCATION_DUE_TO_MULTIPLE_REDEMPTION modifies the portfolio
%   structure due to multiple redemption.
%
%   [mtxTransactions, mtxAllocationOut] = modify_allocation_due_to_multiple_redemption(ftsRedemption, ...
%                                                                                         ftsSumOfRedemption, flagMultiRedemption, mtxAllocationIn, desiredDate)
%   modifies the allocation matrix by reducing the number of units of a certain bond after we
%   recieved some given percentage of its par value.
%	Input:
%       ftsRedemption � fts of dimensions (nDates)*(nSecurities) which
%       holds redemptions of bonds given in percents. The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | redemption1    | redemption2  | redemption3    |
%       ftsSumOfRedemption - fts of the sum of redemptions of a bond starting today until the bond dies.
%       flagMultiRedemption - a vector of dimensions (nSecurities)*(1) of
%       1's and 0's. 1 is for securities with multi-redemption and 0 for
%       other securities.
%       mtxAllocationIn �  matrix of dimensions (nSecurities)*(3).
%                                                                            The format is:
%                                                                            security_id's | units | price |
%                                                                            -------------------------|------------|-----------|
%       desiredDate - a datenum variable of the current date.
%
%   Output:
%       mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%       mtxAllocationOut � matrix similar to mtxAllocation but
%                                                   updated because of multi redemption.
%
%   Sample:
%			Some example of the function using.
%
%		See Also:
%       MODIFY_ALLOCATION
%
% Idan Oren,12/3/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

import simulations.*;

%% Check input arguments type:
flagType(1) = isa(ftsRedemption, 'fints');
flagType(2) = isa(ftsSumOfRedemption, 'fints');
flagType(3) = isa(flagMultiRedemption, 'logical');
flagType(4) = isa(mtxAllocationIn, 'double');
flagType(5) = isa(desiredDate, 'numeric');
if sum(flagType) ~= length(flagType)
    error('modify_allocation:wrongInput', ...
        'One or more input arguments is not of the right type');
end
%% Check input arguments sizes:
nColumns1 = size(mtxAllocationIn, 2);
sizes = [nColumns1];
flagSizes = sizes == [3];
if sum(flagSizes) ~= length(flagSizes)
    error('modify_allocation:wrongInput', ...
        'One or more input arguments is not of the right size');
end

nsinList = mtxAllocationIn(:, 1);
nNsinList = length(nsinList);




redemptionDate =  ftsRedemption.dates;
redemptionIndices = find(redemptionDate == desiredDate); %%% Should get only one index
mtxPayExDates = fts2mat(ftsPayExDatesMapping);
payExMappingAtDesiredDate = mtxPayExDates(redemptionIndices, :);  %%% Should get only one line
indicesOfSecuritiesWithoutRedemptions = isnan(payExMappingAtDesiredDate);
securitiesWithRedemptions = ~indicesOfSecuritiesWithoutRedemptions;
securitiesWithMultipleRedemptions = logical(securitiesWithRedemptions.*flagMultiRedemption);
% Here we only take bonds which have redemption at the desiredDate and
% that are multiple redemption bonds
payExMappingAtDesiredDate(indicesOfSecuritiesWithoutRedemptions) = [];
relevantNsins = nsinList(securitiesWithMultipleRedemptions); 
% This is the list of nsins having multi-redemption at the desiredDate
nRelevantNsins = length(relevantNsins);


mtxRedemption = fts2mat(ftsRedemption);
mtxSumOfRedemption = fts2mat(ftsSumOfRedemption);
indexOfDesiredDate = find(redemptionDate >= desiredDate); % in case desiredDate is not a business day
indexOfDesiredDate = indexOfDesiredDate(1);
relevantLineInRedemption = mtxRedemption(indexOfDesiredDate, :);
relevantLineInSumOfRedemption = mtxSumOfRedemption(indexOfDesiredDate, :);
relevantLineInRedemption = relevantLineInRedemption(securitiesWithMultipleRedemptions);
% Choosing only the relevant bonds
relevantLineInSumOfRedemption = relevantLineInSumOfRedemption(securitiesWithMultipleRedemptions);
% Choosing only the relevant bonds


unitsOfMultiRedemptionBonds = zeros(nNsinList, 1); % This loop build a list of units for each bond at the ex date.
nBondsInAllocation = size(mtxAllocationIn, 1);
for iRelevantNsins = 1:nRelevantNsins
    relevantLineInHistoricalAllocation = intersect(find((mtxHistoricalAllocationIn(:, 1) <= payExMappingAtDesiredDate(iRelevantNsins)),... 
        2*nBondsInAllocation, 'last'), find(mtxHistoricalAllocationIn(:, 2) == nsinList(iRelevantNsins)));
    if ~isempty(relevantLineInHistoricalAllocation)
        relevantLineInHistoricalAllocation = relevantLineInHistoricalAllocation(end); %In case bonds were removed and the allocation was changed
        x = find(mtxAllocationIn(:, 1) == nsinList(iRelevantNsins));
         unitsOfMultiRedemptionBonds(x) = mtxHistoricalAllocationIn(relevantLineInHistoricalAllocation, 3);
       % unitsOfMultiRedemptionBonds(iRelevantNsins) = mtxHistoricalAllocationIn(relevantLineInHistoricalAllocation, 3);
    end
end


securitiesWithMultipleRedemptionsAndNonZeroUnits = (unitsOfMultiRedemptionBonds'.*securitiesWithMultipleRedemptions)~=0;
% Keeping only bonds which had non-zero units at the ex date
nTransactions = sum(securitiesWithMultipleRedemptionsAndNonZeroUnits);
mtxTransactions = zeros(nTransactions, 5);  % creating the transaction matrix to include as many bonds with multiple redemptions
% and which had non-zero units at the ex date.
mRowsOfTransactions = 1;
relevantNsins = relevantNsins(securitiesWithMultipleRedemptionsAndNonZeroUnits);
unitsOfMultiRedemptionBonds = unitsOfMultiRedemptionBonds(securitiesWithMultipleRedemptionsAndNonZeroUnits);

for iTransactions = 1:nTransactions % This loop construct the transaction matrix 
    % for reducing the number of units for each multiple redemption bond
    % if (~isnan(relevantLineInRedemption(iRelevantNsins))) && (unitsOfMultiRedemptionBonds(iRelevantNsins) ~= 0)
        securityID = relevantNsins(iTransactions);
        percentOfParPaid = relevantLineInRedemption(iTransactions)/relevantLineInSumOfRedemption(iTransactions);
        % The percent of units we reduce is normalized by the sum of
        % redemptions starting today until the bond dies.
        nUnitsToReduce = ceil(unitsOfMultiRedemptionBonds(iTransactions)*percentOfParPaid);
        x = find(mtxAllocationIn(:, 1) == securityID);
        priceOfBond = mtxAllocationIn(x, 3);
        mtxTransactions(mRowsOfTransactions, 1) = desiredDate;
        mtxTransactions(mRowsOfTransactions, 2) = securityID;
        mtxTransactions(mRowsOfTransactions, 3) = - nUnitsToReduce;
        mtxTransactions(mRowsOfTransactions, 4) = 0;
        mtxTransactions(mRowsOfTransactions, 5) = - nUnitsToReduce*priceOfBond;
        mRowsOfTransactions = mRowsOfTransactions+1;
    % end
end


[mtxAllocationOut] = modify_allocation(mtxTransactions, mtxAllocationIn, [], [], desiredDate, []);









% for iRelevantNsins = 1:nRelevantNsins
%     % if (~isnan(relevantLineInRedemption(iRelevantNsins))) && (unitsOfMultiRedemptionBonds(iRelevantNsins) ~= 0)
%         securityID = relevantNsins(iRelevantNsins);
%         percentOfParPaid = relevantLineInRedemption(iRelevantNsins)/relevantLineInSumOfRedemption(iRelevantNsins);
%         % The percent of units we reduce is normalized by the sum of
%         % redemptions starting today until the bond dies.
%         nUnitsToReduce = ceil(unitsOfMultiRedemptionBonds(iRelevantNsins)*percentOfParPaid);
%         x = find(mtxAllocationIn(:, 1) == securityID);
%         priceOfBond = mtxAllocationIn(x, 3);
%         mtxTransactions(mRowsOfTransactions, 1) = desiredDate;
%         mtxTransactions(mRowsOfTransactions, 2) = securityID;
%         mtxTransactions(mRowsOfTransactions, 3) = - nUnitsToReduce;
%         mtxTransactions(mRowsOfTransactions, 4) = 0;
%         mtxTransactions(mRowsOfTransactions, 5) = - nUnitsToReduce*priceOfBond;
%         mRowsOfTransactions = mRowsOfTransactions+1;
%     % end
% end

























