function ftsPriceOut = update_prices(ftsPrice, newSecurityID, firstDate, lastDate, benchmarkID,  tblName)


%UPDATE_PRICES updates the fts which holds the information of prices for each security (nsin).
%			
%   ftsPriceOut = update_prices(ftsPrice, newSecurityID, firstDate, lastDate, benchmarkID,  tblName)
%   receives the existing fts which holds prices of existing securities. It
%   also recieves new security id's  and updates the price fts to
%   hold the information for all the securities.
%	Input:
%       ftsPrice � Structure with one field (price_dirty) ftsPrice.price_dirty is an fts of dimensions
%                                                                          (nDates)*(nSecurities). The format is:
%                                                                          date   | security_id1 | security_id2 | security_id3 | ...
%                                                                           ----------|-------------------------|------------------------|-------------------------|
%                                                                           date1 | price1                | price2               | price3                |
%       newSecurityID �  vector of new security id's (nsins). If this is
%                                             non-empty, the variable ftsPrice should be updated.
%                                             Otherwise the output argument ftsPriceOut will be
%                                             identiacl to ftsPrice..
%
%       firstDate - a datenum variable of the date the portfolio was created.   
%       lastDate - a datenum variable of the date the portfolio ends.   
%       benchmarkID - nsin of benchmark if needed. At this point it should
%                                      be empty.
%       tblName - determines from which data base table, the information is
%                            to be drawn. for bonds this is 'bond'.
%   Output:
%       ftsPriceOut � structure similar to ftsPrice but
%       updated due to newSecurityID.
%   Sample:
%			Some example of the function using.
% 	
%		See Also:
%		UPDATE_HISTORICAL_ALLOCATION, UPDATE_FTS_PAY_EX_DATES_MAPPING
%	
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

import dal.market.get.dynamic.*; % Because of: get_multi_dyn_between_dates
import utility.*;
%% Check input arguments type:
flagType(1) = isa(ftsPrice, 'struct');
flagType(2) = isa(newSecurityID, 'double');
flagType(3) = isa(firstDate, 'numeric');
flagType(4) = isa(lastDate, 'numeric');
flagType(5) = isa(tblName, 'char');
if ~isempty(benchmarkID)
    flagType(6) = isa(benchmarkID, 'char');
else
    flagType(6) = 1;
end
if sum(flagType) ~= length(flagType)
     error('update_coupons:wrongInput', ...
                'One or more input arguments is not of the right type');
end


%% Check input arguments sizes:

% if sum(flagSizes) ~= length(flagSizes)
%      error('update_prices:wrongInput', ...
%                 'One or more input arguments is not of the right size');
% end

%% Without Caching
newPriceFTS = get_multi_dyn_between_dates(newSecurityID, {'price_dirty'}, tblName, firstDate, lastDate);
mergedPriceFTS = merge(ftsPrice.price_dirty, newPriceFTS.price_dirty);
ftsPriceOut = struct('price_dirty', mergedPriceFTS);
ftsPriceOut.price_dirty.desc = 'price_dirty';

%% With Caching 
% load ftsDirtyPriceAllBonds.mat;
% dates = ftsPrice0.price_dirty.dates;
% begDate = find(dates <= firstDate, 1, 'last');
% endDate = find(dates <= lastDate, 1, 'last');
% fields = strid2numid(fieldnames(ftsPrice0.price_dirty));
% fields = fields(4: end);
% mtxCache = fts2mat(ftsPrice0.price_dirty);
% index = [];
% for nNewID = 1:length(newSecurityID)
%     spotID = find(fields == newSecurityID(nNewID));
%     index = [index spotID];
% end
% 
% newPriceFTS = fints(dates(begDate:endDate), mtxCache(begDate:endDate, index), numid2strid(newSecurityID));
% 
% a = fts2mat(ftsPriceOut.price_dirty); b = isnan(a); a(b) =1;

%Without Caching
ftsPriceOut.price_dirty = merge(newPriceFTS.price_dirty, ftsPrice.price_dirty);
% 
% aa = fts2mat(ftsPriceOut.price_dirty); bb = isnan(aa); aa(bb) = 1;
% isequal(a(1:end-1, :), aa)