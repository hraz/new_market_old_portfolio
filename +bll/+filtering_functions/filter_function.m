function [filtered error_id] = filter_function(constraints, req_date)
%
% function takes filtering guidelines from simulation script, filters bonds accordingly and returns the nsin list
%
% input: constraints - data structure containing filtering constraints
%    required: constraints.sectors.sectors_to_include               - CELLARRAY list of sectors to include in filter
%                         constraints.sectors.sectors_to_include_id - DOUBLE NSectorsX1 list of sectors ID to include in filter
%
%    Optional: constraints.static.types_to_include - can have one of two options:
%                 - BOOLEAN vector of NTypes X 1 stating which of the types should be
%                   included in the static filter
%                 - dataset with the correct format for the bond_ttl_multi
%
%              constraints.static.corpgovToInclude -
%                 - BOOLEAN vector of 2 X 1 stating which of the corp\gov should be included in the static filter
%
%
%       req_date - DATENUM of the beginning of the simulation
%
%       model_id - 1X3 DOUBLE - according to Hillel's spec
%
%
% output:   filtered.nsin_list - NASSETS X 1 DOUBLE list of nsins
%           filtered.nsin_cell_lists - NType X 1 cell array of the bonds belonging to
%                                    each filter, ment for the building of the conset
%           filtered.min_histlen - DOUBLE the minimal constraint on history length that satisfies the solver model demands
%
%
%           error_id - [] for no error, 116 for empty index
% 
% written by Hillel Raz, BondIT, January 2013

import bll.filtering_functions.*;


constraints.static.dataset = build_staticDataset(constraints);

if constraints.benchmark_id
    [filtered error_id] = filter_by_constraints(constraints,req_date, constraints.benchmark_id);
else
    [filtered error_id] = filter_by_constraints(constraints,req_date);
end