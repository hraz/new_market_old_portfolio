function ds = build_corpgov_struct(SectorsToInclude,varargin)
% this function prepares the data structure needed for the static 'Type'
% filter.
%
% Input:
% Required:
% =========
% SectorsToInclude - 1 X NSectors with the list of sectors the user wants included
%
% Optional:
% =========
% vector of the Types to be included (1 - include, 0 - exclude)
% Output:
% type_struct - cellarray of structures fitting to filtering (in bond_ttl_multi)
%
% NOTE: this function should be in sync with the function
% 'bll\filter_is_corporate_check.m'. Also - until the Solver will use the
% built-in Type-ConSet that the 'filter_by_constraints' produces,  this
% function should also be in sync with the 'build_TypeFilterParams.m'

import utility.dal.*;
% checking that SectorsToInclude is indeed a row cell-vector and not a
% column one. Otherwise - an error will occur in 'bond_ttl_multi'.
if size(SectorsToInclude,1)>1
    SectorsToInclude=SectorsToInclude';
end

% building a structure with the details of each type, which is easier to
% mannage than a dataset.
type_struct=cell(2,1);
%
% type_struct{1}.class_name = {};
% type_struct{1}.bond_type= {'Corporate Bond'};
% type_struct{1}.linkage_index= {'NON LINKED','CPI','USD' , 'UKP', 'EURO'};
% type_struct{1}.coupon_type= {'Fixed Interest' , 'Variable Interest'};
% type_struct{1}.sector= SectorsToInclude;
%
% type_struct{2}.class_name = {};
% type_struct{2}.bond_type= {'Government Bond Shachar' 'Short Term Treasury Bill' 'Government Bond T-Bill' ...
%     'Government Bond Galil' 'Government Bond Improved Galil' ...
%     'Government Bond New Gilon' 'Government Bond Gilon'};
% type_struct{2}.linkage_index= {'NON LINKED','CPI'};
% type_struct{2}.coupon_type= {'Variable Interest','Fixed Interest' 'Zero Coupon'};
% type_struct{2}.sector= {};

type_struct{1}.class_name = {};
type_struct{1}.bond_type= {'Corporate Bond'};
type_struct{1}.linkage_index= {'NON LINKED','CPI','USD' , 'UKP', 'EURO'};
type_struct{1}.coupon_type= {'Fixed Interest' };
type_struct{1}.sector= SectorsToInclude;
type_struct{1}.redemption_error ={'no_error'};
type_struct{1}.redemption_type = {};
type_struct{1}.redemption_sum = {};

type_struct{2}.class_name = {};
type_struct{2}.bond_type= {'Government Bond Shachar' 'Short Term Treasury Bill' 'Government Bond T-Bill' ...
    'Government Bond Galil' 'Government Bond Improved Galil' ...
    'Government Bond New Gilon' 'Government Bond Gilon'};
type_struct{2}.linkage_index= {'NON LINKED','CPI'};
type_struct{2}.coupon_type= {'Fixed Interest' 'Zero Coupon'};
type_struct{2}.sector= {};
type_struct{2}.redemption_error ={'no_error'};
type_struct{2}.redemption_type = {};
type_struct{2}.redemption_sum = {};
%checking if the user entered a vector with his chosen types. if not -
%assume that all the types are needed.
if nargin>1
    type_array_boolean=varargin{1};
else
    type_array_boolean=ones(length(type_struct),1);
end
type_array_find=find(type_array_boolean);


% change from the structure type to a dataset type
ds=dataset();
for m=1:length(type_array_find)
    class_name=type_struct{type_array_find(m)}.class_name;
    bond_type=type_struct{type_array_find(m)}.bond_type;
    linkage_index=type_struct{type_array_find(m)}.linkage_index;
    coupon_type=type_struct{type_array_find(m)}.coupon_type;
    sector=type_struct{type_array_find(m)}.sector;
    redemption_error=type_struct{type_array_find(m)}.redemption_error ;
    redemption_type=type_struct{type_array_find(m)}.redemption_type;
    redemption_sum=type_struct{type_array_find(m)}.redemption_sum;
    
    ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,redemption_error,redemption_type, redemption_sum,ds);
end