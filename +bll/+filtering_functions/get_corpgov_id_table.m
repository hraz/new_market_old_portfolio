function outres = get_corpgov_id_table
% provides the table matching DB-type-id to types constructed in the
% 'build_type_struct.m' function.
%
% output: nTypes X 1 vector of class_id's as appearing in the market DB. the
%                    i'th element is the class_id of the i'th entry in the
%                    full structure built in 'build_corpgov_struct.m'
outres = [0;1];