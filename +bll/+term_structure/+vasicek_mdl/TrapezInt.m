function In=TrapezInt(x,y,x0,xf)
indx0=min(find(x>=x0));
indxf=max(find(x<=xf));
x=x(indx0:indxf);
y=y(indx0:indxf);
In=sum(((y(2:end)+y(1:end-1))/2).*(x(2:end)-x(1:end-1)));