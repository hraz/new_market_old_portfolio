function [ obj ] = vasicek(ir_type, price, cf, varargin)
%VASICEK Fits Vasicek function to market data
%
%   [ obj ] = vasicek(ir_type, price, cf, varargin) fits a Vasicek function to market
%   data. 
%
%   The Vasicek function for the forward rate is the following:
%
%   f = Beta0 + Beta1*(exp(-t/Tau1)) + Beta2*(t/Tau1).*(exp(-t/Tau1))
%
%   Input:
%       ir_type - is a string value specifying the type of interest rate curve (Forward, Zero).
%
%       price - is an 1xNBONDS fts specifying the dirty price at settlement date.
%
%       cf - is a NPAYMENTSxNBONDS fts specifying the cash flow of the given portfolio.
%
%   Optional input:
%       basis - is a scalar value specifying the day-count basis. Possible values include:
%                           0	- actual/actual
%                           1	- 30/360 SIA
%                           2	- actual/360
%                           3	- actual/365
%                           4	- 30/360 PSA
%                           5	- 30/360 ISDA
%                           6	- 30/360 European
%                           7	- actual/365 Japanese
%                           8	- actual/actual ISMA
%                           9	- actual/360 ISMA
%                           10 - actual/365 ISMA  (default)
%                           11 - 30/360 ISMA
%                           12 - actual/365 ISDA
%                           13 - bus/252
%
%       compounding -  is a scalar value specifying the the count of coupon payments per year. Acceptable values are
%                                   -1,1(default),2,3,4,6,12.
%
%   Note:   The additional parameters (basis, compounding) may be passed in via some
%               structure with field_name = parameter_name and its value is a parameter_value. 
%
%   Output:
%       obj - is a scalar of ir_func_curve class object.

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import utility.fts.*;
    import utility.*;
    import dal.market.get.dynamic.*;
    import bll.term_structure.*;
    
    %% Input validation:
    error(nargchk(3, 4, nargin));
    if (~ischar(ir_type))
        error('term_structure:vasicek:wrongInput', ...
                'Wrong type of the firs parameter.')
    end
    if (~isa(price, 'fints')) || (~isa(cf, 'fints'))
        error('term_structure:vasicek:wrongInput', ...
                'Price or Cash_Flow must be financial time series.')
    end
    sz = size(price);
    if (sz(2) ~= size(cf, 2))
        error('term_structure:vasicek:wrongInput', ...
                'The number of assets in the Price and the Cash_Flow parameters must be identical.')
    end
    if (sz(1) ~= 1)
        warning('term_structure:vasicek:wrongInput', ...
                'The price may include any number of observations, but used just first one (first row).')
    end

    %%
    switch lower(ir_type)
        case 'forward'
            func_handle = @vasicekforward;
        case 'zero'
            func_handle = @vasicekzero;
        otherwise
            error('term_structure:vasicek:invalidNelsonSiegelIRtype', ...
                'Wrong type of required vasicek interest rate curve.');
    end

    %% Find out if an options structure has been input
    if any(strcmpi(varargin,'IRFitOptions'))
        optidx = find(strcmpi(varargin,'IRFitOptions')) + 1;
        ir_fit_o = varargin{optidx};
        varargin(optidx-1:optidx) = [];
    else
        ir_fit_o = IRFitOptions([0.56 0.0016 2.9080 0.0446], ...
                                            'LowerBound', [0 0 0 0], ...
                                            'UpperBound', [Inf Inf Inf Inf], ...
                                            'FitType', 'durationweightedprice', ...
                                            'OptOptions', optimset(optimset('lsqnonlin'),'display','off'));
    end
    
    %%
    if any(strfind(ir_fit_o.FitType, 'dur'))
        [ fit_bench ] = get_bond_dyn_single_date(strid2numid(tsnames(price)), 'mod_dur', price.dates);
    elseif any(strfind(ir_fit_o.FitType, 'y'))
        [ fit_bench ] = get_bond_dyn_single_date(strid2numid(tsnames(price)), 'ytm', price.dates);
    end
        
    %%
    [ obj ] = ir_func_curve.fit_func(ir_type, price(1), cf, func_handle, ir_fit_o, fit_bench, varargin{:});
    
end

function [ForwardRate StdFwdRate] = vasicekforward(t,param)
%VASICEKFORWARD Evaluates Vasicek equation for the Forward instantaneous Rate
%
%   ForwardRate = VasicekFwd(t,param)
%
%   Inputs:
%   t: Time To Maturity
%   param: 1 x 4 vector of parameters (rt0, k, teta, sigma)
%          for the Vasicek equation
%
%   Outputs:
%   ForwardRate: Forward Rate for each of the input Time To Maturities
%   StdFwdRate: Standard deviation of the Forward rate as predicted by
%                                   Vasicek model
%
% Vasicek parameters for 2009-03-01 - 2011-12-31  are:
% param(1)=0.56;
% param(2)=0.0016;
% param(3)= 2.9080;
% param(4)=0.0446;
% LowerBound = [0,0,0,0,]
% UpperBound = [Inf, Inf, Inf, Inf]
% 
%   Amit Godel 2012

    rt0 = param(1);
    k = param(2);
    teta = param(3);
    sigma = param(4);

    ForwardRate = rt0*exp(-k*t) +teta*(1-exp(-k*t));
    StdFwdRate = sigma*sqrt((1-exp(-2*k*t))/(2*k));
end
