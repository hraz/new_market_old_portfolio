function [rM t] = VasEulerSim(rt0,para,t0,T,Ndt,M)
% VasEulerSim returns the simulated rate paths according to Vasicek model
%
%    [rM t] = VasEulerSim(rt0,para,t0,T,Ndt,M) receives the Vasicek
%    parameteres and the simulation properties (steps and repetition time )
%    and calculates stochastic rate-time paths according to the Vasicek
%    model using the Euler method:
%    r(t+dt)=r(t)+k*(teta-r(t))*dt + sigma*dW(t)
%
%   Input:
%       rt0 - initial rate = r(t=0)
%       para - structure containing vasicek prameters
%       t0,T - desired initial and end times
%       Ndt - resolution of time steps
%       M - number of repetitions (different realizations)
% 
%   Output:
%       rM - M_X_NDates array containing the M different simulation of the
%               insatatneous rate
%       t - 1_X_NDates array containing dates for which the simulation  was
%           made
% 
% Amit Godel
% Copyright 2012

t=linspace(t0,T,Ndt+1);
dt=(T-t0)/Ndt;
sdt=sqrt(dt);
rM=zeros(M,Ndt+1);

rM(:,1)=rt0;
for k=2:length(t)
    rM(:,k)=rM(:,k-1) + para.k*(para.teta-rM(:,k-1))*dt +  para.sigma*sdt*randn(M,1);
end