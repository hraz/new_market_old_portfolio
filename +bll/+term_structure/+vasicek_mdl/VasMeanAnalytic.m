function [r std_of_r t] = VasMeanAnalytic(rt0,para,t0,T,Ndt)
% VasMeanAnalytic returns the mean rate according to Vasicek model
%
%    [r std_of_r t] = VasMeanAnalytic(rt0,para,t0,T,Ndt) receives the
%    Vasicek parameters and calculates the mean rate from t0 until T,
%   calculated according to (3.7) in 'Interest Rate Models � Theory and
%   Practice' of Brigo & Mercurio.
%
%   Input:
%       rt0 - initial rate = r(t=0)
%       para - structure containing vasicek prameters
%       t0,T - desired initial and end times
%       Ndt - resolution of time steps
% 
%   Output:
%       r - NDatesX1 array containing the mean rate at times t
%       t - NDatesX1 array containing dates for which the calculation was
%           made
%       std_Of_r - standard deviation of r(t), caculated according to (3.7)
%           of Brigo&Mercurio.
% 
% Amit Godel
% Copyright 2012

t=linspace(t0,T,Ndt+1);
r=rt0*exp(-para.k*(t-t0)) + para.teta*(1-exp(-para.k*(t-t0)));
std_of_r=para.sigma*sqrt((1-exp(-2*para.k*(t-t0)))/(2*para.k));