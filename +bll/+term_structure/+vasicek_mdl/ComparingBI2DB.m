%ComparingBI2DB tries to compare the data from Bank of Israel to our
%DataBase for Makams

%%
clear all;
close all;

%% getting the data from Bank Israel
% Output - BI - 2 X NOFDATESLIST, after deleting the NaNs

import dal.market.get.dynamic.*;
import dml.*;

lower_date = '2010-01-01';
upper_date = '2012-01-19';
months =3;
tic
 [ fts ] = da_shortTB_yield( months, lower_date, upper_date );
 toc
 BI=fts2mat(fts,1);
 
 BI=BI(isfinite(BI(:,2)),:);
 
 %% loading data from our DB corresponing to the relevant dates and with ttm<1month
 % Output - n - cell array of length = length(BI). each cell contains the
 %                            nsins (bond list) of relevant maturity for
 %                            the desired date (the one in BI(k,1))
 
 import dal.market.filter.dynamic.*;
 import dal.market.filter.static.*;
 nos=zeros(length(BI(:,1)),1);
 n=cell(size(nos));
 tic
 for k=1:length(BI(:,1))
     rdate=datestr(BI(k,1),'yyyy-mm-dd');
     nsin=intersect(bond_dynamic_partial('ttm',1/400, 1/12, rdate ),...
         bond_static_partial( 'type',{'Short Term Treasury Bill'},rdate ));
     nos(k)=length(nsin);
     n{k}=nsin;
     if mod(k,10)==0 
         fprintf('%i ',k);
     end
 end
 toc
 
 %% Creating a unique (no repetitions) of the nsins in 'n' created above
%  will not be used in this code
 
 all_nsins_dup=zeros(sum(cellfun('size',n,1)),1);
 lastindx=0;
 for k=1:length(n)
     all_nsins_dup((lastindx+1):(lastindx+nos(k)))=n{k};
     lastindx=lastindx+nos(k);
 end
 all_nsins=unique(all_nsins_dup);
 
 %% Creating a list of the relevant bonds and then extracting their ytm's and ttm's
 % Output - ann & cdates - cell array of the relevant bonds and their
                                                      dates, without double bonds or no bonds
%                           dates - relevant days
%                           ytm - relevant ytms
%                           ttm - relevant ttms
%  
%  
 clear ann;
 m=1;
 ann{m}=n{1};
 cdates{m}=BI(1,1);
 m=m+1;
 for k=2:length(n)
     if nos(k)==2
         ann{m}=ann{m-1};
         cdates{m}=cdates{m-1};
     end
     if nos(k)==1
         ann{m}=n{k};
         cdates{m}=BI(k,1);
     end
     if nos(k)==0
         m=m-1;
     end
     m=m+1;
 end
 
 ids=cell2mat(ann);
 dates=zeros(length(ann),1);
 ytm=dates;
 ttm=dates;
 for k=1:length(ann)
     tmp =get_bond_dyn_single_date(ann{k},'ytm',cdates{k});
     tmp=fts2mat(tmp,1);
     dates(k)=tmp(1);
     ytm(k)=tmp(2);
     ttm(k)=fts2mat(get_bond_dyn_single_date(ann{k},'ttm',cdates{k}));
 end
 
 %% checking data
 import dal.market.get.dynamic.*;
 
 rdate=datenum('2010-12-01');
 nsin=8100612;
ttm=da_bond_dyn(nsin,'ttm',rdate);
ytm=da_bond_dyn(nsin,'ytm',rdate);
ttm=fts2mat(ttm,1);
ytm=fts2mat(ytm);
price=fts2mat(da_bond_dyn(nsin,'price_dirty',rdate));
dates=ttm(:,1);