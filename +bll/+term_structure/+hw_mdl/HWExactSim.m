function [rM t] = HWExactSim(rt0,para,fm_interp,t0,T,Ndt,M)
t=linspace(t0,T,Ndt+1);

dt=(T-t0)/Ndt;
eat=exp(-para.a*dt);
last_term=para.sigma*sqrt( (1-eat*eat)/(2*para.a) );

alfa=fm_interp + (para.sigma^2/(2*para.a*para.a))*((1-exp(-para.a*t)).^2);

rM=zeros(M,Ndt+1);
rM(:,1)=rt0;

for k=2:length(t)
    randM=randn(M,1);
    rM(:,k)=rM(:,k-1)*eat + alfa(k) - alfa(k-1)*eat + last_term*randM;    
end




