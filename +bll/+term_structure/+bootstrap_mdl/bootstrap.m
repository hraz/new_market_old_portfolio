function [ ircurve, exitflag ] = bootstrap(ir_type, price, cf, settle, varargin)
%BOOTSTRAP Bootstraps an interest rate curve
%   [ ircurve ] = bootstrap(ir_type, cf, varargin)
%   bootstraps an interest rate curve from market data.  The
%   dates of the bootstrapped curve correspond to the maturity
%   dates of the input instruments.
%
%   Input:
%       ir_type - is a string value specifying the type of the builded curve: Forward or Zero
%
%       cf -  is a (NPAYMENTS+1)xNINSTRUMENTS fts specifying the given cash flow of curve based on
%               bonds. The first row includes the dirty prices at settlement date.
%
%   BOOTSTRAP can also be called with the following optional input
%   parameters, specified as fields in struct, where these
%   input arguments correspond to those in the class definition
%   above.
%
%   Optional input:
%       basis - is a day-count basis. Possible values include:
%                       0 - actual/actual
%                       1 - 30/360 SIA
%                       2 - actual/360
%                       3 - actual/365
%                       4 - 30/360 PSA
%                       5 - 30/360 ISDA
%                       6 - 30/360 European
%                       7 - actual/365 Japanese
%                       8 - actual/actual ISMA
%                       9 - actual/360 ISMA
%                       10 - actual/365 ISMA (default)
%                       11 - 30/360 ISMA
%                       12 - actual/365 ISDA
%                       13 - bus/252
%
%       compounding - is a compounding frequency for curve, acceptable values are -1,1(default),2,3,4,6,12.
%
%       interpmethod - is an interpolation method's name, possible values are: 
%                       'linear','constant', 'pchip' (default), 'spline', 'cubic'.
%
%
%   Further note that simple interest can be specified for an instrument by specifying the
%   compounding as 0. 
%
%   Finally, if instrument basis and instrument compounding are
%   not specified, the following default values are used:
%
%   Output:
%       ircurve - is an object of the class bll.term_structure.ir_data_curve specifying the created
%                       interest rate curve.

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import utility.calendar.*;
    import bll.term_structure.*;

    %% Input validation:
    error(nargchk(4,5,nargin));
    
    % Parse inputs
    p = inputParser;

    p.addRequired('ir_type', @ischar);
    p.addRequired('price', @(x)(isa(x, 'fints')));
    p.addRequired('cf', @(x)(isa(x, 'fints')));
    p.addRequired('settle', @(x)(isnumeric(x) || ischar(x)));

    p.addParamValue('compounding', 1, @(x) all(ismember(x,[-1 1 2 3 4 6 12])));
    p.addParamValue('basis', 10, @(x) all(isvalidbasis(x)));
    p.addParamValue('interpmethod','pchip');

    % Parse method to parse and validate the inputs:
    p.StructExpand = true; % for input as a structure
    p.KeepUnmatched = true;

    try
        p.parse(ir_type, price, cf, settle, varargin{:});
    catch ME
        newME = MException('bll_term_structure_bootsrap_mdl:bootstrap:optionalInputError', 'Error in input arguments');
        newME = addCause(newME,ME);
        throw(newME)
    end
    param.Compounding = p.Results.compounding;
    param.Basis = p.Results.basis;
    param.InterpMethod = p.Results.interpmethod;
        
    %% Step #1 (Construct an object with the inputs):
    Type = ir_type;
    if (~isnumeric(settle))
        Settle = datenum(settle); %cf.dates(1);
    else
        Settle = settle;
    end
    
    Compounding = p.Results.compounding;
    Basis = p.Results.basis;
    InterpMethod = p.Results.interpmethod;

    cf = [-price; cf];
    
    %% Step #2 (Create the cash flow amounts, dates, and time factors):
    BondPrice = -fts2mat(cf(1))';
    CFlowAmounts = fts2mat(cf(2:end))';
    CFlowAmounts(isnan(CFlowAmounts)) = 0;

    BondCFDates = (cf.dates(2:end))';
    BondCFDates = BondCFDates(ones(size(cf, 2), 1), :);
    BondCFDates(CFlowAmounts == 0) = NaN;
    
    BondCFTimes = yearfrac(Settle(ones(size(BondCFDates))), BondCFDates, Basis);

    %% Step #3 (Lay out the cash flows by instrument and by date):
    [CFBondDate, ~, BondTimes] = cfport(CFlowAmounts, BondCFDates, BondCFTimes);

    %% Step #4 (Find the maturity of each cash flow stream (max date of nonzero cash flow)):
    [BondMaturity] = max(BondCFDates .* (CFlowAmounts~=0) ,[],2);

    %% Step #5 (Create bootstrap dates, times and rates):
    FwdYearMats = diff([0;BondTimes]);
    BootDates = unique(BondMaturity(:));
    BootTimes = yearfrac(Settle,BootDates,Basis);
    BootRates = [];

    %% Step #6 (Objective function):
    function yout = bondobjfun(x)
        
        % Import external packages:
        import bll.term_structure.*;
        
        % Create bond discount fractions:
        switch lower(Type)
            case 'forward'
                
                ForwardRatesGuess = [BootRates;x];
                BondForwardRates = ir_data_curve.interp(BootTimes,ForwardRatesGuess,BondTimes,InterpMethod);
                
                % Get discount factors between time zero and CurveDates
                if Compounding == -1
                    BondFwdDF = exp( -BondForwardRates .* FwdYearMats);
                else
                    BondFwdDF = (1 + BondForwardRates/Compounding ).^(-FwdYearMats*Compounding);
                end
                
                % Comput Discounts from settlement
                BondDF = cumprod(BondFwdDF);
                
            case 'zero'
                ZeroRatesGuess = [BootRates;x];
                BondZeroRates = ir_data_curve.interp(BootTimes,ZeroRatesGuess,BondTimes,InterpMethod);
                
                % Get discount factors between time zero and CurveDates
                if Compounding == -1
                    BondDF = exp( -BondZeroRates .* BondTimes);
                else
                    BondDF = (1 + BondZeroRates/Compounding ).^(-BondTimes*Compounding);
                end
            case 'discount'
                
                BondDFTemp = [BootRates;x];
                BondDF = ir_data_curve.interp(BootTimes,BondDFTemp,BondTimes,InterpMethod);
                
        end
        
        % Cash flow calculation:
        predBondPrices = CFBondDate*BondDF;
        
        % Cash flow error calculation:
        yout = BondPrice(:) - predBondPrices(:);
    end

    %% Step #7 (Build start rate values, rate bounds and options structure for interpolation):
    nBondMaturitys = length(BootDates);
    if strcmpi(Type,'discount')
        x0 = .9*ones(nBondMaturitys,1);
    else
        x0 = .05*ones(nBondMaturitys,1);
    end
    lb = zeros(nBondMaturitys,1);
    ub = ones(nBondMaturitys,1);

    options = optimset('lsqnonlin');
    options = optimset(options,'MaxFunEvals',1000, 'display','off');

    %% Step #8 (Interpolation of required rates):
    [NewRates,~,~,exitflag] = lsqnonlin(@bondobjfun,x0,lb,ub,options);

    %% Step #9 (Check the reason of exit from interpolation function):
    if (exitflag == 0)
        warning('dml:ir_data_curve:BondLimitExceeded',...
            ['Function evaluation or iteration limit exceeded for ' ...
            'bootstrapping bond rates.']);
    elseif (exitflag < 0)
        error('dml:ir_data_curve:BondNoSolution',...
            'Cannot solve for bootstrapping bond prices.')
    end

    %% Step #10 (Output creating):
    BootRates = [BootRates;NewRates];
    boot_rate_fts = fints(BootDates, BootRates, [lower(Type), '_coupon_rate']);
    
    %% Step #11 (Create the interest rate data curve on base calculated data curve):
    if (nargin == 3)
        ircurve = ir_data_curve(ir_type, Settle, boot_rate_fts, param );
    else
        ircurve = ir_data_curve(ir_type, Settle, boot_rate_fts );
    end
    
end
