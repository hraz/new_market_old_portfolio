function [ obj, exitflag ] = svensson( ir_type, price, cf, settle, varargin )
%SVENSSON Fits Svensson function to market data
%
%   [ obj ] = svensson( ir_type, price, cf, settle, varargin ) fits a Svensson function to market data.
%
%   The Svensson function for the forward rate is the following:
%
%   f = Beta0 + Beta1*(exp(-t/Tau1)) + Beta2*(t/Tau1).*(exp(-t/Tau1)) + Beta3*(t/Tau2).*(exp(-t/Tau2));
%
%   Input:
%       ir_type - is a string value specifying the type of interest rate curve (Forward, Zero).
%
%       price - is an 1xNBONDS fts specifying the dirty price at settlement date.
%
%       cf - is a NPAYMENTSxNBONDS fts specifying the cash flow of the given portfolio.
%
%   Optional input:
%       basis - is a scalar value specifying the day-count basis. Possible values include:
%                           0	- actual/actual
%                           1	- 30/360 SIA
%                           2	- actual/360
%                           3	- actual/365
%                           4	- 30/360 PSA
%                           5	- 30/360 ISDA
%                           6	- 30/360 European
%                           7	- actual/365 Japanese
%                           8	- actual/actual ISMA
%                           9	- actual/360 ISMA
%                           10 - actual/365 ISMA  (default)
%                           11 - 30/360 ISMA
%                           12 - actual/365 ISDA
%                           13 - bus/252
%
%       compounding -  is a scalar value specifying the the count of coupon payments per year. Acceptable values are
%                                   -1,1(default),2,3,4,6,12.
%
%   Note:   The additional parameters (basis, compounding) may be passed in via some
%               structure with field_name = parameter_name and its value is a parameter_value. 
%
%   Output:
%       obj - is a scalar of ir_func_curve class object.

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import utility.fts.*;
    import utility.*;
    import dal.market.get.dynamic.*;
    import bll.term_structure.*;
    
    %% Input validation:
    error(nargchk(4, 5, nargin));
    if (~ischar(ir_type))
        error('term_structure:svensson:wrongInput', ...
                'Wrong type of the firs parameter.')
    end
    if (~isa(price, 'fints')) || (~isa(cf, 'fints'))
        error('term_structure:svensson:wrongInput', ...
                'Price or Cash_Flow must be financial time series.')
    end
    sz = size(price);
    if (sz(2) ~= size(cf, 2))
        error('term_structure:svensson:wrongInput', ...
                'The number of assets in the Price and the Cash_Flow parameters must be identical.')
    end
    if (sz(1) ~= 1)
        warning('term_structure:svensson:wrongInput', ...
                'The price may include any number of observations, but used just first one (first row).')
    end
    
    %%
    switch lower(ir_type)
        case 'forward'
            func_handle = @svenssonforward;
        case 'zero'
            func_handle = @svenssonzero;
        otherwise
            error('term_structure:svensson:invalidSvenssonType', ...
                'Wrong type of required Svensson interest rate curve.');
    end

    %% Find out if an options structure has been input
    if any(strcmpi(varargin,'IRFitOptions'))
        optidx = find(strcmpi(varargin,'IRFitOptions')) + 1;
        ir_fit_o = varargin{optidx};
        varargin(optidx-1:optidx) = [];
    else
        ir_fit_o = IRFitOptions([5.82 -2.55 -.87 0.45 3.9 0.44], ...
                                            'LowerBound', [0 -Inf -Inf -Inf 0 0], ...
                                            'UpperBound', [Inf Inf Inf Inf Inf Inf], ...
                                            'OptOptions', optimset(optimset('lsqnonlin'),'display','off'), ...
                                            'FitType', 'durationweightedprice');
    end

    %%
    if any(strfind(ir_fit_o.FitType, 'dur'))
        [ fit_bench ] = get_bond_dyn_single_date(strid2numid(tsnames(price)), 'mod_dur', price.dates);
    elseif any(strfind(ir_fit_o.FitType, 'y'))
        [ fit_bench ] = get_bond_dyn_single_date(strid2numid(tsnames(price)), 'ytm', price.dates);
    end
        
    %%
    [ obj, exitflag ] = ir_func_curve.fit_func(ir_type, price(1), cf, settle, func_handle, ir_fit_o, fit_bench, varargin{:});
   
end

function ForwardRate = svenssonforward(t,theta)
%SVENSSONFORWARD Evaluates Svensson equation for the Forward Rate
% 
%   FORWARDRATE = SVENSSONFORWARD(T,THETA)
%
%   Inputs:
%   t: Time To Maturity
%   theta: 1 x 6 vector of parameters (Beta0, Beta1, Beta2, Beta3, Tau1, Tau2)
%          for the Svensson equation
%
%   Outputs:
%   ForwardRate: Forward Rate for each of the input Time To Maturities
%
%   See also SVENSSONFORWARD

% Reference:
%    Svensson, L.E.O. (1994), "Estimating and interpreting forward interest
%     rates: Sweden 1992-4", International Monetary Fund, IMF Working Paper,
%     1994/114

%   Copyright 2008 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2008/05/12 21:25:31 $

    Beta0 = theta(1);
    Beta1 = theta(2);
    Beta2 = theta(3);
    Beta3 = theta(4);
    Tau1 = theta(5);
    Tau2 = theta(6);

    ForwardRate = Beta0 + Beta1*(exp(-t/Tau1)) + Beta2*(t/Tau1).*(exp(-t/Tau1)) + Beta3*(t/Tau2).*((2*t/Tau2) - 1).*(exp(-2*t/Tau2));
end

function ZeroRate = svenssonzero(t,theta)
%SVENSSONZERO Evaluates Svensson equation for the Zero Rate
%
%   ZERORATE = SVENSSONZERO(T,THETA)
%
%   Inputs:
%   t: Time To Maturity
%   theta: 1 x 6 vector of parameters (Beta0, Beta1, Beta2, Beta3, Tau1, Tau2)
%          for the Svensson equation
%
%   Outputs:
%   ZeroRate: ZeroRate for each of the input Time To Maturities
%
%   See also SVENSSONFORWARD

% Reference:
%    Svensson, L.E.O. (1994), "Estimating and interpreting forward interest
%     rates: Sweden 1992-4", International Monetary Fund, IMF Working Paper,
%     1994/114

%   Copyright 2008 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2008/05/31 23:18:27 $

    Beta0 = theta(1);
    Beta1 = theta(2);
    Beta2 = theta(3);
    Beta3 = theta(4);
    Tau1 = theta(5);
    Tau2 = theta(6);

    ZeroRate = Beta0 + Beta1.*(1-exp(-t./Tau1)).*(Tau1./t) + Beta2.*((1-exp(-t./Tau1)).*(Tau1./t) - exp(-t./Tau1)) + ...
                        Beta3.*((1-exp(-t./Tau2)).*(Tau2./t) - exp(-2*t./Tau2));
    ZeroRate(t == 0) = Beta0 + Beta1;
    
    ZeroRate = ZeroRate/100;
end