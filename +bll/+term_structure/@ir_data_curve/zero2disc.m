function [ df req_dates ] = zero2disc( curve_settle, rates, req_dates, req_settle, cur_period, cur_basis, req_period )
%ZERO2DISC converts zero rates to discount factors at given dates.
%
%   It is necessary for simple interest and for deposits with different start dates.
%
%   [ df req_dates ] = zero2disc( curve_settle, rates, req_dates, req_settle, cur_period, cur_basis, req_period )
%
%   Input:
%       curve_settle - is a scalar value specifying the current curve settlement date.
%
%       rates - is an NBASEBONDSx1 vector specifying the current rates in the object.
%
%       req_dates - is an NBASEBONDSx1 vector specifying the required days.
%
%       req_settle - is a scalar value specifying the required settlement date.
%
%       cur_period - is a scalar value specifying the current compounding period.
%
%       cur_basis - is a scalar value specifying the current day-count base.
%
%       req_period - is a scalar vaue specifying the required compounding period.
%
%   Output:
%       df - is a NBASEDATESx1 vector specifying the required discount factors.
%
%       req_dates - is an NBASEBONDSx1 vector specifying the required days.
%
% Yigal Ben Tal
% Copyright 2012

    %% DF calculation according to compounding frequency:
    req_year_mats = yearfrac( req_settle, req_dates,cur_basis );
    if ( cur_period == -1 )
        df = exp( -rates .* req_year_mats ); %Continuous compounding
    elseif (cur_period == 0)
        df = 1 ./ ( 1 + rates .* req_year_mats ); %Simple interest
    else
        df = ( 1 + rates ./ cur_period ) .^ ( - req_year_mats .* req_period ); %Discrete compounding
    end

    %% Find any DF's with settles after curve_settle, this needs to be a loop as they can occur sequentially:
    for depidx = 1 : length( rates )
        if req_settle(depidx) > curve_settle
            df(depidx) = df(depidx) * interp1( req_dates, df, req_settle(depidx) );
        end
    end

end

