function [ fwd_rate dates] = zero2fwd( curve_settle, rates, dates, req_settle, cur_period, cur_basis, req_period, req_basis )
%ZERO2FWD converts zero rates to forward rates at given dates.
%
%   It is necessary to handle simple compound interest.
%
%   [ fwd_rate dates] = zero2fwd( curve_settle, rates, dates, req_settle, cur_period, cur_basis, req_period, req_basis)
%
%   Input:
%       curve_settle - is a scalar value specifying the current curve settlement date.
%
%       rates - is an NBASEBONDSx1 vector specifying the current rates.
%
%       dates - is an NBASEBONDSx1 vector specifying the current days.
%
%       req_settle - is a scalar value specifying the required settlement date.
%
%       cur_period - is a scalar value specifying the current compounding period.
%
%       cur_basis - is a scalar value specifying the current day-count base.
%
%       req_period - is a scalar vaue specifying the required compounding period.
%
%   Output:
%       fwd_rate - is a NBASEDATESx1 vector specifying the required forward rates.
%
%       dates - is an NBASEBONDSx1 vector specifying the required days.
%
% Yigal Ben Tal
% Copyright 2012

    if cur_period == 0
        %% Get values for T in terms of fractional years from the settlement date:
         [ df_from_settle ] = loczero2disc( curve_settle, rates, dates, req_settle, cur_period, cur_basis, req_period, req_basis );

        %% Compute Forward Discounts:
        fwd_df = [ df_from_settle(1); df_from_settle(2:end) ./ df_from_settle(1:end-1) ];

        %% Convert forwad discounts to forward rates:
        req_year_mats = yearfrac( curve_settle, dates, req_basis );
        fwd_year_mats = [ req_year_mats(1); diff(req_year_mats) ];

        if req_period == -1
            fwd_rate = -log( fwd_df ) ./ fwd_year_mats;
        else
            fwd_rate = req_period * ( fwd_df .^ (-1 ./ (fwd_year_mats * req_period)) - 1);
        end
    else
        [fwd_rate dates] = zero2fwd( rates, dates , req_settle, cur_period, req_basis, req_period, req_basis);
    end
end  