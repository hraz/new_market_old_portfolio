function [ yi ] = interp(x,y,xi,method)
%LOCINTERP Wrapper for INTERP1 function
%   LOCINTERP is necessary to piecewise constant interpolation.
%
%   ================================ 
%   This function based on the locInterp MATLAB 
%   function from class IRDataCurve.
%   ================================
%
%   [ YI ] = INTERP(X,Y,XI,METHOD) 
%   interpolates to find YI, the values of the underlying function Y at the points in the array XI.
%   X must be a vector of length N.
%   If Y is a vector, then it must also have length N, and YI is the
%   same size as XI.  If Y is an array of size [N,D1,D2,...,Dk], then
%   the interpolation is performed for each D1-by-D2-by-...-Dk value
%   in Y(i,:,:,...,:).
%   If XI is a vector of length M, then YI has size [M,D1,D2,...,Dk].
%   If XI is an array of size [M1,M2,...,Mj], then YI is of size
%   [M1,M2,...,Mj,D1,D2,...,Dk].
%
%   The default is linear interpolation. Use an empty matrix [] to specify
%   the default. Available methods are:
%
%     'nearest'  - nearest neighbor interpolation
%     'linear'   - linear interpolation
%     'spline'   - piecewise cubic spline interpolation (SPLINE)
%     'pchip'    - shape-preserving piecewise cubic interpolation
%     'cubic'    - same as 'pchip'


    if any(strcmpi(method,{'linear','nearest','spline','pchip', 'cubic'}))
        yi = interp1(x,y,xi,method,'extrap');
    else
        yi = zeros(size(xi));
        for j=1:length(xi)
            i_ind = find(xi(j) > x,1,'last');
            if isempty(i_ind),i_ind = 1;end
            yi(j) = y(i_ind);
        end;
    end
end

