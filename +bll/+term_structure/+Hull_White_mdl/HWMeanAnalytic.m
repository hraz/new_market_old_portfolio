function [r std_of_r] = HWMeanAnalytic(rt0,para,fm_interp,t0,T,Ndt)
a=para.k;
[alfa_0 alfa_t]=
r=rt0*exp(-a*(T-t0)) + alfa_t-alfa_0*exp(-a*(T-t0));
std_of_r=para.sigma*sqrt((1-exp(-2*para.k*(T-t0)))/(2*para.k));