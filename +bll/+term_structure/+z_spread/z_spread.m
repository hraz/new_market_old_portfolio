function [ zSpread ExpInt ] = z_spread( SpotRateData, nsin, rdate)
% This function calculates the z-spread of a given bond based on a given
% term structure.  It is appropriate only for fixed, non linked bonds at
% the moment.

% SpotRateData  - [fts]
%             First column is the SpotDate, and second column is the
%             zero-rate corresponding to maturities on the SpotDate.
%
%             (It is highly recommended that the spot-rates are spaced as
%             evenly apart as possible, perhaps one that is built from
%             3-months deposit rates.)
%
%            Bondnsin - nsin of bond for which we want to find the z-spread
%
%            rdate - date at which price is wanted
%
%            CompFreq - frequency that coupon is computed.  Default value
%            is 1 (annual, allows for variable coupons). 
%
%
%           Output: 
%
%           zSpread - scalar of spread over spot rate curve
%
%           ExpInt - scalar of  expected interest for given rdate                

%% computing frequency default is 1      


      CompFreq = 1;


%% Extract data from fts input

Dates = SpotRateData.dates;
Rates = fts2mat(SpotRateData);


%% interpolate curve data to get points equally spaced 1-month apart
%eventually place all of these in a different script to be handed over
%to main function along with term structure data

BeginDate = datenum(Dates(1));
EndDate = datenum(Dates(end));
CurveDates = BeginDate:30:EndDate;

%get zero rates by interpolation (linear) equally spaced in 30 day
%intervals
ZeroRates = interp1(Dates, Rates, CurveDates);

  

%% Get necessary info for given bond
import dal.market.get.cash_flow.*;
import dal.market.get.dynamic.*;
import dml.*;

Settle = CurveDates(1);

num_nsin = length(nsin);
num_dates = size(rdate);  %if given more than one date for which 

zSpread = NaN*ones(num_nsin,1);
ExpInt = NaN*ones(num_nsin, num_dates(1));

for k=1:1:num_nsin
    
[bond_cf] = get_cash_flow(Settle, EndDate, nsin(k));
    num_inst = size(bond_cf, 2);
fin_pay = bond_cf.dates(end);
    newdates = datenum(rdate);%write dates as numbers

for j=1:1:num_dates
    
      if fin_pay<newdates(j)
    
     fprintf('Warning: rdate %i for %i is past final payment date. \n', newdates(j), nsin(k));
    

      elseif newdates(j)< Settle
             fprintf('Warning: rdate %i for %i is before existence of current term structure. \n', newdates(j), nsin(k));
 
      else
        % get price for appropriate day

data_name = 'price_clean';
 [ fts_price ] = get_bond_dyn_single_date( nsin(k) , data_name , Settle );
 Price = fts2mat(fts_price);
    
 
% check if spotrate data is obsolete
% if min(CurveDates) < Settle
%     error('Finance:bndspread:invalidSettle',...
%         'First spot rate data must be on, or after Settle.')
% end

% appending the spot rate at Settle if not yet done
% it is going to be equal to the shortest maturity
% as measured from Settle.


if min(CurveDates) > Settle
    CurveDates(2:end+1) = CurveDates;
    CurveDates(1) = Settle;
    ZeroRates(2:end+1) = ZeroRates;
end

    %% Step #1 (Create the cash flow amounts, dates, and time factors of the given bond):
    CFAmounts = fts2mat(bond_cf)';
    CFAmounts(isnan(CFAmounts)) = 0;
    
    %set coupon rate to be vector of cash flows (allows for variable coupon
    %rates)
    CouponRate = CFAmounts;
    
    CFDates = (bond_cf.dates)'; %dates of payments
    CFDates = CFDates(ones(num_inst, 1), :);
    CFDates(CFAmounts == 0) = NaN;  % not dealing with linked bonds at the
    %moment
    
    CFTimes = yearfrac(Settle, CFDates); %time from settle to each payment

    %% Step #2 calculate z-spread
    
    % Get spot rates at cash flow dates
CFSpotRates = interp1(CurveDates, ZeroRates, CFDates);

if sum(isnan(CFSpotRates) )
        warning('Finance:Bond payment range beyond spot rate range.\n');
end

    
CFlowAmounts = CFAmounts;
% Subtract price so Spread will make NPV = 0
%CFlowAmounts(:,1) = CFAmounts(:,1) - Price;

% Spread is initially guessed to be close to the coupon at each period

x0 = CouponRate(1)/Price;

% setting an option to suppress optimization results
fZeroOptions = optimset('fzero');
fZeroOptions = optimset(fZeroOptions,'Display','off');
fZeroOptions = optimset(fZeroOptions,'TolX',1e-12);

% Also, set options for FSOLVE -- in case FZERO does not converge
fSolveOptions = optimset('fsolve');
fSolveOptions = optimset(fSolveOptions, 'TolX', 1e-14);
fSolveOptions = optimset(fSolveOptions, 'TolFun', 1e-12);
fSolveOptions = optimset(fSolveOptions, 'Display', 'off');
fSolveOptions = optimset(fSolveOptions, 'LargeScale', 'off');

% Loop through each bond to find the spread
nBonds = length(Price);
X = NaN*ones(nBonds,1);
EXITFLAG = zeros(nBonds,1);

% Figure out the compounding frequency here
% if isnan(CompFreq)
%     CompFreq = 2*ones(nBonds,1);
%     if isnan(DiscountBasis)
%         i = isisma(Basis);
%     else
%         i = isisma(DiscountBasis);
%     end
%     CompFreq(i) = 1;
% end

for bondidx = 1:nBonds
    
    objfun = @(x) nansum(CFlowAmounts(bondidx,:).*1 ./( 1 + ...
        (CFSpotRates(bondidx,:) + x)./CompFreq(bondidx)).^CFTimes(bondidx,:)) - Price;
    
    % FZERO will iteratively solve for the value of OAS (X) that will make
    % the price of the bond equal to given market clean price
    [X(bondidx),~,EXITFLAG(bondidx)]= fzero(objfun, x0(bondidx), fZeroOptions);
    
    % if fzero was unable to find a zero, try using fsolve
    if (EXITFLAG(bondidx) < 0)
        [X(bondidx),~,EXITFLAG(bondidx)] = fsolve(objfun, x0(bondidx), fSolveOptions);
    end
    
end

if any(EXITFLAG <0)
    warning('Finance:bndspread:solutionConvergenceFailure',...
        'Could not solve for the spread for at least one of the bonds.\n');
end

% output returned as z-spread over curve, decimal form
zSpread(k) = X;

if zSpread(k)  > 0.15
    fprintf('Warning: Z-Spread for %i is high\n', nsin(k));
end

if imag(zSpread(k))
        fprintf('Warning: Z-spread for %i is imaginary. Check data. \n', nsin(k));
end

if zSpread(k) <0
    fprintf('Warning: Z-spread for %i is negative\n', nsin(k));
end

ExpInt(k,j) = interp1(Dates, Rates, newdates(j)) + zSpread(k);


    end

end



end

end
