clear; clc;

%% Import external packages:

import bll.term_structure.z_spread.*;
import bll.term_structure.bootstrap_mdl.*;
import dal.data_acess.cash_flow.*;

%% Build zero spot curve .
%can use what ever model, here we use bootstrap

settle = datenum('2010-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.

param.compounding = 1;
param.basis = 10;
param.interpmethod = 'cubic';

%% Cash flow build:
[ tbill_cf ] = cfbuilder(settle, 0);
[ shahar_cf ] = cfbuilder(settle, 1);
gov_cf = merge(tbill_cf, shahar_cf);

%% Build zero-coupon curve:

[ gov_ircurve ] = bootstrap( 'zero', gov_cf, param );

%% calculate z-spread

rdate =  ['2011-01-25'; '2011-03-24'] %rdate at which z-spread is wanted

nsin = [1095157;
     1095207];
%      1099456;
%      1101013;
%      1101575;
%      1102268;
%      1102730;
%      1104405;
%      1105329;
%      1106913;
%      1107788;
%      1108422;
%      1109826;
%      1109859;
%      1110634;
%      1110907]; %nsin of bond for which z-spread is wanted

[zSpread ExpInt ] = z_spread(gov_ircurve.Data, nsin, rdate);

num_nsin = length(nsin);
for k=1:1:num_nsin
    
bond_rate = fts2mat(gov_ircurve.Data) + zSpread(k);
bond_dates = gov_ircurve.Data.dates;
figure;
plot(gov_ircurve.Data, '.-r')
hold on
plot(bond_dates, bond_rate, '--b');
end