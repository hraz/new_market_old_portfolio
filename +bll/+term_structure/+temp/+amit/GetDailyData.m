function [market_rates dates]=GetDailyData(months,lower_date,upper_date)
%GetDailyData import data from the BI database for short term makam yeilds
%while throwing 'bad' dates (dates with Nans)
% 
% Input  - months - the desired period for the yeild (1-month makam,
%                                        2-months makam, etc...)
%                    lower_date, upper date - desired date range
% 
% Output - market rates - NDATESX1 array of the deisred yeilds.
%                     dates - NDATESX1 array of the dates existing in the desired time range.
% 
% Amit Godel
% Copyright 2012

import dal.market.get.dynamic.*;

 [ fts ] = da_shortTB_yield( months, lower_date, upper_date );

 d=fts2mat(fts,1);
 d=d(find(~isnan(d(:,2))),:);
 
 market_rates=d(:,2);
 dates=d(:,1);

