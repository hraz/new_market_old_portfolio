clear; clc;

import bll.term_structure.temp.amit.*;
import utility.graphs.*;

lower_date = '2005-03-01';
upper_date = '2012-01-19';
months=3;

[market_rates dates]=GetDailyData(months,lower_date,upper_date);

dplot(dates,market_rates,'.')