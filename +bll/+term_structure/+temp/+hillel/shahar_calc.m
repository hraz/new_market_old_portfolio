function [ ttm_shahar ytm_shahar ] = shahar_calc( nsin, rdate, ttm, ytm )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    import dal.mysql.connection.*;
    import utility.dal.*;
    import dal.market.get.dynamic.*;
    import dal.market.get.cash_flow.*;


ttm_leng= size(ttm);
ttm_shahar = [ttm; 0];
ytm_shahar = [ytm; 0];

finish_date = datenum(today) + 15*365 + 4;
%15 years from now - including leap years... (shahar max ttm = 15 years)

date_now = datenum(rdate);% can change to 'today'

[ fts_cf ] = get_cash_flow(rdate, finish_date, nsin); 
%get cash flow information upto finish date

num_cf_dates = size(fts_cf.dates); %number of payments left

 cash_dates = (fts_cf.dates - date_now)/365; %time to next payment from rdate in years
rates = zeros(num_cf_dates(1), 1); %keep appropriate interest rate for each date
 
ttm_shahar(ttm_leng(1) + 1) = cash_dates(num_cf_dates(1));
%add ttm of new shahar to ttm list, will sort later if order isn't
%montonously increasing 

cash_flow = fts2mat(fts_cf); %matrix of cash flows 

 data_name = 'price_clean';
[ fts_price ] = da_bond_dyn( nsin, data_name, rdate );
%get price of bond
 
 updated_price = fts2mat(fts_price(rdate)); %price right now
 
 calc_price = updated_price;
 %will be calculate as price minus each cashflow calculated with interest
 
for i=1:1:(num_cf_dates(1)-1)

 if cash_dates(i) <= ttm(ttm_leng(1))
        %as long as ttm hasn't passed information we have from previous
        %bonds
        
        for j=1:1:ttm_leng(1)
           % calculating from the large ttms to the smaller, find two ttms which cash_dates(i) falls in between  
           %and then calculate the right rate
            if j == ttm_leng(1)
            % in this case take the closest point - i.e. the smallest
            % ttm and its corresponding ytm
            
            rates(i) =   ytm(1);  
           %calculate rate
            elseif cash_dates(i) >= ttm(ttm_leng(1)-j)
            
            slope = ( ytm(ttm_leng(1) - j +1) - ytm(ttm_leng(1) - j) )/( ttm(ttm_leng(1) - j + 1) - ttm(ttm_leng(1) - j) );
            % slope of interest line
            
            rates(i) = ytm(ttm_leng(1) - j +1) + slope*( cash_dates(i) - ttm(ttm_leng(1) - j + 1) );
            %calculate rate using line slope formula
            break;
           
            end
        
        end    

elseif (cash_dates(i) > ttm(ttm_leng(1)) ) && (num_cf_dates(1)-i) < 2.01 && (num_cf_dates(1)-i) > 0.01
    %in case there are only two payments left and they are both beyond the
    %domain of ttm's, estimate the next one by a straight line
       
        rates(i) = ytm(ttm_leng(1)); %estimate this one by straight line
        
        ttm_shahar = [ttm_shahar; cash_dates(i); 0]; %add this extra info point
        ytm_shahar = [ytm_shahar; ytm(ttm_leng(1)); 0];

 end
    

   ttm_leng = size(ttm_shahar) - 1; 
 
 
end

if num_cf_dates(1)>1
    
for j=1:1:(num_cf_dates(1)-1)
    
     calc_price =  calc_price - cash_flow(j)/( (1+rates(j))^cash_dates(j) ) ;
 %calculate price after _th payment has been received
 
end

end


  ytm_shahar(ttm_leng(1) + 1) = ( cash_flow(num_cf_dates(1))/calc_price )^( 1/cash_dates(num_cf_dates(1)) ) - 1;
  %final rate
  

if ttm_shahar(ttm_leng(1) + 1) < ttm_shahar(ttm_leng(1))
    %reorder in increasing order
    
    ttm_new = zeros(ttm_leng(1) + 1, 1);
    ytm_new =  ytm_shahar;
    
    [ttm_new indx_sorted] = sort(ttm_shahar);
    
   for k=1:1:ttm_leng(1)+1
       
    ytm_shahar(k) = ytm_new(indx_sorted(k));
   end
   
ttm_shahar = ttm_new;

end




    
  
    %cash dates in terms of years from now
    
%     if cash_dates(i) <= ttm_large && i < num_cf_dates(1) 
%         % we have rates upto ttm_large
%         
% XData=get(get(gca,'children'),'XData'); 
% YData=get(get(gca,'children'),'YData'); 
% rates(i) = interp1(XData,YData,cash_dates(i),'spline');
% %get yield rate from graph of term structure upto this date