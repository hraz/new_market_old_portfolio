% Script calculates term structure given shahar and makam, and then
% produces Z-Spread for given bond list.  It produces warning signs if the
% z-spread is too high (>0.15) or negative.  

clear; clc;

%% Import external packages:
import bll.term_structure.*;
import bll.term_structure.zero_coupon_mdl.*
import dal.market.get.static.*
import dal.market.filter.static.*;
import dml.*;

%% Build start parameters: choose date from which to begin con

settle = datenum('2008-12-21'); % The minimum possible date is 02-01-2005 according to given data from database.

%% Cash flow build:
[ stbill_cf ] = cfbuilder(settle, 0);
[ shahar_cf ] = cfbuilder(settle, 1);
gov_cf = merge(stbill_cf, shahar_cf);


figure(3);
[zero_rates_gov] = zbprice( gov_cf );
plot(zero_rates_gov, '.-')

%% calculate z-spread

rdate =  '2009-01-25'; %rdate at which yield is wanted
% 
% param.class_name = {};
% param.coupon_type = {'Fixed Interest'};
% param.bond_type = {'Corporate Bond'};
% 
% nsin = bond_static_complex( rdate, param ); %enter nsin for bond
nsin = [9267139 ];
NumNsin = length(nsin);
data = zeros(NumNsin, 3);
data(:, 1) = nsin;


for k=1:1:NumNsin
[zSpread ExpInt ] = z_spread(zero_rates_gov, data(k, 1), rdate);


data(k, 2) = zSpread;

data(k, 3) = ExpInt;

end
