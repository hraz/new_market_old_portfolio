function [ BondFinVal ] = BondInvAtYieldRate( Bond, t_0, t_f )
%calculates total value given back to customer for a given Bond, given time
%of investment.

%Extract info from FTS of bond, in particular need BondPrice0, BondYield,
%BondMaturity, BondPriceT, CoupVal (vector of coupons plus early
%redemptions at given dates)
%and CoupDates (vector of dates descring when coupons/early redemption are given)

TotValCoup = 0; %total value of all coupons including interest

if BondMaturity <= t_f 


for t=1:1:length(CoupDateS)

    if CoupDates(t) <= t_f %as long as we are before final time 
        
    TotValCoup = TotValCoup + CoupVal(t)*(1 + BondYield)^( (t_f - CoupDates(t))/365 );
    end
    
end

BondFinVal = TotValCoup + BondPrice0 + CoupVal; %Bond is redeemed 

BondFinVal = BondFinVal*(1 + BondYield)^(t_f-BondMaturity); %if there is time left, then continue calculating interest
    
end

% if bond maturity is longer than time planned for portfolio

if BondMaturity > t_f
    
for t=1:1:length(CoupDateS)
    
    if CoupDates(t) <= t_f %as long as we are before final time 
        
    TotValCoup = TotValCoup + CoupVal(t)*(1 + BondYield)^( (t_f - CoupDates(t) )/365 );

    end
    
end
    
    BondFinVal = TotValCoup + BondPriceT; %add price of bond at current time, note that we do not take account of x-date

end

end

