clear; clc;

import dal.market.filter.static.*;
import dal.market.get.static.* 
import dal.market.get.dynamic.*;
import dal.market.get.cash_flow.*;

import utility.*

import dml.*;

rdate = '2011-03-23';
global BASE_DATA;
BASE_DATA = base_data(rdate);

%% Coupon Type + Linkage + Sector + Bond Type:

 filter_name = 'type';
% filter_cond = {'Convertable'};
 filter_cond = {'Short Term Treasury Bill'};
% filter_cond = {'Government Bond - Shachar'};

[ nsin_makam ] = bond_static_partial( filter_name, filter_cond, rdate );%makam


filter_cond = {'Government Bond - Shachar'};

[ nsin_shahar ] = bond_static_partial( filter_name, filter_cond, rdate );%shachar

%% Get data static

 [ ds ] = get_bond_stat( nsin_makam, rdate );

 %% Create Bonds matrix
 
NumBonds =  length(nsin_makam) + length(nsin_shahar);

Bonds = zeros(NumBonds, 5);
 
%% Get data Dyn

data_name = 'ttm';

 [ fts ] = da_bond_dyn( nsin_makam, data_name, rdate );
 
t = fts(rdate);
ttm = fts2mat(t)'; %extract ttm for relevant day and put in matrix form

 Bonds(:, 1) = ttm;
  
 CouponRateArb = 1; %arbitrary coupon rate - won't be used
 
 Bonds(:, 2) = CouponRateArb;
 
 FaceVal = 100; %arb facevalue - won't be used
 
 Bonds(:, 3) = FaceVal;
 
 CouponFrequencyShahar = 1;
CouponFrequencyMakam = 0; %coupon frequencies for bonds

Bonds(:, 4) = CouponFrequencyShahar;

BondBaseValue = 10; %value specifying how coupon is calculated -  10 - actual/365 ISMA
Bonds(:, 5) = BondBaseValue;


CouponRateNew = %matrix of coupon rates per bond

 data_name = 'ytm';

 [ fts ] = da_bond_dyn( nsin_makam, data_name, rdate );

 y = fts(rdate);
 
 ytm = fts2mat(y)'; %extract ytm for relevant day and put in matrix form