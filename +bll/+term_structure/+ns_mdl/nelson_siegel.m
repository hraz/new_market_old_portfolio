function [ obj, exitflag ] = nelson_siegel(ir_type, price, cf, settle, varargin)
%NELSON_SIEGEL Fits Nelson Siegel function to market data
%
%   [ obj ] = nelson_siegel(ir_type, price, cf, varargin) fits a Nelson Siegel function to market
%   data. 
%
%   The Nelson Siegel function for the forward rate is the following:
%
%   f = Beta0 + Beta1*(exp(-t/Tau1)) + Beta2*(t/Tau1).*(exp(-t/Tau1))
%
%   Input:
%       ir_type - is a string value specifying the type of interest rate curve (Forward, Zero).
%
%       price - is an 1xNBONDS fts specifying the dirty price at settlement date.
%
%       cf - is a NPAYMENTSxNBONDS fts specifying the cash flow of the given portfolio.
%
%   Optional input:
%       basis - is a scalar value specifying the day-count basis. Possible values include:
%                           0	- actual/actual
%                           1	- 30/360 SIA
%                           2	- actual/360
%                           3	- actual/365
%                           4	- 30/360 PSA
%                           5	- 30/360 ISDA
%                           6	- 30/360 European
%                           7	- actual/365 Japanese
%                           8	- actual/actual ISMA
%                           9	- actual/360 ISMA
%                           10 - actual/365 ISMA  (default)
%                           11 - 30/360 ISMA
%                           12 - actual/365 ISDA
%                           13 - bus/252
%
%       compounding -  is a scalar value specifying the the count of coupon payments per year. Acceptable values are
%                                   -1,1(default),2,3,4,6,12.
%
%   Note:   The additional parameters (basis, compounding) may be passed in via some
%               structure with field_name = parameter_name and its value is a parameter_value. 
%
%   Output:
%       obj - is a scalar of ir_func_curve class object.

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import utility.fts.*;
    import utility.*;
    import dal.market.get.dynamic.*;
    import bll.term_structure.*;
    
    %% Input validation:
    error(nargchk(4, 5, nargin));
    if (~ischar(ir_type))
        error('term_structure:nelson_siegel:wrongInput', ...
                'Wrong type of the firs parameter.')
    end
    if (~isa(price, 'fints')) || (~isa(cf, 'fints'))
        error('term_structure:nelson_siegel:wrongInput', ...
                'Price or Cash_Flow must be financial time series.')
    end
    sz = size(price);
    if (sz(2) ~= size(cf, 2))
        error('term_structure:nelson_siegel:wrongInput', ...
                'The number of assets in the Price and the Cash_Flow parameters must be identical.')
    end
    if (sz(1) ~= 1)
        warning('term_structure:nelson_siegel:wrongInput', ...
                'The price may include any number of observations, but used just first one (first row).')
    end

    %%
    switch lower(ir_type)
        case 'forward'
            func_handle = @nelsonsiegelforward;
        case 'zero'
            func_handle = @nelsonsiegelzero;
        otherwise
            error('term_structure:nelson_siegel:invalidNelsonSiegelIRtype', ...
                'Wrong type of required Nelson_Siegel interest rate curve.');
    end

    %% Find out if an options structure has been input
    if any(strcmpi(varargin,'IRFitOptions'))
        optidx = find(strcmpi(varargin,'IRFitOptions')) + 1;
        ir_fit_o = varargin{optidx};
        varargin(optidx-1:optidx) = [];
    else
        ir_fit_o = IRFitOptions([7 -3 -2 3], ...
                                            'LowerBound', [0 -Inf -Inf 0], ...
                                            'UpperBound', [Inf Inf Inf Inf], ...
                                            'FitType', 'durationweightedprice', ...
                                            'OptOptions', optimset(optimset('lsqnonlin'),'display','off'));
    end
    
    %%
    if any(strfind(ir_fit_o.FitType, 'dur'))
        [ fit_bench ] = get_bond_dyn_single_date(strid2numid(tsnames(price)), 'mod_dur', price.dates);
    elseif any(strfind(ir_fit_o.FitType, 'y'))
        [ fit_bench ] = get_bond_dyn_single_date(strid2numid(tsnames(price)), 'ytm', price.dates);
    end
        
    %%
    [ obj, exitflag ] = ir_func_curve.fit_func(ir_type, price(1), cf, settle, func_handle, ir_fit_o, fit_bench, varargin{:});
    
end

function ForwardRate = nelsonsiegelforward(t,theta)
%NELSONSIEGELFORWARD Evaluates Nelson Siegel equation for the Forward Rate
%
%   ForwardRate = nelsonsiegelforward(t,theta)
%
%   Inputs:
%   t: Time To Maturity
%   theta: 1 x 4 vector of parameters (Beta0, Beta1, Beta2, Tau1)
%          for the Nelson Siegel equation
%
%   Outputs:
%   ForwardRate: Forward Rate for each of the input Time To Maturities
%
%   See also NELSONSIEGELZERO

% Reference:
%    Nelson, C.R., Siegel, A.F., (1987), "Parsimonious modelling of yield
%     curves", Journal of Business, 60, pp 473-89 

%   Copyright 2008 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2008/05/12 21:25:29 $

    Beta0 = theta(1);
    Beta1 = theta(2);
    Beta2 = theta(3);
    Tau1 = theta(4);

    ForwardRate = Beta0 + Beta1*(exp(-t/Tau1)) + Beta2*(t/Tau1).*(exp(-t/Tau1));
    ForwardRate(t == 0) = Beta0 + Beta1;
    ForwardRate = ForwardRate/100;
end

function ZeroRate = nelsonsiegelzero(t,theta)
%NELSONSIEGELZERO Evaluates Nelson Siegel equation for the Zero Rate
%   ZeroRate = nelsonsiegelzero(t,theta)
%
%   Inputs:
%   t: Time To Maturity
%   theta: 1 x 4 vector of parameters (Beta0, Beta1, Beta2, Tau1)
%          for the Nelson Siegel equation
%
%   Outputs:
%   ZeroRate: ZeroRate for each of the input Time To Maturities
%
%   See also NELSONSIEGELFORWARD

% Reference:
%    Nelson, C.R., Siegel, A.F., (1987), "Parsimonious modelling of yield
%     curves", Journal of Business, 60, pp 473-89

%   Copyright 2008 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2008/05/31 23:18:26 $

    Beta0 = theta(1);
    Beta1 = theta(2);
    Beta2 = theta(3);
    Tau1 = theta(4);

    ZeroRate = Beta0 + (Beta1 + Beta2) .*(Tau1./t).*(1-exp(-t./Tau1)) - Beta2 .* exp(-t./Tau1);
    ZeroRate(t == 0) = Beta0 + Beta1;
    ZeroRate = ZeroRate/100;
end