function err_graphs(market_price, fitted_price, fit_error, selection, req_selection)
    
    %% Import external packages:
    import utility.graphs.*;

    %% Input validation:
    
    %% Step #1 (Data size determination):
    scrsz = get(0,'ScreenSize');
    
    %% Step #2 (Graphs creation):
    switch lower(selection)
        case 'assets' % through simulations            
            for k = 1: size(market_price,2)
                h = figure(k); 

                fgr_sz = [8, 38, (scrsz(3)-14) , (scrsz(4)-125)];
                set(h, 'Position',fgr_sz) %[left, bottom, width, height]:

                subplot(2,1, 1); 
                plot(market_price{req_selection,k}, '.-k'); hold on; legend off;
                plot(fitted_price{req_selection,k}); hold on; legend off;
                title(['Simulated Prices vs. Market Prices, ', market_price{req_selection,k}.desc]);
                
                subplot(2,1, 2); 
                plot(fit_error{req_selection,k}); hold off; legend show;
                title('Model Errors');
            end
        case 'simulations' % through assets
            for j = 1: size(market_price,1)
                h = figure(j); 

                fgr_sz = [8, 38, (scrsz(3)-14) , (scrsz(4)-125)];
                set(h, 'Position',fgr_sz) %[left, bottom, width, height]:

                subplot(2,1, 1);
                plot(market_price{j,req_selection}, '.-k'); hold on; legend off;
                plot(fitted_price{j,req_selection}); hold on; legend off;
                title(['Simulated Prices vs. Market Prices, ', market_price{j, req_selection}.desc]);
                
                subplot(2,1, 2);
                plot(fit_error{j,req_selection}); hold off; legend show;
                title('Model Errors');
            end
        otherwise
            error('err_graphs:wrongInput', 'Selection must be ''assets'' or ''simulations''.');
    end
    
end