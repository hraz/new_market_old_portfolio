function [nsin, ttm] = max_ttm_asset(nsin, ttm)
    if (~isempty(nsin))  && (~isempty(ttm))
        [ttm, i] = max(ttm);
        nsin = nsin(i);
    end
end