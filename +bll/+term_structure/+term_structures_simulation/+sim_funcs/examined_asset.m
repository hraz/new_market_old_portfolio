function [ nsin, ttm ] = examined_asset( req_date, stat_param, dyn_param )
%EXAMED_ASSET returns nsin and ttm of the filtered asset.

    %% Import external packages:
    import dal.market.filter.total.*;
    import dal.market.get.dynamic.*;
    
    %% Input validation:
    error(nargchk(3,3,nargin));
    
    %% Step #1 (Primary asset filtration):
    nsin = bond_ttl( req_date, stat_param,  dyn_param);

    %% Step #2 (Find asset with max ttm): 
    if (~isempty(nsin))   
        ttm = fts2mat(get_bond_dyn_single_date( nsin, 'ttm', req_date ))';
    else
        ttm = [];
    end
end

