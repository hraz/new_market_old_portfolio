function [] = err_stat_graphs(data, yname, leg, flag )
%ERR_STAT_GRAPHS Summary of this function goes here
%   Detailed explanation goes here

    %% Import external packages:
    import utility.fts.*;

    %% Input validation:
%     error(nargchk(2,2,nargin));
    
    %% Step #1 (Basic parameters initialization):
    obs = (1:1:length(data{1}))';

    %% Step #2 (Graph representation):
    switch flag
        case {'assets', 'simulations'}
            for j = 1 : length(data)
                figure(j);
                plot(obs, fts2mat(data{j}));
                title(strrep(data{j}.desc, 'id', ''));
                xlabel('Observations'); ylabel(yname);
                legend(leg);
%                 legend(strrep(tsnames(data{j}), '_', ''));
            end
        case 'models'
            model = tsnames(data{1});
            for k = 1 : size(data{1},2)
                graphs = cell(1,length(data));
                legends = cell(1, length(data));
                for j = 1: length(data)
                    graphs{:,j} = fts2mat(data{j}.(model{k}));
                    legends{j} = strrep(data{j}.desc, 'id', '');
                end
                figure(k);
                plot(obs, cell2mat(graphs));
                legend(leg);
%                 legend(cellstr([legends{:}]));
                title(upper(strrep(model{k}, '_', '-')));
                xlabel('Observations'); ylabel(yname);
            end
        otherwise
            
    end
    
end

