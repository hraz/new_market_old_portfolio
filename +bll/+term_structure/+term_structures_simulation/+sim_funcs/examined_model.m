function [ ircurve, exitflag ] = examined_model(model_name, curve_type, settle, coupon_type, param)
%EXAMED_MODEL Summary of this function goes here
%   Detailed explanation goes here

    %% Import external packages:
    import utility.cash_flow.*;
    import dal.market.get.dynamic.*;
    
    %% Input validation:
    error(nargchk(4,5,nargin));
    
    %% Step #1 (Build cash_flow of free risk assets):
    if coupon_type == -1
        [ zero_cf, zero_nsin ] = cfbuilder(settle, 0);
        [ zero_price ] = get_bond_dyn_single_date(zero_nsin, 'price_dirty', settle);
        
        [ non_zero_cf, non_zero_nsin ] = cfbuilder(settle, 1);
        [ non_zero_price ] = get_bond_dyn_single_date(non_zero_nsin, 'price_dirty', settle);

        [ cf ] = merge(zero_cf, non_zero_cf);
        [ price ] = merge(zero_price, non_zero_price);
    else
        [ cf, nsin ] = cfbuilder(settle, coupon_type);
        [ price ] = get_bond_dyn_single_date(nsin, 'price_dirty', settle);            
    end
           
    exitflag = NaN(1, numel(model_name));
    
    %% Step #2 (Term Structure building):
    for i = 3 : numel(model_name)
        [ircurve.(model_name{i}), exitflag] = eval([ model_name{i} '( curve_type, price, cf, settle, param );']);        
    end

end

