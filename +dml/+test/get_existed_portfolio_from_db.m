clear; clc;

%% Import external packages
import dal.portfolio.data_access.*;

%% Definition of portfolio creation information:
account_name = 'test010120084';
creation_date = datenum('01012008', 'ddmmyyyy');

owner_id = 2;
manager_id = 1;

%%  Get old portfolio from database:
tic
old_port = da_portfolio( account_name, [], manager_id );
toc

%% Save result measures into database:
tic
[bool, err_msg] = old_port.save();
toc