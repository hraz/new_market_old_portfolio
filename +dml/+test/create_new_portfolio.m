clear; clc;

%% Import external packages
import dml.*;

%% Definition of portfolio creation information:
account_id = [];
account_name = 'test010120084';
creation_date = datenum('01012008', 'ddmmyyyy');
benchmark_id = 601;
owner_id = 1;
manager_id = 2;
initial_amount = 10000000;
filter_id = 101;
reinv_bnd = [-0.2, 0.5];

%% Build portfolio allocation sample:
nsin = sort([9268137;1105659;3720117;9230533;4160099;5490115;6110225;1102284;7480015;9548033;2310043;4010054;1093079;6110142;7360043;9547134;1101005;1103555;1104355;1097088;6370050;1105048;1099746;1098094;1104017;3190014;1103100;6110191;1091859;1095959;1099670;7480031;1940303;7410087;1940360;1940386;1940329;1940287;1940063;1096270;2300069;1610138;2200145;1098615;1102300;1750074;1097138;6050025;1102482;1105089;3570025;6120117;1098797;1105535;6490312;7980121;1098789;1100064;6390157;7980162;1100056;7980139;1101013;1102730;7780166;1095066;1060045;8230070;1101963;1098706;5740022;1110089;3950128;1102433;4340030;4440079;1104397;3660024;1103910;1104066;1104942;1101211;1920107;1092956;5490123;1100304;1102375;1790062;1790054;5620091;1104504;1980150;2260131;7430069;1260306;3900206;1098656;1097385;7230279;1260405;6990139;3230067;3900099;1410174;6000020;1100973;9547233;9547035;9548132;9542739;9268335;9268236;9999999;]);
count = 1000*ones(length(nsin), 1);
for i = 1:1:length(nsin)
    count(i) = i*count(i);
end
count(end) = 0;
initial_allocation = dataset(nsin, count);
sim = 01012008;

%% Simulation date:
up_date = datenum(creation_date);

%% Portfolio model declaration:
port_model = dataset(sim, {'Single Index'}, {'MinVar'},0, 1, reinv_bnd(1), reinv_bnd(2), 5, NaN, 60,  'VarNames', {'sim_id', 'model', 'optimization_type', 'port_ret', 'port_risk', 'reinv_low_bnd', 'reinv_up_bnd', 'port_duration', 'calc_duration', 'hist_length'});

%% Creation of the portfolio object:
tic
port = portfolio([], account_name, owner_id, manager_id, creation_date, up_date, initial_amount, benchmark_id, filter_id, initial_allocation, port_model);
toc

%%  Add calculated current risk measures:
% Sample of calculated risk measures by during the allocation process:
% exp_return = 0.07;
% volatility = 0.03;
% skewness = 0.2;
% kurtosis = 0.4;
% 
% alpha = 0.05;
% beta = 0.98;
% unique_risk = 0.056;
% 
% sharpe = 2.5;
% jensen = 0.02;
% omega = 0.45;
% inf_ratio = 0.2;
% 
% rmm_param.stat_rm = dataset(exp_return, volatility, skewness, kurtosis);
% rmm_param.capm_rm = dataset(alpha, beta, unique_risk);
% rmm_param.fin_rm = dataset(sharpe, jensen, omega, inf_ratio, NaN, NaN, NaN, NaN, NaN, ...
%                 'VarNames', {'sharpe', 'jensen', 'omega', 'inf_ratio', 'hurst', 'debt_statistics', 'alt_alpha', 'alt_inf_ratio', 'coherent_fisher'});
% rmm_param.var_rm = dataset(NaN, NaN, NaN, NaN, 'VarNames', {'v_a_r', 'cond_v_a_r', 'marginal_v_a_r', 'component_v_a_r'});
% port.risk = rmm(rmm_param);

%% Save current portfolio into database:
tic
[bool, err_msg] = port.save();
toc