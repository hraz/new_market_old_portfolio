classdef rmm < hgsetget
    %RMM determine Risk Measure Module class.
    % This class inherited from hgsetget class.
    %
    % Each object of rrm class has following properties (Default values are NaN):
    % --------------------------------------------------------------------------
    %     statistic
    %     value_at_risk
    % --------------------------------------------------------------------------
    %
    %   The class includes following public methods:
    % --------------------------------------------------------------------------
    %   [risk] = get_risk(obj, risk_name)
    % --------------------------------------------------------------------------
    %
    % Yigal Ben Tal
    % Copyright 2011-2012 BondIT Ltd.
    
    properties (SetAccess = private, GetAccess = public)
        statistic
        value_at_risk
    end
    
    methods
        function [ obj ] = rmm(varargin)
        %RMM determine the asset or portfolio risk measures class constructor.
        %
        %   [ obj ] = rrm(stat_rm, var_rm) receives statistical and Value-at-Risk risk measures
        %   and assign it to the rrm class properties.
        %
        %   Input:
        %       stat_rm - is an 1x4 dataset specifying the first four moments of some asset or portfolio.
        %
        %       var_rm - is an 1x4 dataset specifying the Value-at-Risk different model values of some asset or portfolio.
        %
        %   Output:
        %       obj - it is an object of the rmm class.
        %
        % Yigal Ben Tal
        % Copyright 2011-2012 BondIt Ltd.

            %% Pre Initialization %%
            error(nargchk(0,2,nargin));

            % Input parsing:
            p = inputParser;

            p.addParamValue('statistic', dataset(NaN, NaN, NaN, NaN, NaN, NaN, 'VarNames', {'port_return', 'volatility', 'skewness', 'kurtosis', 'r_square', 'rmse'}),  @(x)(isa(x, 'dataset')));
            p.addParamValue('value_at_risk', dataset(NaN, NaN, NaN, NaN, 'VarNames', {'v_a_r', 'cond_v_a_r', 'marginal_v_a_r', 'component_v_a_r'}), @(x)(isa(x, 'dataset')));

            % Parse method to parse and validate the inputs:
            p.StructExpand = true; % for input as a structure
            p.KeepUnmatched = true;

            try
                p.parse(varargin{:});
            catch ME
                newME = MException('dml:rmm:optionalInputError', 'Error in input arguments');
                newME = addCause(newME,ME);
                throw(newME)
            end
            
            %% Initialization %%
            obj.statistic = p.Results.statistic;
            obj.value_at_risk = p.Results.value_at_risk;
            
            %% Post Initialization %%
            
        end
        function [ risk ] = get_risk(obj, risk_name)
        %RMM\GET_RISK returns all risk values of the given risk object.
        %
        %   [ risk ] = get_risk(obj, risk_name)
        %   receives rmm object and returns all risk measures.
        %
        %   Input:
        %       obj - is a dml.rmm object specifying all groups of risk measures.
        %
        %       risk_name - is a 1xNRISKMEASURES cell of strings array specifying the required risk
        %                       measures.
        %
        %   Output:
        %       risk - is a 1xNRISKMEASURES dataset specifying the values of the given list of risk
        %                       measures. 
        %
        % Yigal Ben Tal
        % Copyright 2012.
            
            %% Import external packages:
            import dal.portfolio.crud.*;
            import utility.dataset.*;
            
            %% Input validation:
            error(nargchk(1,2,nargin));
            if (isempty(obj))
                error('rmm:get_risks:mismatchInput', 'An empty input.');
            elseif (~isa(obj, 'dml.rmm'))
                error('rmm:get_risks:wrongInput', 'Wrong object type.');
            end
            
            if (nargin < 2) || (isempty(risk_name))
                risk_name = 'all';
            end
                
            %% Step #1 (Concatenate all risks together):
            risk = [obj.statistic, obj.value_at_risk];
            
            %% Step #2 (Get part of risks according to given risk name list):
            if (~strcmpi( 'all', risk_name ))
                req_risk = ismember(dsnames(risk), risk_name);
                risk = risk(:, req_risk);
            end
            
        end
    end
    
end

