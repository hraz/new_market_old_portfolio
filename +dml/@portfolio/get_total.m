function [ value ] = get_total( obj )
%GET_TOTAL returns the total value of the object
%
%   [ value ] = get_total( obj ) receives the portfolio and returns its total value.
%
%   Input:
%       obj - is a portfolio object.
%
%   Output:
%       value - is a scalar value specifying the total value of the given portfolio.

% Yigal Ben Tal
% Copyright 2012.

    %% Input validation:
    error(nargchk(1,1,nargin));
    if (~isa(obj, 'dml.portfolio'))
        error('portfolio:get_total:NotValidObject', 'Not valid object type.');
    end
    
    %% Step #1 (Output data assignment):
    if (isempty( obj ))
        value = [];
    else
        value = obj.performance.total_value;
    end

end

