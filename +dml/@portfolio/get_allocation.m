function [ ds ] = get_allocation( obj, rdate )
%GET_ALLOCATION  returns assets allocation of the given porfolio.
%
%   [ ds ] = get_allocation( obj ) receives some portfolio and returns its own structure.
%
%   Inputs:
%       obj - is an object of portfolio class.
%
%   Outputs:
%       ds - is a dataset specifying the given portfolio structure - nsin of included
%                       assets, counts, holdings, weights and sectors.

% Yigal Ben Tal
% Copyright 2011-2012.

    %% Import external packages:
    import dal.portfolio.data_access.*;
    import dal.market.get.dynamic.*;
    import dml.*;
    
    %% Input validation:
    error(nargchk(1, 2, nargin));
    if (~isa(obj, 'dml.portfolio'))
        error('portfolio:get_allocations:NotValidObject', 'Not valid object type.');
    end
    
    if isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif ischar(rdate)
        rdate = datenum(rdate, 'yyyy-mm-dd');
    end
       
    %% Step #1 (Output data assignment):
    if (isempty( obj.allocation ))
        ds = dataset();
    elseif (rdate >= obj.creation_date) && (rdate < obj.last_update)
        ds = da_port_dynamic(obj.account_id, rdate, 'allocation');
        % Temporary solution for an empty allocation table:
        if isempty(ds)
            error('portfolio:get_allocation:mismathData', 'The allocation is empty.');
        end
        ds = ds(:,3:end);
        
        price = adjdata2alloc(obj, obj.allocation, get_bond_dyn_single_date( ds.nsin, 'price_dirty', rdate));
        ds.holding = ds.count .* price';
        ds.weight = holdings2weights(ds.count', price', 1)';
    else
        ds = obj.allocation;
    end
    
end

