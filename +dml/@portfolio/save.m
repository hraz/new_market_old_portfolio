function [ bool, err_msg, account_id ] = save(obj)
%SAVE saves portfolio data into database.
%
%   [ bool, err_msg ] = save(obj)
%   receives portfolio object and returns boolean value true if it was successfull and false if it
%   was failure. In the failure case it returns error message too.
%
%   Input:
%       obj - is a portfolio object;
%
%   Output:
%       bool - is a boolean value specifying the success of the operation.
%
%       err_msg - is a string value or cell arra of strings specifying the error message with failure reason.
%
% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import dal.portfolio.crud.*;

    %% Input validation:
    error(nargchk(1,1,nargin));
    if (isempty(obj))
        error('portfolio:save:mismatchInput', 'An empty input.');
    elseif (~isa(obj, 'dml.portfolio'))
        error('portfolio:save:wrongInput', 'Wrong object type.');
    end
    
    %% Step #1 (Save static data):
    % In case of portfolio creation:
    if isempty(obj.account_id)
        static_data = dataset(obj.owner_id, obj.manager_id, {obj.account_name}, {datestr(obj.creation_date, 'yyyy-mm-dd')}, obj.initial_amount, ...
            'VarNames' , {'owner_id', 'manager_id', 'account_name', 'creation_date', 'initial_amount'});
     end
    
    %% Step #2 (Preparing dynamic data):
    dyn_param.allocation = obj.allocation(:,1:8);
    dyn_param.model = obj.model;
    dyn_param.model.transaction_cost_range={''};%bypassing yigal validation
    dyn_param.risk = get_risk(obj.risk);
    dyn_param.performance =  get_ratio(obj.performance);
    dyn_param.port_filter_history = dataset(obj.filter_id, 'VarNames', 'filter_id');
    
    if (obj.creation_date < obj.last_update) && isempty(obj.calc_err_msg)
        dyn_param.benchmark_perf = obj.benchmark;
        if obj.flagFinalSave
            dyn_param.port_deltas = obj.deltas;
        end
        
        given_cumulative_ret_dates = obj.cumulative_return.obs_date;
        new_cumulative_ret_date_place = ismember(given_cumulative_ret_dates, given_cumulative_ret_dates(datenum(given_cumulative_ret_dates) >= obj.prev_update));
        
        if (obj.prev_update == obj.creation_date)
            is_first_perf_checking = true;
        else
            is_first_perf_checking = false;
        end
        ds_cumulative_return = obj.cumulative_return(new_cumulative_ret_date_place, :);
%         ds_cumulative_return.obs_date = datestr(ds_cumulative_return.obs_date, 'yyyy-mm-dd');      
        
        account_id = obj.account_id;
    end

 
    %% Step #3 (Save portfolio data):    
    if isempty(obj.account_id)
        [bool, err_msg, account_id] = set_portfolio(obj.creation_date, [], dyn_param, [], 'static', static_data);
        obj.account_id = account_id;
    elseif (obj.prev_update < obj.last_update)
        if obj.flagFinalSave
            [bool, err_msg, account_id] = set_portfolio(obj.last_update, account_id, dyn_param, is_first_perf_checking, 'income', ds_cumulative_return);
        else
            [bool, err_msg, account_id] = set_portfolio(obj.last_update, account_id, dyn_param, is_first_perf_checking);
        end
    else
        err_msg = 'The portfolio data did not changed.'; % Check obj.prev_update vs. last_update.
        bool = false;
    end

    
end


