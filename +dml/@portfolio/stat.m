function [ exp_ret, volatility, skew, kurt ] = stat( obj, asset_data )
        %STAT calculates expected return and volatility of the portfolio.
        %
        %   [ exp_ret, volatility, skew, kurt, rsq, rmse ] = stat( obj, asset_data )
        %   receives the portfolio, its history of assets yield and asset_data porfolio statistical information.
        %
        % Inputs:
        %       obj - is an object specifying the given portfolio.
        %
        %       asset_data - is an MAXNUMOBSxNASSETS matrix specifying the optimized by length assets
        %                   yields of the given portfolio.  
        %
        %   Outputs:
        %       exp_ret - is a scalar specifying the portfolio expected return.
        %
        %       volatility - is a scalar specifying the portfolio volatility (standard deviation).
        %
        %       skew - is a scalar specifying the portfolio's 3-moment.
        %
        %       kurt - is a scalar specifying the portfolio 4-moment.
        %
        %       rsq - is a scalar value specifying the coefficient of determination r-square.
        %
        %       rmse - is a scalar value specifying the root-mean-square-error value.
        %
        % Yigal Ben Tal
        % Copyright 2010-2012.

            %% Import external packages and global variables declaration:
            import utility.statistics.*;

            %% Input validation:
            error(nargchk(2, 2, nargin));
            if (~isa(obj, 'dml.portfolio'))
                error('portfolio:stat:wrongInput', 'Wrong type of the first argument.');
            end
            if isempty(asset_data) %|| isempty(weight)
                error('portfolio:stat:mismatchInput', 'asset_data or weight cann''t be an empty.');
            end

            %% Step #1 (Calculation four moments for portfolio asset_data):
            [ exp_ret, volatility ] = nanstat(asset_data);
            skew = skewness( asset_data );
            kurt = kurtosis( asset_data );
            
        end