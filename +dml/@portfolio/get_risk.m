function [ value ] = get_risk(obj, risk_measure)
%PORTFOLIO\GET_RISK returns risk measure value by its name.
%
%   [ value ] = get_rm(obj, risk_measure) 
%   receives portfolio, required risk measure and returns the value of the measure.
%
%   Inputs:
%       obj - is a handle on the asset object.
%
%       risk_measure - it is a scalar or an NRISKMEASURESx1 cell array of string values specifying the
%                               required risk measures. 
%
%   Outputs:
%       value - it is a scalar value or an NRISKMEASURESx1 vector specifying the risk measure value
%                               in order of the given measure's names. 
%

% Yigal Ben Tal
% Copyright 2011-2012.
    
    %% Input validation:
    error(nargchk(1, 2, nargin));
    if (~isa(obj, 'dml.portfolio'))
        error('portfolio:get_rm:wrongInput', 'Not valid object.');
    end
    if (~ischar(risk_measure)) && (~iscellstr(risk_measure))
        error('portfolio:get_rm:wrongInput', 'The risk measure name must be a string or cell array of strings.');
    end
    
    %% Assignment output argument:
    value = get_risk(obj.risk, risk_measure);
    
end

