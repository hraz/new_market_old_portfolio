function [ value ] = get_account_id( obj )
%GET_ACCOUNT_ID returns portfolio identification number.
%
%   [ value ] = get_account_id( obj ) receives portfolio object and returns its identification
%   number.
%
%   Input:
%       obj - is a portfolio object.
%
%   Output:
%       value - is a scalar value specifying the portfolio's identification number.

% Yigal Ben Tal
% Copyright 2012.

    %% Input validation:
    error(nargchk(1,1,nargin));
    if (~isa(obj, 'dml.portfolio'))
        error('portfolio:get_account_id:NotValidObject', 'Not valid object type.');
    end
    
    %% Step #1 (Output data assignment):
    if (isempty( obj ))
        value = [];
    else
        value = obj.account_id;
    end

end
