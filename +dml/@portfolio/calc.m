function [err_msg] = calc( obj, benchmark_id )
%CALC calculates all groups of the dynamic properties.
%
%   [bool, err_msg] = calc( obj, mar, rf, up_date )
%   receives porfolio object, minimal accepted return, risk neutral rate of return and the date of
%   the calculation, and calculates all groups of dynamic portfolio characteristics.
%
%   Inputs:
%       obj - is a handle on the given portfolio.
%
%   Optional inputs:
%       mar - is a scalar value specifying the minimal accepted return value. (Default is 0.)
%
%       rf - is a scalar value specifying the risk neutral rate of return for the given portfolio.
%                   (Default is 0.)
%
%       up_date - is a scalar value specifying the required for the calculation date. (Default is
%                   today).
%
%   Output:
%       err_msg - is a string value specifying the error message in case of all allocated assets are
%                   not existed.
%
% Yigal Ben Tal
% Copyright 2011-2012.

%% Import external packages:
import bll.performance_calculations.*;
import dml.portfolio;
import dml.*;
import dal.market.get.dynamic.*;
import dal.market.get.static.*;
import dal.market.get.base_data.*;
import utility.dataset.*;
import utility.*;
import utility.fts.*;
import utility.dal.*;

%% Constant initialization:
err_msg = ''; % default err_msg value;
RETURN_METHOD=1;

%% Input validation:
error(nargchk(1, 2, nargin));
if (~isa(obj, 'dml.portfolio'))
    error('portfolio:up_date:NotValidObject', 'There is not valid object.');
end

up_date = obj.last_update;

if (nargin < 2) || (isempty(benchmark_id))
    benchmark_id = 601;
end

hist_length=obj.model.reinv_low_bnd;


%% THE CALCULATION
if (obj.creation_date < obj.last_update)
    risklessRate=obj.riskLessRate;
    initialExpReturn=obj.model.expected_return;
    percentileRisk=obj.model.reinv_up_bnd;
    
    % RISK
    % NOTE: in ver 2.00 we are saving the 'risk'  values (expected
    % return and expected volatility) under the 'portfolio_model' label
    % since the 'risk' label is taken by the 4 performance moments (see
    % step 5 below)
    portTTM=((obj.creation_date + obj.model.real_holding_period*365) - up_date )/365;
    if portTTM > 0.0027 % If less than a single day is left in the portfolio
        obj.model.expected_return= calc_expected_return(RETURN_METHOD, obj.ReinvestmentStrategy, obj.allocation,portTTM,benchmark_id, up_date);
    else % Otherwise, expected return is 0 at this point.
        obj.model.expected_return = 0;
    end
    switch obj.model.model{1}
        case 'Marko'
            risk_method=1;
        case 'SI'
            risk_method=2;
        case 'Omeg'
            risk_method=3;
        case 'MI'
            risk_method=4;
        case 'Spread'
            risk_method=5;
        case 'RiP'
            risk_method=6;
        case 'HighMom'
            risk_method=7;
        otherwise
            risk_method=1;
    end
    obj.model.expected_volatility = calc_expected_risk([], obj.allocation,hist_length,up_date,risk_method,benchmark_id);
    
    % PERFORMANCE STAGE 1
    % NOTE: in ver 2.00 we are saving the performance moments under the
    % 'risk' label because there's no other place for them. The 'risk'
    % values (expected return and expected volatility) are saved under the
    % 'portfolio_model' label (see step 4 above).
    portfolioValue = obj.cumulative_return.value;
    portLength=length(portfolioValue);
    if hist_length<portLength
        portfolioValue=portfolioValue(end-hist_length+1:end);
    else
        hist_length=portLength;
    end
    [port_return volatility skewness kurtosis] = compute_moments(portfolioValue);
    ftsCumulativeValues=obj.ftsCumulative_return;
    ftsHoldingValuePerBond=obj.ftsCumulative_return_perBond;
    [valueAtRisk conditionalValueAtRisk ] = value_at_risk(percentileRisk, ftsCumulativeValues, hist_length);
    [ marginalValueAtRisk componentValueAtRisk] = marginal_value_at_risk( percentileRisk, ftsHoldingValuePerBond, hist_length );
    
    %  BENCHMARK PERFORMANCE
    first_date=ftsCumulativeValues.dates(end-hist_length+1);
    [ bench_holding_ret ] = get_bench_holding_return( benchmark_id, first_date , up_date );
    benchTotalReturn = bench_holding_ret(:,2)+1; %Total return initiated at 1
    benchDailyReturn = benchTotalReturn(2:end)./benchTotalReturn(1:end-1) -1;
    benchLength=length(benchDailyReturn);
    % match the benchmark and portfolio history lengths
    if hist_length<=benchLength
        benchDailyReturn=benchDailyReturn(end-hist_length+1:end);
    else
        hist_length=benchLength;
        benchDailyReturn=benchDailyReturn(end-hist_length+1:end);
    end
    
    [bench_sharpe ~] = compute_ratios(benchTotalReturn, benchDailyReturn, risklessRate);
    [bench_holding_return bench_volatility bench_skewness bench_kurtosis] = compute_moments(benchTotalReturn);
    obj.benchmark = dataset(benchmark_id, bench_holding_return, bench_volatility, bench_skewness, bench_kurtosis, bench_sharpe, ...
        'VarNames', { 'benchmark_id', 'holding_return', 'volatility', 'skewness', 'kurtosis','sharpe'});
    
    % PERFORMANCE STAGE 2
    beta = compute_beta(portfolioValue,benchDailyReturn);
    alpha = compute_alpha(portfolioValue, benchDailyReturn, beta);
    [rsq_vs_exp_ret rmse_vs_exp_ret] = compute_rsquare(portfolioValue,benchDailyReturn);
    [sharp inf_ratio] = compute_ratios(portfolioValue, benchDailyReturn, risklessRate);
    jensen = compute_jensen_alpha(portfolioValue, benchDailyReturn, risklessRate);
    omega = compute_omega(portfolioValue, initialExpReturn);
    unique_risk=rmse_vs_exp_ret;
    obj.performance = perf_ratio('total_value', portfolioValue(end), ...
        'capm',         dataset(alpha, beta, unique_risk), ...
        'fin',              dataset(sharp, jensen, omega, inf_ratio, 'VarNames', {'sharpe', 'jensen', 'omega', 'inf_ratio'}), ...
        'rsquare',       dataset(rsq_vs_exp_ret, rmse_vs_exp_ret, 0, 0, ...
        'VarNames', {'rsquare_vs_expret', 'rmse_vs_expret', 'rsquare_vs_bench', 'rmse_vs_bench'})...
        );
    
    % DELTAS
    current_holding_period = (obj.last_update - obj.creation_date)/365; % period in years
    totalReturnYear=(obj.cumulative_return.value(end)/obj.cumulative_return.value(1))...
        ^(1/current_holding_period) - 1;
    benchTotalReturnYear=(benchTotalReturn(end)/benchTotalReturn(1)) ^(1/current_holding_period) -1;
    delta1=(totalReturnYear-benchTotalReturnYear) / (volatility);
    delta2= (totalReturnYear - initialExpReturn) / (volatility);
    delta3=(sharp - bench_sharpe);
    delta4=inf_ratio;
    obj.deltas = dataset(delta1, delta2, delta3, delta4, ...
        'VarNames', {'delta1', 'delta2', 'delta3', 'delta4'});
    
else
    % RISK - not relevant, inserted directely during portfolio constructor
    % using data from 'simultion_function.m'
    
    % PERFORMANCE STAGE 1
    % NOTE: in ver 2.00 we are saving the performance moments under the
    % 'risk' label because there's no other place for them. The 'risk'
    % values (expected return and expected volatility) are saved under the
    % 'portfolio_model' label (see step 4 above).
    port_return=NaN;
    volatility=NaN;
    skewness=NaN;
    kurtosis=NaN;
    valueAtRisk=NaN;
    conditionalValueAtRisk=NaN;
    marginalValueAtRisk=NaN;
    componentValueAtRisk=NaN;
    
    % PERFORMANCE STAGE 2
    portfolioInitValue = sum(obj.allocation.holding);
    obj.performance = perf_ratio('total_value', portfolioInitValue, ...
        'fin',              dataset(NaN, NaN, obj.model.transaction_cost_range, NaN, 'VarNames', {'sharpe', 'jensen', 'omega', 'inf_ratio'}));
    % above is a misuse of the unused obj.model.transaction_cost_range
    % for the sake of saving the omega value at creation date. this
    % adjustment appears also in save_solution lines 82-84
    
    %  BENCHMARK PERFORMANCE - NOT RELEVANT
end

% RISK OBJECT EXPLOITED FOR PERFORMANCE USAGE
rmm_param.statistic = dataset(port_return, volatility, skewness, kurtosis);
rmm_param.value_at_risk = dataset(valueAtRisk, conditionalValueAtRisk, ...
    marginalValueAtRisk(1), componentValueAtRisk(1), 'VarNames', {'v_a_r', ...
    'cond_v_a_r', 'marginal_v_a_r', 'component_v_a_r'});
% (Initialization of risk measures object):
obj.risk = rmm(rmm_param);


end


