function [ ds ] = get_counts( obj, nsin )
%PORTFOLIO\GET_COUNTS returns list of included asset counts.
%
%   [ ds ] = get_counts( obj, nsin ) receives some portfolio, nsin of included
%   assets and returns list of asset counts.
%
%   Inputs:
%       obj - is an object of a portfolio class.
%
%       nsin - is an NASSETSx1 vector specifying the nsin numbers of
%                   included assets.
%
%   Outputs:
%       ds - it is an NASSETSx2 dataset specifying the counts
%                   of included in this portfolio assets.
%

% Yigal Ben Tal
% Copyright 2011-2012 BondIT Ltd.

    %% Input valnsination:
    error(nargchk(1, 2, nargin));
    if (~isa(obj, 'dml.portfolio'))
        error('portfolio:get_counts:notValidObject',  'Not valid object type.');
    end
        
    %% Step #1 (Output data assignment):
    if (~isempty( obj.allocation ))
        ds = get(obj.allocation, {'nsin', 'count'});
        if (nargin == 2) && (~isempty(nsin))
            ds = ds(ismember(obj.allocation.nsin, nsin),:);
        end
    else
        ds = dataset();
    end
    
end
