function [bool, err_msg] = update(obj, up_date)
%UPDATE refresh dynamic data vs. database according to required date.
%
%   [] = update(obj, up_date)
%   receives porfolio object and changes all dynamic properties to values
%   from the given date.
%
%   Input:
%       obj - is a portfolio object.
%
%       up_date - is a required date for dynamic properties updating.
%
%   Output:
%       bool - is a boolean value specifying true or false result of the existed asset checking.
%
%       err_msg - is a string value specifying the error message in case of all allocated assets are
%                   not existed.
%
% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
    import dal.portfolio.data_access.*;
    import dal.market.get.dynamic.*;
    import utility.dataset.*;
    import utility.dal.*;
    import dml.*;
    
    %% Input validation:
    error(nargchk(2,2,nargin));
    if isempty(obj)
        error('portfolio:update:mismatchInput','The object argument is empty.');
    elseif (~isa(obj, 'dml.portfolio'))
        error('portfolio:update:wrongInput','Wrong object type.');
    end
    
    %% Step #1  (Update risk):
    rm = da_port_dynamic(obj.account_id, obj.last_update, 'risk');
    rm_name = dsnames(rm);
    
    rm_group.statistic = rm(1, ismember(rm_name, {'port_return', 'volatility', 'skewness', 'kurtosis'}));
    rm_group.value_at_risk = rm(1, ismember(rm_name, {'v_a_r', 'cond_v_a_r', 'marginal_v_a_r', 'component_v_a_r'}));
    
    obj.risk = rmm(rm_group);
    
    %% Step #2 (Update allocation properties):
    % For the  care the case when the number of  allocated assets is different from alived assets at some given date
    last_known_price = get_bond_dyn_single_date( obj.allocation.nsin, 'price_dirty', obj.last_update);
    if isempty(last_known_price)
        obj.calc_err_msg = 'All allocated assets already not traded. Cann''t calculate any portfolio measure.';
        return;
    else
        obj.calc_err_msg = '';
    end
    price = adjdata2alloc(obj, obj.allocation, last_known_price);    
    obj.allocation.holding = obj.allocation.count .* price';
    obj.allocation.weight = holdings2weights(obj.allocation.count', price', 1)'; 
    
    %% Step #3 (Benchmark historical performance):
    if (obj.creation_date < obj.last_update)
        bench_perf = da_port_dynamic(obj.account_id, obj.last_update, 'benchmark_perf');
        if (~isempty(bench_perf))
            obj.benchmark = bench_perf(:,2:end);
        end
    end
        
    %% Step #4 (Get portfolio delta values at given date):
    if (obj.creation_date < obj.last_update)
        delta_ds = da_port_dynamic(obj.account_id, obj.last_update, 'port_deltas');
        obj.deltas = delta_ds(:,2:end);
    end
     
    %% Step #5 (Get cumulative return on given holding period):
    if (obj.creation_date < obj.last_update)
        obj.cumulative_return  = da_port_income_history(obj.account_id, obj.creation_date, obj.last_update);
    end
    
    %% Step #6 (Update costs):
%     costs = da_port_dynamic(obj.account_id, up_date, 'cost');
%     obj.cost = costs(:,3:end);
    
end

