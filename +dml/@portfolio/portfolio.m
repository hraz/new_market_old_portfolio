classdef portfolio < hgsetget
    %PORTFOLIO determines the portfolio class.
    properties (Access = private)
        prev_update = [];   
        calc_err_msg = '';
    end
    
    properties 
        % Static properties:
        account_id = [];
        owner_id = [];
        manager_id = [];
        account_name = [];
        creation_date = [];
        initial_amount = [];
        last_update = [];
        filter_id = [];
        
        flagFinalSave=false;
        % Model of the portfolio allocation:
        model = dataset(NaN, {''}, {''},  NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN, {''}, {''}, {''}, {''}, {''}, NaN, {''}, {''}, ...
                'VarNames', {'sim_id', 'model', 'optimization_type', 'expected_return', 'expected_volatility', 'reinv_low_bnd', 'reinv_up_bnd', ...
                'req_holding_horizon', 'real_holding_period', 'hist_length', 'asset_ttm_range', 'rating_range', 'class_linkage_combination', ...
                'sector_combination', 'yield_range', 'turnover_range', 'max_allocation_weight', 'transaction_cost_range', 'special_range'});
        
        % Benchmark performance:
        benchmark = dataset(NaN, NaN, NaN, NaN, NaN, NaN, 'VarNames', {'benchmark_id', 'holding_return', 'volatility', 'skewness', 'kurtosis', 'sharpe'});
        
        % Asset allocation property:
        allocation = dataset(NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN, 'VarNames', ...
            {'nsin', 'count', 'rf_reinv_flag', 'rf_reinv_rate', 'rf_reinv_amount','bench_reinv_flag', 'bench_reinv_coef', 'bench_reinv_amount', 'holding', 'weight'});
        
        % performance properties:
        performance = dml.perf_ratio();
        
        % risk properties:
        risk = dml.rmm();
        
        % holding history:
        cumulative_return = dataset(NaN, NaN, 'VarNames', {'obs_date', 'value'});
        ftsCumulative_return=fints();
        ftsCumulative_return_perBond = fints();
        riskLessRate=NaN;

        % deltas fro simulation
        deltas = dataset(NaN, NaN, NaN, NaN, 'VarNames', {'delta1', 'delta2', 'delta3', 'delta4'});
        
        ReinvestmentStrategy = [];
%         % cost properties:
%         cost = dataset(NaN, NaN, NaN, 'VarNames', {'transaction', 'holding', 'tax_corporate'});

%         % performance by asset:
%         money_breakdown = dataset(NaN, NaN, NaN, NaN, NaN, NaN, NaN, 'VarNames', {'nsin', 'asset_income', 'coupon_income', 'iirr', 'interest_paid', 'tbill_interest', 'nav'});
    end
    
    methods (Access = private)
        function [ data ] = adjdata2alloc(obj, alloc,  fts_data)
            
            %% Import external packages:
            import utility.*;
            import utility.fts.*;
            
            %% Input validation:
            error(nargchk(3, 3, nargin));
            
            if (~isa(fts_data, 'fints'))
                error('potrfolio:adjdata:wrongInput', 'The fts_data must have a financial time series data format.');
            end
            
            %% Step #1 (Create output data matrix according to given allocation size and hilstory length):
            data = zeros(size(fts_data,1), size(alloc, 1));
            
            %% Step #2 (Find alive assets from allocation list):
            ind = ismember(alloc.nsin, strid2numid(tsnames(fts_data)));
            
            %% Step #3 (Build output data according to alive assets historical values):
            data(:,ind) = fts2mat(fts_data);
            
        end
        
        % Risk measures calculation:
        [ exp_ret, volatility, skew, kurt ] = stat( obj, asset_data )
                
        % Performance calculation:
        function [ alpha, beta, nonsysrisk, bench_expret ] = capm_ratio( obj, port_return, bench_return )
        %CAPM asset_data a risk measures according to CAPM model.
        %
        %   [ alpha, beta, nonsysrisk, bench_expret ] = capm( obj, port_return, bench_return )
        %   receives portfolio, assets values, benchmark values and port_return risk measures of the
        %   given portfolio according to CAPM model.
        %
        %   Inputs:
        %       obj - is a porfolio object specifying the given portfolio.
        %
        %       port_return - is an MAXNUMOBSxNASSETS fts specifying the assets yields of the given portfolio. 
        %
        %       bench_return - is an NOBSERVATIONSx1 fts specifying the given benchmark values.
        %
        %	Outputs:
        %       alpha - is a scalar specifying the alpha value of the given portfolio against the benchmark.
        %
        %       beta - is a scalar specifying the beta value of the given portfolio against the benchmark.
        %
        %       nonsysrisk - is a scalar specifying the unique risk of the portfolio.
        %
        %       bench_expret - is a scalar specifying the benchmark expected return.
        %
        %   Notes:
        %     - The Beta of a stock is a number describing the relation of its port_return
        %       with that of the financial market as a whole.
        %       
        %     - An asset with a Beta of zero means that its port_return change independently 
        %       of changes in the market's port_return.
        %       
        %     - A positive beta means that the asset's port_return generally follow the
        %       market's port_return, in the sense that they both tend to be above their
        %       respective averages together, or both tend to be below their
        %       respective averages together. 
        %
        %     - A negative beta means that the asset's port_return generally move opposite 
        %       the market's port_return: one will tend to be above its average when the other 
        %       is below its average.
        %
        %     - The beta coefficient is a key parameter in the Capital Asset Pricing Model
        %       (CAPM). It measures the part of the asset's statistical variance  that cannot
        %       be mitigated by the diversification provided by the portfolio of many risky
        %       assets, because of the correlation of its port_return with the port_return of the 
        %       other assets that are in the portfolio. 
        %
        %     - Beta can be estimated for individual companies using regression analysis 
        %       against a stock market index.
        %
        %    - If both given vectors have different length, the function will
        %      cuts it until minimum length if it not less then 30, othewise it creates an error.
        %   
        % Yigal Ben Tal
        % Copyright 2010-2012.

            %% Import external packages:
            import utility.rm.*;

            %% Input validation:
            error(nargchk(3, 3, nargin));
            if (~isa(obj, 'dml.portfolio'))
                error('portfolio:stat:wrongInput', 'Wrong type of the first argument.');
            end
            if isempty(port_return)
                error('portfolio:capm:mismatchInput', 'port_return cann''t be an empty.');
            end
            if isempty(bench_return)
                error('portfolio:capm:wrongInput', 'Benchmark port_return cann''t be an empty.');
            end

            %% Step #1 (There is Alpha-Beta calculation):
            [ alpha, beta, nonsysrisk, bench_expret ] = nanab( port_return, bench_return );    

        end
        function [ sharp, jensen, omega, inf_ratio ] = fin_ratio( obj, port_return, bench_return, beta, bench_expret, rf )
        %FIN calculates financial ratios of the porfolio.
        %
        %   [ sharp, jensen, omega, inf_ratio ] = fin_ratio( obj, port_return, bench_return, beta, bench_expret, rf )
        %   receives porfolio, expected return and volatility of the portfolio, and risk neutral
        %   rate of return, and asset_data financial ratios.
        %
        %	Inputs:
        %       obj - 
        %
        %       port_return - is a scalar specifying the portfolio expected return.
        %
        %       bench_return - 
        %
        %       beta - 
        %
        %       bench_expret - is a scalar specifying the benchmark expected return.
        %
        %       rf - is a scalar specifying the risk free return.
        %
        %   Outputs:
        %       sharp - is a scalar specifying the Sharpe ratio of the portfolio.
        %
        %       jensen - 
        %
        %       omega - 
        %
        %       inf_ratio - 
        %
        %   Notes: The function uses number 30 as minimum history length according to
        %                   Central Limit Theorem.
        %
        % Yigal Ben Tal
        % Copyright 2010-2012.

            %% Import external packages:
            import utility.rm.*;

            %% Input validation:
            error(nargchk(6, 6, nargin));
            if (~isa(obj, 'dml.portfolio'))
                error('portfolio:fin:wrongInput', 'Wrong type of the first argument.');
            end
            if isempty(port_return)
                error('portfolio:fin:mismatchInput', ...
                    'asset_data is empty.');
            end
            if isempty(bench_return)
                error('portfolio:fin:mismatchInput', ...
                    'bench_return is empty.');
            end
            if isempty(bench_expret)
                error('portfolio:fin:mismatchInput', ...
                    'bench_expret is empty.');
            end
            
            %% Portfolio return vector calculation:
            port_return(isnan(port_return)) = 0; % in case of NaN values at the start of measured period or at the end of it
            
            %% Sharpe ratio calculation:
            sharp = sharpe( port_return, rf );
            
            %% Jensen alpha calculation:
            jensen = jensen_ratio(obj.risk.statistic.port_return, beta, bench_expret, rf);

            %% Omega ratio calculation:
            omega = omega_ratio(port_return, obj.model.expected_return);
            
            %% Information ratio:
            inf_ratio = inforatio(port_return, bench_return);
            
        end
        function [rsq_vs_exp_ret, rsq_vs_bench_ret ] = r_square(obj, bench_return)
        %R_SQUARE returns the coefficient of determination vs. expected return and benchmark return.
        %
        %   [rsq_vs_exp_ret, rsq_vs_bench_ret ] = r_square(obj, bench_return)
        %
        %
        %   Input:
        %       obj - is a portfolio object.
        %
        %       bench_return - is a NOBSERVATIONSx1 vector of benchmark holding return.
        %
        %   Output:
        %       rsq_vs_exp_ret - is an 1x2 vector specifying the r-square test results: Rsq and RMSE
        %                                   in case of fitting to expected return.
        %
        %       rsq_vs_bench_ret - is an 1x2 vector specifying the r-square test results: Rsq and RMSE
        %                                   in case of fitting to benchmark holding return.
        %
        % Yigal Ben Tal
        % Copyright 2012.
                
            %% Import external packages:
            import utility.statistics.*;
            
            %% Input validation:
            error(nargchk(2,2, nargin));
            
            %% Step #1 (Build vector of expected return per each observation):
%             obs = double(obj.cumulative_return(:,1));
            init_date = datevec(obj.cumulative_return.obs_date(1,1));
            time_period = etime(datevec(obj.cumulative_return.obs_date(:,1)), init_date(ones(size(obj.cumulative_return,1),1), :)) / (3600*24*365);          

            %% Step #2 (Calculating R-square of real holding return vs. expected return):
            expected_return =  double(obj.cumulative_return(:,2)) - 1;
            [rsq_vs_exp_ret(1), rsq_vs_exp_ret(2)] = rsquare(obj.model.expected_return(ones(size(time_period),1),1) .* time_period, expected_return );
            
            %% Step #3 (Calculating R-square of real holding return vs. benchmark holding return):
            [rsq_vs_bench_ret(1), rsq_vs_bench_ret(2)] = rsquare(bench_return, expected_return);
            
        end
    
    end
    methods (Access = public)
         function [ obj ] = portfolio( account_id, account_name, owner_id, manager_id, creation_date, up_date, initam, benchmark_id, filter_id, allocation, port_model, prev_update, fts_port_hold_ret,fts_bond_hold_ret,riskLessRate, ReinvestmentStrategy)
        %PORTFOLIO determine portfolio class object constructor.
        %
        %   [ obj ] = portfolio() creates an empty bond portfolio.
        %
        %   [ obj ] = portfolio(account_id, account_name, owner_id, manager_id, creation_date, initam, allocation, rf ) 
        %   receives list of bonds with its counts, risk neutral rate of return, required date and
        %   asset_data the bond portfolio object. 
        %
        %   Inputs:
        %       account_id - is a positive scalar numerical value specifying the number of user account. 
        %
        %       account_name - is a string that identifies some unique portfolio.
        %
        %       owner_id - is a positive scalar numerical value specifying the internal owner id
        %                       number.
        %
        %       manager_id - is a positive scalar numerical value specifying the internal account
        %                       manager id number.
        %
        %       creation_date - is a MATLAB formatted date value specifying creation date of the
        %                       portfolio.
        %
        %       initam - is an initial amount of this portfolio.
        %
        %       allocation - is an NASSETSx3 dataset specifying the list of bonds that
        %                      included into the portfolio together with its counts and value. (During object
        %                      creation the assets with NaN or zero count values were excluded
        %                      from the list, changed in ver 2.00)
        %
        %       rf - is a scalar value specifying the risk neutral rate of return.
        %
        %       mar - is a scalar value specifying the minimal accepted return.
        %        
        %       ReinvestmentStrategy - string - specifying typs of
        %           reinvestment strategy
        %
        %   Outputs:
        %       obj - it is a created portfolio object.
        %
        % Yigal Ben Tal
        % Copyright 2011.
        
            %% Pre Initialization %%
            % Import external packages:
            import dal.market.get.dynamic.*;
            import dal.market.get.static.*;
            import dal.market.filter.market_indx.*;
            import dml.*;
            import utility.dataset.*;
            
            % Number input arguments checking:
            error(nargchk(0, 16, nargin));
            
            % 1. Kind of account_id argument checking:
            if (nargin == 0) || (isempty(account_id))
                account_id = [];
            elseif all(size(account_id) ~= [1,1])
                error('dml:portfolio:mismatchInput', 'Account id must be a scalar.');
            elseif (~isnumeric(account_id))
                error('dml:portfolio:mismatchInput', 'Non-numeric account id value.');
            elseif (account_id < 0) % account_id = 0 - for new account
                error('dml:portfolio:mismatchInput', 'Account id must be positive.');
            end
            
            %2. Kind of the account_name argument checking:
            if (nargin > 0)
                if (~ischar(account_name))
                   error('dml:portfolio:wrongInput', 'Wrong account_name.');
                end
            
                %3. Kind of the owner_id argument checking:
                if (isempty(owner_id))
                    error('dml:portfolio:mismatchInput', 'An empty owner id.');
                elseif all(size(owner_id) ~= [1,1])
                    error('dml:portfolio:mismatchInput', 'owner id must be a scalar.');
                elseif (~isnumeric(owner_id))
                    error('dml:portfolio:mismatchInput', 'Non-numeric owner id value.');
                elseif (owner_id <= 0)
                    error('dml:portfolio:mismatchInput', 'owner id must be positive.');
                end

                %4. Kind of the account manager id argument checking:
                if (isempty(manager_id))
                    error('dml:portfolio:mismatchInput', 'An empty account manager id.');
                elseif all(size(manager_id) ~= [1,1])
                    error('dml:portfolio:mismatchInput', 'Account manager id must be a scalar.');
                elseif (~isnumeric(manager_id))
                    error('dml:portfolio:mismatchInput', 'Non-numeric account manager id.');
                elseif (manager_id <= 0)
                    error('dml:portfolio:mismatchInput', 'Account manager id must be positive.');
                end

                %5. Kind of the creation date argument checking:
                if (isempty(creation_date))
                    creation_date = today;
                elseif all(size(creation_date) ~= [1,1])
                    error('dml:portfolio:mismatchInput', 'Account creation_date must be a scalar.');
                end
              
                if (~isnumeric(creation_date))
                    creation_date = datenum(creation_date, 'yyyy-mm-dd');
                elseif (creation_date <= 0)
                    error('dml:portfolio:mismatchInput', 'Account creation_date must be positive.');
                end

                % 6. Kind of the last update argument checking:
                if isempty(up_date)
                    up_date = creation_date;
                elseif (~isnumeric(up_date))
                    up_date = datenum(up_date, 'yyyy-mm-dd');
                end

                %7. Kind of the initial amount argument checking:
                if (isempty(initam))
                    error('dml:portfolio:mismatchInput', 'An empty initial amount.');
                elseif all(size(initam) ~= [1,1])
                    error('dml:portfolio:mismatchInput', 'Initial amount must be a scalar.');
                elseif (~isnumeric(initam))
                    error('dml:portfolio:mismatchInput', 'Non-numeric initial amount value.');
                elseif (initam <= 0)
                    error('dml:portfolio:mismatchInput', 'Negative initial amount value.');
                end

                %8. Kind of benchmark:
%                 if isempty(benchmark_id)
%                     benchmark_id = 601;
%                 end; 

                %9. Kind of filter id:
                if isempty(filter_id)
                    filter_id = 0;
                end

                %10. Kind of allocation argument checking:
                if (nargin > 9) 
                    if ((isempty(allocation)) || (~isa(allocation, 'dataset')))
                        sec_flag = false;   
                        warning('dml:portfolio:mismatchInput', ...
                            'The list of portfolio assets is empty or built by using some non-structure data unit.');
                    else
                        sec_flag = true;
% %                         next line is remarked in new version (old portfolio DB, new Idan's & Hillel's cumulative function)
% %                         since in this version the cum-func needs all allocation, including the dead bonds
%                         allocation = allocation(intersect(find(~isnan(allocation.count)), find(allocation.count ~= 0)), :);
                    end
                end

                %11. Collecting optional variable values:
                if (nargin > 10) && (~isempty(port_model)) && ~isa(port_model, 'dataset')
                    error('dml:portfolio:mismatchInput', 'The portfolio model must have dataset type.');
                end
                
            end
            
            
            %% Initialization %%
            if nargin > 0
                obj.account_id = account_id;
                obj.owner_id = owner_id;
                obj.manager_id = manager_id;

                obj.account_name = account_name;
                obj.initial_amount = initam;            
                
                obj.creation_date = creation_date;
                if  (nargin > 11) && (~isempty(prev_update))
                    if isnumeric(prev_update)
                        obj.prev_update = prev_update;
                    else
                        obj.prev_update = datenum(prev_update, 'yyyy-mm-dd');
                    end
                else
                    obj.prev_update=creation_date;
                end
                obj.last_update = up_date;
                obj.filter_id = filter_id;

                % Create portfolio allocation data:
                if ( sec_flag )
%                     allocation = sortrows(allocation,1); %remarked 
                    obj.allocation.nsin = allocation.nsin;
                    obj.allocation.count = allocation.count;
                    obj.allocation.holding = allocation.holding;
                    if (size(allocation,2) > 3)
                        obj.allocation.rf_reinv_flag = allocation.rf_reinv_flag;
                        obj.allocation.rf_reinv_rate = allocation.rf_reinv_rate;
                        obj.allocation.rf_reinv_amount = allocation.rf_reinv_amount;
                        obj.allocation.bench_reinv_flag = allocation.bench_reinv_flag;
                        obj.allocation.bench_reinv_coef = allocation.bench_reinv_coef;
                        obj.allocation.bench_reinv_amount = allocation.bench_reinv_amount;
                    else
                        obj.allocation.rf_reinv_flag = NaN(size(allocation,1),1);
                        obj.allocation.rf_reinv_rate = NaN(size(allocation,1),1);
                        obj.allocation.rf_reinv_amount = NaN(size(allocation,1),1);
                        
                        obj.allocation.bench_reinv_flag = NaN(size(allocation,1),1);
                        obj.allocation.bench_reinv_coef = NaN(size(allocation,1),1);
                        obj.allocation.bench_reinv_amount = NaN(size(allocation,1),1);
                    end                    
                    obj.allocation.weight = obj.allocation.holding / sum(obj.allocation.holding);
                end
                
                % Create portfolio model properties:
                if ~isempty(port_model)
                    obj.model = port_model;
                end

                % Create portfolio cumulative return:
                if (nargin > 12) && (~isempty(fts_port_hold_ret)) && isa(fts_port_hold_ret, 'fints')
                    port_hold_ret_dates=cellstr(datestr(fts_port_hold_ret.dates,'yyyy-mm-dd'));
                    port_hold_ret_value=fts2mat(fts_port_hold_ret);
                    port_hold_ret=dataset(port_hold_ret_dates,port_hold_ret_value , 'VarNames', {'obs_date', 'value'});
                    obj.cumulative_return = port_hold_ret;
                    obj.ftsCumulative_return=fts_port_hold_ret;
                end
                
                % Create portfolio's seperate bonds cumulative return:
                if (nargin > 13) && (~isempty(fts_bond_hold_ret)) && isa(fts_bond_hold_ret, 'fints')                    
                    obj.ftsCumulative_return_perBond=fts_bond_hold_ret;
                end
                
                % Create portfolio's seperate bonds cumulative return:
                if (nargin > 14) && (~isempty(riskLessRate))
                    obj.riskLessRate=riskLessRate;
                end
                
                if (nargin >15) && (~isempty(ReinvestmentStrategy))
                    obj.ReinvestmentStrategy = ReinvestmentStrategy;
                end
            end
                        
            %% Post Initialization %%
            if (nargin > 0)
                % Update computed properties:
               [obj.calc_err_msg] = calc( obj, benchmark_id );

    %             % Add nsin to money_breakdown group:
    %             obj.money_breakdown = obj.money_breakdown(ones(size(obj.allocation,1),1),:);
    %             obj.money_breakdown.nsin = obj.allocation.nsin;
            end
        end
        [err_msg] = calc( obj, varargin ) 
        [ ds ] = get_allocation( obj, rdate )
        [ ds ] = get_account_id( obj )
        [ ds ] = get_assets_id( obj )
        [ ds ] = get_holdings( obj, nsin )
        [ ds ] = get_counts(obj, nsin )
        [ ds ] = get_total(obj)
        [ ds ] = get_risk(obj, rm_prop)       
        [bool, err_msg, account_id] = save(obj)
        [] = update(obj, varargin)
    end
    methods (Static)
        [ ds ] = get_history(account_name, dyn_prop_name, first_date, last_date);
    end   
    
end

