function [ value ] = get_assets_id( obj )
%PORTFOLIO\GET_ASSETS_ID returns list of included asset's ids.
%
%   [ value ] = get_assets_id( obj ) 
%   receives object and returns list of ids.
%
%   Inputs:
%       obj - is an object class.
%
%   Outputs:
%       value - is an NASSETSx1 vector specifying the nsin numbers of the assets from the
%                  portfolio. 
%

% Yigal Ben Tal
% Copyright 2011-2012.

    %% Input validation:
    error(nargchk(1, 1, nargin));
           
    %% Step #1 (Output data assignment):
    if (~isempty( obj.allocation ))
        value = obj.allocation.nsin;
    else
        value = [];
    end

end
