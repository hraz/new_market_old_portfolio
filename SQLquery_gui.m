function varargout = SQLquery_gui(varargin)
% SQLQUERY_GUI MATLAB code for SQLquery_gui.fig
%      SQLQUERY_GUI, by itself, creates a new SQLQUERY_GUI or raises the existing
%      singleton*.
%
%      H = SQLQUERY_GUI returns the handle to a new SQLQUERY_GUI or the handle to
%      the existing singleton*.
%
%      SQLQUERY_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SQLQUERY_GUI.M with the given input arguments.
%
%      SQLQUERY_GUI('Property','Value',...) creates a new SQLQUERY_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SQLquery_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SQLquery_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SQLquery_gui

% Last Modified by GUIDE v2.5 28-May-2013 16:52:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SQLquery_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @SQLquery_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SQLquery_gui is made visible.
function SQLquery_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SQLquery_gui (see VARARGIN)

% Choose default command line output for SQLquery_gui
handles.output = hObject;
handles.default_sql_string=get(handles.sql_query_txtbox,'String');
handles.default_outVarName=get(handles.outVarName,'String');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SQLquery_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SQLquery_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function sql_query_txtbox_Callback(hObject, eventdata, handles)
% hObject    handle to sql_query_txtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.sql_string=get(hObject,'String');
% Hints: get(hObject,'String') returns contents of sql_query_txtbox as text
%        str2double(get(hObject,'String')) returns contents of sql_query_txtbox as a double
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function sql_query_txtbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sql_query_txtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
strSqlQuery=get(handles.sql_query_txtbox,'String');
%% Step #1 (Execution of sql function):
import dal.mysql.connection.*;
setdbprefs ('DataReturnFormat','dataset');                                                     
conn = mysql_conn('portfolio');
tic;sqlRes=fetch(conn,strSqlQuery);toc
%% Step #2 (Close the opened connection):
close(conn);

%% Step #3 exporting the data to WS
contents = get(handles.WS_for_export,'String');
strWS=contents{get(handles.WS_for_export,'Value')};
nameOfVariable=get(handles.outVarName,'String');
assignin(strWS,nameOfVariable,sqlRes);
nameofstrVariable=[nameOfVariable '_txt'];
assignin(strWS,nameofstrVariable,strSqlQuery);



function outVarName_Callback(hObject, eventdata, handles)
% hObject    handle to outVarName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outVarName as text
%        str2double(get(hObject,'String')) returns contents of outVarName as a double


% --- Executes during object creation, after setting all properties.
function outVarName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outVarName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in WS_for_export.
function WS_for_export_Callback(hObject, eventdata, handles)
% hObject    handle to WS_for_export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns WS_for_export contents as cell array
%        contents{get(hObject,'Value')} returns selected item from WS_for_export


% --- Executes during object creation, after setting all properties.
function WS_for_export_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WS_for_export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
