clear; clc;

import utility.calendar.*;

days1 = ['01-01-2010'; '01-07-2011'];
days2 = datenum(['29-01-2012'], 'dd-mm-yyyy');

[year_fraction] = yearfrac(days1, days2, 10)
