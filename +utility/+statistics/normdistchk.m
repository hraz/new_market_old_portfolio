function [chitRes, kstRes, lltRes, proptRes, ztRes] = normdistchk(sampleDist,nullProportion,significance)
% NORMDISTCHK checks the null hypothesis at a 95% confidence level that the
% given data has a normal distribution (chi-squared test,
% Kolmogorov-Smirnov test, and Lilliefors test) or that it came from a
% reference normal distribution (proportion test and z-test).
%
% 	[chit_res, kst_res, llt_res, propt_res, zt_res] = normdistchk(x)
% 	receives a vector of sample data and returns the results of five
% 	different statistical tests.
%
%	Input:
%		sampleDist - is a Nx1 vector of doubles.  Note that the calls to
%           the location tests (proportion test and ztest) assume that the
%           null distribution of the data has a mean of 0.  For example, if
%           the data in sampleDist are portfolio returns minus benchmark
%           returns, sampleDist will be compared to a null normal
%           distribution that is centered around the point where the
%           portfolio returns are equal to the benchmark returns (i.e.
%           zero).
%
%       nullProportion - a double between 0 and 1 that represents the
%           proportion of values larger than setPoint.  Default is 0.5
%           unless an input is provided.
%
%       significance - a double between 0 and 1 that represents the desired
%           level of confidence in the test.  The p-value resulting from
%           the test is compared to this value to determine whether to
%           reject or not reject the null hypothesis. For example, a
%           significance of 0.05 corresponds to a 95% confidence level.
%           Default is 0.05 unless an input is provided.
%
%	Output:
%       All of the 5 outputs are 1x2 cell arrays that contain a char array
%       in the first cell  and a double in the second cell. 'Yes' in the
%       first cell indicates rejection of the null hypothesis for the
%       particular test, while 'No' indicates not to reject the null
%       hypothesis.  The double in the second cell is the p-value for the
%       particular test and will have a value between 0 and 1.  The p-value
%       quantifies the probability of obtaining a statistic at least as
%       extreme as the one seen in the sample, assuming the null hypothesis
%       is true.  For example, if p = 0.02, there is only a 2% probability
%       of obtaining a test statistic more extreme than the one seen in the
%       sample.  This suggests that the null hypothesis may not be true,
%       depending on the specified level of significance.
%
%		chitRes - are the results from the chi-squared goodness of fit
%           test.  The null hypothesis is that sampleDist is a random
%           sample from a normal distribution with the same mean and
%           standard deviation as sampleDist.
%
%		kstRes - are the results from the Kolmogorov-Smirnoff test.  The
%           null hypothesis is that sampleDist has a standard normal
%           distribution.  Thus, sampleDist is scaled before it is inputted
%           into kstest.
%
%		lltRes - are the results from the Lilliefors test.  The null
%           hypothesis is that sampleDist is a random sample from a normal
%           distribution with the same mean and standard deviation as
%           sampleDist.
%
%		proptRes - are the results from the proportion test.  The null
%           hypothesis is that sampleDist is a random sample from a normal
%           population where an inputted proportion of the data is larger
%           than 0.  The alternative hypothesis is that sampleDist is a
%           random sample from a population where more than the inputted
%           proportion of the data is larger than 0. Proportiontest is a
%           right one-tailed (one-sided) test.
%
%       ztRes - are the results from the z-test.  The null
%           hypothesis is that sampleDist is a random sample from a normal
%           population with mean 0 and the same standard deviation as
%           sampleDist.
%
% Developed by Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 18, 2013. Added z-test and proportion
% test, extended input validation, and wrote more comprehensive comments.
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

import utility.statistics.*

%precision at which the p-values will be displayed in the gui
PRECISION_STRING = '%.4f';

%% Input validation:
error(nargchk(3, 3, nargin));

if isempty(sampleDist)
    chitRes = {'Normdistchk: Not enough data', []};
    kstRes = {'Normdistchk: Not enough data', []};
    lltRes = {'Normdistchk: Not enough data', []};
    proptRes = {'Normdistchk: Not enough data', []};
    ztRes = {'Normdistchk: Not enough data', []};
    return
end

flagType(1) = isa(sampleDist, 'double');

if nargin < 2 || isempty(nullProportion)
    flagType(2) = 1;
    nullProportion = 0.5; % Default null proportion
else  
    flagType(2) = isa(nullProportion, 'double');
    
    if nullProportion > 1 | nullProportion < 0
        error('Normdistchk: nullProportion must be between 0 and 1')
    end
end

if nargin < 3 || isempty(significance)
    flagType(3) = 1;
    significance = 0.05; % Standard 95% confidence level.

else
    flagType(3) = isa(significance, 'double');
    
    if significance>1 | significance<0
        error('Normdistchk: significance must be between 0 and 1')
    end
end

if sum(flagType) ~= length(flagType)
    error('Normdistchk: One or more input arguments is not of the right type');
end
%% Step #1 (Check distribution type using chi-test):

[chitH, chitP] = chi2gof(sampleDist,'alpha',significance);
if ~isnan(chitH) && ~isnan(chitP)
    if chitH
        chitRes = {'Yes', sprintf(PRECISION_STRING,chitP)};
    else
        chitRes = {'No', sprintf(PRECISION_STRING,chitP)};
    end
else
    chitRes = {'---', '---'};
end

%% Step #2 (Check distribution type using Kolmogorov-Smirnov test):

% Scale sampleDist so that it is comparable to the standard normal
% distribution (required for the K-S test)
[kstH, kstP] = kstest((sampleDist-mean(sampleDist))/std(sampleDist),[],significance);
if ~isnan(kstH) && ~isnan(kstP)
    if kstH
        kstRes = {'Yes', sprintf(PRECISION_STRING,kstP)};
    else
        kstRes = {'No', sprintf(PRECISION_STRING,kstP)};
    end
else
    kstRes = {'---', []};
end

%% Step #3 (Check distribution type using Lilliefors Test):

% Suppress lillietest warning about very small p values
warning('off','stats:lillietest:OutOfRangePLow');

[lltH, lltP] = lillietest(sampleDist,significance);
if ~isnan(lltH) && ~isnan(lltP)
    if lltH
        lltRes = {'Yes', sprintf(PRECISION_STRING,lltP)};
    else
        lltRes = {'No', sprintf(PRECISION_STRING,lltP)};
    end
else
    lltRes = {'---', []};
end

%% Step #4 (Check distribution type using the proportion test):

% Because there are no additional arguments inputted into proportiontest,
% the defaults (nullProportion = 0.5, setPoint = 0, and significance =
% 0.05) will be into effect.

[proptH, proptP] = proportiontest(sampleDist,nullProportion,significance);
if ~isnan(proptH) && ~isnan(proptP)
    if proptH
        proptRes = {'Yes', sprintf(PRECISION_STRING,proptP)};
    else
        proptRes = {'No', sprintf(PRECISION_STRING,proptP)};
    end
else
    proptRes = {'---', []};
end

%% Step #5 (Check distribution type using z-test):

% Inputting 0 and std(sampleDist) into the ztest tells it to compare sampleDist to a
% normal distribution with mean 0 and the same standard dev as sampleDist.
[ztH, ztP] = ztest(sampleDist, 0, std(sampleDist), significance,'right');
if ~isnan(ztH) && ~isnan(ztP)
    if ztH
        ztRes = {'Yes', sprintf(PRECISION_STRING,ztP)};
    else
        ztRes = {'No', sprintf(PRECISION_STRING,ztP)};
    end
else
    ztRes = {'---', []};
end
