clear; clc;
tic
import utility.statistics.*;

x_norm = normrnd(0,0.05,[10000,1]);

%tests that check the error handling of bad inputs. All of the following
%should result in exiting from the function.
%proportiontest()
%proportiontest(x_norm,2,3,4,5)
%proportiontest([],1)
%proportiontest(x_norm,1.2)
%proportiontest(x_norm,0.5, 1.2)
%proportiontest('test')
%proportiontest(x_norm,'test')
%proportiontest(x_norm,0.5,'test')
%proportiontest(x_norm,0.5, 0.05, 'test')

%should all give the same value
disp('without inputs')
[h p]=proportiontest(x_norm)
disp('inputting defaults')
[h p]=proportiontest(x_norm, 0.5, 0.05, 0)
disp('changing significance to 0.01')
[h p]=proportiontest(x_norm, 0.5, 0.0001, 0)

%should not reject null
disp('changing nullProportion to 0.9')
[h p]=proportiontest(x_norm, 0.9, 0.05, 0)
%should reject null
disp('changing nullProportion to 0.1')
[h p]=proportiontest(x_norm, 0.1, 0.05, 0)

%should reject null
disp('changing setPoint to -0.03')
[h p]=proportiontest(x_norm, 0.5, 0.05, -0.03)
%should not reject null
disp('changing setPoint to 0.03')
[h p]=proportiontest(x_norm, 0.5, 0.05, 0.03)


%compare proportiontest to matlab's ztest
for i = 1:10000
    
x_norm = normrnd(0,0.05,[10000,1]);
x_rand = rand([10000,1])-0.5;

[h p1_norm]=proportiontest(x_norm);
[h p1_rand]=proportiontest(x_rand);

%Note that the matlab documentation on their website is for an updated
% function.  They say to use the form "[h,p] =
%ztest(x,65,10,'Tail','right'), but that doesn't work in version R2012b of
%Matlab.  The documentation from our version of Matlab (R2012b) gives the
%call form I used below.

[h p2_norm c1 zScore]=ztest(x_norm, 0, std(x_norm), 0.05,'right');
[h p2_rand c1 zScore]=ztest(x_rand, 0, std(x_rand), 0.05,'right');

difference_norm(i)=p2_norm-p1_norm;
difference_rand(i)=p2_rand-p1_rand;

end

figure(1)
hist(difference_norm,100)
title('Normally distributed data')
xlabel('p of ztest - p of proportional test')
ylabel('Number of hits')


figure(2)
hist(difference_rand,100)
title('Randomly distributed data')
xlabel('p of ztest - p of proportional test')
ylabel('Number of hits')


%mockup of real data
x_real = x_norm+6*x_rand+0.5;
[h p_prop_real]=proportiontest(x_real);
[h p_z_real c1 zScore]=ztest(x_real, 0, std(x_real), 0.05,'right');

%The values for these variables may seem ridiculous, but they are accurate for this
%data.  The mean is 1.76 and is being compared to a normal
%distribution with a mean at 0.5.  Likewise, the proportion of data larger than
%zero is the data sample is very large (0.91) compared to the null
%distribution (0.5).  Lastly, the sample size is very large, so the
%standard error (used to calculate the zscore) is extremely small.  As
%a result, the zscore is huge, resulting in a p value of almost zero.
figure(3)
hist(x_real,100)
title('Real data')
xlabel('Yield?  Not sure...')
ylabel('Number of hits')
p_prop_real
p_z_real
zScore

toc