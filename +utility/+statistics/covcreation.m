function [ cov_mtx ] = covcreation(A) %dim)
%COVCREATION simulated covariance matrix.
%
%   [ cov_mtx ] = covcreation(dim) receives dimention and creates some
%   random covariance matrix.
%
%   Inputs:
%       dim - is a scalar value specifying the needed matrix dimention.
%
%   Outputs:
%       cov_mtx - is a DIMxDIM simmetric positive defined covariance
%                         matrix.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(1,1,nargin));
    
%     if (isempty(dim))
%         error('utility:covcreation:wrongInput', ...
%             'The argument dim cann''t get an empty value.');
%     elseif (~isinteger(dim))
%         error('utility:covcreation:wrongInput', ...
%             'The value of argument dim must be an integer number.');
%     end

    %% Step #1 (Creation random matrix as uniform random values from [0,1]):
%     RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
%     A = abs(sum(rand(dim, dim,  12), 3) - 6);
    
    %% Step #2 (Creation low triangle matrix from the last given):
    LA = tril(A);
    
    %% Step #3 (Creation covariance simmetric matrix):
    cov_mtx = LA * LA';
    
end
