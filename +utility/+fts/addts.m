function [ res_fts ] = addts( old_fts, new_fts )
%ADDTS adds to given fts newest timeseries from new fts.
%
%   [ res_fts ] = addts( old_fts, new_fts )
%   receives old and new financial timeseries and return sorted by series
%   names result financial timeseries.
%   
%   Input:
%       old_fts - is a scalar financial timeseries object specifying old
%                     dynamic data of some type.
%
%       new_fts - is a scalar financial timeseries object specifying new
%                      dynamic data of some type.
%
%   Output:
%       res_fts - is a scalar financial timeseries object specifying the
%                    result of merging old and new financial timeseries
%                    that sorted by its series names.
%
% Yigal Ben Tal
% Copyright 2011

    %% Input validation:
    error(nargchk(2,2,nargin));
    if (~isa(old_fts, 'fints') || ~isa(new_fts, 'fints'))
        error('utility_fts:addts:wrongInput', 'Wrong parameter type.');
    end
    if (isempty(new_fts)) && (isempty(old_fts))
        res_fts = fints();
        return;
    elseif (isempty(new_fts))
        res_fts = old_fts;
        return;
    elseif (isempty(old_fts))
        res_fts = new_fts;
        return;
    end

    %% Import external packages:
    import utility.fts.*;
    
    %% Step #1 (Get series names):
    old_series = tsnames(old_fts);
    new_series = tsnames(new_fts);
    
    %% Step #2 (Find new series names):
    new_ind = find(ismember(new_series, old_series));
    
    %% Step #3 (Extract new data from new_fts):
    new_data_date = fts2mat(new_fts, 1);
    
    %% Step #4 (Select just newest data):
    new_series = new_series(new_ind);
    new_data_date = new_data_date(:, [1, new_ind+1]);
    
    %% Step #5 (Build newest fts):
    new_fts = fints(new_data_date(:, 1), new_data_date(:, 2:end), new_series);
    
    %% Step #6 (Add newest data to old fts object):
    res_fts = [old_fts, new_fts];
    
    %% Step #7 (Sort result fts by its series names):
    res_fts = sortfts(res_fts, sort(tsnames(res_fts)));

end

