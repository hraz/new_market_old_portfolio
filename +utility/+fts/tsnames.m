function [ fts_name ] = tsnames( fts )
%TSNAMES return just time series names of given fts object.
%
%   [ fts_name ] = tsnames( fts )
%   receives fints object and returns names of its series.
%
%   Input:
%       fts - is a scalar financial timeseries object.
%
%   Output:
%       fts_name - is an NSERIESNAMESx1 cell string array specifying the
%                        timeseries names.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(1,1,nargin));
    if (~isa(fts, 'fints'))
        error('utility_fts:tsnames:wrongInput', 'Wrong parameter type.');
    end
    if (isempty(fts))
        fts_name = {};
        return;
    end
    
    %% Step #1 (Extract all fields):
    info = ftsinfo(fts);
    
    %% Step #2 (Select just time series names):
    fts_name = info.seriesnames;
    
end

