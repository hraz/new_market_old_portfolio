function dplot(varargin)
plot(varargin{:}); 
% set(gca,'XTickLabel',datestr(get(gca,'XTick'),'dd/mm HH:MM'))
set(gca,'XTickLabel',datestr(get(gca,'XTick'),'dd/mm/yy'))
h=zoom;
% set(h,'ActionPostCallback','set(gca,''XTickLabel'',datestr(get(gca,''XTick''),''dd/mm HH:MM''))');
set(h,'ActionPostCallback','set(gca,''XTickLabel'',datestr(get(gca,''XTick''),''dd/mm/yy''))');
