function [exp_return exp_return_annually]=calculate_expected_return(nsin_list,Wts,req_date,min_hist_length,portfolio_end_date,reinvest_strategy_id,ExpInflation,varargin)
import dal.market.get.dynamic.*;
import bll.inflation_mdl.*;

if ~isempty(varargin)
    Dataset=varargin{1};
else
    import dal.market.get.static.*;
    static_data_names = {'linkage_index'};
    Dataset = get_bond_stat( nsin_list, datenum(req_date), static_data_names);                     %extract all static data fields at once    
end


tbl_name = 'bond';
data_name = {'yield', 'ttm'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin_list, data_name , tbl_name, req_date - min_hist_length, req_date);    
all_yield=fts2mat(res_fts.yield);
current_yield=all_yield(end,:);
current_yield=AddExpInflation(current_yield,Dataset.linkage_index,ExpInflation);  %add expected inflation to YTM.
all_ttm=fts2mat(res_fts.ttm);
current_ttm=all_ttm(end,:);
portfolio_ttm=(portfolio_end_date-req_date)/365;
actual_ttm=min(current_ttm,portfolio_ttm);
exp_return=((1+current_yield).^(actual_ttm)-1)*Wts;
if portfolio_ttm==0
    exp_return_annually=0;
else
    exp_return_annually=(1+exp_return)^(1/portfolio_ttm)-1;
end
