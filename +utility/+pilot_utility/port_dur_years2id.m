function [portfolio_duration_id notification_id]=port_dur_years2id(portfolio_duration)

durations_table=[1,5,8,13,17];

if portfolio_duration<durations_table(1)
    portfolio_duration_id=1;
    notification_id=121;
elseif portfolio_duration>=durations_table(end)
    portfolio_duration_id=length(durations_table)-1;
    notification_id=121;
else
    portfolio_duration_id=find(durations_table<=portfolio_duration,1,'last');
    notification_id=0;
end
