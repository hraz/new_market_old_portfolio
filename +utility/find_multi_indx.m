function out_indx=find_multi_indx(partial_list,full_list)
% finds the indices of array A in array B for A that is contained in B. 
% roughly - 10% faster than its equal arrayfun command:
% arrayfun(@(x)find(B==x,1,'first'),A)
% 
% out_indx=find_multi_indx(A,B) where A,B are vectors returns the indices of B such 
%     that B(out_indx)=A
% out_indx=find_multi_indx(A,B) where A,B are multi dimesional returns the indices of B such 
%     that Bv(out_indx)=Av where Av=A(:) and  Bv=B(:). notice that out_indx
%     is a vector
% % 
% Amit Godel 2012
partial_list=partial_list(:);
full_list=full_list(:);
out_indx=nan(size(partial_list));
if isempty(setdiff(partial_list,full_list))
    for m=1:length(partial_list)
        out_indx(m)=find(full_list==partial_list(m),1,'first');
    end
else
    error('A is not contained in B in find_multi_indx(A,B)')
end