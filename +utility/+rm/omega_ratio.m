function [ omega ] = omega_ratio( data, mar )
%OMEGA returns the probability weighted ratio of gains to losses, relative to the given threshold.
%
%   [ omega ] = omega_ratio( data, mar )
%   receives NOBSERVATIONSxNASSET matrix of assets returns, minimum acceptable return and returns
%   the omega ratio value. 
%
%   Input:
%       data - is an NOBSERVATIONSxNASSET matrix specifying the assets/portfolio returns.
%
%       mar - is a scalar specifying the minimum acceptable return.
%
%   Output:
%       omega - is a scalar specifying the Omega ratio.
%
% Yigal Ben Tal
% Copyright 2012.

    %% Input validation:
    error(nargchk(2,2,nargin));
    if isempty(data)
        error('utility_rm:omega_ratio:mismatchInput', ...
            'The data is empty.');
    elseif ~isnumeric(data)
        error('utility_rm:omega_ratio:wrongInput', ...
            'The data is not numeric.');
    end
    if isempty(mar)
        error('utility_rm:omega_ratio:mismatchInput', ...
            'The minimal acceptable return value is empty.');
    elseif ~isnumeric(mar)
        error('utility_rm:omega_ratio:wrongInput', ...
            'The minimal acceptable return value is not numeric.');
    end
        
    %% Omega calculation:
    omega = lpm(-data, -mar, 1) / lpm(data, mar,1); 
    
end

