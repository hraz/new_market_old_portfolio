function [ string ] = ds2str4sql( ds, flag, format )
%DS2STR4SQL Convert a 2-D MATLAB dataset to a string in MySQL syntax.
%
%   [ string ] = ds2str4sql(ds, flag) converts the 2-D dataset ds to a string value in MySQl syntax
%
%   Input:
%       ds - is a dataset value specifying any alpha-numeric data. 
%               In most cases it must be full numeric data, but in
%               case of rating constraints it the last column must be a string value.
%
%       flag- is a scalar value specifying the follow cases:
%               - 0 - means regular full numeric dataset;
%               - 1 - means first column is datetime values in numeric MATLAB format;
%               - 2 - means last column is a cell of strings.
%
%   Output:
%       string - is a string value specifying the given dataset values, where row' delimiter is comma,
%               and the column delimiter is a semicolon.
%
%   Example
%       ds = [234, 345; 230, 456];
%       ds2str4sql(ds) produces the string '234,345;230,456'; its equal to
%                               234,345;230,456 as result list for MySQL.

% Created by Yigal Ben Tal.
% Date:		08.05.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages;
    import utility.dal.*;

    %% Input validation:
   error(nargchk(2,3,nargin));
    
    if isempty(ds)
        error('utility_dataset:ds2str4sql:mismatchInput','Input argument is empty.');
    end
    if (flag ~=2) && (~all(cell2mat(datasetfun(@isnumeric,ds,'UniformOutput',false))))
        error('utility_dataset:ds2str4sql:wrongInput','Input argument must be a dataset of numbers.');
    elseif (flag ==2) && (~all(cell2mat(datasetfun(@isnumeric,ds(:,1:end-1),'UniformOutput',false)))) && ...
            (~all(cell2mat(datasetfun(@iscellstr, ds(:, end), 'UniformOutput',false))))
        error('utility_dataset:ds2str4sql:wrongInput',...
            'Input argument must be a dataset of numbers and just last column must be a strings.');
    end
    
    if nargin < 3
        format = 'g';
    end
   
    %% Retype dataset into matrix:
    if (flag ~= 2)
        mtx = double(ds);
    else
        mtx = double(ds(:,1:end-1));
    end
        
    %% Remove datetime if it exists:  
    switch flag
        case 0
            string = strrep(strrep(strrep(mat2str(mtx, format), ' ', ','), '[', ''), ']', ''); 
        case 1
            datetime = cellstr(datestr(mtx(:,1), 'yyyy-mm-dd HH:MM:SS'));
            string = '';
            for i = 1:size(ds,1)
                string = [string,  '''' datetime{i} '''', strrep(strrep(strrep(mat2str(mtx(i,2:end), format), ' ', ','), '[', ','), ']', ';')];
            end
            string = string(1:end-1);
        case 2
            string = strrep([strrep(strrep(strrep(mat2str(mtx, format), ' ', ','), '[', ''), ']', ''), ',''',ds{:,end}, ''''], ''',', ',');
        otherwise
            error('utility_dataset:ds2str4sql:wrongInput',...
                'Last input parameter FLAG may have just one of {0,1,2} values.');
    end
    
    %% Replace NaN with NULL:
    string = strrep(string, 'NaN', 'NULL');
     
end
