function handles = remove_nans_pilot(handles)
YtmFTS=handles.YtmFTS;
ModDurFTS=handles.ModDurFTS;
PriceFTS=handles.PriceFTS;
PriceYld=handles.PriceYld;
price_clean_yld=handles.price_clean_yld;
ConvexityFTS=handles.ConvexityFTS;

M=fts2mat(YtmFTS);
nan_indx_YtmFTS=find(sum(isnan(fts2mat(YtmFTS)),2)>0);
nan_indx_ModDurFTS=find(sum(isnan(fts2mat(ModDurFTS)),2)>0);
nan_indx_PriceFTS=find(sum(isnan(fts2mat(PriceFTS)),2)>0);
nan_indx_PriceYld=find(sum(isnan(fts2mat(PriceYld)),2)>0);
nan_indx_price_clean_yld=find(sum(isnan(fts2mat(price_clean_yld)),2)>0);
nan_indx_ConvexityFTS=find(sum(isnan(fts2mat(ConvexityFTS)),2)>0);
nan_indx=union(nan_indx_YtmFTS,nan_indx_ModDurFTS);
nan_indx=union(nan_indx,nan_indx_PriceYld);
nan_indx=union(nan_indx,nan_indx_PriceFTS);
nan_indx=union(nan_indx,nan_indx_price_clean_yld);

all_indx=1:length(YtmFTS);
good_indx=setdiff(all_indx,nan_indx);

handles.YtmFTS=YtmFTS(good_indx);
handles.ModDurFTS=ModDurFTS(good_indx);
handles.PriceFTS=PriceFTS(good_indx);
handles.PriceYld=PriceYld(good_indx);
handles.price_clean_yld=price_clean_yld(good_indx);
handles.ConvexityFTS=ConvexityFTS(good_indx);
if length(good_indx)<handles.HistLen
    handles.HistLen=length(good_indx);
end





