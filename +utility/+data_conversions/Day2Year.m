function annualReturn=Day2Year(dailyReturn)

%   DAY2YEAR translates daily return to annual returna by compounding the
%   daily return over 252 average business days per year. 
%			
%   annualReturn=Day2Year(dailyReturn) receives the daily return of some
%   asset and compounds it to annual terms according to the internal rate
%   of return conversion formula.
%	Input:
%       dailyReturn � a vector or scalar of average daily returns of assets. 
%   Output:
%       annualReturn � a vector or scaler of compounded annual returns
%       according to the following formula:
%       annualReturn=(1 + dailyReturn).^252 -1
%   Sample:
%		Some example of the function using.
% 	
%		See Also:
%		year2day
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

annualReturn =(1 + dailyReturn).^252 -1;