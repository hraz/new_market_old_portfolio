function [ out_param ] = set_fltr_ttl_stat_args( stat_param )
%SET_FLTR_TTL_STAT_ARGS prepare the dynamic arguments for filter_ttl
%
%   [ out_param ] = set_fltr_ttl_stat_args( dyn_param )
%  recieves a struct and return a struct
%
%   Inputs:
%       dyn_param - is a struct containing static arguments for
%       total filter
%
%   Outputs:
%       out_param - is a struct containing all ready arguments for calling
%       total filter

%  Yaakov Rechtman
% 11/7/2013
% BondIT LTD
 import utility.dal.*;
    import utility.*;
    %% Input validation:
    error(nargchk(1, 1, nargin));
      %% Step #1 (Input parsing):
    p_stat = inputParser;    % Create an instance of the class_name:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
    p_stat.addParamValue('class_name', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('coupon_type', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('linkage_index', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('sector', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('bond_type', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('redemption_error', {}, @(x)(iscellstr(x) || iscell(x)));
    
    % Parse method to parse and validate the inputs:
    p_stat.StructExpand = true; % for input as a structure
    p_stat.KeepUnmatched = true;
    p_stat.parse(stat_param);

    %% Step #2 (MATLAB puts the results of the parse into a property named Results):
    out_param = p_stat.Results;
    var_stat = p_stat.Parameters;
    num_vars_stat = numel(var_stat);
        
    %% Step #3 (Fill the missing parameters (empty values)):
    for i = 1 : num_vars_stat
        if isempty(out_param.(var_stat{i})) || all(cell2mat(cellfun(@(x)isempty(x),out_param.(var_stat{i}), 'UniformOutput', false)))
            out_param.(var_stat{i}) = 'NULL';
        else
            out_param.(var_stat{i}) = cell2str4sql( out_param.(var_stat{i}));
        end
    end   
    
    
end

