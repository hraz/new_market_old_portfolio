function [ tu ] = timeunitdef( time )
%TIMEUNITDEF determines the size of time step.
%
%   [ tu ] = timeunitdef( time )  receives time vector in numerical representation and returns
%   string that specifying the avarage size between two successive time values.
%
%   Inputs:
%       time - is a NOBSERVATIONx1 vector of time (or dates) specifying the observation time values.
%
%   Outputs:
%       tu - is a character value specifying the first letter of time period name like 'd' - 'day' or 'w' - 'week'.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(1, 1, nargin));
    if (isempty(time)), tu = '';    return; end
    
    if (~isnumeric(time))
        error('utility:timeunitdef:wrongInput', ...
            'The time must be a numeric vector.');
    end
    
    %% Step #1 (Building the time deviations):
    time_l = length(time);  diff = zeros(time_l, 1);
    for i = 1:time_l-1,     diff(i) = time(i+1) - time(i);  end
    
    %% Step #2 (Mean time deviation computing):
    Ex = fix(mean(diff));
    
    %% Step #3 (Time step size determination):
    import db.const.time_units.*;
    if (Ex < 5)
        tu = time_units.d;
    elseif (Ex < 10)
        tu = time_units.w;
    end

end

