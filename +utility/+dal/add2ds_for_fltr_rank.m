function [ ds ] = add2ds_for_fltr_rank( mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr ,ds )
%ADD2ds_FOR_FLTR_RANK add the given parameters to the given dataset
%  
%    [ ds ] = dd2ds_for_fltr_rank( mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr ,ds )receives a paramters and add them to the
%    dataset

%   Input:
%       cInput:
%       mlt_lb - is a string value specifying the infinum value of the Maalot rank of bond. 
%
%       mlt_ub - is a string value specifying the supremum value of the Maalot rank of bond. 
%
%       mdg_lb - is a string value specifying the infinum value of the Midroog rank of bond. 
%
%       mdg_ub - is a string value specifying the supremum value of the Midroog rank of bond. 
%
%       log_opr - is a string value specifying the logical operator for case when user need
%                       select bonds according to both of existed ranks. 
%
%       ds - a dataset on which to add the parameters
% 
%   Output:
%       ds - is dataset containg the given parameters . 
%
% Yaakov Rechtman
% 02/07/2012
 %% Import external packages:
    import utility.dal.*;
     import utility.*;
    
%% Input validation:
    error(nargchk(5, 6, nargin));
    
      % Bounds checking:
    if isempty(mlt_lb)
        mlt_lb =  {'NULL' };
    elseif ~ischar(mlt_lb)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Maalot rating''s lower bound.');
    end
    
    if isempty(mlt_ub)
        mlt_ub = { 'NULL'};
    elseif ~ischar(mlt_ub)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Maalot rating''s upper bound.');
    end
 
    if isempty(mdg_lb)
        mdg_lb = {'NULL'};
    elseif ~ischar(mdg_lb)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Midroog rating''s lower bound.');
    end
    
    if isempty(mdg_ub)
        mdg_ub = {'NULL'};
    elseif ~ischar(mdg_ub)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Midroog rating''s upper bound.');
    end

    if (nargin < 5) || isempty(log_opr)
        log_opr = {'OR'};
    end
        
    %% Step #1 (Coverting of the non-empty input):
    if ~strcmpi(mlt_lb, 'NULL')
        mlt_lb = {['', mlt_lb ,'']};
    end
    if ~strcmpi(mlt_ub, 'NULL')
        mlt_ub = {['', mlt_ub ,'']};
    end
    if ~strcmpi(mdg_lb, 'NULL')
        mdg_lb = {['''', mdg_lb ,'''']};
    end
    if ~strcmpi(mdg_ub, 'NULL')
        mdg_ub ={ ['''', mdg_ub ,'''']};
    end
    log_opr = { ['', log_opr, '']};
    
%     checking if it's the first input
    if( nargin < 6 || isempty(ds) )
        ds = dataset(mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr);
    else
        ds_1 = dataset(mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr);
        ds = [ds; ds_1];
    end
end

