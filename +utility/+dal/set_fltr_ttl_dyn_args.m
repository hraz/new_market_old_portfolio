function [ out_param ] = set_fltr_ttl_dyn_args( dyn_param )
%SET_FLTR_TTL_DYN_ARGS prepare the dynamic arguments for filter_ttl
%
%   [ out_param ] = set_fltr_ttl_dyn_args( dyn_param )
%  recieves a struct and return a struct
%
%   Inputs:
%       dyn_param - is a struct containing dynamic arguments for
%       total filter
%
%   Outputs:
%       out_param - is a struct containing all ready arguments for calling
%       total filter

%  Yaakov Rechtman
% 11/7/2013
% BondIT LTD
 
    %% Import external packages:
    import utility.dal.*;
    import utility.*;
    
    %% Input validation:
    error(nargchk(1, 1, nargin));
    
    %% Step #1 (Input parsing):
    p_dyn = inputParser;    % Create an instance of the class:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
    p_dyn.addParamValue('history_length', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('yield', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('mod_dur', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('turnover', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('ttm', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    
    % Parse method to parse and validate the inputs:
    p_dyn.StructExpand = true; % for input as a structure
    p_dyn.KeepUnmatched = true;
    p_dyn.parse(dyn_param);
    try
         p_dyn.parse(dyn_param);
    catch ME
        newME = MException('dal_filter_dynamic:bond_dynamic_complex:optionalInputError',...
                'Error in input arguments');
        newME = addCause(newME,ME);
        throw(newME)
    end

    %% Step #2 (MATLAB puts the results of the parse into a property named Results):
    out_param = p_dyn.Results;
    var_dyn = p_dyn.Parameters;
    num_vars_dyn = numel(var_dyn);
    
    %% Step #3 (Fill the missing '' (empty values or NaN)):
    for i = 1 : num_vars_dyn
        emp_ind = cellfun(@(x)isempty(x), out_param.(var_dyn{i}));

        nan_ind = cellfun(@(x)isnan(x), out_param.(var_dyn{i}), 'UniformOutput', false);
        nan_ind(cellfun(@(x)isempty(x), nan_ind)) = {false};
        nan_ind = cell2mat(nan_ind);

        out_param.(var_dyn{i})(emp_ind | nan_ind) = {'NULL'};

        num_ind = cellfun(@(x)isnumeric(x), out_param.(var_dyn{i}), 'UniformOutput', true);
        out_param.(var_dyn{i})(num_ind) = cellfun(@(x)num2str(x), out_param.(var_dyn{i})(num_ind), 'UniformOutput', false); 
    end
    
end

