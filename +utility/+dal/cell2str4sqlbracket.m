function [ string ] = cell2str4sqlbracket( cellstr )
%CELL2STR4SQL Convert a 2-D cell array of strings to a string array in MySQL syntax.
%   This function adapted on base cell2str function of Per-Anders Ekstrm.
%   ================================================
%   Exclusion: 
%   This adapted function convert just 1-D cell array of strings
%   to a string in MySQL syntax!
%   ================================================
%
%CELL2STR Convert a 2-D cell array of strings to a string in MATLAB syntax.
%   STR = CELL2STR(CELLSTR) converts the 2-D cell-string CELLSTR to a 
%   MATLAB string so that EVAL(STR) produces the original cell-string.
%   Works as corresponding MAT2STR but for cell array of strings instead of 
%   scalar matrices.
%
%   Example
%       cellstr = {'U-234','Th-230'};
%       cell2str(cellstr) produces the string '''U-234''',',','''Th-230'''; its equal to
%                               'U-234','Th-230' as result list for MySQL.
%
%   See also MAT2STR, STRREP, CELLFUN, EVAL.
%
%   Developed by Per-Anders Ekstrm, 2003-2007 Facilia AB.
%   Adapted by Yigal Ben Tal, 2011

    %% Input validation:
    if nargin~=1
        error('CELL2STR:Nargin','Takes 1 input argument.');
    end
    
    if ischar(cellstr)
       string = ['(' strrep(cellstr,')','''''') ''''];
       return
    end
    
%     if ~iscellstr(cellstr)
%         error('CELL2STR:Class','Input argument must be cell array of strings.');
%     end
    if ndims(cellstr)>2
        error('CELL2STR:TwoDInput','Input cell array must be 2-D.');
    end

    %% Step #1 (Cell array rebuild to array of strings):
    ncols = size(cellstr,2);
    for i = 1 : ncols-1
        cellstr(:,i) = cellfun(@(x)['(''''' strrep(x,'''','''') '''''),'],...
            cellstr(:,i),'UniformOutput',false);
    end
    if ncols>0
        cellstr(:,ncols) = cellfun(@(x)['(''''' strrep(x,'''','''') '''''),'],...
            cellstr(:,ncols),'UniformOutput',false);
    end       
    
    %% Step #2 (Round the cell from column to row):
    cellstr = cellstr';
    
    %% Step #3 (Return the result string):
    string = [ cellstr{:} ];
     string = [ '''' string(1:end-1) ''''];
    
end

