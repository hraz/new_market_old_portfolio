function [ outStrList ] = cellstr2strlist( inCellOfStr )
%CELLSTR2STRLIST accepts a cell array of strings and reformats it as a
%single string.
%
%   [ outStrList ] = cellstr2strlist( inCellOfStr ) turns a cell array of
%   strings (that represent constraints) into the necessary string format
%   to call the SQL database.  Numerical constraints and non-numerical
%   constraints are treated differently.  For example, {'MV','OS','Tar'}
%   will become '"'MV','OS','Tar'"' and {'1','2','3'} will become
%   '"1,2,3"'.
%
%   Inputs:
%       inCellOfStr - is an Nx1 cell array of strings.
%
%   Outputs:
%       outStrList - is a string representation of the given values.
%
% Author: Yigal Ben Tal
% Copyright 2011-2013, BondIT Ltd.
%
% Updated by Eleanor Millman, August 11, 2013: changed the output format
% and added ability to work for numerical and non-numerical strings.
% Copyright 2013, BondIT Ltd.

%% Input validation:
error(nargchk(1, 1, nargin));

if (isempty(inCellOfStr))
    error('utility_dal:cellstr2strlist:mismatchInput', ...
        'Input is empty.');
elseif (~iscellstr(inCellOfStr))
    error('utility_dal:cellstr2strlist:mismatchInput', ...
        'Cell string needed as input.');
end

%% Reformatting the given cell array of strings:

if isnan(str2double(inCellOfStr))
    % Strings are non-numerical.
    if length(inCellOfStr) == 1
        outStrList =  sprintf('"''%s''"',inCellOfStr{1});
    else
        outStrList =  ['"',sprintf('''%s'',',inCellOfStr{1:end-1}),'''', inCellOfStr{end},'''"'];
    end
else
    % Strings are numerical.
    outStrList =  ['"',sprintf('%s,',inCellOfStr{1:end-1}), inCellOfStr{end},'"'];
end
