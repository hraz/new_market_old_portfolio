function [ strID ] = numid2strid( numID )
%NUMID2STRID redefine id from number to string value for upload data from mat file.
%
%   [ strID ] = numid2strid( numID )
%   receives the vector of numeric id values and returns cell-vector of
%   string id values.
%
%   Inputs:
%       numID - is an NASSETSx1 vector specifying the asset id numbers.
%
%   Outputs:
%       strID - is an NASSETSx1 cell-vector specifying the asset id
%                    numbers in string format corresponding to db structure.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(1, 1, nargin));
    
     if (isempty(numID)) 
        error('utility_db:numid2strid:mismatchInput', ...
            'ID value can''t be an empty.');
     elseif (~isnumeric(numID))
        error('utility_db:numid2strid:mismatchInput', ...
            'ID value must be numeric.');
     end
    
    %% Step #1 (Creation needed id):
    id_l  = length(numID);
    strID = cell(id_l, 1);
    for i = 1: id_l
        strID{i} = strcat('id', num2str(numID(i)));
    end

end
