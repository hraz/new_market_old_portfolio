function [sim_num pointFrontier portDuration reinvestStrategy] = sim_num_save_reader(sim_num_save, nSimMonthsTotal)

%   Reader:
%               First number ? model
%							1  Marko
%							2  Single Index
%							3  Omega
%							rest  to be determined
%               Second number ? frontier point
%						   'MV' - mean variance - second digit = 1
%                               'OS' - optimal sharpe - second digit = 2
%                               'Tar' - target return, specific target
%                                   sought specified in target_choice - second
%                                   digit = 3
%                               'TarOS' - target + optimal sharpe point on
%                                   the efficient frontier - second digit = 4
%                               'Om' - Omega - second digit = 5
%                               'HighMom' - Skewness Kurtosis - second
%                                   digit = 6
%
%               Third/fourth number ? target return  between 0 -20
%               Fifth number ? index
%							1  601
%							2- Gov
%							3  NonGov
%							4 TelBond 20
%							5  TelBond 40
%							6  TelBond60
%
%               6th number - reinvestment strategy.  
%                             1 - 'Specific-Bond-Reinvestment'
%                             2 - 'risk-free'
%                             3 - 
%               7th-8th number ? months
%               9th number - horizon

numSims = length(sim_num_save);
numModel = zeros(numSims, 1); portDuration = zeros(numSims, 1); pointFrontier = zeros(numSims, 1);
sim_num = zeros(numSims, 1); index = zeros(numSims, 1);

for sim = 1:numSims
    numModel(sim) = floor(sim_num_save(sim)/100000000);
    if numModel(sim) == 1
        model = 'Marko';
        begPoint = 0;
    elseif numModel(sim) == 3
        model = 'Omega';
        begPoint = 447;
    end
    
    portDuration(sim) = mod(sim_num_save(sim), 10);
    
    numFrontierPoint = mod(floor(sim_num_save(sim)/10000000), 10);
    switch numFrontierPoint
        case 1
            namePoint = 'MV';
            pointFrontier(sim) = 1;
        case 2
            namePoint = 'OS';
            pointFrontier(sim) = 2;
        case 3
            namePoint = 'Tar';
        case 4
            namePoint = 'TarOS';
        case 5
            namePoint = 'Om';
    end
    
    target = mod(floor(sim_num_save(sim)/100000), 100);
    
    if numFrontierPoint == 3 % Outputs the frontier point that led to this target
        pointFrontier(sim) = numFrontierPoint + floor(target/3) - 1;
    elseif numFrontierPoint == 4
        pointFrontier(sim) = 6 + floor(target/3);
    elseif    numFrontierPoint == 5
        switch target/100
            case 3/100
                pointFrontier(sim) = 1;
            case 4/100
                pointFrontier(sim) = 2;
            case 6/100
                pointFrontier(sim) = 3;
            case 7/100
                pointFrontier(sim) = 4;
            case 9/100
                pointFrontier(sim) = 5;
            case 11/100
                pointFrontier(sim) = 6;
            case 12/100
                pointFrontier(sim) = 7;
            case 14/100
                pointFrontier(sim) = 8;
            case 17/100
                pointFrontier(sim) = 9;
            case 20/100
                pointFrontier(sim) = 10;
        end
    elseif numFrontierPoint == 6
        pointFrontier = target + 2;
    elseif numFrontierPoint == 7
        pointFrontier = mod(floor(sim_num_save/100000), 10);
    end
    
    nMonthsBeginSim = mod(floor(sim_num_save(sim)/10), 100);
    numIndex = mod(floor(sim_num_save(sim)/10000), 10);
    
    
    TelBond20 = 13; % Number of months after beginning of data that index was born
    TelBond40and60 = 25; % Also means that we must go that many months into data before simulating
    
    switch numIndex
        case 1
            index = 'AllBonds';
            sim_num(sim) = nSimMonthsTotal+1 - nMonthsBeginSim +begPoint;
        case 2
            index = 'GovBonds';
            sim_num(sim) = nSimMonthsTotal*2+1 -nMonthsBeginSim+begPoint;
        case 3
            index = 'NonGovBonds';
            sim_num(sim) = nSimMonthsTotal*3+1 -nMonthsBeginSim+begPoint;
        case 4
            index = 'TelBond20';
            sim_num(sim) = nSimMonthsTotal*4 +1 -nMonthsBeginSim+begPoint;
        case 5
            index = 'TelBond40';
            sim_num(sim) = nSimMonthsTotal*4 + 1 + (nSimMonthsTotal-TelBond20)  - nMonthsBeginSim+begPoint;
        case 6
            index = 'TelBond60';
            sim_num(sim) = nSimMonthsTotal*4 + 1 + (nSimMonthsTotal-TelBond20) + (nSimMonthsTotal-TelBond40and60) -nMonthsBeginSim+begPoint;
    end
    
    reinvestStrategyID =  mod(floor(sim_num_save(sim)/1000), 10);
    
    switch reinvestStrategyID
        case 1
            reinvestStrategy = 'Specific-Bond-Reinvestment';
        case 2
            reinvestStrategy = 'risk-free';
        case 3
        case 4 
            reinvestStrategy = 'benchmark';
        case 7
            reinvestStrategy = 'real-specific-bond-reinvestment';
    end
    
end