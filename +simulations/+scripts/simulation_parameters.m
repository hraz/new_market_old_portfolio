function [ nMonthsBeginSim benchmark_flag model_choice indexChoice] = simulation_parameters( sim_num, nSimMonthsTotal )
%SIMULATION_PARAMETERS chooses the simulation parameters (flags) according to the
%simulation number entered
%
%   [ nMonthsBeginSim benchmark_flag model_choice indexChoice] = simulation_parameters( sim_num, nSimMonthsTotal )
%   function chooses the simulation parameters according to which the
%   simulation proceeds. Note that for each benchmark there is a different
%   start date.  For AllBonds, all data is valid.  For TelBond20 the data
%   begins 13 months in.  For TelBond40 and TelBond 60, data begins 25
%   months in.
%
% input: sim_num - scalar determining the simulation number
%
%        nSimMonthsTotal - DOUBLE - number of months for simulation.  Number of
%           possible simulations is this number - 3 as we allow a
%           simulation to run for at least three months.
%
% output:
%           nMonths - DOUBLE - number of months for simulation.  Note that
%              this number should be 3 less then the total number of months of
%              data as we require at least 3 months for the simulation.
%              Further we count this number here backwards - i.e. we start at
%              the date closest to the final simulation date and go down since
%              for some indices the first dates (~2006-2007) are irrelevant as
%              the index didn't exist yet.
%
%           benchmark_flag - STRING - name of benchmark for filtering and
%               simulation. choices are: 'AllBonds' 'None' 'GovBonds''NonGovBonds''TelBond20''TelBond40''TelBond60'
%
%           model_choice - STRING - model chosen for simulation
%               choices are: 'Marko''SI''Omeg' 'MI''Spread'
%
%           indexChoice - SCALAR - number to be used as the fifth digit in
%           simulation number saved.
%
%               First number ? model
%               Second number ? frontier point
%               Third/fourth number ? target return
%               Fifth number ? index
%               6th-7th number ? months
%               8th number - horizon
%
%
% Written by Hillel Raz, Jan 2013, two days before Amit left us for gelato land, BondIT

%% change to 'Omeg' when doing second run
TelBond20 = 13; % Number of months after beginning of data that index was born
TelBond40and60 = 25; % Also means that we must go that many months into data before simulating

if sim_num <= nSimMonthsTotal*6 -  TelBond20 - 2*TelBond40and60
    modulatedSimNumber = sim_num;
    model_choice = 'Marko';
else
    modulatedSimNumber = sim_num - 447;
    model_choice = 'Omeg'; % 'Marko''SI''Omeg' 'MI''Spread'
end

if modulatedSimNumber < nSimMonthsTotal +1
    benchmark_flag = 'AllBonds';
    indexChoice = 1;
    nMonthsBeginSim = nSimMonthsTotal+1 - modulatedSimNumber;
    
elseif modulatedSimNumber < nSimMonthsTotal*2 +1
    benchmark_flag = 'GovBonds';
    indexChoice = 2;
    nMonthsBeginSim = nSimMonthsTotal*2+1  - modulatedSimNumber;
    
elseif modulatedSimNumber < nSimMonthsTotal*3 + 1
    benchmark_flag = 'NonGovBonds';
    indexChoice = 3;
    nMonthsBeginSim = nSimMonthsTotal*3+1  - modulatedSimNumber;
    
elseif modulatedSimNumber < nSimMonthsTotal*3 + (nSimMonthsTotal-TelBond20) + 1
    benchmark_flag = 'TelBond20';
    indexChoice = 4;
    nMonthsBeginSim = nSimMonthsTotal*4 +1 - modulatedSimNumber;
    
elseif modulatedSimNumber < nSimMonthsTotal*3+ (nSimMonthsTotal-TelBond20) + (nSimMonthsTotal-TelBond40and60) +1
    benchmark_flag = 'TelBond40';
    indexChoice = 5;
    nMonthsBeginSim = nSimMonthsTotal*4 + 1 + (nSimMonthsTotal-TelBond20)  - modulatedSimNumber;
    
elseif modulatedSimNumber < nSimMonthsTotal*3 + (nSimMonthsTotal-TelBond20) + (nSimMonthsTotal-TelBond40and60)*2 +1
    benchmark_flag = 'TelBond60';
    indexChoice = 6;
    nMonthsBeginSim = nSimMonthsTotal*4 + 1 + (nSimMonthsTotal-TelBond20) + (nSimMonthsTotal-TelBond40and60)  - modulatedSimNumber;
end
