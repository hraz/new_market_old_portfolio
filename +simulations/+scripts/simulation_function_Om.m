function  simulation_function_Om(sim_num)

account_name=[];

import bll.data_manipulations.*;
import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;
import dal.portfolio.data_access.*;
import dal.portfolio.crud.*;
import dml.*;
import simulations.choose_functions.*;
import dal.market.get.base_data.*;
import bll.filtering_functions.*;
import bll.solver_calculations.*;
import bll.cumulative_return.*;
import utility.*;
import simulations.scripts.*;
% try
dbstop if error

errorDS  = dataset({}, 'VarNames', 'err_msg');

%% Set time
t_beg = 732678; %01/01/2006
t_fin= 735312-10; %As of 19/03/2013, -10 days just to be safe

[ nMonthsBeginSim benchmark_choice model_choice indexChoice] = simulation_parameters( sim_num );
nSimMonthsTotal = floor((t_fin-t_beg)/30) - 3;
datesvector=floor(linspace(t_beg,t_fin-30, nSimMonthsTotal)); %Enough time to simulate for at least a month
DateNum = datesvector(mod(nMonthsBeginSim -1, nSimMonthsTotal) + 1); % amit:same as:  DateNum = datesvector(nMonths);

time_left = (t_fin - DateNum)/365;

port_dur_yrs = min(ceil(time_left), 6); %Any fracion of a year is counted as a year for simulation's sake

%try
errorDS  = dataset({}, 'VarNames', 'err_msg');

[solver_props] = create_solver_props();
[constraints] = create_constraints();
filtered =[];

%% Filter choices
%%model and point on frontier
%point_choice = 'Tar'; %'TarOS'; %'MV''OS''Tar' 'HighMom' 'Om'
%% Default values for simulation
BondDuration_choice = 'Unlimited';%'Unlimited' 'UpperLimitOnBondsDuration' 'LowerLimitOnBondsDuration' 'UpperAndLowerLimitOnBondsDuration'
YTM_choice = 'YTM60'; %'YTM20' 'YTM40' 'YTM60''YTM300'
MinTurnover_choice = 'MinTurnOver0'; %% 'MinTurnOver0' 'MinTurnOver0.1''MinTurnOver1'
rating_choice = 'AAA'; % 'AAA''AAA-BBB''AAA-CCC' 'AAA-NVR''BBB-NVR'
sector_choice = 'Unlimited'; % 'Fin' '~I&H' '~Bio'  'Ind' 'Risky'
type_choice = 'G&C'; % 'Gov''Cor' 'G&C'  'G-Link''C-Link'
wgt_per_bond_choice = 20/100; %10/100 15/100
% benchmark_choice = 'AllBonds'; % 'AllBonds' 'None' 'GovBonds''NonGovBonds''TelBond20''TelBond40''TelBond60'

%%special instructions
capital_TC = ''; %'B/S.15''B.15/S.25''B/S.25'
special_case_choice = ''; %'Z' 'Clean'

%% Setting filter parameters

%% relevant benchmark
if benchmark_choice ~= 0
    [constraints.benchmark_id] = choose_relevant_benchmark(benchmark_choice, DateNum);
end

%% YTM range
if YTM_choice ~= 0
    [constraints] = choose_max_YTM(YTM_choice, constraints);
end

%% Turnover range
if MinTurnover_choice ~= 0
    [constraints] = choose_min_Turnover(MinTurnover_choice, constraints);
end

%% rating choice
if rating_choice ~= 0
    [constraints] = choose_rating(rating_choice, constraints);
end

%%  sector choice
if sector_choice ~= 0
    [constraints] = choose_sector(sector_choice, constraints);
end

%% choose type
if type_choice ~= 0
    [constraints] = choose_type(type_choice, constraints);
end

%% choose weight per bond
if wgt_per_bond_choice ~= 0
    [solver_props] = choose_wgt_per_bond(wgt_per_bond_choice, solver_props);
end

%% choose transaction costs
if ~isempty(capital_TC)
    [solver_props] = choose_TC(capital_TC, solver_props);
end

%% choose special cases
if ~isempty(special_case_choice)
    [solver_props] = choose_special_case(special_case_choice, solver_props);
end

%% Portfolio creation

%% get risk less rate for each of the portfolio durations calculated according to its investment horizon
interest_period = [1:port_dur_yrs];
nominal_yield = get_nominal_yield_with_param(interest_period, DateNum, DateNum);
solver_props.risk_free_interests = fts2mat(nominal_yield.yield);
solver_props.risk_free_bond = fts2mat(nominal_yield.id);
solver_props.risk_free_bond_ttm = fts2mat(nominal_yield.ttm);

constraints.sectors.minWeight = 0;
constraints.static.minWeight = 0;
constraints.sectors.maxWeight = 1;
constraints.static.maxWeight = 1;

%% Filter
point_choice = 'Om';% 'MV';% For Omega change to 'Om'  Just for filterization purposes.
if ~isempty(model_choice)
    [solver_props constraints point_choice] = choose_model(model_choice, point_choice, solver_props, constraints);
    constraints.is_markowitz = strcmp(model_choice, 'Marko'); %Will be used to check number of bonds versus days of history ***************
end
constraints.dynamic.port_dur = 8; % Random number, just to fill it in and be able to filter before setting port_duration
[constraints] = choose_Bond_Duration_Range(BondDuration_choice, constraints);

if isempty(filtered) %only filter once bond duration is unlimited
    [filtered error_id] = filter_function(constraints, DateNum);
elseif ~strcmp(BondDuration_choice, 'Unlimited') %if bond duration is not portfolio duration dependent, filter again every iteration of loop
    [filtered error_id] = filter_function(constraints, DateNum);
end

filtered.NumBonds = length(filtered.nsin_list);
%% Solver

for port_duration = 1:port_dur_yrs %constraints.dynamic.port_dur  = 1:port_dur_yrs
    
    constraints.dynamic.port_dur = port_duration; % Set it to the correct value now.
    solver_props.port_duration_sought = port_duration;
    %         try
    errorDS  = dataset({}, 'VarNames', 'err_msg');
    
    for pointFrontier = 1:10
        % When running Omega, all point_choice need to be 'Om' and
        % model_choice, 'Omeg'.  The Tar should be 3, 4, 6, 7,
        % 9, 11, 12, 14,  - so total of 8 points. sim_num_save should be
        % 300 + tar
        
        switch pointFrontier
            case 1
                point_choice = 'Om';%'MV';
                target_choice = 3/100;% [];
                sim_num_save = 503;% 100; % Second digit in sim_num_save
            case 2
                point_choice = 'Om';%'OS';
                target_choice = 4/100; %[];
                sim_num_save = 504;%200;
            case 3
                point_choice = 'Om';%'Tar';
                target_choice = 6/100;% 4/100;
                sim_num_save = 506;%304;
            case 4
                point_choice = 'Om';%'Tar';
                target_choice = 7/100;
                sim_num_save = 507;
            case 5
                point_choice = 'Om';%'Tar';
                target_choice = 9/100;
                sim_num_save = 509;
            case 6
                point_choice = 'Om';%'Tar';
                target_choice = 11/100;% 12/100;
                sim_num_save = 511;%312;
            case 7
                point_choice = 'Om';%'TarOS';
                target_choice = 12/100;% 4/100;
                sim_num_save = 512;%404;
            case 8
                point_choice = 'Om';%'TarOS';
                target_choice = 14/100; % 7/100;
                sim_num_save = 514;% 407;
%             case 9
%                 point_choice = 'Om';% 'TarOS';
%                 target_choice = 9/100;
%                 sim_num_save = 409;
%             case 10
%                 point_choice = 'Om';% 'TarOS';
%                 target_choice = 12/100;
%                 sim_num_save = 412;
        end
        
        if strcmp(model_choice, 'Marko')
            sim_num_save = 10000000 + 10000*sim_num_save + 1000*indexChoice + 10*nMonthsBeginSim +solver_props.port_duration_sought;
        elseif strcmp(model_choice, 'Omeg')
            sim_num_save = 30000000 + 10000*sim_num_save + 1000*indexChoice + 10*nMonthsBeginSim + solver_props.port_duration_sought;
        end

        %% frontier point
        if point_choice ~= 0
            [solver_props] = choose_frontier_point(point_choice, target_choice, solver_props);
        end
        
        if filtered.NumBonds >= 1/solver_props.MaxBondWeight %Checks if there are enough bonds to solve
            
            account_name = [model_choice point_choice target_choice num2str(constraints.dynamic.port_dur)]; %account name to be saved ***************
            
            [solution nsin_data solver_props] = solver_ultimate(constraints, filtered, solver_props, DateNum);
            %% Check error messages here
            if ~isempty(solver_props.ConSeterror) % ************ still exists
                errors1 = [errors1 account_name];
                errorDS = [errorDS; dataset(cellstr(errors1), 'VarNames', 'err_msg')];
            end
            
            if ~isempty(solution.error)
                errors1 = [solution.error account_name];
                errorDS = [errorDS; dataset(cellstr(errors1), 'VarNames', 'err_msg')];
            end
            
            if ~isempty(solution.nsin_sol)
                [solution solver_props nsin_data] = Lawnmower(solver_props, solution, nsin_data); % Removes bonds of low weight
            end
            
            if strcmp(solver_props.frontier_point, 'TarOS')&& ~isempty(solution.nsin_sol)
                solver_props.solved = 1; % Ensure that solver is run again so that risk free bond is chosen if necessary
            end
            
            if solver_props.solved
                [solution nsin_data solver_props] = solver_ultimate(constraints, filtered, solver_props, DateNum, nsin_data, solution);
            end
            
            if any(solution.selected_indx)
                nsin_data.redemption_type = [nsin_data.redemption_type(solution.selected_indx)];
            end
            
            solution.flagMultiRedemption = strcmp(nsin_data.redemption_type, 'multi');
            solution.port_beginning = DateNum;
            
            if ~isempty(solver_props.ConSeterror)
                errors1 = [errors1 account_name];
                errorDS = [errorDS; dataset(cellstr(errors1), 'VarNames', 'err_msg')];
            end
            
            if ~isempty(solution.error)
                errors1 = [errors1 account_name];
                errorDS = [errorDS; dataset(cellstr(errors1), 'VarNames', 'err_msg')];
            end
            
           if isempty(constraints.benchmark_id)
               constraints.benchmark_id = 601;
           end
           
            %% check and save solution
            PortRisk = solution.PortRisk;
            PortRet = solution.PortReturn;
            
            if isempty(PortRet)
                PortRet = NaN;
            end
            if isempty(PortRisk)
                PortRisk = NaN;
            end
            %% Begin simulation
            if isnan(PortRet) %if no solution, don't begin simulation
                
                account_name = ['NoSolution'];
                err_msg = 'No solution from solver';
                err_msg = [err_msg];
                errorDS = [ dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                
                benchmark_id = 601;
                account_id = [];
                sim_num_save = 10*sim_num_save + solver_props.port_duration_sought;%simulation number to be saved, 9 means error.
                account_name = ['NoSolution' account_name num2str(sim_num_save)];%fill in sim_num_name as a number which changes with every run, must be unique
                %name_file{mod_num, sim} = account_name;
                owner_id = 2;
                manager_id = 1;
                investment_amount = 100000;
                
                underlying_data_name = 'ytm';
                filter_id = 23;
                
                set_sim_err( DateNum, sim_num , errorDS); % expected errors
                
            else
                
                nsin_i = solution.nsin_sol;
                
                [rows_n col_n] = size(nsin_i);
                if rows_n <col_n
                    nsin_i = nsin_i';
                end
                
                solution.PortWts(abs(solution.PortWts) <= solver_props.MaxWeight2NonAllocBonds) = 0; % Removing bonds of low weight, last time
                wts = solution.PortWts;
                wts0 = (wts == 0);
                
                wts_no_0 = wts( ~wts0 );
                nsin_non0 = nsin_i( ~wts0 );
                
                if any(wts0)
                    solution.PortWts(wts0) =[];
                    solution.NumBondsSol = length(nsin_non0);
                    solution.nsin_removed_due_to_small_weights = [solution.nsin_removed_due_to_small_weights; solution.nsin_sol(wts0)];
                    if ~isempty(solution.selected_indx)
                        solution.selected_indx(wts0) = [];
                    end
                    solution.nsin_sol= nsin_non0;
                     solution.flagMultiRedemption = solution.flagMultiRedemption(~wts0) ; % ******* Updated by Idan
                end
                
                %% Figure out count
                portfolioMaturityDate = DateNum + ceil(365*solution.PortDuration);
                [ftsPrice] = get_multi_dyn_between_dates(solution.nsin_sol, {'price_dirty'}, 'bond', DateNum, portfolioMaturityDate);
                solution.initial_prices = fts2mat(ftsPrice.price_dirty(1))';
                count_rough = 10000000 * wts_no_0' ./ solution.initial_prices; %100,000 Shekels
                
                solution.count = floor(count_rough);
                
                %% Create Portfolio object
                
                required_holding_period = solver_props.port_duration_sought;
                
                account_id = [];
                
                account_name = ['OmegaTB60x' account_name num2str(sim_num_save)]; %fill in sim_num_name as a number which changes with every run, must be unique
                name_file{solver_props.port_duration_sought, sim_num} = account_name;
                owner_id = 2;
                manager_id = 1;
                investment_amount = 10000000;
                underlying_data_name = 'ytm';
                filter_id = 23; %get from amit, number of vector
                
                port_model = dataset(sim_num_save, {model_choice}, {point_choice}, ...
                    PortRet, PortRisk, 60, 0.05,  ...
                    required_holding_period, solution.PortDuration, constraints.dynamic.hist_len, {BondDuration_choice},...
                    {rating_choice}, {type_choice},{sector_choice} , {YTM_choice}, {MinTurnover_choice}, {wgt_per_bond_choice}, {capital_TC}, {special_case_choice}, {benchmark_choice}, {target_choice},...
                    'VarNames', {'sim_id', 'model', 'optimization_type', ...
                    'expected_return', 'expected_volatility', 'reinv_low_bnd', 'reinv_up_bnd', ...
                    'req_holding_horizon', 'real_holding_period', 'hist_length', 'asset_ttm_range', ...
                    'rating_range', 'class_linkage_combination', 'sector_combination', 'yield_range', ...
                    'turnover_range', 'max_allocation_weight', 'transaction_cost_range', 'special_range', 'benchmark_id', 'target_return' });
                
                [ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, DateNum, t_fin, benchmark_choice, ftsPrice);
                holding = fts2mat(ftsHoldingValuePerBond(1))';
                nanSpots = isnan(holding);
                holding(nanSpots) = 0;
                idsNsins = fieldnames(ftsHoldingValuePerBond);
                nsin = strid2numid(idsNsins(4:end));
                nNsin = length(nsin);
                nSpot = 1;
                count = zeros(nNsin, 1);
                %get initial allocation according to final allocation gotten
                %from cumulative_returns
                n = 1;
                while nSpot <= solution.NumBondsSol
                    if nsin(n) == nsin_non0(nSpot);
                        count(n) = solution.count(nSpot);
                        nSpot = nSpot + 1;
                    end
                    n = n+1;
                end
                
                initial_allocation = dataset(nsin, count, holding); %create initial allocation
                
                %% Creation of the portfolio object:
                port = portfolio(account_id, account_name, owner_id, manager_id, DateNum, DateNum, investment_amount, constraints.benchmark_id, filter_id, initial_allocation, port_model, []);
                
                %% Save current portfolio into database:
                err_msg =[];
                [bool, err_msg] = port.save(); % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
                
                if isempty(err_msg)
                    err_msg =['no error, portfolio initiation:' account_name];
                end
                errorDS = [dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                
                %% Simulation:
                yearsTotal = ceil(solution.PortDuration);
                t = zeros(1,yearsTotal);
                
                for j = 1:yearsTotal
                    if t <  t_fin;
                        t(j) = DateNum + j*365;
                    end
                end
                
                numSavePoints = min(yearsTotal, 5);
                for  save_points = 1:numSavePoints
                    
                    if t(save_points)
                        
                        if t(save_points) >= t_fin;
                            t(save_points) = t_fin;
                        end        
                        timeSpot = find(ftsTotalHoldingValue.dates >= t(save_points), 1, 'first');
                        if isempty(timeSpot) && t(save_points) > t_fin-10 % In case actual date checked is beyond last
                            timeSpot = length(ftsTotalHoldingValue.dates); % date of data, take last data-date. 
                        end
                        holding = fts2mat(ftsTotalHoldingValue(timeSpot));
                        mtxHoldingValuePerBond= fts2mat(ftsHoldingValuePerBond); % CHECK THIS!!
                        holdingPerBond = mtxHoldingValuePerBond(end,:)'; % CHECK THIS!!
                        
                        %% Update allocation
                        
                        % allocation = dataset(nsin, count, holding); % CHECK THIS!!
                        allocation = dataset(nsin, count, holdingPerBond, 'VarNames', {'nsin', 'count', 'holding'}); % CHECK THIS!!
                        if save_points ~= 1
                            previous_update = t(save_points-1);
                        else
                            previous_update = DateNum; % Initial time portfolio was saved
                        end
                        
                        port = portfolio(port.account_id, account_name, owner_id, manager_id, DateNum, t(save_points), investment_amount, constraints.benchmark_id, filter_id, allocation, port_model, previous_update, ftsTotalHoldingValue(1:timeSpot), ftsHoldingValuePerBond(1:timeSpot), solver_props.risk_free_interests(port_dur_yrs));
                        
                        err_msg =[];
                        port.deltas = dataset(NaN, NaN, NaN, NaN, 'VarNames', {'delta1', 'delta2', 'delta3', 'delta4'});
                        [bool, err_msg] = save(port); %bool returns yes no if saved or not. % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
                        err_msg = [err_msg, account_name];
                        errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                        
                    end
                    if ~bool
                        set_sim_err(t(save_points), sim_num , errorDS); % expected errors
                    end
                end
                port_model.reinv_low_bnd = length(ftsTotalHoldingValue);
                port = portfolio(port.account_id, account_name, owner_id, manager_id, DateNum, ftsTotalHoldingValue.dates(end), investment_amount, constraints.benchmark_id, filter_id, allocation, port_model, previous_update, ftsTotalHoldingValue, ftsHoldingValuePerBond, solver_props.risk_free_interests(port_dur_yrs));
                err_msg =[];
                
                [bool, err_msg] = save(port); %bool returns yes no if saved or not. 
                err_msg = [err_msg, account_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                
                
            end
            
        else
            
            account_name = ['NoBonds'];
            err_msg = 'Not enough bonds through filter, no solution';
            err_msg = [err_msg];
            errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
            
            benchmark_id = 601;
            account_id = [];
            sim_num_save = floor(sim_num_save/10) + 9;%simulation number to be saved, 9 means error.
            account_name = ['NoBonds'  num2str(sim_num)];%fill in sim_num_name as a number which changes with every run, must be unique
            owner_id = 2;
            manager_id = 1;
            investment_amount = 100000;
            
            underlying_data_name = 'ytm';
            filter_id = 23;
            
            set_sim_err( DateNum, sim_num , errorDS); % expected errors
        end
        
        %         catch ME %inside for loop
        %             err_msg = [ME.message, act_name];
        %             set_sim_err( DateNum, sim_num*10 +port_duration, dataset(cellstr(err_msg), 'VarNames', 'err_msg')); % unexpected errors
        %             save('name_file.mat');
        %         end
    end
    % catch ME %general errors
    %     err_msg = [ME.message, act_name];
    %     set_sim_err( DateNum, sim_num*10 , dataset(cellstr(err_msg), 'VarNames', 'err_msg')); % unexpected errors
    %     save('name_file.mat');
end
end
