function [solution account_name ftsPrice nsin_non0 account_id owner_id manager_id investment_amount filter_id initial_allocation port_model] = save_solution(sim_num_save, solution, solver_props, constraints, DateNum, varargin)
% SAVE_SOLUTION function saves solution
%
%  [solution account_name] = save_solution(sim_num_save, solution, constraints, DateNum)- function saves solution with different parameters
%
%     Input:  sim_num_save - INTEGER - number specifying simulation
%             solution - data structure - required fields - nsin_sol - N X 1 Integer - list of nsins making up solution
%                                                          PortWts - N X 1 Double - list of weights making up solution, must add up to 1
%                                                          PortDuration - Double - time of actual portfolio duration
%                                                          selected_indx - M X 1 integer - indices of nsins chosen from original filtered list
%                                                          ReinvestmentStrategy - string - what type of reinvestment strategy to calculate, possibilities:
%                                                          1. 'Specific-Bond-Reinvestment' - we invest in the same bond from which we got a coupon.
%                                                          2. 'portfolio': we invest in the entire portfolio.
%                                                          3. 'risk-free': we invest in a risk free asset (government bond).
%                                                          4. 'benchmark': we invest in the appropriate benchmark.
%                                                          5. 'bank': we put the money in the bank to earn interest.
%                                                          6. 'none':
%                                                          PortRet - Double - expected portfolio return
%                                                          PortRisk - Double - expected portfolio risk
%                                                          error - string - specifying possible error from solver
%           constraints - data structure - required fields: benchmark_id - Integer - referring to benchmark against which portfolio was built
%                                                          wgt_per_bond_choice - Double - the max weight per bond allowed in portfolio
%                                                          model_choice
%                                                          point_choice
%                                                          rating_choice
%                                                          type_choice
%                                                          sector_choice
%                                                          MinTurnover_choice
%                                                          capital_TC
%                                                          special_case_choice
%                                                          target_choice
%           solver_props - data structure - required fields - port_duration_sought
%                                                             ConSeterror
%                                                             MaxWeight2NonAllocBonds
%           DateNum - Integer - specifying date of simulation
%
%           optional input:     reinvChoice - INT - to be used for now in
%                                                   place of manager_id,
%                                                   choices:
%                                                   1 =     'Specific-Bond-Reinvestment'
%                                                   2 =     'risk-free'
%                                                   3 =     'benchmark'
%
%     Output:     solution - with possibly new fields -  count, NumBondsSol, nsin_removed_due_to_small_weights, initial_prices
%                 account_name - STRING - specifying account name
%                 ftsPrice - FTS - M dates X N nsins - fts of prices for bonds
%                 nsin_non0 - Integer array - indices of nsins with
%                   positive weight
%
%     See also: simulation_function
%
% Hillel Raz, June 11th, BABY DAY!!!
% Copyright 2013, Bond IT Ltd

import utility.*;
import dal.portfolio.crud.*;
import dal.market.get.dynamic.*;
import dml.*;
if nargin == 6
    reinvChoice = varargin{1};
end

account_id = [];
owner_id = sim_num_save;
manager_id = reinvChoice;
investment_amount = 10000000;

underlying_data_name = 'ytm';
filter_id = 23;
required_holding_period = solver_props.port_duration_sought;

initial_allocation = [];
port_model = dataset(sim_num_save, {constraints.model_choice}, {constraints.point_choice}, ...
    solution.PortRetExpectedWReinvestment, solution.PortRisk, 60, 0.05,  ...
    required_holding_period, solution.PortDuration, constraints.dynamic.hist_len, {constraints.BondDuration_choice},...
    {constraints.rating_choice}, {constraints.type_choice},{constraints.sector_choice} , {constraints.YTM_choice}, {constraints.MinTurnover_choice}, constraints.wgt_per_bond_choice,...
    {constraints.capital_TC}, {constraints.special_case_choice}, constraints.benchmark_id, {constraints.target_choice},...
    'VarNames', {'sim_id', 'model', 'optimization_type', ...
    'expected_return', 'expected_volatility', 'reinv_low_bnd', 'reinv_up_bnd', ...
    'req_holding_horizon', 'real_holding_period', 'hist_length', 'asset_ttm_range', ...
    'rating_range', 'class_linkage_combination', 'sector_combination', 'yield_range', ...
    'turnover_range', 'max_allocation_weight', 'transaction_cost_range', 'special_range', 'benchmark_id', 'target_return' });
if isfield(solution,'PortOmega')
    port_model.transaction_cost_range=solution.PortOmega;
else
    port_model.transaction_cost_range=NaN;
end
% above is a misuse of the unused obj.model.transaction_cost_range
% for the sake of saving the omega value at creation date. this adjustment
% appears also in calc line 175

errorDS = [];
if isnan(solution.PortRetExpectedWReinvestment)||any(isnan(solution.PortWts))||(solution.PortRetExpectedWReinvestment+solution.PortRisk+solution.PortDuration == 0) %if no solution, don't begin simulation
    
    account_name = ['NoSolution'];
    if ~isempty(solution.error)
        error(solution.error);
    elseif ~isempty(solver_props.ConSeterror)
        error(solver_props.ConSeterror);
    end
    
    nsin = []; count = []; holding = [];
    initial_allocation = dataset(nsin, count, holding); %create initial allocation
    
    %% Creation of the portfolio object:
    port = portfolio(account_id, account_name, owner_id, manager_id, DateNum, DateNum, investment_amount, constraints.benchmark_id, filter_id, initial_allocation, port_model, []);
    ftsPrice = []; nsin_non0 = [];
    
else
    solution.PortDuration = round(solution.PortDuration*10000)/10000; %Get error up to 1/100*1/365th days.
    nsin_i = solution.nsin_sol;
    
    [rows_n col_n] = size(nsin_i);
    if rows_n <col_n
        nsin_i = nsin_i';
    end
    
    solution.PortWts(abs(solution.PortWts) <= solver_props.MaxWeight2NonAllocBonds) = 0; % Removing bonds of low weight, last time
    wts = solution.PortWts;
    wts0 = (wts < 1000*eps);
    
    wts_no_0 = wts( ~wts0 );
    nsin_non0 = nsin_i( ~wts0 );
    
    if any(wts0)
        solution.PortWts(wts0) =[]; % Erase the 0 weights and accordingly everything related
        solution.NumBondsSol = length(nsin_non0);
        solution.nsin_removed_due_to_small_weights = [solution.nsin_removed_due_to_small_weights; solution.nsin_sol(wts0)];
        if ~isempty(solution.selected_indx)
            solution.selected_indx(wts0) = [];
        end
        solution.nsin_sol= nsin_non0;
        solution.flagMultiRedemption = solution.flagMultiRedemption(~wts0) ; % ******* Updated by Idan
    end
    
    %% Figure out count
    solution.portfolioMaturityDate = DateNum + ceil(365*solution.PortDuration);
    
    %%%% Data arrangement
    %% Without Caching
    [ftsPrice] = get_multi_dyn_between_dates(solution.nsin_sol, {'price_dirty'}, 'bond', DateNum, min( solution.portfolioMaturityDate, datenum('05-04-2013')));
    solution.initial_prices = fts2mat(ftsPrice.price_dirty(1))';
    
    %% With Caching
    %     load ftsDirtyPriceAllBonds.mat;
    %     dateIndex = find(ftsPrice0.price_dirty.dates <= DateNum, 1, 'last');
    %     nsinIndex = [];
    %     namesNsin = fieldnames(ftsPrice0.price_dirty);
    %     idNsinFromFTS = strid2numid(namesNsin(4:end));
    %     for numBond = 1:solution.NumBondsSol
    %         bondIndex = find(idNsinFromFTS == solution.nsin_sol(numBond));
    %         nsinIndex = [nsinIndex bondIndex];
    %     end
    %     mtxPrices = fts2mat(ftsPrice0.price_dirty);
    %     solution.initial_prices = mtxPrices(dateIndex, nsinIndex)';
    
    %%%%%
    
    count_rough = 10000000 * wts_no_0' ./ solution.initial_prices; %100,000 Shekels
    solution.count = floor(count_rough);
    
    %% Create Portfolio object
    
    account_name = ['SimRun' num2str(sim_num_save) '-' datestr(now)]; %fill in sim_num_name as a number which changes with every run, must be unique
end
