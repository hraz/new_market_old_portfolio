alpha = [0.1 0.2 0.05 0.3 0.2]' %0.02 0.1 0.03 0.2 0.2 0.02 0.1 0.03 0.2 0.05 0.3 0.2 0.02 0.1]';
beta = [ 1 1.1 0.9 0.8 1]' %0.9 0.89 1.02 1.11 1 1.1 0.9 0.8 1 0.9 0.89 1.02 1.11 1]';
skew= [ 3 3.4 2.2 2.5 2.8 2.9 3.1 3.02 3.3]';
kurt = [0 0.2 0.1 -0.04 0.2 0.1 0.4 0.5 0.01]';
results = [1 1 0 1 0]'% 0 0 1 1  0 1 0 1 1 1 1 0 0 1]';

% alpha = rand(80, 1)/10;
% beta = rand(80, 1)/10 + 1;
% results =rand(80,1) > 0.5;


%% checking correlation and info-gain

alpha = [0.1 0.05 0.15 0.07 0.105]';
beta = [1.1 0.9 1 1 0.8]';
results = [1 0 1 0 0]';
x =[alpha beta];% skew kurt];

load clustering4.mat
alphas = round(performance.alpha(1:end)*10000)/10000;
betas = round(performance.beta(1:end)*10000)/10000;
omega = performance.omega;

perf = performance.total_value;
nans = find(isnan(alphas)==1);
endport = nans - 1;

if endport(1)==0
    endport = endport(2:end);
end
[a, i1, i2] = unique(alphas(endport)); [b, k1, k2] = unique(betas(endport));
i3 = [];
for j =1:length(i2)-1
    if i2(j) ~= i2(j+1)
        i3 = [i3 i2(j)];
    end
end
i3 = [i3 i2(end)];
alpha = a(i3); 
% k3 = [];
% for j =1:length(k2)-1
%     if k2(j) ~= k2(j+1)
%         k3 = [k3 k2(j)];
%     end
% end
% k3 = [k3 k2(end)];
beta = b(i3);
p = perf(i3);
omega = omega(i3);
for j = 1:length(i3)
    if p(j) < 1.1*10^7
        results(j) = 0;
%     elseif p(j) < 1.12 *10^7
%         results(j) = 1;
%     elseif p(j) < 1.18*10^7
%         results(j) = 2;    
    else
        results(j) = 1;
    end
end


x = [alpha beta omega];
t= classregtree(x, results, 'names', {'alpha','beta', 'omega'}, 'minparent', 1);%, 'skew', 'kurt'})
%  RegressionTree.fit(x, results)%, 'names', {'alpha','beta', 'skew'})
view(t)
