function [constraints] = choose_min_Turnover(MinTurnover_flag, constraints)
% function receives MinTurnover_flag  and chooses the
% minimum acceptable turnover. This is a measure of liquidity of a bond.
% Not clear what the units are!It is probably in units of Million NIS/Year.
%
% input:    MinTurnover_flag - string:
%               'MinTurnOver0' = Minimum Turnover is 0.
%               'MinTurnOver0.1' = Minimum Turnover is 0.1.
%               'MinTurnOver1' = Minimum Turnover is 1.
%
%           constraints - data structure containing the constraints to be imposed on the filter
%
%
% output: constraints with minimum Turnover chosen.
%

switch  MinTurnover_flag
    
    case  'MinTurnOver0'
        constraints.dynamic.turnover.min = 0;
    case  'MinTurnOver0.1'
        constraints.dynamic.turnover.min = 0.1*1e6;
    case  'MinTurnOver1'
        constraints.dynamic.turnover.min = 1*1e6;
    otherwise
        constraints.dynamic.turnover.min = 0; % Default value
end
