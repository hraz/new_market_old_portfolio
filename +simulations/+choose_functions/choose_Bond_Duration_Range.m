function [constraints] = choose_Bond_Duration_RangeSim(BondDuration_flag, constraints)
% function receives MinPortDuration_flag  and chooses the
% minimum acceptable duration for the entire portfolio.
%
%function recieves one number and a flag:
% 1. Portfolio Duration (Denoted as portfolio_duration).
% 2. BondDuration_flag is a string and can take four values:
% a. 'AllBonds' - Bonds can assume all possible duration values.
% b. 'UpperLimitOnBondsDuration' - Bonds can assume duration values between
%                                                                         0 and 1.2*portfolio_duration
% c. 'LowerLimitOnBondsDuration' - Bonds can assume duration values larger
%                                                                         than 0.8*portfolio_duration
% d. 'UpperAndLowerLimitOnBondsDuration' - Bonds can assume duration values
%                                                                                             between portfolio_duration (+-) 0.2;
%
%
% output: handles with bonds duration values range chosen.
%
switch  BondDuration_flag
    
    case  'Unlimited'
        %         solver_props.DynFilterParams.ttm{1} = 0;
        %         solver_props.DynFilterParams.ttm{2} = 100000;
        constraints.dynamic.ttm_filter_flag = 1;
    case  'LowerLimitOnBondsDuration'
        %         solver_props.DynFilterParams.ttm{1} = 0.8*portfolio_duration;
        %         solver_props.DynFilterParams.ttm{2} = 100000;
        constraints.dynamic.ttm_filter_flag = 2;
    case  'UpperLimitOnBondsDuration'
        %         solver_props.DynFilterParams.ttm{1} = 0;
        %         solver_props.DynFilterParams.ttm{2} = 1.2*portfolio_duration;
        constraints.dynamic.ttm_filter_flag = 3;
    case 'UpperAndLowerLimitOnBondsDuration'
        %         solver_props.DynFilterParams.ttm{1} = portfolio_duration-0.2;
        %         solver_props.DynFilterParams.ttm{2} = portfolio_duration+0.2;
        constraints.dynamic.ttm_filter_flag = 4;
end

