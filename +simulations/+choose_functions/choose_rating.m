function [constraints] = choose_rating(rating_flag, constraints)
%CHOOSE_RATING assigns the rating max/min values
%
%   [constraints] = choose_rating(rating_flag, constraints)
%   function receives the rating_flag and assigns the correlated ratings'
%   filters
%
%   input:  constraints - data structure containing constraints for filtering
%           purposes
%           rating_flag - string.
%
%   output: constraints with :constraints.rating.min_maalot,  constraints.rating.max_maalot,
%           constraints.rating.min_midroog, constraints.rating.max_midroog and, constraints.rating.logical_operator
%           chosen
%
%
%
constraints.rating.logical_operator = 'OR';

Rankmaalot={'AAA' ; 'AAA-'; 'AA+' ;'AA'; 'AA-'; 'A+'; 'A'; 'A-';...
    'BBB+' ;'BBB'; 'BBB-' ;'BB+'; 'BB'; 'BB-'; 'B+' ;'B'; 'B-';...
    'CCC+'; 'CCC'; 'CCC-'; 'CC'; 'C';'SD'; 'D'; 'NR'; 'NVR'};
Rankmidroog={'Aaa'; 'Aa1' ; 'Aa2'; 'Aa3' ;'A1'; 'A2'; 'A3';...
    'Baa1' ;'Baa2'; 'Baa3' ;'Ba1'; 'Ba2'; 'Ba3'; 'B1' ;'B2'; 'B3';...
    'Caa1'; 'Caa2'; 'Caa3'; 'Ca'; 'C'; 'NR';'NVR'};

switch rating_flag
    
    case  'AAA'
        constraints.rating.min_maalot = Rankmaalot{9};%A-
        constraints.rating.max_maalot = Rankmaalot{1};
        constraints.rating.min_midroog = Rankmidroog{7};%A3
        constraints.rating.max_midroog = Rankmidroog{1};
    case 'AAA-BBB'
        constraints.rating.min_maalot = Rankmaalot{11};%BBB
        constraints.rating.max_maalot = Rankmaalot{1};
        constraints.rating.min_midroog = Rankmidroog{10};%Baa3
        constraints.rating.max_midroog = Rankmidroog{1};
    case 'AAA-CCC'
        constraints.rating.min_maalot = Rankmaalot{20};%CCC
        constraints.rating.max_maalot = Rankmaalot{1};
        constraints.rating.min_midroog = Rankmidroog{17};%Caa1
        constraints.rating.max_midroog = Rankmidroog{1};
    case 'AAA-NVR'
        constraints.rating.min_maalot = Rankmaalot{26};%NVR
        constraints.rating.max_maalot = Rankmaalot{1};
        constraints.rating.min_midroog = Rankmidroog{23};%NVR
        constraints.rating.max_midroog = Rankmidroog{1};
    case 'BBB-NVR'
        constraints.rating.min_maalot = Rankmaalot{26};%A-
        constraints.rating.max_maalot = Rankmaalot{10};
        constraints.rating.min_midroog = Rankmidroog{23};%A3
        constraints.rating.max_midroog = Rankmidroog{10};
    otherwise % Default
        constraints.rating.min_maalot = Rankmaalot{26};%A-
        constraints.rating.max_maalot = Rankmaalot{11};
        constraints.rating.min_midroog = Rankmidroog{23};%A3
        constraints.rating.max_midroog = Rankmidroog{1};
end



