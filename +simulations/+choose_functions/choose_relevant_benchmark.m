function [ benchmark_id] = choose_relevant_benchmark(benchmark_flag, t_0)
%CHOOSE_RELEVANT_BENCHMARK function sets the benchmark_id from which the
%bonds will be chosen.
%
%   [ benchmark_id] = choose_relevant_benchmark(benchmark_flag, t_0)
%	function receives benchmark_flag and time of simulation and chooses the
%   wanted benchmark .
%
%   input:
%        benchmark_flag - string.   'None' - All bonds 601
%                                   'AllBonds' - All bonds 601
%                                   'GovBonds' - 602
%                                   'NonGovBonds' - 603        
%                                   'TelBond20'
%                                   'TelBond40' 
%                                   'TelBond60'
%  
%       t_0 - DATENUM - date of beginning of simulation.  Needed in case benchmark 
%               chosen is not yet alive. In this case, 601 is assigned.
%
%    output: benchmark_id - VALUE - the relevant id for the chosen
%     benchmark.
%

switch benchmark_flag
    case 'None'
        benchmark_id = 601;
    case 'AllBonds'
        benchmark_id = 601;
    case 'GovBonds'
        benchmark_id = 602;
    case 'NonGovBonds'
        benchmark_id = 603;
    case 'TelBond20'
        if t_0 > 733074 %beginning of index
            benchmark_id = 707;
        else
            benchmark_id = 601;
        end
    case 'TelBond40'
        if t_0 > 733438 %beginning of index
            benchmark_id = 708;
        else
            benchmark_id = 601;
        end
    case 'TelBond60'
        if t_0 > 733438 %beginning of index
            benchmark_id = 709;
        else
            benchmark_id = 601;
        end
    otherwise % Default - All Bonds benchmark
        benchmark_id = 601;
end


