function [solver_props] = choose_TC(capital_TC, solver_props)

% function receives TC_flag and sets the sell/buy transaction costs
%
%    input:
%           TC_flag - string.  specify buy cost, B, and sell cost, S.
%           B/S.15, B.15/S.2, B/S.25
%           solver_props - data structure containing properties needed for
%           solver
%            
%   output: solver_props - updated data structure with sell_cost and buy_cost updated.                                
%
%
%


solver_props.AddTCConstraint = 1;
switch capital_TC
    case 'B/S.15'
        solver_props.sell_cost = 0.0015;
        solver_props.buy_cost = 0.0015;
    case 'B.15/S.25'
        solver_props.sell_cost = 0.0025;
        solver_props.buy_cost = 0.0015;
    case 'B/S.25'
        solver_props.sell_cost = 0.0025;
        solver_props.buy_cost = 0.0025;
end
