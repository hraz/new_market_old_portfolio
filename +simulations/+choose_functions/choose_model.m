function [solver_props constraints point_flag] = choose_model(model_flag, point_flag, solver_props, constraints)

% function receives choices of model and optimization point and accordingly
% sets the model in the solver props.
%
% input:
%        model_flag - string.  'Marko' = Markowitz
%                              'SI' = Single Index
%                              'Omeg' = Omega
%                              'HighMom' = Skewness Kurtosis
%                              'MI' = Multi index
%                              'Spread' = Spread
%                              'RiP' = Risk Parity
%
%        point_flag - string, only relevant in case that the model_flag is Omeg.  In this case, point_flag is set to 'Om'.
%
%        solver_props - data structure containing the necessary data for the
%           solver.
% 
%        constraints - data structure containing fields necessary for
%           filtering.
%
%
% output:
%       solver_props with model chosen set to 1.  If Omega was chosen then the point_flag is set to 'Om'.
%
%       point_flag - string is returned as is unless Omega was chosen.
%
%       constraints - model_id set according to model chosen, 
%           1 = Markowitz
%           2 = Single Index
%           3 = Omega
%           4 = Multi Index
%           5 = Spread
%           6 = Risk Parity
%           7 = Higher Moments
%
%
%
% Written by Hillel Raz, Jan 2013
% Copyright, BondIT Ltd

switch model_flag
    
    case  'Marko'
        solver_props.Markowitz = 1;
        constraints.model_id = 1;
    case 'SI'
        solver_props.SingleIndex = 1;
        constraints.model_id = 2;
    case 'Omeg'
        solver_props.MaxOmega = 1;
        point_flag = 'Om';
        constraints.model_id = 3;
    case 'MI'
        solver_props.MultiIndex = 1;
        constraints.model_id = 4;
    case 'Spread'
        solver_props.Spread = 1;
        constraints.model_id = 5;
    otherwise % Default - for now single index
        solver_props.SingleIndex = 1;
end


