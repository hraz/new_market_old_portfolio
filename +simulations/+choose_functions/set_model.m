function [point_choice target_choice sim_num_save] = set_model(model_choice, pointFrontier, benchmarkSolutionFlag, varargin)
% SET_MODEL chooses model and point on frontier.
%
%     [point_choice target_choice sim_num_save] = set_model(model_choice, pointFrontier)
%     function chooses  the model, the point on the frontier and sets the simulation number
%     to be saved according to input.
%
%     Input:    model_choice - STRING - dictating which model is to be
%                   simulated
%               pointFrontier - INT - dictating which point on the
%                   frontier the solver is to solve for.
%
%               benchmarkSolutionFlag - Logical - dictating whether
%                   solution is to be built from benchmark
%
%               varargin{1} = WantedPortReturns - Double - array - target
%                   returns for benchmark built solutions
%
%     Output:   point_choice - STRING - dictating which point on the
%                   frontier the solver is to solve for.
%               target_choice - DOUBLE - decimal representing target percentage
%                   the solver is to solve for.
%               sim_num_save - INT - number representing choices of
%                   frontier point, target, model etc. to be saved.
%
%     See also: simulation_function

%     Hillel Raz, May, 2013
%     Copyright 2013, BondIT LTD.

if nargin == 4
    WantedPortReturns = varargin{1};
else
    WantedPortReturns = [];
end

if benchmarkSolutionFlag && strcmp(model_choice, 'Marko')
    switch pointFrontier
        
        case 1
            point_choice = 'OS';
            target_choice = [];
            sim_num_save = 200;
        case 2
            point_choice = 'MV';
            target_choice = [];
            sim_num_save = 100; % Second digit in sim_num_save
        case 3
            point_choice = 'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(1);
            end
            sim_num_save = 601;
        case 4
            point_choice = 'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(2);
            end
            sim_num_save = 602;
        case 5
            point_choice = 'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(3);
            end
            sim_num_save = 603;
        case 6
            point_choice = 'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(4);
            end
            sim_num_save = 604;
        case 7
            point_choice = 'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(5);
            end
            sim_num_save = 605;
    end
elseif benchmarkSolutionFlag && strcmp(model_choice, 'Omeg')
    switch pointFrontier
        case 1
            point_choice = 'Om';%'Tar';
            target_choice = 3.14/100;
            sim_num_save = 701;
        case 2
            point_choice = 'Om';%'MV';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(1);
            end
            sim_num_save = 702;% 100; % Second digit in sim_num_save
        case 3
            point_choice = 'Om';%'OS';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(2);
            end
            sim_num_save = 703;%200;
        case 4
            point_choice = 'Om';%'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(3);
            end
            sim_num_save = 704;%304;
        case 5
            point_choice = 'Om';%'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(4);
            end
            sim_num_save = 705;
        case 6
            point_choice = 'Om';%'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(5);
            end
            sim_num_save = 706;
        case 7
            point_choice = 'Om';%'Tar';
            if isempty(WantedPortReturns)
                target_choice = [];
            else
                target_choice = WantedPortReturns(6);
            end
            sim_num_save = 707;
    end
    
elseif strcmp(model_choice, 'Marko')
    switch pointFrontier
        case 1
            point_choice = 'MV';
            target_choice = [];
            sim_num_save = 100; % Second digit in sim_num_save
        case 2
            point_choice = 'OS';
            target_choice = [];
            sim_num_save = 200;
        case 3
            point_choice = 'Tar';
            target_choice = 4/100;
            sim_num_save = 304;
        case 4
            point_choice = 'Tar';
            target_choice = 7/100;
            sim_num_save = 307;
        case 5
            point_choice = 'Tar';
            target_choice = 9/100;
            sim_num_save = 309;
        case 6
            point_choice = 'Tar';
            target_choice = 12/100;
            sim_num_save = 312;
        case 7
            point_choice = 'TarOS';
            target_choice =  4/100;
            sim_num_save = 404;
        case 8
            point_choice = 'TarOS';
            target_choice = 7/100;
            sim_num_save =  407;
        case 9
            point_choice = 'TarOS';
            target_choice = 9/100;
            sim_num_save = 409;
        case 10
            point_choice = 'TarOS';
            target_choice = 12/100;
            sim_num_save = 412;
    end
    
elseif strcmp(model_choice, 'Omeg')
    % When running Omega, all point_choice need to be 'Om' and
    % model_choice, 'Omeg'.  The Tar should be 3, 4, 6, 7,
    % 9, 11, 12, 14,  - so total of 8 points. sim_num_save should be
    % 300 + tar
    switch pointFrontier
        case 1
            point_choice = 'Om';%'MV';
            target_choice = 3/100;% [];
            sim_num_save = 503;% 100; % Second digit in sim_num_save
        case 2
            point_choice = 'Om';%'OS';
            target_choice = 4/100; %[];
            sim_num_save = 504;%200;
        case 3
            point_choice = 'Om';%'Tar';
            target_choice = 6/100;% 4/100;
            sim_num_save = 506;%304;
        case 4
            point_choice = 'Om';%'Tar';
            target_choice = 7/100;
            sim_num_save = 507;
        case 5
            point_choice = 'Om';%'Tar';
            target_choice = 9/100;
            sim_num_save = 509;
        case 6
            point_choice = 'Om';%'Tar';
            target_choice = 11/100;% 12/100;
            sim_num_save = 511;%312;
        case 7
            point_choice = 'Om';%'TarOS';
            target_choice = 12/100;% 4/100;
            sim_num_save = 512;%404;
        case 8
            point_choice = 'Om';%'TarOS';
            target_choice = 14/100; % 7/100;
            sim_num_save = 514;% 407;
        case 9
            point_choice = 'Om';%'TarOS';
            target_choice = 17/100; % 7/100;
            sim_num_save = 517;% 407;
        case 10
            point_choice = 'Om';%'TarOS';
            target_choice = 20/100; % 7/100;
            sim_num_save = 520;% 407;
            
    end
end