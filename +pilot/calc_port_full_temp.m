function calc_port_full_temp (in_string)
%% intializing 
import Pilot.*;
import utility.pilot_utility.*;
import dal.market.get.dynamic.*;
import dml.*;
import dal.market.get.cash_flow.*;

error_id=0;
out_string=[];
handles=[];
default_date=datenum('2011-11-23');
default_hist_length=30;
currency_rate=100;
ExpInflation=0.02;
% default_date=today;

%% intial parsing
in_string_split=regexp(in_string, '\','split');
field_name=cell(1,length(in_string_split)-1);
field_data=cell(1,length(in_string_split)-1);
for m=2:length(in_string_split)
    input_temp=regexp(in_string_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end

%% data filling
if sum(strcmpi('security_list',field_name))
    allocation=eval(field_data{find(strcmpi('security_list',field_name))});
else
    error_id=105;
end

if sum(strcmpi('value_history',field_name))
    value_history=eval(field_data{find(strcmpi('value_history',field_name))});
else
    value_history=[];
end

if sum(strcmpi('start_date',field_name))
    start_date=datenum(field_data{find(strcmpi('start_date',field_name))},'yyyy-mm-dd');
else
    start_date=default_date;
end

if sum(strcmpi('current_date',field_name))
    req_date=datenum(field_data{find(strcmpi('current_date',field_name))},'yyyy-mm-dd');
else
    req_date=default_date;
end

if sum(strcmpi('end_date',field_name))
    end_date=datenum(field_data{find(strcmpi('end_date',field_name))},'yyyy-mm-dd');
else
    end_date=default_date;
end

if sum(strcmpi('last_update',field_name))
    last_update=datenum(field_data{find(strcmpi('last_update',field_name))},'yyyy-mm-dd');
else
    last_update=default_date;
end

if sum(strcmpi('hist_length',field_name))
    hist_length=str2num(field_data{find(strcmpi('hist_length',field_name))});
else
    hist_length=default_hist_length;
end

if sum(strcmpi('invested_amount',field_name))
    invested_amount=str2num(field_data{find(strcmpi('invested_amount',field_name))});
else
    error_id=105;
end

if sum(strcmpi('reinvest_strategy_id',field_name))
    reinvest_strategy_id=str2num(field_data{find(strcmpi('reinvest_strategy_id',field_name))});
else
    reinvest_strategy_id=0;
end

if sum(strcmpi('cash',field_name))
    cash=str2num(field_data{find(strcmpi('cash',field_name))});
else
    cash=0;
end

if sum(strcmpi('related_benchmark',field_name))
    benchmark_list=str2num(field_data{find(strcmpi('related_benchmark',field_name))});
else
    benchmark_list=601;
end

%% calculating the historical statistics
if ~error_id
    nsin_list=allocation(:,1);
    Wts_units=allocation(:,2);
    fts_hist_length=get_bond_history_length(nsin_list, req_date);
    min_hist_length=min(min(fts_hist_length.history_length),hist_length);
    tbl_name = 'bond';
    data_name = {'yield', 'ttm', 'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
    res_fts=get_multi_dyn_between_dates(nsin_list, data_name , tbl_name, req_date - min_hist_length, req_date);    
    all_prices=fts2mat(res_fts.price_dirty);
    current_prices=all_prices(end,:)';
%     all_yield=fts2mat(res_fts.yield);
%     current_yield=all_yield(end,:);
%     all_ttm=fts2mat(res_fts.ttm);
%     current_ttm=all_ttm(end,:);
    Wts_amount=Wts_units.*(current_prices/currency_rate);
    holding_value=sum(Wts_amount);
    Wts=Wts_amount/holding_value;
    [exp_return exp_return_annually]=calculate_expected_return(nsin_list,Wts,req_date,min_hist_length,end_date,reinvest_strategy_id,ExpInflation);
       
    
    last_cf_fts=get_cash_flow(last_update,req_date, nsin_list);
    if ~isempty(last_cf_fts)
        cf=fts2mat(last_cf_fts);
%         cf_dates=last_cf_fts.dates;
%         fields_temp=fieldnames(last_cf_fts);
%         cf_nsins=str2num(str2mat(cellfun(@(x) x(3:end),fields_temp(4:end),'UniformOutput',0)));
        total_cf=sum(cf(~isnan(cf)));
    end
    cash=cash+total_cf;    
    
        
    holding_return=holding_value/invested_amount -1;
    estimated_final_capital=holding_value*(1+exp_return) + cash;
    current_capital=holding_value+cash;
    simple_return=current_capital/invested_amount -1;
    if ~isempty(value_history)
        volatility=std(value_history(:,2))/mean(value_history(:,2));
    else
        all_values=(all_prices/currency_rate)*Wts_units;
        volatility=std(all_values)/mean(all_values);
    end
    
    fts0 = get_index_dyn_market( benchmark_list, 'value', start_date);
    fts1 = get_index_dyn_market( benchmark_list, 'value', req_date);
    
    initial_bench=fts2mat(fts0(end));
    current_bench=fts2mat(fts1(end));
    bench_return=current_bench/initial_bench - 1 ;
    
    out_string=strcat(out_string,'\error_message:');
    out_string=strcat(out_string,sprintf('%i',error_id));
    
    out_string=strcat(out_string,'\expected_return:');
    out_string=strcat(out_string,sprintf('%g',exp_return));

    out_string=strcat(out_string,'\expected_return_annually:');
    out_string=strcat(out_string,sprintf('%g',exp_return_annually));

    out_string=strcat(out_string,'\expected_volatility:');
    out_string=strcat(out_string,sprintf('%g',volatility));

    out_string=strcat(out_string,'\holding_return:');
    out_string=strcat(out_string,sprintf('%g',holding_return));
    
    out_string=strcat(out_string,'\estimated_final_capital:');
    out_string=strcat(out_string,sprintf('%g',estimated_final_capital));   
  
    out_string=strcat(out_string,'\current_capital:');
    out_string=strcat(out_string,sprintf('%g',current_capital));
    
    out_string=strcat(out_string,'\simple_return:');
    out_string=strcat(out_string,sprintf('%g',simple_return));
    
    out_string=strcat(out_string,'\volatility:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    
    out_string=strcat(out_string,'\bench_return:');
    out_string=strcat(out_string,sprintf('%g',bench_return));    
    
    out_string=strcat(out_string,'\downside_volatility :');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\skewness:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\kurtosis:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\sharpe:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\omega:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\jensen:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\omega:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\inf_ratio:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\alpha:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\beta:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\total_tx_cost:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\cond_v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\marginal_v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\component_v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\rsquare_vs_expret:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\rmse_vs_expret:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\rsquare_vs_bench:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    out_string=strcat(out_string,'\rmse_vs.bench:');
    out_string=strcat(out_string,sprintf('%g',volatility));    
else
    out_string=strcat(out_string,'\error_message:');
    out_string=strcat(out_string,sprintf('%i',error_id));
end

fprintf(1,'%s\n',out_string)