function [nsin_list_markowitz nsin_list_singleindex identical_flag]  = basic_pilot_filter_sim(filter_id,portfolio_duration,req_date)
% this function return the filter result (nsin list) of a
% 'basic-constraint' filter (relevant for the pilot product) which consists
% of a model, filter type, duration of the portfolio and the start date
%
%Input:   filter_id : INTEGER   - 1 for all durations
%                                                                      - 2 for durations >= portfolio_duraion - ceil(0.25*portfolio_duration)
%                                                                      - 3 for durations <= portfolio_duraion + ceil(0.25*portfolio_duration)
%                                                                      - 4 for duration in portfolio_duration +- ceil(0.25*portfolio_duration)
%                    portfolio_duration: DOUBLE    - the required portfolio weighted duration, in years
%                    req_date : DATE of the beginning of the simulation (usuall format, YYYY-MM-DD)
% 
% Output: NASSETS X 1 list of nsins

import dal.market.filter.dynamic.*;
import dal.market.get.static.*;
import dal.market.get.dynamic.*;
import dml.*;
import dal.market.filter.total.*;
param.history_length = {60 100000};
switch filter_id
    case 1
        horizon_min=0;
        horizon_max=9999;
    case 2
        horizon_min=portfolio_duration-ceil(0.25*portfolio_duration);
        horizon_max=9999;
    case 3
        horizon_min=0;
        horizon_max=portfolio_duration+ceil(0.25*portfolio_duration);
    case 4
        horizon_min=portfolio_duration-ceil(0.25*portfolio_duration);
        horizon_max=portfolio_duration+ceil(0.25*portfolio_duration);
end

param.mod_dur = {horizon_min horizon_max};
param.yield={-0.20 0.60};
tic;
%nsin_list_singleindex = bond_dynamic_complex(req_date, param);
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};

nsin_list_singleindex = bond_ttl( req_date,  stat_param, param );
t_end=toc;
fprintf('filtering with duration-filter type %i took %f seconds\n',filter_id,t_end)

if length(nsin_list_singleindex)>60
    identical_flag=0;
    temp_ds = get_bond_history_length(  nsin_list_singleindex,  req_date );
    tic;
    histlen_of_nsins=temp_ds.history_length;
    t_end=toc;
    fprintf('query of histlen took %f seconds\n',t_end)
    min_histlen=find((arrayfun(@(x) sum(histlen_of_nsins>x)-x,0:800))>0,1,'last');
    param.history_length = {min_histlen 100000};
    tic;
%    nsin_list_markowitz = bond_dynamic_complex(req_date, param);
    nsin_list_markowitz = bond_ttl( req_date,  stat_param, param );
    t_end=toc;
    fprintf('filtering with duration-filter type %i for Markowitz took %f seconds\n',filter_id,t_end)
else
    identical_flag=1;
    nsin_list_markowitz=nsin_list_singleindex;
end

% temp_ds = da_bond_( nsin_list_singleindex, req_date, 'first_trading_date');
% toc
% tic;
% begin_of_nsin=datenum(temp_ds.first_trading_date);
% toc
% histlen_of_nsin=(req_date-begin_of_nsin)*0.65;



