function [nsin_list]  = basic_pilot_filter(model_id,filter_id,port_dur_id,req_date,hist_len)
% this function return the filter result (nsin list) of a
% 'basic-constraint' filter (relevant for the pilot product) which consists
% of a model, filter type, duration of the portfolio and the start date
%
%Input:   model_id : INTEGER   - 1 for Markowitz and 2 for Single index
%                  filter_id : INTEGER   - 1 for all durations
%                                                                      - 2 for durations >= portfolio_duraion - ceil(0.25*portfolio_duration)
%                                                                      - 3 for durations <= portfolio_duraion + ceil(0.25*portfolio_duration)
%                                                                      - 4 for duration in portfolio_duration +- ceil(0.25*portfolio_duration)
%                     port_dur_id: INTEGER    - the requierd portfolio weighted duration, as determined by a pre-set table (via function port_dur_id2years)
%                    req_date : DATE of the beginning of the simulation (usuall format, YYYY-MM-DD)
%                    hist_len : minimum length of historical data needed
% 
% Output: NASSETS X 1 list of nsins

import dal.market.filter.total.*;
import dal.market.get.dynamic.*;
import utility.pilot_utility.*;
% import dml.*;

bad_nsin_list=[1100791];
[portfolio_duration port_duration_min port_duration_max] = port_dur_id2years(port_dur_id);    

param.history_length = {hist_len 100000};
switch filter_id
    case 1
        horizon_min=0;
        horizon_max=9999;
    case 2
        horizon_min=portfolio_duration-(0.2*portfolio_duration);
        horizon_max=9999;
    case 3
        horizon_min=0;
        horizon_max=portfolio_duration+(0.2*portfolio_duration);
    case 4
        horizon_min=portfolio_duration-(0.2*portfolio_duration);
        horizon_max=portfolio_duration+(0.2*portfolio_duration);
end

param.ttm = {horizon_min horizon_max};
param.yield={-0.20 0.60};
%nsin_list_singleindex = bond_dynamic_complex(req_date, param);
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest','Zero Coupon'};
stat_param.bond_type = {'Short Term Treasury Bill','Government Bond T-Bill','Corporate Bond','Government Bond Shachar', 'Government Bond New Gilon' , 'Government Bond Galil' ,'Government Bond Gilboa' ,  'Government Bond Improved Galil'   };
nsin_list = bond_ttl( req_date,  stat_param, param );

if (model_id==1) && (length(nsin_list)>hist_len)
    temp_ds = get_bond_history_length(  nsin_list,  req_date );
    tic;
    histlen_of_nsins=temp_ds.history_length;
    t_end=toc;
    fprintf('query of histlen took %f seconds\n',t_end)
    min_histlen=find((arrayfun(@(x) sum(histlen_of_nsins>x)-x,0:800))>0,1,'last');
    param.history_length = {min_histlen 100000};
    tic;    
    nsin_list = bond_ttl( req_date,  stat_param, param );
    t_end=toc;
    fprintf('filtering with duration-filter type %i for Markowitz took %f seconds\n',filter_id,t_end)
end

for m=1:length(bad_nsin_list)
    bad_nsin_indx=find(nsin_list==bad_nsin_list(m));
    if ~isempty(bad_nsin_indx)        
        nsin_list(bad_nsin_indx)=[];
    end
end




