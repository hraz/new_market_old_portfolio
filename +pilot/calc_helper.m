%% simulate import 1

import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.total.*;

date1=datenum('2009-01-01');
date2=datenum('2011-01-01');
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};
dyn_param.history_length = {NaN NaN};
dyn_param.yield = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};
nsin1=bond_ttl( date1,  stat_param, dyn_param);
nsin2=bond_ttl( date2,  stat_param, dyn_param);
nsin=intersect(nsin1,nsin2);
nsin=[1103738
     1103753
     1103795
     1103910]; %101:104
 
allocation=[nsin (101:104)'];
transaction=zeros(size(allocation,1),5);
transaction(:,1)=date1;
transaction(:,2)=allocation(:,1);
transaction(:,3)=allocation(:,2);
tbl_name = 'bond';
data_name = {'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin, data_name , tbl_name,date1,date1);    
buy_price=(fts2mat(res_fts.price_dirty))'/100;
transaction(:,5)=buy_price.*allocation(:,2);
in_string_calc=['\security_list:' mat2str(allocation)];
transaction_str=[];
for m=1:size(transaction,1)
    transaction_str=strcat(transaction_str,datestr(transaction(m,1),29));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,2)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,3)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,4)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,5)),';');    
end
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date1,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];

%% simulate import 2

import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.total.*;

date1=datenum('2009-01-01');
date2=datenum('2011-01-01');
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};
dyn_param.history_length = {NaN NaN};
dyn_param.yield = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};
nsin1=bond_ttl( date1,  stat_param, dyn_param);
nsin2=bond_ttl( date2,  stat_param, dyn_param);
nsin=setdiff(nsin1,nsin2);
nsin=[1103738
     1103753
     1103795
     7440076]; %101:104
 
allocation=[nsin (101:104)'];
transaction=zeros(size(allocation,1),5);
transaction(:,1)=date1;
transaction(:,2)=allocation(:,1);
transaction(:,3)=allocation(:,2);
tbl_name = 'bond';
data_name = {'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin, data_name , tbl_name,date1,date1);    
buy_price=(fts2mat(res_fts.price_dirty))'/100;
transaction(:,5)=buy_price.*allocation(:,2);
in_string_calc=['\security_list:' mat2str(allocation)];
transaction_str=[];
for m=1:size(transaction,1)
    transaction_str=strcat(transaction_str,datestr(transaction(m,1),29));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,2)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,3)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,4)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,5)),';');    
end
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date1,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];
  
%% simulate generate 1

import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.total.*;

date1=datenum('2009-12-31');
date2=datenum('2011-12-31');
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};
dyn_param.history_length = {NaN NaN};
dyn_param.yield = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};
generate_portfolio_output='\security_list:[1098516,205;1102854,265;1103936,617;1104017,329;1104116,330;1104975,365;1105089,523;1105121,186;1106764,256;1108349,251;1108356,580;1109685,242;1109909,647;1110378,222;1112721,345;1320118,295;1380104,337;1490044,427;1790062,503;2510113,382;3570025,473;3650041,289;4110102,273;4250130,198;4340063,272;4440079,254;5490115,623;5490156,2279;5620083,209;6120117,239;]\efficient_frontier:\risk_units:0\return_units:0\portfolio_maturity_date:2016-06-29\error_message:0';
generate_portfolio_output_split=regexp(generate_portfolio_output, '\','split');
field_name=cell(1,length(generate_portfolio_output_split)-1);
field_data=cell(1,length(generate_portfolio_output_split)-1);
for m=2:length(generate_portfolio_output_split)
    input_temp=regexp(generate_portfolio_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
end_date=datenum(field_data{find(strcmpi('portfolio_maturity_date',field_name))},29);
allocation=eval(field_data{find(strcmpi('security_list',field_name))});
nsin=allocation(:,1);
transaction=zeros(size(allocation,1),5);
transaction(:,1)=date1;
transaction(:,2)=allocation(:,1);
transaction(:,3)=allocation(:,2);
transaction(:,4)=0.01;
tbl_name = 'bond';
data_name = {'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin, data_name , tbl_name,date1,date1);    
buy_price=(fts2mat(res_fts.price_dirty))'/100;
transaction(:,5)=buy_price.*allocation(:,2);
in_string_calc=['\security_list:' mat2str(allocation)];
transaction_str=[];
for m=1:size(transaction,1)
    transaction_str=strcat(transaction_str,datestr(transaction(m,1),29));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,2)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,3)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,4)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,5)),';');    
end
transaction_str(end)=[];
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date1,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(end_date,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];
    
%% simulate generate 2

import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.total.*;

date1=datenum('2009-12-31');
date2=datenum('2011-12-31');
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};
dyn_param.history_length = {NaN NaN};
dyn_param.yield = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};
generate_portfolio_output='\security_list:[1099738,140;1103670,780;1106046,981;6080204,851;6390207,658;6910095,919;6990154,909;7480049,386;7560048,1113;7560055,1328;7590128,864;7980154,956;]\efficient_frontier:\risk_units:0\return_units:0\portfolio_maturity_date:2024-12-27\error_message:0';
generate_portfolio_output_split=regexp(generate_portfolio_output, '\','split');
field_name=cell(1,length(generate_portfolio_output_split)-1);
field_data=cell(1,length(generate_portfolio_output_split)-1);
for m=2:length(generate_portfolio_output_split)
    input_temp=regexp(generate_portfolio_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
end_date=datenum(field_data{find(strcmpi('portfolio_maturity_date',field_name))},29);
allocation=eval(field_data{find(strcmpi('security_list',field_name))});
nsin=allocation(:,1);
transaction=zeros(size(allocation,1),5);
transaction(:,1)=date1;
transaction(:,2)=allocation(:,1);
transaction(:,3)=allocation(:,2);
transaction(:,4)=0.01;
tbl_name = 'bond';
data_name = {'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin, data_name , tbl_name,date1,date1);    
buy_price=(fts2mat(res_fts.price_dirty))'/100;
transaction(:,5)=buy_price.*allocation(:,2);
in_string_calc=['\security_list:' mat2str(allocation)];
transaction_str=[];
for m=1:size(transaction,1)
    transaction_str=strcat(transaction_str,datestr(transaction(m,1),29));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,2)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,3)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,4)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,5)),';');    
end
transaction_str(end)=[];
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date1,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(end_date,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];

%% edit import 1

import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.total.*;

date1=datenum('2009-01-01');
date2=datenum('2011-01-01');
% stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
% stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};
% dyn_param.history_length = {NaN NaN};
% dyn_param.yield = {NaN NaN};
% dyn_param.mod_dur = {NaN NaN};
% dyn_param.turnover = {NaN NaN};
% nsin1=bond_ttl( date1,  stat_param, dyn_param);
% nsin2=bond_ttl( date2,  stat_param, dyn_param);
% nsin=intersect(nsin1,nsin2);
nsin=[1103738
     1103753
     1103795
     1103910]; %101:104
 
allocation=[nsin (101:104)'];
transaction=zeros(size(allocation,1),5);
transaction(:,1)=date1;
transaction(:,2)=allocation(:,1);
transaction(:,3)=allocation(:,2);
tbl_name = 'bond';
data_name = {'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin, data_name , tbl_name,date1,date1);    
buy_price=(fts2mat(res_fts.price_dirty))'/100;
transaction(:,5)=buy_price.*allocation(:,2);
in_string_calc=['\security_list:' mat2str(allocation)];
transaction_str=[];
for m=1:size(transaction,1)
    transaction_str=strcat(transaction_str,datestr(transaction(m,1),29));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,2)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,3)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,4)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,5)),';');    
end
if strcmp(transaction_str(end),';')
    transaction_str(end)=[];
end
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date1,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];

calc_output_010611=evalc('calc_port_full_stat_simple(in_string_calc)');
calc_output_010611(end)=[];
% calc_output_010611='\error_message:0\expected_return:1.3465\expected_return_annually:0.0648041\expected_volatility:0.00638756\holding_return:0.128327\estimated_final_capital:27088.5\current_capital:11906.2\available_cash:630.826\money_weighted_return:0.191454\time_weighted_return:0.191454\real_volatility:0.00638756\benchmark_id:601\bench_return:0.0881607\downside_volatility :0.00638756\skewness:0.00638756\kurtosis:0.00638756\sharpe:0.00638756\omega:0.00638756\jensen:0.00638756\inf_ratio:0.00638756\alpha:0.00638756\beta:0.00638756\v_a_r:0.00638756\cond_v_a_r:0.00638756\marginal_v_a_r:0.00638756\component_v_a_r:0.00638756\rsquare_vs_expret:0.00638756\rmse_vs_expret:0.00638756\rsquare_vs_bench:0.00638756\rmse_vs.bench:0.00638756';
calc_output_010611_split=regexp(calc_output_010611, '\','split');
field_name=cell(1,length(calc_output_010611_split)-1);
field_data=cell(1,length(calc_output_010611_split)-1);
for m=2:length(calc_output_010611_split)
    input_temp=regexp(calc_output_010611_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
cash_010611=str2num(field_data{find(strcmpi('available_cash',field_name))});
%edit
allocation_old=allocation;
date0=date1;
date1=datenum('2011-01-01');
date2=datenum('2011-01-01');

nsin=[1103738
     1103753
     1103795
     1103936]; %101:103 105 
allocation=[nsin (101:104)'];

in_string_tx=['\security_list1:' mat2str(allocation_old) '\security_list2:' mat2str(allocation) '\transaction_cost:0.01\transaction_date:' datestr(date1,29)];
transaction_output=evalc('generate_transaction_list(in_string_tx)');transaction_output(end)=[];
transaction_output_split=regexp(transaction_output, '\','split');
field_name=cell(1,length(transaction_output_split)-1);
field_data=cell(1,length(transaction_output_split)-1);
for m=2:length(transaction_output_split)
    input_temp=regexp(transaction_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
transaction_string=field_data{find(strcmpi('transaction_list',field_name))};
transaction_string(1)=[];
transaction_string(end)=[];
if strcmp(transaction_string(end),';')
    transaction_string(end)=[];
end

transaction_str=[transaction_str ';' transaction_string];

in_string_ded=['\security_list:' mat2str(allocation) '\current_date:' datestr(date1,29)];
deduce_output=evalc('deduce_portfolio_constraints(in_string_ded)');deduce_output(end)=[];
deduce_output_split=regexp(deduce_output, '\','split');
field_name=cell(1,length(deduce_output_split)-1);
field_data=cell(1,length(deduce_output_split)-1);
for m=2:length(deduce_output_split)
    input_temp=regexp(deduce_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
end_date=datenum(field_data{find(strcmpi('portfolio_maturity_date',field_name))},29);

in_string_calc=['\security_list:' mat2str(allocation)];
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date0,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(end_date,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];
in_string_calc=[in_string_calc '\available_cash:' num2str(cash_010611)];

%% edit generate 1

import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.total.*;

date1=datenum('2009-12-31');
date2=datenum('2011-06-01');
stat_param.coupon_type = {'Fixed Interest', 'Variable Interest'};
stat_param.bond_type = {'Currency-Linked', 'CPI-Linked', 'Government Bond Improved Galil', 'Corporate Bond', 'Makam (T-Bill)', 'Government Bond Gilon', 'Non-Linked Fixed Rate', 'Non-Linked Floating Rate', 'Non-Linked short Term'};
dyn_param.history_length = {NaN NaN};
dyn_param.yield = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};
generate_portfolio_output='\security_list:[1099738,140;1103670,780;1106046,981;6080204,851;6390207,658;6910095,919;6990154,909;7480049,386;7560048,1113;7560055,1328;7590128,864;7980154,956;]\efficient_frontier:\risk_units:0\return_units:0\portfolio_maturity_date:2024-12-27\error_message:0';
generate_portfolio_output_split=regexp(generate_portfolio_output, '\','split');
field_name=cell(1,length(generate_portfolio_output_split)-1);
field_data=cell(1,length(generate_portfolio_output_split)-1);
for m=2:length(generate_portfolio_output_split)
    input_temp=regexp(generate_portfolio_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
end_date=datenum(field_data{find(strcmpi('portfolio_maturity_date',field_name))},29);
allocation=eval(field_data{find(strcmpi('security_list',field_name))});
nsin=allocation(:,1);
transaction=zeros(size(allocation,1),5);
transaction(:,1)=date1;
transaction(:,2)=allocation(:,1);
transaction(:,3)=allocation(:,2);
transaction(:,4)=0.01;
tbl_name = 'bond';
data_name = {'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
res_fts=get_multi_dyn_between_dates(nsin, data_name , tbl_name,date1,date1);    
buy_price=(fts2mat(res_fts.price_dirty))'/100;
transaction(:,5)=buy_price.*allocation(:,2);
in_string_calc=['\security_list:' mat2str(allocation)];
transaction_str=[];
for m=1:size(transaction,1)
    transaction_str=strcat(transaction_str,datestr(transaction(m,1),29));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,2)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,3)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,4)));
    transaction_str=strcat(transaction_str,',',num2str(transaction(m,5)),';');    
end
transaction_str(end)=[];
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date1,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(end_date,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];
%calc_output_010611_new=evalc('calc_port_full_stat_simple(in_string_calc)');
%calc_output_010611_new(end)=[];
calc_output_010611='\error_message:0\expected_return:1.3465\expected_return_annually:0.0648041\expected_volatility:0.00638756\holding_return:0.128327\estimated_final_capital:27088.5\current_capital:11906.2\available_cash:630.826\money_weighted_return:0.191454\time_weighted_return:0.191454\real_volatility:0.00638756\benchmark_id:601\bench_return:0.0881607\downside_volatility :0.00638756\skewness:0.00638756\kurtosis:0.00638756\sharpe:0.00638756\omega:0.00638756\jensen:0.00638756\inf_ratio:0.00638756\alpha:0.00638756\beta:0.00638756\v_a_r:0.00638756\cond_v_a_r:0.00638756\marginal_v_a_r:0.00638756\component_v_a_r:0.00638756\rsquare_vs_expret:0.00638756\rmse_vs_expret:0.00638756\rsquare_vs_bench:0.00638756\rmse_vs.bench:0.00638756';
calc_output_010611_split=regexp(calc_output_010611, '\','split');
field_name=cell(1,length(calc_output_010611_split)-1);
field_data=cell(1,length(calc_output_010611_split)-1);
for m=2:length(calc_output_010611_split)
    input_temp=regexp(calc_output_010611_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
cash_010611=str2num(field_data{find(strcmpi('available_cash',field_name))});
% edit
allocation_old=allocation;
date0=datenum('2009-12-31');
date1=datenum('2011-06-01');
date2=datenum('2011-06-01');
in_string_gen='\amount:10000.00\port_dur:4\target_function:return\target_value:0.4\transaction_cost:0.01\number_of_points:3\nsin_list:*\calc_date:2011-06-01';
generate_portfolio_output_new=evalc('generate_portfolio_basic(in_string_gen)');generate_portfolio_output_new(end)=[];
generate_portfolio_output='\security_list:[1117423,374;1119213,807;1119221,412;1119825,935;1120120,519;1121763,991;6110365,522;6390207,820;6990154,852;7560048,926;7560055,1163;7590128,265;7980154,840;]\efficient_frontier:\risk_units:0\return_units:0\portfolio_maturity_date:2026-05-28\error_message:0';
generate_portfolio_output_split=regexp(generate_portfolio_output, '\','split');
field_name=cell(1,length(generate_portfolio_output_split)-1);
field_data=cell(1,length(generate_portfolio_output_split)-1);
for m=2:length(generate_portfolio_output_split)
    input_temp=regexp(generate_portfolio_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
end_date=datenum(field_data{find(strcmpi('portfolio_maturity_date',field_name))},29);
allocation=eval(field_data{find(strcmpi('security_list',field_name))});
nsin=allocation(:,1);
in_string_tx=['\security_list1:' mat2str(allocation_old) '\security_list2:' mat2str(allocation) '\transaction_cost:0.01\transaction_date:' datestr(date1,29)];
transaction_output=evalc('generate_transaction_list(in_string_tx)');transaction_output(end)=[];
transaction_output_split=regexp(transaction_output, '\','split');
field_name=cell(1,length(transaction_output_split)-1);
field_data=cell(1,length(transaction_output_split)-1);
for m=2:length(transaction_output_split)
    input_temp=regexp(transaction_output_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
transaction_string=field_data{find(strcmpi('transaction_list',field_name))};
transaction_string(1)=[];
transaction_string(end)=[];
if strcmp(transaction_string(end),';')
    transaction_string(end)=[];
end
transaction_str=[transaction_str ';' transaction_string];

in_string_calc=['\security_list:' mat2str(allocation)];
in_string_calc=[in_string_calc '\transaction_history:[' transaction_str ']'];
in_string_calc=[in_string_calc '\start_date:' datestr(date0,29)];
in_string_calc=[in_string_calc '\current_date:' datestr(date2,29)];
in_string_calc=[in_string_calc '\end_date:' datestr(end_date,29)];
in_string_calc=[in_string_calc '\last_update:' datestr(date1,29)];
in_string_calc=[in_string_calc '\invested_amount:' num2str(sum(transaction(:,5)))];
in_string_calc=[in_string_calc '\available_cash:' num2str(cash_010611)];