function [errors handles] = SolverPilot(handles, port_duration_max,Discrete_Return_Flag)
%Solves the optimization problem using the bonds from the FindBonds_Callback, and returns the weights to allocate for each bond according to the user preferences.
%clc
% dbstop if error
import dal.market.get.risk.*;
import dal.market.filter.total.*;

% import simulations.*;
import pilot.*;

% profile on

% [handles]  = MiscSolverCallbackSettingsNoGui(handles);       % set up differnt GUI fields and variables for the solver callback. Also check the ratio beween number of bonds that passed the filters and number of observations, in case of Markowitz model
handles.BondsHistLenRatioError=0; % instead of MiscSolverCallbackSettingsNoGui
handles.ResMat = []; % instead of MiscSolverCallbackSettingsNoGui

errors =0;

% if handles.BondsHistLenRatioError                              % if the number of bonds is greater than the number of observations, in the case of the Markowitz model, the solver cannot be called since the estimated covariance matrix won't be positive definite. Thus, the single index model is chosen instead
%     handles.HistLen = handles.NumBonds;
%    
% %     msgbox(sprintf('Number of bonds must be smaller than history length, when using Markowitz model.\n Singele Index Model used instead. \nTo use Markowitz model, Either:\n1. Increase the history length. \n2. Decrease the number of candidate bonds. \n3. Use the Single Index Model.'),'Markowitz Model Warning','warning');
%    % errors = ['Number of bonds must be smaller than history length, when using Markowitz model. Continuing with the Singele Index Model instead.\n'];
% end
%axes(handles.EffFro);                                       %define the axes as the GUI plot axes (in case the above error message has changed this)
%[handles errors2]=UpdateNsinWithSpecificBondsNoGui(handles);
errors2=[];
handles = GetBondDataNoGui(handles);               % Get static and dynamic bond data in FTSs and datasets.
[handles.IndexPriceYld handles.IndexYTM]=GetMktIndex(handles.DateNum,handles.HistLen);            %return the market index which corresponds to the types of bonds the user requested

[handles.Sharpe handles.InfoRatio handles.ReturnMat]=CalcSharpeInfoReturnMatNoGui(handles.price_clean_yld, handles.HistLen, handles.RisklessRate,handles.IndexPriceYld); %also calc the return matrix
    [handles.alpha,handles.beta,handles.Sigma_ei,handles.SingIndExpCovMat]=CalcSingleIndexParams(handles.ReturnMat,handles.YTM,handles.IndexPriceYld,handles.IndexYTM,handles.Sigma_eiMethod,handles.HistLen); %beta is already adjusted

  %  handles=CalcSpread(handles);    
 
 %% set port duration
  handles.MaxPortDuration = port_duration_max;
 %%
 
    %calc the spread of each bond from the Gov bond with the same type and with the same duration (as close as possible)
    [handles.ConSet handles.error]=CreatConSet(handles);    %create constraints matrix:
errors2 = handles.error;

    if isempty(handles.error)                               %choose model
        if handles.MarkowitzG
            handles = EstCovMat_Marko(handles);
        elseif handles.SingleIndexG
            handles = EstCovMat_SingInd(handles);
        elseif handles.MultiIndexG
            handles = EstCovMat_MultiInd(handles);
        else
            handles = EstCovMat_Spread(handles);
        end
        handles.RiskLessWt=0; %this is zero for all algorithms except 4, where it can be something other than 0
        handles.ClosestErrorFlag=0; %a flag that indicates whether or not there was a warning during the optimization which is relavent to the user. if it turns 1 - this means that the target return that the user entered wasn't feasible (and also its neighbourhood wasn't feasiable) so we need to let him know that. sometimes there are warnings which are relvanet to the user, (if there was some target return which wasn't feasiable) since the target return that the user wanted was feasible
        handles.FminConFlag=0; %if 0, user didn't specify advance constraitns which require fmincon function
        if  (handles.ConMinNumOfBonds || handles.ConMaxNumOfBonds || handles.ConMinWeight2AlloBonds || handles.ConMaxWeight2NonAlloBonds) %if there is an advanced constraint that makes us use fmincon function instead of the regular optimizations
            handles.FminConFlag=1; %Tell the optimization function to use the FminCon optimization
        end
        
        %% choose algorithm (type of optimization problem)

       [dur_ord dur_ind] = sort(handles.Duration);
        
        if sum(dur_ord(1:5))/5 > handles.MaxPortDuration || handles.NumBonds == handles.MinNumOfBonds %if average of 5 smallest is more than the max allowed, just take portfolio made of those 5
            [  handles.Wts handles.PortReturn handles.PortRisk handles.PortDuration] = port_dur_exceed_sim(handles.NumBonds, handles.ExpCovMat, handles.YTM, handles.Duration, dur_ind, 1 );
            handles.PortWts = [handles.Wts'; handles.Wts'];
        elseif sum(dur_ord(end-4:end))/5 < handles.MinPortDuration %if average of 5 largest is smaller than the minimum allowed, take portfolio of those 5             
            [  handles.Wts handles.PortReturn handles.PortRisk handles.PortDuration] = port_dur_exceed_sim(handles.NumBonds, handles.ExpCovMat, handles.YTM, handles.Duration, dur_ind, 0 );
            handles.PortWts = [handles.Wts'; handles.Wts'];
        elseif handles.MinVar                                                         %if handles.Algorithm==1
            [handles.PortDuration handles.PortReturn handles.PortRisk handles.PortWts handles.Wts handles.PortReturnYear handles.PortRiskYear handles.MarkerPosition handles.error handles.warning]=MinVarPortErrorFTSPilot(handles.YTM,handles.ExpCovMat,...
                handles.NumPorts,handles.ConSet,handles.FminConFlag,handles);
        elseif handles.TarRetType                                                     %elseif handles.Algorithm==2 %target return
            [handles.PortDuration handles.PortReturn handles.PortRisk handles.PortWts handles.Wts handles.PortReturnYear handles.PortRiskYear handles.MarkerPosition handles.error handles.warning handles.ClosestErrorFlag]=TarRetPortErrorFTSPilot(handles.YTM,handles.ExpCovMat,...
                handles.NumPorts,handles.ConSet,handles.TarRet,handles.FminConFlag,handles.Duration,handles,Discrete_Return_Flag);            
            selected_indx=find(handles.Wts>0.0075);
            if length(selected_indx)>handles.MaxNumOfBonds
                [sorted_wts_JUNK sort_indx]=sort(handles.Wts(selected_indx),'descend');
                selected_indx=selected_indx(sort_indx(1:handles.MaxNumOfBonds));
            end
            NewConSet=handles.ConSet(:,[selected_indx' end]);
            [handles.PortDuration handles.PortReturn handles.PortRisk PortWts_temp handles.Wts(selected_indx) handles.PortReturnYear handles.PortRiskYear handles.MarkerPosition handles.error handles.warning handles.ClosestErrorFlag]=TarRetPortErrorFTSPilot(handles.YTM(selected_indx),handles.ExpCovMat(selected_indx,selected_indx),...
                handles.NumPorts,NewConSet,handles.TarRet,handles.FminConFlag,handles.Duration(selected_indx),handles,Discrete_Return_Flag);
            unselected_indx=setdiff(1:length(handles.Wts),selected_indx');
            handles.PortWts=zeros(size(PortWts_temp,1),length(handles.nsin));
            handles.PortWts(:,selected_indx)=PortWts_temp;            
            handles.Wts(unselected_indx)=0;
             if handles.error
                 errors=handles.error;
             end
        elseif handles.Opt                                                      %elseif handles.Algorithm==3 %Optimal Sharpe ratio
            [handles.PortDuration handles.PortReturn handles.PortRisk handles.PortWts handles.Wts handles.PortReturnYear handles.PortRiskYear handles.MarkerPosition handles.TangPortRetYear handles.TangPortRiskYear handles.error handles.warning ]=OptimalPortErrorFTSNoGui(handles.YTM,handles.ExpCovMat,...
                handles.NumPorts,handles.ConSet,handles.RisklessRateDay, handles.BorrowRateDay, handles.RiskAversion,handles.FminConFlag,handles);
        elseif handles.OptTarRet                                                                                        %elseif handles.Algorithm==4 %Optimal target return + Sharpe
            [handles.PortDuration handles.PortReturn handles.PortRisk handles.PortWts handles.Wts handles.PortReturnYear handles.PortRiskYear handles.MarkerPosition handles.TangPortRetYear handles.TangPortRiskYear handles.error handles.warning]=TarRetOptimalPortErrorFTSNoGui(handles.YTM,handles.ExpCovMat,...
                handles.NumPorts,handles.ConSet,handles.RisklessRateDay, handles.BorrowRateDay, handles.RiskAversion,...
                handles.TarRet,handles.FminConFlag,handles);
            handles.RiskLessWt=1-sum(handles.Wts); %the weight to allocate to the riskless asset
        
        end
        
        %handles=OutputResults(handles); %print the SolOutMsg (success, error or warning) and the data in the results table
        %guidata(hObject, handles);
%         set(handles.ColorSlider,'enable','on');                                                 % Activate the lower slider, which selects between reglular optimization results and where am I
%         set(handles.CustomSlider,'visible','on')                                                % Activate the upper left slider bar in the figure - to navigate beween the different solutions
%         set(handles.CustomSlider,'sliderstep',[1/( handles.NumPorts-1) 1/( handles.NumPorts-1)])
%         set(handles.CustomSlider,'value',(handles.MarkerPosition-1)/(handles.NumPorts-1))
    else %if there was an error in the creation of the constraint matrix, ConSet, output the error
        errors=105;        
    end
        
