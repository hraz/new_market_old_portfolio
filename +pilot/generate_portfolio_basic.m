function generate_portfolio_basic (in_string)
%% intializing 
import Pilot.*;
import utility.pilot_utility.*;
error_id=0;
out_string=[];
handles=[];
efficient_frontier=[];
ExpInflation=0.03;
Discrete_Return_Flag=1;
default_date=datenum('2011-11-23');
% default_date=today;

%% intial parsing
in_string_split=regexp(in_string, '\','split');
field_name=cell(1,length(in_string_split)-1);
field_data=cell(1,length(in_string_split)-1);
for m=2:length(in_string_split)
    input_temp=regexp(in_string_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end

%% data filling
if sum(strcmpi('amount',field_name))
    total_amount=str2num(field_data{find(strcmpi('amount',field_name))});
else
    error_id=105;
end

if sum(strcmpi('hist_length',field_name))
    hist_len=str2num(field_data{find(strcmpi('hist_length',field_name))});
else
    hist_len=60;
end

if sum(strcmpi('calc_date',field_name))
    req_date=datenum(field_data{find(strcmpi('calc_date',field_name))},'yyyy-mm-dd');
else
    req_date=default_date;
end

if sum(strcmpi('model_name',field_name))
    model=field_data{find(strcmpi('model',field_name))};
    switch lower(model)
        case 'markowitz'
            model_id=1;
            handles.MarkowitzG=1;
            handles.SingleIndexG=0;
            handles.MultiIndexG=0;
        case 'single_index'
            model_id=2;
            handles.MarkowitzG=0;
            handles.SingleIndexG=1;
            handles.MultiIndexG=0;
    end         
else
    % default model should be used
    model_id=2;
    handles.MarkowitzG=0;
    handles.SingleIndexG=1;
    handles.MultiIndexG=0;
end

filter_id=4; % port_dur+-1 or 2
if sum(strcmpi('port_dur',field_name))
    port_dur_id=str2num(field_data{find(strcmpi('port_dur',field_name))});
    [portfolio_duration port_duration_min port_duration_max] = port_dur_id2years(port_dur_id);    
end

if sum(strcmpi('target_function',field_name))
    tar_func=field_data{find(strcmpi('target_function',field_name))};
    if Discrete_Return_Flag
        tar_val=str2num(field_data{find(strcmpi('target_value',field_name))});
    else
        tar_val=0.01*str2num(field_data{find(strcmpi('target_value',field_name))});
    end
    
    switch lower(tar_func)
        case 'risk'
            handles.MinVar=0;
            handles.TarRetType=2;
            handles.TarRisk=tar_val;
            handles.Opt=0;
            handles.OptTarRet=0;
            handles.NumPorts=1;
        case 'return'
            handles.MinVar=0;
            handles.TarRetType=1;
            handles.TarRet=tar_val;
            handles.Opt=0;
            handles.OptTarRet=0;
            handles.NumPorts=1;
        case 'opt-ratio'
            handles.MinVar=0;
            handles.TarRetType=0;
            handles.Opt=1;
            handles.OptTarRet=0;
            handles.NumPorts=1;
        case 'min-var'
            handles.MinVar=1;
            handles.TarRetType=0;
            handles.Opt=0;
            handles.OptTarRet=0;
            handles.NumPorts=1;
    end    
end

risk_id=0;
return_id=0;

%% generating the porfolio
if sum(strcmpi('port_dur',field_name))
    port_dur_id=str2num(field_data{find(strcmpi('port_dur',field_name))});
    [nsin_list]  = basic_pilot_filter(model_id,filter_id,port_dur_id,req_date,hist_len);
    handles.nsin=nsin_list;
    handles.DateNum=req_date;
    handles.ExpInflation=ExpInflation;
    handles.UseTermStructure=0;
    [portfolio_duration handles.MinPortDuration handles.MaxPortDuration] = port_dur_id2years(port_dur_id);    
    handles.HistLen=hist_len;
    handles.RisklessRate=0.025;
    handles.Sigma_eiMethod=1;
    handles.NumBonds=length(handles.nsin);
    handles.TypeFilterParams=cell(7,1);
    handles.UserBondTypes=ones(7,1);
    handles.BondsPerType0 =  zeros(7,1);%handles.UserBondTypes';
    handles.BondsPerType =  zeros(7, 1);%  handles.UserBondTypes';
    handles.CorpGovTypes=[0 0];
    handles.SpecBonHistLenError=0;
    handles.SpecBondLoc=[];
    handles.MaxWeight=[];
    handles.SpecBondCon=[];
    handles.MinWeight=[];
    handles.MaxTypeWeight=ones(7,1);
    handles.MinTypeWeight=zeros(7,1);
    handles.TargetBetaFlag=0;
    handles.MinBondWeight=0;
    handles.MaxBondWeight=0.1;

    handles.AllSectors={  'FASHION & CLOTHING'
        'FOREIGN SHARES AND OPTIONS'
        'INDEX PRODUCTS';
        'BIOMED'
        'CHEMICAL RUBBER & PLASTIC '
        'COMMERCE'
        'COMMERCIAL BANKS'
        'COMMUNICATIONS & MEDIA '
        'COMPUTERS'
        'ELECTRONICS'
        'FINANCIAL SERVICES'
        'FOOD'
        'GOVERNMENT'
        'HOTELS & TOURISM '
        'INSURANCE COMPANIES'
        'INVESTMENT & HOLDINGS'
        'ISSUANCES'
        'METAL'
        'MISCELLANEOUS INDUSTRY'
        'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS'
        'OIL & GAS EXPLORATION'
        'REAL-ESTATE AND CONSTRUCTION'
        'SERVICES'
        'STRUCTURED BONDS '
        'UNKNOWN'
        'WOOD & PAPER'};
    
    handles.MaxSectorWeight=ones(size(handles.AllSectors));
    handles.MinSectorWeight=zeros(size(handles.AllSectors));
    % handles.MarkowitzG=0;
    % handles.SingleIndexG=1;
    % handles.MultiIndexG=0;
    handles.ConMinNumOfBonds=0;
    handles.ConMaxNumOfBonds=0;
    handles.ConMinWeight2AlloBonds=0;
    handles.ConMaxWeight2NonAlloBonds=0;
    handles.FminConFlag=0;
    handles.MinNumOfBonds=10;
    handles.MinNumOfBonds0=10;
    % handles.MinVar=0;
    % handles.TarRetType=1;
    % handles.Opt=0;
    % handles.OptTarRet=0;
    handles.MyBonds=[];
    handles.MyWeights=[];
    handles.MaxNumOfBonds=30;
    handles.MinWeight2AllocBonds=0.05;
    handles.MaxWeight2NonAllocBonds=1e-4;
    
    if handles.MinNumOfBonds<=handles.NumBonds
        [error_id handles] = SolverPilot(handles, port_duration_max,Discrete_Return_Flag);
        Wts_amount=handles.Wts*total_amount;
        prices=fts2mat(handles.PriceFTS);
        Wts_units=floor(Wts_amount./(prices(end,:)/100)');
        selected_index=find(Wts_units>0);
        allocation=[handles.nsin(selected_index) Wts_units(selected_index)];
        security_list='[';
        for m=1:size(allocation,1)        
            security_list=strcat(security_list,sprintf('%i,%i;',allocation(m,1),allocation(m,2)));
        end
        security_list=strcat(security_list,']');
        efficient_frontier=[];
        
        out_string=strcat(out_string,'\security_list:');
        out_string=strcat(out_string,sprintf('%s',security_list));       

        out_string=strcat(out_string,'\efficient_frontier:');
        out_string=strcat(out_string,sprintf('%s',efficient_frontier));

        out_string=strcat(out_string,'\risk_units:');
        out_string=strcat(out_string,sprintf('%i',risk_id));
        out_string=strcat(out_string,'\return_units:');
        out_string=strcat(out_string,sprintf('%i',return_id));
        maturity_date=floor(portfolio_duration*365 + datenum(req_date));
        out_string=strcat(out_string,'\portfolio_maturity_date:',datestr(maturity_date,'yyyy-mm-dd'));
    else
        error_id=106;
    end
    
else
    % no port_dur entered
    error_id=105;
end

out_string=strcat(out_string,'\error_message:');
out_string=strcat(out_string,sprintf('%i',error_id));

fprintf(1,'%s\n',out_string)

