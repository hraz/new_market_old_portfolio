function calc_port_full_stat (in_string)
%% intializing 
import Pilot.*;
import utility.pilot_utility.*;
import utility.*;
import dal.market.get.dynamic.*;
import dal.market.get.base_data.*;
import dml.*;
import dal.market.get.cash_flow.*;

error_id=0;
out_string=[];
handles=[];
default_date=datenum('2011-11-23');
default_hist_length=30;
default_benchmark=601;
currency_rate=100;
ExpInflation=0.02;
% default_date=today;

%% intial parsing
in_string_split=regexp(in_string, '\','split');
field_name=cell(1,length(in_string_split)-1);
field_data=cell(1,length(in_string_split)-1);
for m=2:length(in_string_split)
    input_temp=regexp(in_string_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end

%% data filling
if sum(strcmpi('security_list',field_name))
    allocation=eval(field_data{find(strcmpi('security_list',field_name))});
else
    error_id=105;
end

if sum(strcmpi('transaction_history',field_name))
    transaction_string=field_data{find(strcmpi('transaction_history',field_name))};
    if isempty(transaction_string)||strcmp(transaction_string,'[]')
        transactions=[];
    else
        transaction_string(1)=[];
        transaction_string(end)=[];
        if strcmp(transaction_string(end),';')
            transaction_string(end)=[];
        end
        if ~isempty(transaction_string)
            transaction_cell=regexp(transaction_string, ';','split')';
            transaction_cell_split=regexp(transaction_cell,',','split','once');
            transactions=cell2mat(cellfun(@(x)[datenum(x{1}) str2num(x{2})],transaction_cell_split,'UniformOutput',0));
        end
    end
else
    transactions=[];
end

if sum(strcmpi('value_history',field_name))
    value_history=eval(field_data{find(strcmpi('value_history',field_name))});
else
    value_history=[];
end

if sum(strcmpi('start_date',field_name))
    start_date=datenum(field_data{find(strcmpi('start_date',field_name))},29);
else
    start_date=default_date;
end

if sum(strcmpi('current_date',field_name))
    req_date=datenum(field_data{find(strcmpi('current_date',field_name))},29);
else
    req_date=default_date;
end

if sum(strcmpi('end_date',field_name))
    end_date=datenum(field_data{find(strcmpi('end_date',field_name))},29);
else
    end_date=default_date;
end

if sum(strcmpi('last_update',field_name))
    last_update=datenum(field_data{find(strcmpi('last_update',field_name))},29);
else
    last_update=default_date;
end

if sum(strcmpi('invested_amount',field_name))
    invested_amount=str2num(field_data{find(strcmpi('invested_amount',field_name))});
else
    error_id=105;
end

if sum(strcmpi('hist_length',field_name))
    hist_length=str2num(field_data{find(strcmpi('hist_length',field_name))});
else
    hist_length=default_hist_length;
end

if sum(strcmpi('number_of_points',field_name))
    number_of_points_str=str2num(field_data{find(strcmpi('number_of_points',field_name))});
else
    number_of_points=0;
end

if sum(strcmpi('reinvest_strategy_id',field_name))
    reinvest_strategy_id=str2num(field_data{find(strcmpi('reinvest_strategy_id',field_name))});
else
    reinvest_strategy_id=0;
end

if sum(strcmpi('available_cash',field_name))
    cash=str2num(field_data{find(strcmpi('available_cash',field_name))});
else
    cash=0;
end

if sum(strcmpi('related_benchmark',field_name))
    benchmark_list=str2num(field_data{find(strcmpi('related_benchmark',field_name))});
else
    benchmark_list=default_benchmark;
end

%% calculating the historical statistics
if ~error_id
    nsin_list=allocation(:,1);
    Wts_units=allocation(:,2);
    fts_hist_length=get_bond_history_length(nsin_list, req_date);
    min_hist_length=min(min(fts_hist_length.history_length),hist_length);
    tbl_name = 'bond';
    data_name = {'yield', 'ttm', 'price_dirty'};%, 'price_yld', 'conv', 'price_clean_yld'};
    res_fts=get_multi_dyn_between_dates(nsin_list, data_name , tbl_name, req_date - min_hist_length, req_date);    
    all_prices=fts2mat(res_fts.price_dirty);
    current_prices=all_prices(end,:)';        
    fields_temp=fieldnames(res_fts.price_dirty);
    req_live_nsins=str2num(char(cellfun(@(x) x(3:end),fields_temp(4:end),'UniformOutput',0)));
    req_live_indx=find_multi_indx(req_live_nsins,nsin_list);
    Wts_amount=zeros(size(Wts_units));
    Wts_amount(req_live_indx)=Wts_units(req_live_indx).*(current_prices/currency_rate);
    holding_value=sum(Wts_amount);
    Wts=Wts_amount/holding_value;
    [exp_return exp_return_annually]=calculate_expected_return(nsin_list(req_live_indx),Wts(req_live_indx),req_date,min_hist_length,end_date,reinvest_strategy_id,ExpInflation);
    if last_update==start_date
        last_cf_fts=get_cash_flow(last_update,req_date, nsin_list);
        if ~isempty(last_cf_fts)
            coupons=fts2mat(last_cf_fts)/100;
            cf_dates=last_cf_fts.dates;
            fields_temp=fieldnames(last_cf_fts);
            cf_nsins=str2num(char(cellfun(@(x) x(3:end),fields_temp(4:end),'UniformOutput',0)));
            cf_nsins_indx=find_multi_indx(cf_nsins,nsin_list);
            cf=coupons.*repmat(Wts_units(cf_nsins_indx)',length(cf_dates),1);
            total_cf=nansum(nansum(cf));
        else
            total_cf=0;
        end
    else
        total_cf=-sum(transactions(transactions(:,1)>(last_update-0.01/3600/24),5));        
    end
    
    cash=cash+total_cf;    
    if cash<0
        cash=0;
        if ~error_id
            error_id=130;
        else
            error_id=131;
        end
    end
    
    holding_return=holding_value/invested_amount -1;
    estimated_final_capital=holding_value*(1+exp_return) + cash;
    current_capital=holding_value+cash;
    simple_return=current_capital/invested_amount -1;
    if ~isempty(value_history)
        volatility=std(value_history(:,2))/mean(value_history(:,2));
    else
        all_values=(all_prices/currency_rate)*Wts_units(req_live_indx);
        volatility=std(all_values)/mean(all_values);
    end
    
    fts0 = get_index_dyn_market( benchmark_list, 'value', start_date);
    fts1 = get_index_dyn_market( benchmark_list, 'value', req_date);
    
    initial_bench=fts2mat(fts0(end));
    current_bench=fts2mat(fts1(end));
    bench_return=current_bench/initial_bench - 1 ;
    
    out_string=strcat(out_string,'\error_message:');
    out_string=strcat(out_string,sprintf('%i',error_id));
    
    out_string=strcat(out_string,'\expected_return:');
    out_string=strcat(out_string,sprintf('%g',exp_return));

    out_string=strcat(out_string,'\expected_return_annually:');
    out_string=strcat(out_string,sprintf('%g',exp_return_annually));

    out_string=strcat(out_string,'\expected_volatility:');
    out_string=strcat(out_string,sprintf('%g',volatility));

    out_string=strcat(out_string,'\holding_return:');
    out_string=strcat(out_string,sprintf('%g',holding_return));
    
    out_string=strcat(out_string,'\estimated_final_capital:');
    out_string=strcat(out_string,sprintf('%g',estimated_final_capital));   
  
    out_string=strcat(out_string,'\current_capital:');
    out_string=strcat(out_string,sprintf('%g',current_capital));
    
    out_string=strcat(out_string,'\available_cash:');
    out_string=strcat(out_string,sprintf('%g',cash));
    
    out_string=strcat(out_string,'\money_weighted_return:');
    out_string=strcat(out_string,sprintf('%g',simple_return));  % STUB
    
    out_string=strcat(out_string,'\time_weighted_return:');
    out_string=strcat(out_string,sprintf('%g',simple_return));  % STUB
    
    out_string=strcat(out_string,'\real_volatility:');
    out_string=strcat(out_string,sprintf('%g',volatility));
    
    out_string=strcat(out_string,'\benchmark_id:');
    out_string=strcat(out_string,sprintf('%g',benchmark_list));    
    
    out_string=strcat(out_string,'\bench_return:');
    out_string=strcat(out_string,sprintf('%g',bench_return)); 
    
    out_string=strcat(out_string,'\downside_volatility :');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\skewness:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\kurtosis:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\sharpe:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\omega:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\jensen:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\inf_ratio:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\alpha:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\beta:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\cond_v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\marginal_v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\component_v_a_r:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\rsquare_vs_expret:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\rmse_vs_expret:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\rsquare_vs_bench:');
    out_string=strcat(out_string,sprintf('%g',volatility));  % STUB
    out_string=strcat(out_string,'\rmse_vs.bench:');
    out_string=strcat(out_string,sprintf('%g',volatility));      % STUB
else
    out_string=strcat(out_string,'\error_message:');
    out_string=strcat(out_string,sprintf('%i',error_id));
end

fprintf(1,'%s\n',out_string)