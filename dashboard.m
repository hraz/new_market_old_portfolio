function varargout = dashboard(varargin)
%DASHBOARD M-file for dashboard.fig
%      DASHBOARD, by itself, creates a new DASHBOARD or raises the existing
%      singleton*.
%
%      H = DASHBOARD returns the handle to a new DASHBOARD or the handle to
%      the existing singleton*.
%
%      DASHBOARD('Property','Value',...) creates a new DASHBOARD using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to dashboard_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      DASHBOARD('CALLBACK') and DASHBOARD('CALLBACK',hObject,...) call the
%      local function named CALLBACK in DASHBOARD.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dashboard

% Last Modified by GUIDE v2.5 25-Jul-2013 17:33:43

% Import external packages:
import dashboard.*

global TIMING
TIMING = cell(50,2);

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',        fullfile('dashboard', mfilename), ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dashboard_OpeningFcn, ...
                   'gui_OutputFcn',  @dashboard_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before dashboard is made visible.
function dashboard_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Import external packages:
import dashboard.*;

% Suppress warning about uitab and uitabgroup's usage being deprecated
warning('off', 'MATLAB:uitab:DeprecatedFunction')
warning('off', 'MATLAB:uitabgroup:DeprecatedFunction')

% Suppress warning about uitabgroup being an old version
warning('off', 'MATLAB:uitabgroup:OldVersion')

% Create group of tabulations:
hTabGroup = uitabgroup('Parent',gcf);


handles.unselectedTabColor=get(hTabGroup,'BackgroundColor');
% handles.selectedTabColor=handles.unselectedTabColor-0.1;

% Create tabs:
hTabs(1) = uitab('Parent',hTabGroup, 'Title','Model');
hTabs(2) = uitab('Parent',hTabGroup, 'Title','Portfolio');
hTabs(3) = uitab('Parent',hTabGroup, 'Title','Additional Filter Options');

% Select main tab:
set(hTabGroup, 'SelectedTab',hTabs(1));

%% Add content to Model tab
set(handles.mdl_sim_filters_pnl,'Parent',hTabs(1));
handles.filter_list = {'model'; 'optimization'; 'target_return'; 'investment_horizon'; 'benchmark';   'rating_range'; 'assets_ttm'; ...
    'class_linkage'; 'sector'; 'yield'; 'turnover'; 'max_weight'; 'transaction_cost'; 'special'; 'reinvestment'};

set(handles.mdl_histograms_pnl,'Parent',hTabs(1));
handles.delta_hist_name = {'mdl_excess_ret_vs_benchmark_ret_hist', 'mdl_excess_ret_vs_benchmark_ret_tbl'; ...
                                          'mdl_excess_ret_vs_expected_ret_hist', 'mdl_excess_ret_vs_expected_ret_tbl'; ...
                                          'mdl_excess_sharpe_hist','mdl_excess_sharpe_tbl'; ...
                                          'mdl_information_ratio_hist','mdl_information_ratio_tbl' ...
                                          };


set(handles.mdl_statistics_pnl,'Parent',hTabs(1));

%% Add content to Portfolio tab
set(handles.prt_sim_filters_pnl, 'Parent',hTabs(2));
set(handles.prt_graphical_info_pnl,'Parent',hTabs(2));
set(handles.prt_performance_pnl, 'Parent',hTabs(2))

%% Add content to Additional Filters Options tab
set(handles.afo_filter_options_pnl, 'Parent',hTabs(3));

%% Finish up 
% Choose default command line output for dashboard
handles.output = hObject;

% Set listbox defaults:
[ handles ] = update_menu_lists( handles );

% Set Creation Range "To" edit box to the present date so that simulations
% created up to the present date are shown as a default.

set(handles.mdl_date_to_edit,'String',datestr(date,'yyyy-mm-dd'));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes dashboard wait for user response (see UIRESUME)
% uiwait(handles.mdl_tab);


% --- Outputs from this function are returned to the command line.
function varargout = dashboard_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Import external packages:
import dashboard.*;

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in mdl_model_listbox.
function mdl_model_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_model_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_model_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_model_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

%%% Start of changed part %%%
%dashboard.update_menu_lists( handles );
%%% End of changed part %%%

% --- Executes during object creation, after setting all properties.
function mdl_model_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_model_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_optimization_listbox.
function mdl_optimization_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_optimization_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_optimization_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_optimization_listbox

selected_constraint_indices = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraint_indices) > 1 & ismember(1,selected_constraint_indices)
    set(hObject,'Value',selected_constraint_indices(2:end));
end

% =========================================================================
% Check if "Target" is selected.  If it is, then allow (ungrey) "Target
% Return" listbox.  If "Target" is not selected, then make sure that
% "Target Return" listbox is greyed out.

% Get possible constraints listed within each constraint listbox.
possible_constraint_values = get(hObject, 'String');

% Get the current constraint values using their indices.
selected_constraint_values = possible_constraint_values(selected_constraint_indices);

% Ungrey Target Return listbox if Target is selected or grey out Target
% Return and reset its value to the default (all).

if any(ismember(selected_constraint_values,'Tar'))
    set(handles.mdl_target_return_listbox,'Enable','on')
else
    set(handles.mdl_target_return_listbox,'Enable','off')
    set(handles.mdl_target_return_listbox,'Value',1)
end


% --- Executes during object creation, after setting all properties.
function mdl_optimization_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_optimization_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_target_return_listbox.
function mdl_target_return_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_target_return_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_target_return_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_target_return_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end


% --- Executes during object creation, after setting all properties.
function mdl_target_return_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_target_return_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_investment_horizon_listbox.
function mdl_investment_horizon_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_investment_horizon_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_investment_horizon_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_investment_horizon_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end


% --- Executes during object creation, after setting all properties.
function mdl_investment_horizon_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_investment_horizon_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_benchmark_listbox.
function mdl_benchmark_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_benchmark_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_benchmark_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_benchmark_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_benchmark_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_benchmark_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_rating_range_listbox.
function mdl_rating_range_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_rating_range_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_rating_range_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_rating_range_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_rating_range_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_rating_range_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_assets_ttm_listbox.
function mdl_assets_ttm_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_assets_ttm_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_assets_ttm_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_assets_ttm_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_assets_ttm_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_assets_ttm_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in mdl_class_linkage_listbox.
function mdl_class_linkage_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_class_linkage_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_class_linkage_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_class_linkage_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_class_linkage_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_class_linkage_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_sector_listbox.
function mdl_sector_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_sector_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_sector_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_sector_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_sector_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_sector_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_yield_listbox.
function mdl_yield_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_yield_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_yield_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_yield_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_yield_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_yield_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_turnover_listbox.
function mdl_turnover_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_turnover_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_turnover_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_turnover_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_turnover_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_turnover_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_max_weight_listbox.
function mdl_max_weight_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_max_weight_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_max_weight_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_max_weight_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_max_weight_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_max_weight_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_transaction_cost_listbox.
function mdl_transaction_cost_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_transaction_cost_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_transaction_cost_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_transaction_cost_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_transaction_cost_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_transaction_cost_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_special_listbox.
function mdl_special_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_special_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_special_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_special_listbox

selected_constraints = get(hObject,'Value');

% Remove all option if other specific constraints are also selected.  For
% example, if both "Model" and "Marko" are selected, remove "Model" from
% the list of selected values (since it represents the "all" option, which
% is inconsistant with also selecting only some constraints from the list).
if length(selected_constraints) > 1 & ismember(1,selected_constraints)
    set(hObject,'Value',selected_constraints(2:end));
end

% --- Executes during object creation, after setting all properties.
function mdl_special_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_special_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in prt_get_btn.
function prt_get_btn_Callback(hObject, eventdata, handles)
% hObject    handle to prt_get_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Import external packages:
import dashboard.*;

tic
set(handles.retrieving_text,'String','Retrieving...');
drawnow
%% Get simulation list:
sim_list = get_sim_list(handles);
current_sim = get(handles.prt_req_sim_id_edit, 'String');
if ~isempty(current_sim)
    current_sim = str2double(current_sim);
else
    error('You did not choose any simulation!');
end

%% Get portfolio id according to given simulation:
ind = ismember(sim_list.sim_id, current_sim);

%% Get benchmark id:
[ benchmark_id ] = get_current_benchmark_id( sim_list.portfolio_id(ind), sim_list.obs_date{ind} );

%% Callback execute:
% setup_portfolio( handles, sim_list, sim_list.sim_id(ind), sim_list.portfolio_id(ind), sim_list.obs_date{ind}, benchmark_id);
setup_portfolio( handles, sim_list, sim_list.sim_id(ind), ind, benchmark_id);

%% Add all data to GUI:
guidata(hObject, handles);

time_string = ['Completed retrieval',char(10),'in ',num2str(ceil(toc)),' seconds.'];
set(handles.retrieving_text,'String',time_string);

function mdl_sim_from_edit_Callback(hObject, eventdata, handles)
% hObject    handle to text_sim_from (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_sim_from as text
%        str2double(get(hObject,'String')) returns contents of text_sim_from as a double


function mdl_sim_to_edit_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_sim_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mdl_sim_to_edit as text
%        str2double(get(hObject,'String')) returns contents of mdl_sim_to_edit as a double


% --- Executes during object creation, after setting all properties.
function mdl_sim_to_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_sim_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mdl_date_from_edit_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_date_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mdl_date_from_edit as text
%        str2double(get(hObject,'String')) returns contents of mdl_date_from_edit as a double

%%% Start of changed part %%%
set(handles.prt_date_from_edit, 'String', get(handles.mdl_date_from_edit, 'String'));
%%% End of changed part %%%




% --- Executes during object creation, after setting all properties.
function mdl_date_from_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_date_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mdl_date_to_edit_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_date_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mdl_date_to_edit as text
%        str2double(get(hObject,'String')) returns contents of mdl_date_to_edit as a double

%%% Start of changed part %%%
set(handles.prt_date_to_edit, 'String', get(handles.mdl_date_to_edit, 'String'));
%%% End of changed part %%%

% --- Executes during object creation, after setting all properties.
function mdl_date_to_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_date_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function mdl_reinvestment_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_reinvestment_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in mdl_reinvestment_listbox.
function mdl_reinvestment_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_reinvestment_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mdl_reinvestment_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mdl_reinvestment_listbox


function prt_model_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_model_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_model_listbox as text
%        str2double(get(hObject,'String')) returns contents of prt_model_listbox as a double


% --- Executes during object creation, after setting all properties.
function prt_model_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_model_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_optimization_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_optimization_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_optimization_listbox as text
%        str2double(get(hObject,'String')) returns contents of prt_optimization_listbox as a double


% --- Executes during object creation, after setting all properties.
function prt_optimization_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_optimization_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_target_return_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_target_return_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_target_return_listbox as text
%        str2double(get(hObject,'String')) returns contents of prt_target_return_listbox as a double


% --- Executes during object creation, after setting all properties.
function prt_target_return_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_target_return_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_investment_horizon_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_investment_horizon_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_investment_horizon_listbox as text
%        str2double(get(hObject,'String')) returns contents of prt_investment_horizon_listbox as a double


% --- Executes during object creation, after setting all properties.
function prt_investment_horizon_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_investment_horizon_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_benchmark_listbox.
function prt_benchmark_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_benchmark_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_benchmark_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_benchmark_listbox


% --- Executes during object creation, after setting all properties.
function prt_benchmark_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_benchmark_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_rating_range_listbox.
function prt_rating_range_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_rating_range_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_rating_range_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_rating_range_listbox


% --- Executes during object creation, after setting all properties.
function prt_rating_range_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_rating_range_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_assets_ttm_listbox.
function prt_assets_ttm_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_assets_ttm_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_assets_ttm_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_assets_ttm_listbox


% --- Executes during object creation, after setting all properties.
function prt_assets_ttm_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_assets_ttm_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_class_linkage_listbox.
function prt_class_linkage_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_class_linkage_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_class_linkage_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_class_linkage_listbox


% --- Executes during object creation, after setting all properties.
function prt_class_linkage_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_class_linkage_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_sector_listbox.
function prt_sector_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_sector_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_sector_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_sector_listbox


% --- Executes during object creation, after setting all properties.
function prt_sector_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_sector_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_yield_listbox.
function prt_yield_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_yield_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_yield_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_yield_listbox


% --- Executes during object creation, after setting all properties.
function prt_yield_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_yield_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_turnover_listbox.
function prt_turnover_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_turnover_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_turnover_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_turnover_listbox


% --- Executes during object creation, after setting all properties.
function prt_turnover_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_turnover_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_max_weight_listbox.
function prt_max_weight_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_max_weight_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_max_weight_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_max_weight_listbox


% --- Executes during object creation, after setting all properties.
function prt_max_weight_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_max_weight_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_transaction_cost_listbox.
function prt_transaction_cost_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_transaction_cost_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_transaction_cost_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_transaction_cost_listbox


% --- Executes during object creation, after setting all properties.
function prt_transaction_cost_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_transaction_cost_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_special_listbox.
function prt_special_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_special_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_special_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_special_listbox


% --- Executes during object creation, after setting all properties.
function prt_special_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_special_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in prt_reinvestment_listbox.
function prt_reinvestment_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_reinvestment_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns prt_reinvestment_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from prt_reinvestment_listbox


% --- Executes during object creation, after setting all properties.
function prt_reinvestment_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_reinvestment_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in prt_random_btn.
function prt_random_btn_Callback(hObject, eventdata, handles)
% hObject    handle to prt_random_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Import external packages:
import dashboard.*;

tic
set(handles.retrieving_text,'String','Retrieving...');
drawnow

%% Get simulation list:
sim_list = get_sim_list(handles);
if ~isempty(sim_list)
    [ sim_id ] = random_id( sim_list.sim_id );
end

%% Get portfolio id according to given simulation:
ind = find(ismember(sim_list.sim_id, sim_id));

%% Get benchmark id:
[ benchmark_id ] = get_current_benchmark_id( sim_list.portfolio_id(ind), sim_list.obs_date{ind} );

%% Callback execute:
% setup_portfolio( handles, sim_list, sim_list.sim_id(ind), sim_list.portfolio_id(ind), sim_list.obs_date{ind}, benchmark_id);
setup_portfolio( handles, sim_list, sim_list.sim_id(ind), ind, benchmark_id);

%% Add all data to GUI:
guidata(hObject, handles);


time_string = ['Completed retrieval',char(10),'in ',num2str(ceil(toc)),' seconds.'];
set(handles.retrieving_text,'String',time_string);

% --- Executes on button press in prt_prev_btn.
function prt_prev_btn_Callback(hObject, eventdata, handles)
% hObject    handle to prt_prev_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Import external packages:
import dashboard.*;

tic
set(handles.retrieving_text,'String','Retrieving...');
drawnow

%% Get simulation list:
sim_list = get_sim_list(handles);
current_sim = get(handles.prt_req_sim_id_edit, 'String');
if ~isempty(current_sim)
    current_sim = str2double(current_sim);
else
    error('You did not choose any simulation!');
end

%% Get portfolio id according to given simulation:
ind = find(ismember(sim_list.sim_id, current_sim));
if ~isempty(ind) && ind >1
    prev_ind = ind - 1;
elseif ind == 1
    prev_ind = lenght(sim_list);
else
    prev_ind = find(sim_list.sim_id < current_sim, 1, 'first');
end

%% Get benchmark id:
[ benchmark_id ] = get_current_benchmark_id( sim_list.portfolio_id(prev_ind), sim_list.obs_date{prev_ind} );

%% Callback execute:
% setup_portfolio( handles, sim_list, sim_list.sim_id(prev_ind), sim_list.portfolio_id(prev_ind), sim_list.obs_date{prev_ind}, benchmark_id);
setup_portfolio( handles, sim_list, sim_list.sim_id(prev_ind), prev_ind, benchmark_id);

%% Add all data to GUI:
guidata(hObject, handles);

time_string = ['Completed retrieval',char(10),'in ',num2str(ceil(toc)),' seconds.'];
set(handles.retrieving_text,'String',time_string);

% --- Executes on button press in prt_next_btn.
function prt_next_btn_Callback(hObject, eventdata, handles)
% hObject    handle to prt_next_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Import external packages:
import dashboard.*;

tic
set(handles.retrieving_text,'String','Retrieving...');
drawnow

%% Get simulation list:
sim_list = get_sim_list(handles);
current_sim = get(handles.prt_req_sim_id_edit, 'String');
if ~isempty(current_sim)
    current_sim = str2double(current_sim);
else
    error('You did not choose any simulation!');
end

%% Get portfolio id according to given simulation:
ind = find(ismember(sim_list.sim_id, current_sim));
if ~isempty(ind) && ind < length(sim_list)
    next_ind = ind + 1;
elseif ind == length(sim_list)
    next_ind = 1;
else
    next_ind = find(sim_list.sim_id > current_sim, 1, 'first');
end

%% Get benchmark id:
[ benchmark_id ] = get_current_benchmark_id( sim_list.portfolio_id(next_ind), sim_list.obs_date{next_ind} );

%% Callback execute:
% setup_portfolio( handles, sim_list, sim_list.sim_id(next_ind), sim_list.portfolio_id(next_ind), sim_list.obs_date{next_ind}, benchmark_id);
setup_portfolio( handles, sim_list, sim_list.sim_id(next_ind), next_ind, benchmark_id);

%% Add all data to GUI:
guidata(hObject, handles);

time_string = ['Completed retrieval',char(10),'in ',num2str(ceil(toc)),' seconds.'];
set(handles.retrieving_text,'String',time_string);

function prt_req_sim_id_edit_Callback(hObject, eventdata, handles)
% hObject    handle to prt_req_sim_id_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_req_sim_id_edit as text
%        str2double(get(hObject,'String')) returns contents of prt_req_sim_id_edit as a double


% --- Executes during object creation, after setting all properties.
function prt_req_sim_id_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_req_sim_id_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_date_from_edit_Callback(hObject, eventdata, handles)
% hObject    handle to prt_date_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_date_from_edit as text
%        str2double(get(hObject,'String')) returns contents of prt_date_from_edit as a double


% --- Executes during object creation, after setting all properties.
function prt_date_from_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_date_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_date_to_edit_Callback(hObject, eventdata, handles)
% hObject    handle to prt_date_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_date_to_edit as text
%        str2double(get(hObject,'String')) returns contents of prt_date_to_edit as a double


% --- Executes during object creation, after setting all properties.
function prt_date_to_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_date_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function mdl_sim_from_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_sim_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_sim_from_edit_Callback(hObject, eventdata, handles)
% hObject    handle to prt_sim_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_sim_from_edit as text
%        str2double(get(hObject,'String')) returns contents of prt_sim_from_edit as a double


% --- Executes during object creation, after setting all properties.
function prt_sim_from_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_sim_from_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function prt_sim_to_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_sim_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prt_sim_to_edit_Callback(hObject, eventdata, handles)
% hObject    handle to prt_sim_to_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_sim_to_edit as text
%        str2double(get(hObject,'String')) returns contents of prt_sim_to_edit as a double

function significance_edit_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function significance_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function null_proportion_edit_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function null_proportion_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function mdl_reset_btn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_calculate_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in mdl_calculate_btn.
function mdl_reset_btn_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_calculate_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

import dashboard.*;
reset_current_menu_value(handles)

function mdl_clear_sim_range_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_clear_sim_range_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mdl_clear_sim_range_checkbox as text
%        str2double(get(hObject,'String')) returns contents of mdl_clear_sim_range_checkbox as a double


% --- Executes during object creation, after setting all properties.
function mdl_clear_sim_range_checkbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_clear_sim_range_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function prt_graphed_benchmark_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to prt_graphed_benchmark_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prt_graphed_benchmark_listbox as text
%        str2double(get(hObject,'String')) returns contents of prt_graphed_benchmark_listbox as a double

import dashboard.*

graph_multiple_benchmarks(handles)


% --- Executes during object creation, after setting all properties.
function prt_graphed_benchmark_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prt_graphed_benchmark_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function mdl_calculate_btn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mdl_calculate_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%% Import external packages;
import dashboard.*;

%% Set button icon:
% set_icon(hObject, 'Search');
% set(hObject, 'String', '');
% get(hObject, 'String')
% 

% --- Executes on button press in mdl_calculate_btn.
function mdl_calculate_btn_Callback(hObject, eventdata, handles)
% hObject    handle to mdl_calculate_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Import external packages:
import dashboard.*;

tic
set(handles.calculating_text,'String','Calculating...');
drawnow

%% Callback execute:
handles = dashboard_exe( handles );

%% Add all data to GUI:
guidata(hObject, handles);

time_string = ['Completed calculation',char(10),'in ',num2str(ceil(toc)),' seconds.'];
set(handles.calculating_text,'String',time_string);
