function [ portStatisticsTbl ] = get_portfolio_statistics( portfolioID, creationDate, lastUpdate )
%GET_PORTFOLIO_STATISTICS calculates all shown rates about given portfolio.
%  
%   [ portStatisticsTbl ] = get_portfolio_statistics( portfolioID, creationDate, lastUpdate )
%   receives some portfolio id with its creation date and last update, and returns list of given portfolio
%   statistics in each of checked periods.
%
%   Input:
%       porfolioID - is a scalar value defines some investment portfolio.
%
%       creationDate - is a DATETIME string value specifying the given portfolio creation date.
%
%       lastUpdate - is a DATETIME string value specifying the given portfolio last statistics
%                           update.
%
%   Output:
%       portStatisticsTbl - is a dataset that includes following statistical information:
%                               - Expected Return
%                               - Holding Return
%                               - Volatility
%                               - Skewness
%                               - Kurtosis
%                               - Alpha
%                               - Beta
%                               - Unique Risk
%                               - Sharpe Ratio
%                               - Jensen Ratio
%                               - Omega
%                               - Info Ratio
%                               - RSquare vs. Exp. Return
%                               - RSquare vs. Benchmark
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.portfolio.data_access.*;
        
    %% Input validation:
    error(nargchk(3,3,nargin));
    
    %% Step #1 (Collect portfolio risk information):
    riskData = da_multi_hist(portfolioID, datenum(creationDate)+1, lastUpdate, {'risk'});
    riskMeasure =  fts2mat(rmfield(riskData.risk, {'account_id', 'v_a_r', 'cond_v_a_r', 'marginal_v_a_r', 'component_v_a_r'}));
    riskMeasure = round (riskMeasure * 10000) / 10000;
    
    %% Step #2 (Collect portfolio performance information):
    perfData = da_multi_hist(portfolioID, datenum(creationDate)+1, lastUpdate, {'performance'});
    perfMeasure = fts2mat(rmfield(perfData.performance, {'account_id', 'total_value', 'rmse_vs_expret','rmse_vs_bench'}));
    perfMeasure = round(perfMeasure * 10000) / 10000;

    %% Step #3 (Get expected return at creation date):
    model = da_port_model(portfolioID, lastUpdate);
    expRet  = round(model.expected_return * ones(1, size(perfMeasure,1)) * 10000) / 10000;

    %% Step #4 (Build portfolio statistics table):
    if all(~isempty(perfMeasure) || ~isempty(riskMeasure))
        portStatisticsTbl = [{'Expected Return'; 'Holding Return'; 'Volatility'; 'Skewness'; 'Kurtosis'; 'Alpha'; 'Beta'; 'Unique Risk'; 'Sharpe Ratio'; 'Jensen Ratio'; 'Omega'; 'Info Ratio'; 'RSquare vs. Exp. Return'; 'RSquare vs. Benchmark'}, ...
            num2cell([expRet; riskMeasure'; perfMeasure'])];        
    end
    
end

