function [ portSummaryTbl, creationDate ] = get_portfolio_summary( portfolioID, lastUpdate )
%GET_PORTFOLIO_SUMMARY calculates all shown rates about given portfolio.
%  
%   [ portSummaryTbl, creationDate ] = get_portfolio_summary( portfolioID, lastUpdate )
%   receives some portfolio id with its last update and returns top 10 allocated assets
%   with its minimal information summary about it.
%
%   Input:
%       porfolioID - is a scalar value defines some investment portfolio.
%
%   Output:
%       portSummaryTbl - is a dataset that includes following information about top 10 allocated
%                               assets:
%                               - symbol - is a TASE asset symbol (unique value)
%                               - sector - is an asset issuer sector of economy (for government is a
%                                              GOVERNMENT)
%                               - weight - is a percent of the total portfolio investment into the asset
%                               - duration - is a modified duration of the asset
%                               - yield - is a current (last known) yield of the asset
%
%       creationDate - is a DATETIME value specifying the given portfolio creation date.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, August 18, 2013. Added a fix for the problem
% that cash gotten in the portfolios is saved as zeros, but the database
% can't deal with zeros in the nsins field.
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.portfolio.data_access.*;
    import dal.market.get.dynamic.*;
    import dal.market.get.static.*;
    import utility.dataset.*;
    import utility.fts.*;
    import utility.*;
        
    %% Input validation:
    error(nargchk(2,2,nargin));
    
    %% Step #1 (Get portfolio allocation):
    portStatic = da_port_static( portfolioID );
    creationDate = portStatic.creation_date{:};
    
    % Allocation table with 10 top assets: 
    allocDs = da_port_dynamic(portfolioID, creationDate, 'allocation');
    
    % Removing nsin = 0 as a cash from allocation:
    allocDs = allocDs(allocDs.nsin ~= 0,:);
    allocDs = allocDs(allocDs.count ~= 0,:);
        
    lastKnownPrice = get_bond_dyn_single_date( allocDs.nsin, 'price_dirty', creationDate);
    % In case of dead bonds:
    price = zeros(size(lastKnownPrice,1), size(allocDs, 1));
    price(:,ismember(allocDs.nsin, strid2numid(tsnames(lastKnownPrice)))) = fts2mat(lastKnownPrice);
    if isempty(price)
        allocDs.weight = zeros(size(allocDs,1),1);
    else
        allocDs.holding = allocDs.count .* price';
        allocDs.weight = holdings2weights(allocDs.count', price', 1)';
    end
    weightInd = find(ismember(dsnames(allocDs), {'weight'}));
    nsinInd = find(ismember(dsnames(allocDs), {'nsin'}));
    
    allocation = sortrows(allocDs, weightInd, 'descend');
    allocation = allocation(1 : size(allocation,1), [nsinInd, weightInd]);

    % A fix for the problem that cash gotten in the portfolios is saved as
    % zeros, but the database can't deal with zeros in the nsins field.
    nsins = allocation.nsin;
    nsins = setdiff(nsins, 0);
    
    %% Step #2 (Get additional static information about given assets):
    symbolSectorClass = get_bond_stat( nsins, creationDate, {'symbol', 'sector', 'bond_class'} );
    
    %% Step #3 (Get additional dynamic information about given assets):
    ytm = fts2mat(get_bond_dyn_single_date( nsins , 'ytm' , creationDate ))';%fts2mat(get_bond_dyn_single_date( allocation.nsin , 'ytm' , portfolio.last_update ))';
    duration = fts2mat(get_bond_dyn_single_date( nsins , 'mod_dur' , creationDate ))';%fts2mat(get_bond_dyn_single_date( allocation.nsin , 'mod_dur' , portfolio.last_update ))';
 
    %% Step #3.1 (Recovery primary allocation table):
    allocation = allocation(ismember(nsins,symbolSectorClass.security_id),:);
    
    %% Step #4 (Build portfolio summary table):
    portSummaryTbl = [symbolSectorClass.symbol, symbolSectorClass.sector, symbolSectorClass.bond_class, num2cell(allocation.weight * 100), num2cell(duration), num2cell(ytm*100)];
    
end

