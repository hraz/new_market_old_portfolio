function [] = reset_current_menu_value(handles)
%RESET_CURRENT_MENU_VALUE resets the current values of each constraint
%   listbox on the dashboard to their default "all" values.
%
%   [] = reset_current_menu_value(handles) receives the handles of the GUI
%   object and returns nothing.
%
%   Input:
%       handles - is the dashboard GUI handles.
%
%   Output:
%       There is no output for this function.
%
% Eleanor Millman, July 28, 2013
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

%% Input validation.
error(nargchk(1, 1, nargin));


%% Set current constraint values to default (all).
numFilters = size(handles.filter_list,1);
for i = 1:numFilters
    set(handles.([ 'mdl_' , handles.filter_list{i}, '_listbox']), 'Value',1);
end

%% Set time period bounds to default (all).
set(handles.mdl_date_from_edit, 'String','2005-01-02');
set(handles.mdl_date_to_edit, 'String',datestr(date,'yyyy-mm-dd'));

%% Set given simulation range to default (all).
set(handles.mdl_sim_from_edit, 'String','');
set(handles.mdl_sim_to_edit, 'String','');

%% Set null_proportion and significance to defaults.
set(handles.null_proportion_edit,'String','0.5')
set(handles.significance_edit,'String','5')