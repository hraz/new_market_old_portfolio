function [ filtExp ] = sim_num_key( sim )
%Will provide key explainig how nsins were filtered
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

duration_vec = [2 4 6 10];
arg1 = num2str(duration_vec(mod(ceil(sim/60)-1, 4) +1));


sectors = {'Fin '; '~I&H '; 'All '; '~Bio '; 'Ind '; 'Stbl '};
arg2 = sectors{mod(ceil(sim/240)-1,6)+1};

rankings = {'A/B '; 'A/C '; 'A/NR '; 'BBB+/NR '; 'C-/NR '};  
arg3 = rankings{mod(ceil(sim/1440)-1,5)+1};

logical = [{'&& '}, {'|| '}];
arg4 = logical{mod(ceil(sim/7200)-1,2)+1};

types = {'C-NV'; 'C-CU '; 'C-all '; 'G-NV '; 'CG- NV/NV '; 'CG-CU/NV '; 'CG-A/NV '; 'G-C '; 'CG-NV/C '; 'CG-CU/C '; 'CG-A/C '; 'G-A '; 'CG-NV/A '; 'CG-CU/A '; 'A '}; 
arg5 = types{mod(ceil(sim/14400)-1,15)+1};

filtExp = [arg1 ' ' arg2 ' ' arg3 ' ' arg4 ' ' arg5];


