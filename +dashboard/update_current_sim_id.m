function [ handles ] = update_current_sim_id( handles, currentSimID )
%UPDATE_CURRENT_SIM_ID Summary of this function goes here
%   Detailed explanation goes here
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    
    %% Step #1 (set current portfolio id):
    set(handles.prt_req_sim_id_edit, 'String', num2str(currentSimID));
    
end

