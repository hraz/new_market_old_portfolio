clear; clc;

%% Import external packages:
import dashboard.*;
import dml.*;
import dal.portfolio.crud.*;
import dal.portfolio.data_access.*;
import dal.market.get.dynamic.*;
import utility.statistics.*;
import utility.dal.*;

%% Get random model and optimization type:
tic
[ model, opt_type ] = da_port_random_model_opt(); % ds = model & optimization_type
toc

%% Find some random portfolio with given random optimization model:
tic
[ account_id ] = da_port_random_account( model, opt_type , 1 );
toc
% account_id = 5693;


%% Get all data about this porfolio:
update_flag  = true;
up_date = datenum( '2012-01-01', 'yyyy-mm-dd' );
manager_id = 1;
tic
port = da_portfolio( account_id, up_date, manager_id, update_flag );
toc

%% Show graph of cumulative return (portfolio vs. benchmark):
[ ~, bench_cumulative] = get_bench_holding_return( port.benchmark.benchmark_id, port.creation_date, port.last_update );
bench_cumulative_ret = fints(bench_cumulative(:,1), bench_cumulative(:,2), {'benchmark_cumulative_return'});
[ port_cumulative ]  = da_port_income_history(port.account_id, port.creation_date, port.last_update);
port_cumulative_ret = fints(port_cumulative.obs_date,port_cumulative.value, {'port_return'} );

figure(1)
plot_ret = plot([bench_cumulative_ret, port_cumulative_ret]);

%% Definition of the portfolio holding period:
holding_period = round(etime(datevec(port.last_update), datevec(port.creation_date)) ./ (3600*24*365));

%% Get all deltas from database with such optimization model and holding period:
[ model_delta ] = da_port_deltas( model, opt_type, holding_period );

%% Calculation required statistical tests:
[chit_h, kst_h, llt_h, tt_h] = normdistchk(model_delta.delta1);

%% Calculation required histograms:
% figure(2)
delta1_hist = histfit(model_delta.delta1);
% figure(3)
delta2_hist = histfit(model_delta.delta2);
% figure(4)
delta3_hist = histfit(model_delta.delta3);

%% Open dashboard:
load dashboard_gui;
dashboard_gui_OpeningFcn(hObject, eventdata, handles, varargin);

