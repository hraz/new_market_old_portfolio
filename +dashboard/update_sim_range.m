function [ handles ] = update_sim_range( handles, first_sim_id, last_sim_id )
%UPDATE_SIM_RANGE Summary of this function goes here
%   Detailed explanation goes here

    %% Step #1 (Set date range in model tab):
%     if ~isempty(first_sim_id)
        set(handles.mdl_sim_from_edit, 'String', num2str(first_sim_id));
        set(handles.mdl_sim_to_edit, 'String', num2str(last_sim_id));
%     end
    
    %% Step #2 (Set date range in portfolio tab):
%     if ~isempty(last_sim_id)
        set(handles.prt_sim_from_edit, 'String', num2str(first_sim_id));
        set(handles.prt_sim_to_edit, 'String', num2str(last_sim_id));
%     end
    
end

