function [handles] = setup_model( handles, valueList, currentValue, timePeriodBound, simBound )
%SETUP_MODEL Gets information about the portfolios that passed specified
%   criteria and displays that information.
%
%   [handles] = setup_model( handles, valueList, currentValue, timePeriodBound, simBound )
%   takes in GUI information as well as specified constraint and portfolio
%   information and displays that information for the user.
%
%   Input:
%       handles - the struct of GUI handles
%
%       valueList - a struct of Nx1 cell arrays.  The elements of the struct
%           are 'optimization', target_return', 'investment_horizon',
%           'benchmark', 'rating_range', 'assets_ttm', 'class_linkage',
%           'sector', 'yield', turnover', 'max_weight', 'transaction_cost',
%           'special', and 'reinvestment'.  The elements of all of the cell
%           arrays are char strings that represent all of the menu items of
%           each listbox (for example, menuList.model =
%           {'Model','Marko','Omeg'}).
%
%       currentValue - a struct of 1x1 cell arrays.  The elements of the struct
%           are the same as the elements of menuList.  The elements of all
%           the cell arrays are char strings that represent the currently selected
%           menu item for each listbox.
%
%       timePeriodBound - a struct of 1x1 cell arrays.  The elements of the
%           struct are 'from_date' and 'to_date'.  The elements of the
%           cell arrays are char strings that represent dates in the form
%           yyyy-mm-dd (for example, '2005-01-02' is January 2, 2005).
%           
%
%       simBound - a struct of 1x1 cell arrays.  The elements of the
%           struct are 'from_sim' and 'to_sim'.  The elements of the
%           cell arrays are char strings that portfolio sim numbers.
%
%   Output:
%       handles - the struct of GUI handles
%
% Developed by Yigal Ben Tal
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dashboard.*;
%     global TIMING

    flagNormHistogram = true;
    %% Input validation:
    error(nargchk(5, 5, nargin));
    
    %% Step #1 (Get all model information):
    
    % Retrieve null proportion and sigificance from edit boxes.  If for
    % some reason there is no value, use defaults.
    nullProportion = str2double(get(handles.null_proportion_edit,'String'));
    if isempty(nullProportion) nullProportion = 0.5; end
    significance = str2double(get(handles.significance_edit,'String'))/100;
    if isempty(significance) significance = 0.05; end
    
%     t5=tic;
    [ modelDeltaTbl, modelDistTest, modelStatAvg, portfolioSim, modelDelta ] = get_model_info( valueList, currentValue, timePeriodBound, simBound, nullProportion, significance );
%     TIMING(5,:) = {'get_model_info', toc(t5)};

    %% Step #2 (Update simulation range):
%     t6=tic;
    numsim = size(portfolioSim,1);
    if ~isempty(portfolioSim.portfolio_id)
        set(handles.prt_sim_range_pnl, 'UserData', {portfolioSim});
    end
%     TIMING(6,:) = {'Update simulation range', toc(t6)};
    
    %% Step #3 (Show required Model's statistics):
%     t7=tic;
    set(handles.mdl_histograms_pnl, 'Title', ['Model Graphical Information: ' num2str(numsim), ' portfolios used ' ]);
    deltaName = fieldnames(modelDeltaTbl);
    for i = 1:size(handles.delta_hist_name,1)
        show_model_info( handles.(handles.delta_hist_name{i,1}), ...
            modelDelta.(deltaName{i}), handles.(handles.delta_hist_name{i,2}),...
            modelDeltaTbl.(deltaName{i}), flagNormHistogram );        
    end
%     TIMING(7,:) = {'show_model_info', toc(t7)};

    %% Step #4 (Show average statistical data and result of the Normal distribution checking of the given data):
%     t8 = tic;
    set(handles.mdl_avg_statistics_tbl, 'Data', modelStatAvg);
    set(handles.mdl_distribution_statistics_tbl, 'Data', modelDistTest);
%    TIMING(8,:) = {'Show average statistical data and result of the Normal distribution checking of the given data', toc(t8)};
    
end

