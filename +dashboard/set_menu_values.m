function [ handles ] = set_menu_values( handles, menuList, currentValue )
%SET_MENU_VALUES set on dashboard portfolio parameters random values.
%
%   [ handles ] = set_menu_values( handles, menuList, currentValue )
%   receives dashboard GUI handles set and return its already updated.
%
%   Input:
%       handles - is a set of dashboard GUI handles set.
%
%   `   menuList - a struct of Nx1 cell arrays.  The elements of the struct
%           are 'optimization', target_return', 'investment_horizon',
%           'benchmark', 'rating_range', 'assets_ttm', 'class_linkage',
%           'sector', 'yield', turnover', 'max_weight', 'transaction_cost',
%           'special', and 'reinvestment'.  The elements of all of the cell
%           arrays are char strings that represent all of the menu items of
%           each listbox (for example, menuList.model =
%           {'Model','Marko','Omeg'}).
%   
%
%       currentValue - a struct of 1x1 cell arrays.  The elements of the struct
%           are the same as the elements of menuList.  The elements of all
%           the cell arrays are char strings that represent the currently selected
%           menu item for each listbox.
%
%   Output:
%       handles - is a set of updated dashboard GUI handles set.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    
    %% Input validation:
    error(nargchk(3, 3, nargin));
    
    %% (Set current menu values):
    for i = 1 : numel(handles.filter_list)
        % Setup filter menu list and update chosen value:
        set(handles.([ 'mdl_' , handles.filter_list{i}, '_listbox']), 'String', menuList.(handles.filter_list{i}));

        % Update portfolio filter part:
        if strcmpi(handles.filter_list{i}, 'Benchmark') && any(strcmpi(currentValue.(handles.filter_list{i}), {'Benchmark', 'All'}))
            set(handles.([ 'prt_' , handles.filter_list{i}, '_listbox']), 'Enable', 'on');
        else
            set(handles.([ 'prt_' , handles.filter_list{i}, '_listbox']), 'Enable', 'inactive');
        end
            % Set list of data and current value:
            set(handles.([ 'prt_' , handles.filter_list{i}, '_listbox']), 'String', menuList.(handles.filter_list{i}));
            set(handles.([ 'prt_' , handles.filter_list{i}, '_listbox']), 'Value', find(ismember(menuList.(handles.filter_list{i}), currentValue.(handles.filter_list{i}))));
    end
    
end

