function [ benchmarkID ] = set_prt_benchmark_listbox( handles, portfolioID, lastUpdate )
%SET_PRT_BENCHMARK_LISTBOX takes a portfolio ID and the date of the last
%   update, changes the portfolio tab listbox for benchmark ID, and outputs
%   the benchmark ID associated with that portfolio.
%   
%   [ benchmarkID ] = get_current_benchmark_id( portfolioID, lastUpdate )
%   queries the portfolio database to determine which benchmark was used to
%   build a specific portfolio, changes the benchmark ID listbox in the
%   portfolio tab, and returns the benchmark ID.
%
%   Input: 
%       handles - struct of GUI handles
%
%       portfolioID - an integer (really a double) that corresponds to a
%       portfolio in the portfolio database.
%
%       lastUpdate - a char string that represents a date in the format
%       yyyy-mm-dd (for example, 2008-12-17 is December 17, 2008).
%
%   Output:
%       benchmarkID - an integer (really a double) that represents to the
%       benchmark used to create the portfolio specified by portfolioID.
%
% Eleanor Millman, August 1, 2013
% Copyright 2013, BondIT Ltd.

%% Import external packages:
import dal.portfolio.data_access.*;
import dashboard.*;

%% Find current benchmark ID
benchmarkID = da_portfolio_benchmark_list( portfolioID, lastUpdate, lastUpdate);

%set the portfolio benchmark listbox to the correct value
possibleBenchmarks = get(handles.mdl_benchmark_listbox, 'String');
currentBenchmarkIndex = find(cell2mat(cellfun(@(x) isequal(x,num2str(benchmarkID)),possibleBenchmarks,'UniformOutput',false)));

set(handles.prt_benchmark_listbox, 'String',possibleBenchmarks);
set(handles.prt_benchmark_listbox, 'Value',currentBenchmarkIndex);