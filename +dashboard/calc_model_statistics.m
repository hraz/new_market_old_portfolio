function [ deltaTbl, modelDistTest, avgStatTbl ] = calc_model_statistics( delta, portfolioStatistics, nullProportion, significance )
%CALC_MODEL_STATISTICS returns model distribution test table, average of all simulation statistics
%and some delta table.
%
%   [ deltaTbl, modelDistTest, avgStatTbl ] = calc_model_statistics( delta, portfolioStatistics, nullProportion, significance )
%   receives list of portfolio's deltas and its statistical summary, and returns deltas table, table
%   of the average statistical values and test results of the checking delta's Normal Distribution
%   probabilities.
%
%   Input:
%       delta - an Nx4 dataset of doubles.  The names of the 4 columns are
%       'delta1', 'delta2', 'delta3', and 'delta4'.  For a specific portfolio,
%       delta1 is the excess return over benchmark, delta2 is realized
%       return minus predicted return, all divided by volatility, delta3 is
%       the excess Sharpe over benchmark, and delta4 is the information
%       ratio.
%
%       portfolioStatistics - is a 1x14 dataset of doubles.  The names of
%       the 14 columns are 'avg_expected_return', 'avg_holding_return',
%       'avg_volatility', 'avg_skewness', 'avg_kurtosis', 'avg_alpha',
%       'avg_beta', 'avg_unique_risk', 'avg_sharpe', 'avg_jensen',
%       'avg_omega', 'avg_inf_ratio', 'avg_rsquare_vs_expret', and
%       'avg_rsquare_vs_bench'.
%
%       nullProportion - a double between 0 and 1 that represents the
%       proportion of values larger than setPoint.  Default is 0.5 unless
%       an input is provided.
%
%       significance - a double between 0 and 1 that represents the desired
%       level of confidence in the test.  The p-value resulting from the
%       test is compared to this value to determine whether to reject or
%       not reject the null hypothesis. For example, a significance of 0.05
%       corresponds to a 95% confidence level.  Default is 0.05 unless an
%       input is provided.
%
% Yigal Ben Talproportiontest: 
% Copyright 2011, BondIT Ltd.
%
% Modified by Eleanor Millman, July 28, 2013. Added null_proportion and
% signficance as inputs.
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.


    %% Import external packages:
    import utility.dataset.*;
    import utility.statistics.*;

    
    %% Input validation;
    error(nargchk(4, 4, nargin));
    
    %% Step #1 (Definition of  Model Statistics):
    deltaNumber = dsnames(delta); 
    deltaStatName = {'Mean', 'Std', 'Positive values (%)', 'Positive values> 1% (%)'};

    %% Step #2 (Calculation of histogram statistics tables):
    for i = 1 : numel(deltaNumber)
        deltaTbl.(deltaNumber{i}) = cell(numel(deltaStatName),2);
        deltaTbl.(deltaNumber{i})(:,1) = deltaStatName;
        
        if ~isempty(delta.(deltaNumber{i}))
            [ avg, vol ] = nanstat( delta.(deltaNumber{i}) );
            deltaTbl.(deltaNumber{i}){1,2} = avg;
            deltaTbl.(deltaNumber{i}){2,2} = vol ;
            deltaTbl.(deltaNumber{i}){3,2} = (nansum(delta.(deltaNumber{i}) > 0 ) / numel( delta.(deltaNumber{i})));
            if i == 1
                deltaTbl.(deltaNumber{i}){4,2} = (nansum(delta.(deltaNumber{i}) > 0.01 ) / numel( delta.(deltaNumber{i})));
            else
                deltaTbl.(deltaNumber{i}){4,2} = [];
            end
        else
            deltaTbl.(deltaNumber{i}){1,2} = [];
            deltaTbl.(deltaNumber{i}){2,2} = [] ;
            deltaTbl.(deltaNumber{i}){3,2} = [];
            deltaTbl.(delta_number{i}){4,2} = [];
        end
    end
    
    %% Step #3 (Calculation distribution test statistics table):
    modelDistTest = cell(10,3);
    modelDistTest(:,1) = { 'Graph 1: Chi-Sqrd'; 'Graph 1: Kolmogorov-Smirnov'; ...
        'Graph 1: Lilliefors'; 'Graph 1: Proportion Test'; 'Graph 1: Z-Test';...
        'Graph 2: Chi-Sqrd'; 'Graph 2: Kolmogorov-Smirnov'; ...
        'Graph 2: Lilliefors'; 'Graph 2: Proportion Test'; 'Graph 2: Z-Test'};
    
    if ~isempty(delta.delta1)
        [modelDistTest(1,2:3), modelDistTest(2,2:3), modelDistTest(3,2:3), modelDistTest(4,2:3),...
            modelDistTest(5,2:3)] = normdistchk(delta.delta1,nullProportion,significance);
    else
        modelDistTest(1:5,2) = {'No Data'};
    end
    
    
    if ~isempty(delta.delta2) 
        [modelDistTest(6,2:3), modelDistTest(7,2:3), modelDistTest(8,2:3), modelDistTest(9,2:3),... 
            modelDistTest(10,2:3)] = normdistchk(delta.delta2,nullProportion,significance);
    else
        modelDistTest(6:10,2) = {'No Data'};
    end
        
    %% Step #4 (Get average statistical values of all simulation):
    if  nargout > 2 && nargin > 1 && ~isempty(portfolioStatistics)
        avgStatTbl = [dsnames(portfolioStatistics)', num2cell(double(portfolioStatistics))'];
    else
        avgStatTbl = {};
    end
    
end

