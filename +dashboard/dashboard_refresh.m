function [errMsg] = dashboard_refresh(obj, event)
%DASHBOARD_REFRESH is update periodically the shown on the dashboard data.
%
%   [errMsg] = dashboard_refresh(obj, event)
%   receives dashboard figure with timer state and renew the shown data,
%   Input:
%       obj - is a timer object.
%
%       event - is a type of timer state.
%
%   Output:
%       none.
%
% Developed by Amit Godel 
% Updated by Yigal Ben tTal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dashboard.*;
    
    %% Input validation:
    % There is no any input validation at this stage.

    %% Step #1 (Define current figure and its own handle set):
    set(0,'showHiddenHandles','on')
    hfig = gcf;
    handles = guidata(hfig);

    %% Step #2 (Get random input model name, optimization type and holding period):
    [ handles ] = update_menu_lists( handles, true );
    
    %% Step #3 (Callback execute):
    try
        % Get values from drop boxes:
        menu_choosed_value.model = get(handles.mdl_list_dropbx, 'Value');
        menu_choosed_value.opt_type = get(handles.opt_type_list_dropbx, 'Value');
        menu_choosed_value.portfolio_holding_period = get(handles.portfolio_holding_period_list_dropbx, 'Value');
        menu_choosed_value.asset_ttm = get(handles.asset_ttm_list_dropbx, 'Value');

        menu_choosed_value.from_date = get(handles.from_date_etxt, 'String');
        menu_choosed_value.to_date = get(handles.to_date_etxt, 'String');

        % Callback execute:
        handles = dashboard_exe( handles, menu_choosed_value );
    catch ME
        errMsg = ME.message;
    end
    
    %% Step #4 (Add all data to GUI):
    guidata(hfig, handles);

    %% Step #5 (Hiding the current figure):
    set(0,'showHiddenHandles','off')

end
