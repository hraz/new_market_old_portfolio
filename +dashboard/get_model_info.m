function [ modelDeltaTbl, modelDistTest, modelStatAvg, portfolioSim, modelDelta ] = get_model_info( valueList, currentValue, timePeriodBound, simBound, nullProportion, significance )
%GET_MODEL_INFO prepare all model information according to given choices.
%
%   [ modelDeltaTbl, modelDistTest, modelStatAvg, portfolioSim, modelDelta ] = get_model_info( valueList, currentValue, timePeriodBound, simBound, nullProportion, significance )
%   receives lists of possible context filter values, user choices, time and simulation bounds, and
%   returns delta table minimal summary, results of Normal distribution tests on given deltas, model
%   averge statistical values and list of possible in the given bounds simulations.
%
%   Inputs:
%       nullProportion - a double between 0 and 1 that represents the
%       proportion of values larger than setPoint.  Default is 0.5 unless
%       an input is provided.
%
%       significance - a double between 0 and 1 that represents the desired
%       level of confidence in the test.  The p-value resulting from the
%       test is compared to this value to determine whether to reject or
%       not reject the null hypothesis. For example, a significance of 0.05
%       corresponds to a 95% confidence level.  Default is 0.05 unless an
%       input is provided.
%
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 28, 2013. Added null_proportion and
% signficance as inputs.
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.


    %% Import external packages:
    import dashboard.*;    
    import dal.portfolio.data_access.*;  
    import utility.dataset.*;
    
%     global TIMING
    
    %% Input validation:
error(nargchk(6, 6, nargin));
    
    %% Step #1 (Get all deltas from database according to given constraints):
%     t10=tic;
    [ adaptedCurrentValue, adaptedTimeBound, adaptedSimBound ] = adapt_current_value_4db( valueList, currentValue, timePeriodBound, simBound );
%     TIMING{10}=toc(t10);
%     t9=tic;
     [ modelDelta, portfolioLastUpdate ] = get_deltas( adaptedCurrentValue, adaptedTimeBound.from_date, adaptedTimeBound.to_date, adaptedSimBound.from_sim, adaptedSimBound.to_sim );    
%     TIMING(9,:) = {'da_port_deltas_new', toc(t9)};
%     t10=tic;
    if ~isempty(portfolioLastUpdate.portfolio_id)
        [portStatistics ] = da_port_statistics_by_constraints(portfolioLastUpdate.portfolio_id, adaptedTimeBound.from_date, adaptedTimeBound.to_date );
    else
        portStatistics = dataset();
    end
%     TIMING(10,:) = {'da_port_statistics_by_constraints', toc(t10)};
    
    %% Step #2 (Calculate Model statistics):
%     t11=tic;
    [ modelDeltaTbl, modelDistTest, modelStatAvg ] = calc_model_statistics( modelDelta, portStatistics, nullProportion, significance );
%     TIMING(11,:) = {'calc_model_statistics', toc(t11)};
    
    for j=1:size(modelStatAvg,1)
        modelStatAvg(j,1) = strrep(modelStatAvg(j,1), '_', ' ');    
        modelStatAvg{j,1}(1) = upper(modelStatAvg{j,1}(1));
    end
    
    %% Step #3 (Return list of result portfolio simulations):
    portfolioSim = portfolioLastUpdate;

end

