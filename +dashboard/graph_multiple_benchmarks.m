function [] = graph_multiple_benchmarks( handles )
%GRAPH_MULTIPLE_BENCHMARKS graphs the current portfolio and any selected
%   benchmarks on the same graph.
%
%   [] = graph_multiple_benchmarks( handles ) takes in the GUI handles,
%   graphs the current portfolio as well as any benchmarks selected by the
%   user on the same graph in the portfolio tab.
%
%   Input:
%       handles - struct of all GUI handles.
%
%   Output:
%       none
%
% Eleanor Millman, August 1, 2013
% Copyright 2013, BondIT Ltd.

import dal.market.get.dynamic.*

% Retrieve the portfolio cumulative return data from where it was stored in
% handles and graph it.
%
% **Note that this method of getting the portCumulativeReturn will only work
% when we are using Matlab. If this code is moved to another platform, the
% GUI handles struct 'handles' can no longer be used to store
% portCumulativeReturn, so another solution will need to be found.**

portCumulativeReturn = get(handles.prt_graphed_benchmark_listbox,'UserData');

axes(handles.prt_ret_vs_benchmark_graph)
h1 = plot(portCumulativeReturn);
set(h1,'Color','black', 'LineWidth',2 )
drawnow

set(handles.prt_ret_vs_benchmark_graph,'NextPlot','add')
% Get the benchmark cumulative return data for each benchmark selected by
% the user in the Graphing Benchmark listbox and plot them.

possibleBenchmarks = get(handles.prt_graphed_benchmark_listbox, 'String');
selectedBenchmarkIndices = get(handles.prt_graphed_benchmark_listbox, 'Value');
benchmarkIDs = possibleBenchmarks( selectedBenchmarkIndices );
 
C = {'b','m','c','y',[.5 .6 .7],'g','r',}; % Cell array of colors.

for i = 1:size(benchmarkIDs,1)
    [ ~, benchCumulativeReturn] = get_bench_holding_return( str2double(benchmarkIDs(i)), portCumulativeReturn.dates(1), portCumulativeReturn.dates(end) );
    ind = ismember(benchCumulativeReturn(:,1),portCumulativeReturn.dates);
    benchCumulativeReturn = benchCumulativeReturn(ind, :);
    benchCumulativeReturn = fints(benchCumulativeReturn(:,1), benchCumulativeReturn(:,2), {'bench_cumulative_ret'});

    h1 = plot(benchCumulativeReturn);

    set(h1,'Color',C{i},'LineWidth',2,'DisplayName',benchmarkIDs{i} )
end

set(handles.prt_ret_vs_benchmark_graph,'NextPlot','replace')

legend([{'Portfolio'};benchmarkIDs],'Location','best');

set(handles.prt_ret_vs_benchmark_graph,'XColor',[0.729 0.831 0.957]);
set(handles.prt_ret_vs_benchmark_graph,'YColor',[0.729 0.831 0.957]);
grid(handles.prt_ret_vs_benchmark_graph, 'on');
    