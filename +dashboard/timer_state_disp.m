function [] = timer_state_disp(obj, event, stringArg)
%TIMER_STATE_DISP show current state of the timer object according to given event.
%
%   [] = timer_state_disp(obj, event, string_arg)
%   receives timer object, event data and its state and display this state.
%
% Developed by Amit Godel.
% Updated by Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Step #1 (Message creation): 
    msg = [event.Type ' The event occurred at ' datestr(event.Data.time)];

    %% Step #2 (Display the message):
    disp(msg);
    disp(stringArg);
    
end