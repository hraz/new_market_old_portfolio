function [ randomID, errMsg ] = random_id( idList )
%RANDOM_ID returns random portfolio id from given list.
%
%   [ randomID, errMsg ] = random_id( idList )
%   receives some ID list and returns some random value from this list.
%   In case of an empty input the result is an empty too.
%
%   Input:
%       idList - is an NPORTFOLIOIDx1 numerical array specifying the given ID universe.
%
%   Output:
%       randomID - is a scalar value specifying the some random ID number.
%
%       errMsg - is a string value represented some error message.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Input validation:
    error(nargchk(1,1,nargin));
    
    %% Random portfolio ID calculation:
    if isempty(idList)
        randomID = [];
        errMsg = 'There was an empty input.';
    else
        randomID = idList(randi([1, length(idList)],1,1));
        errMsg = '';
    end

end

