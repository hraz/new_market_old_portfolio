function numbersList = names_to_numbers_reinvestment(namesList)

%NAMES_TO_NUMBERS_REINVESTMENT changes a list of names to a list of
%   numbers using an internal table.
%
%   *THIS IS A TEMPORARY FUNCTION THAT WILL BECOME UNNECESSARY AFTER THE
%   SWITCH TO THE NEW PORTFOLIO DATABASE*
%
%   numbersList = names_to_numbers_reinvestment(namesList) receives an
%   Nx1 cell array of strings of names and changes them to a list of numbers.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

REINVESTMENT_TABLE = {'Spec Bond','Risk Free','Portfolio','Benchmark','Bank','None','Real Bond'};

for i = 1 : numel(namesList)
    if isequal(namesList{1},'Reinvestment')
        numbersList{1} = namesList{1};
    else
        numbersList{i} = num2str(find(cell2mat(cellfun(@(x) isequal(x,namesList{i}),REINVESTMENT_TABLE,'UniformOutput',false))));
    end
end

% output the required column vector
numbersList = numbersList';