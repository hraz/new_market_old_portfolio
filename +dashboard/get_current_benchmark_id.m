function [ benchmarkID ] = get_current_benchmark_id( portfolioID, lastUpdate )
%GET_CURRENT_BENCHMARK_ID takes a portfolio ID and the date of the last
%   update and outputs the benchmark ID associated with that portfolio
%   
%   [ benchmarkID ] = get_current_benchmark_id( portfolioID, lastUpdate )
%   queries the portfolio database to determine which benchmark was used to
%   build a specific portfolio.
%
%   Input: 
%       portfolioID - an integer (really a double) that corresponds to a
%       portfolio in the portfolio database.
%
%       lastUpdate - a char string that represents a date in the format
%       yyyy-mm-dd (for example, 2008-12-17 is December 17, 2008).
%
%   Output:
%       benchmarkID - an integer (really a double) that represents to the
%       benchmark used to create the portfolio specified by portfolioID.
%
% Developed by Yigal Ben Tal
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

%% Import external packages:
import dal.portfolio.data_access.*;

%% Find current benchmark ID
benchmarkID = da_portfolio_benchmark_list( portfolioID, lastUpdate, lastUpdate);

