%% Import external packages:
import dal.market.test.get_bond_cf_dates.*;
import utility.dal.*;


%% Test description:
funName = 'get_bond_cf_dates';
testDesc = 'Correct arguments. -> result set:';
expectedStatus = 1;
expectedError = '';

start_date = '2010-01-01';
end_date = '2012-01-01';
nsin = [1060045,1060078];

expectedResult = dataset( [ 
{'2010-11-30'}
{'2011-05-31'}
],'VarNames',{'date'});
expectedResult =[expectedResult, dataset( [ 
	{'2010-11-21'}
  {  '2011-05-19'}
],'VarNames',{'id1060045'}) ] ;

expectedResult =[expectedResult, dataset( [ 
'null'
{ '2011-05-19'}
],'VarNames',{'id1060078'}) ];
expectedResult = dataset2fts( expectedResult, 'ex_day' );
%% Test execution:
 test_script( funName, testDesc, expectedError, expectedResult, start_date,end_date,nsin );
