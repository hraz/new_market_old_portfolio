function [] = test_script( funName, testDesc, expectedError, expectedResult,start_date,end_date,nsin )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.get.cash_flow.*;
    import dal.market.test.*;
    
    if isempty(expectedError)
        %% Test execution:
       res_fts = get_bond_cf_dates( start_date,end_date,nsin );


        %% Result analysis:
        if (isequal(res_fts,expectedResult))
            testResult = 'Success';
        else
            testResult = 'Failure';
        end
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
