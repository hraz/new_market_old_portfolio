function [] = test_script( funName, testDesc, expectedError, expectedResult,mlt_lb, mlt_ub,mdg_lb, mdg_ub, log_opr, rdate )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.market.filter.risk.*;
    import dal.market.test.*;
    
    if isempty(expectedError)
        %% Test execution:
       nsin = bond_rank_complex( mlt_lb, mlt_ub,mdg_lb, mdg_ub, log_opr, rdate );


        %% Result analysis:
        if (isequal(nsin,expectedResult))
            testResult = 'Success';
        else
            testResult = 'Failure';
        end
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
