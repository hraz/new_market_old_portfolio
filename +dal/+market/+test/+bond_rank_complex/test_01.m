%% Import external packages:
import dal.market.test.bond_rank_complex.*;


%% Test description:
funName = 'bond_rank_complex';
testDesc = 'Correct arguments. -> result set:';
expectedStatus = 1;
expectedError = '';

rdate = '2013-01-01';

%% Test AND operator:
mlt_lb = 'AA+';
mlt_ub = 'AAA';
mdg_lb = 'Aa1';
mdg_ub = 'Aaa';
log_opr = 'AND';

expectedResult = [1940287
1940329
1940360
1940451
1940469
1940477
1940485
1940493
1940527
1940535
2300051
7410079
7410087
7410129
7410137
7411135
7411192
7460108
7460140
7460207
];
%% Test execution:
 test_script( funName, testDesc, expectedError, expectedResult, mlt_lb,  mlt_ub, mdg_lb, mdg_ub, log_opr, rdate );
