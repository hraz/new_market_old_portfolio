function [] = save_test_result(result)
%SAVE_TEST_RESULT export the given test result into tsv file.

    global FILETYPE;
    
    %% Save tests' result on hard disk:
    targetFile = fullfile(pwd,'+dal', '+test', '+log', ['test_log.', FILETYPE]);
    if (exist(targetFile, 'file') == 2)
        if strcmpi(FILETYPE, 'xls')
            result = [dataset('XLSfile',targetFile); result];
        else
            result = [dataset('File',targetFile); result];
        end
    end
    
    if strcmpi(FILETYPE, 'xls')
        export(result,'XLSfile',targetFile);
    else
        export(result,'file',targetFile,'Delimiter','tab', 'WriteVarNames',true);
    end
    
end
