%% Import external packages:
import dal.market.test.get_dynamic_data.*;
import utility.dal.*;

%% Test description:
funName = 'get_dynamic_data';
testDesc = 'Correct arguments. -> result set:';
expectedStatus = 1;
expectedError = '';

start_date = '2005-01-01';
end_date = '2005-01-10';
nsin = [1081777,1081223];
data_name ={'ytm'};
tbl_name = 'bond';
obs_count = [];

res = dataset( [ 
{'2005-01-02'}
{'2005-01-03'}
{'2005-01-04'}
{'2005-01-05'}
{'2005-01-06'}
{'2005-01-09'}
{'2005-01-10'}

],'VarNames',{'date'});
res =[res, dataset( [ 
0.048254
0.050753
0.051916
0.053569
0.053628
0.053002
0.052464
],'VarNames',{'id1081777'}) ] ;

res =[res, dataset( [ 
 0.045884
0.045996
0.046108
0.046221
0.046335
0.046679
0.046795
],'VarNames',{'id1081223'}) ] ;
% res = dataset2fts( res, 'yield' );
% result.('yield') = res;
 struct_of_fts.(data_name{1}) = dataset2fts( res, data_name(1) );
% struct_of_fts.(data_name{i}) = dataset2fts( ds.data{i}, data_name(i) );
% Test execution:
 test_script( funName, testDesc, expectedError, struct_of_fts,  nsin, data_name, tbl_name, start_date, end_date, obs_count )
