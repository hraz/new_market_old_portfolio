%% Import external packages:
import dal.market.test.get_dynamic_data.*;
import utility.dal.*;


%% Test description:
funName = 'get_dynamic_data';
testDesc = 'Correct arguments. -> result set:';
expectedStatus = 1;
expectedError = '';

start_date = '2005-01-01';
end_date = '2005-01-10';
nsin = [601,602];
data_name ={'ytm','value'};
tbl_name = 'index';
obs_count = 5;

res1 = dataset( [ 
{'2005-01-04'}
{'2005-01-05'}
{'2005-01-06'}
{'2005-01-09'}
{'2005-01-10'}

],'VarNames',{'date'});
res1 =[res1, dataset( [ 
0.04594838
0.04589526
0.04615012
0.04622081
0.04589118
],'VarNames',{'id601'}) ] ;

res1 =[res1, dataset( [ 
0.0455859
0.04552035
0.04549438
0.04561859
0.04523994
],'VarNames',{'id602'}) ] ;

res2 = dataset( [ 
{'2005-01-04'}
{'2005-01-05'}
{'2005-01-06'}
{'2005-01-09'}
{'2005-01-10'}


],'VarNames',{'date'});
res2 =[res2, dataset( [ 
184.93
184.93
185.04
185.12
185.43
],'VarNames',{'id601'}) ] ;

res2 =[res2, dataset( [ 
186.25
186.23
186.34
186.41
186.82
],'VarNames',{'id602'}) ] ;

 struct_of_fts.(data_name{1}) = dataset2fts( res1, data_name(1) );
 struct_of_fts.(data_name{2}) = dataset2fts( res2, data_name(2) );

 test_script( funName, testDesc, expectedError, struct_of_fts,  nsin, data_name, tbl_name, start_date, end_date, obs_count )
