function [ nsin ] = bond_rank_complex(mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr, rdate )
%BOND_RANK_COMPLEX return list of bonds that are satisfied the input conditions.
%
%   [ nsin ] = bond_rank(mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr, rdate )
%   receives user defined bounds for required bond ranks (of one or of both ranking companies) and
%   return list of nsins of all bonds that are satisfied to given conditions.
%
%   Input:
%       mlt_lb - is a string value specifying the infinum value of the Maalot rank of bond. 
%
%       mlt_ub - is a string value specifying the supremum value of the Maalot rank of bond. 
%
%       mdg_lb - is a string value specifying the infinum value of the Midroog rank of bond. 
%
%       mdg_ub - is a string value specifying the supremum value of the Midroog rank of bond. 
%
%       log_opr - is a string value specifying the logical operator for case when user need
%                       select bonds according to both of existed ranks. 
%
%       rdate - is a scalar value specifying the required date.
%
%   Output:
%       nsin - is an NBONDSx1 vector specifying the nsins of bonds that satisfied given conditions. 
%
% Yigal Ben Tal
% Copyright 2011.
    
    %% Import external packages and declaration of global variables:
    import dal.mysql.connection.*;

    %% Input validation:
    error(nargchk(4,6, nargin));
        
    % Date checking:
    if (nargin < 4) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
            
    % Bounds checking:
    if isempty(mlt_lb)
        mlt_lb = 'NULL';
    elseif ~ischar(mlt_lb)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Maalot rating''s lower bound.');
    end
    
    if isempty(mlt_ub)
        mlt_ub = 'NULL';
    elseif ~ischar(mlt_ub)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Maalot rating''s upper bound.');
    end
    
    if isempty(mdg_lb)
        mdg_lb = 'NULL';
    elseif ~ischar(mdg_lb)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Midroog rating''s lower bound.');
    end
    
    if isempty(mdg_ub)
        mdg_ub = 'NULL';
    elseif ~ischar(mdg_ub)
        error('dal_filter_risk:bond_rank_complex:wrongInput', ...
            'Empty or not valid data type of the Midroog rating''s upper bound.');
    end
    
    if (nargin < 5) || isempty(log_opr)
        log_opr = 'OR';
    end
        
    %% Step #1 (Coverting of the non-empty input):
    if ~strcmpi(mlt_lb, 'NULL')
        mlt_lb = ['''', mlt_lb ,''''];
    end
    if ~strcmpi(mlt_ub, 'NULL')
        mlt_ub = ['''', mlt_ub ,''''];
    end
    if ~strcmpi(mdg_lb, 'NULL')
        mdg_lb = ['''', mdg_lb ,''''];
    end
    if ~strcmpi(mdg_ub, 'NULL')
        mdg_ub = ['''', mdg_ub ,''''];
    end
    log_opr = ['''', log_opr, ''''];
    
    %% Step #2 (SQL query for this filter):
    sqlquery = ['CALL fltr_rank(''' rdate ''', ''asset'', ' mlt_lb ', ' mlt_ub ', ' mdg_lb ', ' mdg_ub ', ' log_opr ');'];
    
    %% Step #3 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','cellarray');
    conn = mysql_conn();
    nsin = fetch(conn, sqlquery);
    
    %% Step #4 (Close opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');    
 
    %% Step #5 (Convert cell-array to numeric vector):
    nsin = cell2mat(nsin);
    
end
