% clear; clc;

import dal.market.filter.risk.*;
import utility.dal.*;
import utility.dataset.*;

%% Basic data:
rdate = '2007-09-28';
% nsin_list  = [1098789, 1098797, 1260306, 1260405, 1980150, 2260131, 2300069, 3900099, 6390157, 7410087];

%% Test AND operator:
 % First group includes:
% mlt_lb = 'BBB-';    mlt_ub = 'AAA';
% mdg_lb = 'NULL';     mdg_ub = 'NULL';
% log_opr = 'OR';
% % mlt_lb = 'BBB-';    mlt_ub = 'AAA';
% % mdg_lb = 'Baa3';     mdg_ub = 'Aaa';
% % log_opr = 'AND';
% 
% % ds = dataset({mlt_lb}, {mlt_ub}, {mdg_lb}, {mdg_ub}, {log_opr}, 'VarNames', {'mlt_lb', 'mlt_ub', 'mdg_lb', 'mdg_ub', 'log_opr'}); 
% [ ds ] = add2ds_for_fltr_rank( mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr);
% 
% % Second group includes:
% mlt_lb = 'NVR';       mlt_ub = 'BB+';
% mdg_lb = 'NULL';     mdg_ub = 'NULL';
% log_opr = 'OR';
% 
% % mlt_lb = 'NVR';       mlt_ub = 'BB+';
% % mdg_lb = 'NVR';     mdg_ub = 'Ba1';
% % log_opr = 'AND';

mlt_lb = {'BBB-'        % first group
               'NVR'};      % second group
mlt_ub = {'AAA'        % first group
                'BB+'};      % second group
mdg_lb = {''     % first group
                 ''};  % second group
mdg_ub = {''    % first group
                  ''}; % second group
log_opr = {'OR'      % first group
                  'OR'};   % second group

ds = dataset( mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr);

%% Filter results are:
tic
[struct_nsin ] = bond_rank_multi(  rdate, ds)
toc
