clear; clc;

import dal.market.filter.risk.*;
import dml.*;

rdate = '2011-11-23';

%% Test AND operator:
mlt_lb = 'AA+';
mlt_ub = 'AAA';

mdg_lb = 'Aaa';
mdg_ub = 'Aa1';
% 
% log_opr = 'AND';

%% Test OR operator:
% mlt_lb = 'AA+';
% mlt_ub = 'AAA';
log_opr = 'OR';
% 
% mdg_lb = '';
% mdg_ub = '';
% mlt_ub = '';
nsin = bond_rank_complex( mlt_lb, mlt_ub,mdg_lb, mdg_ub, log_opr, rdate )
mlt_lb = 'A-';
mlt_ub = 'AA-';
% 
mdg_lb = 'Baa1';
mdg_ub = 'A1';
% % 
log_opr = 'OR';
tic
nsin = bond_rank_complex( mlt_lb, mlt_ub,mdg_lb, mdg_ub, log_opr, rdate )
% toc

%%
% mlt_lb = 'RF';
% mlt_ub = 'RF';
% mdg_lb = 'RF';
% mdg_ub = 'RF';
% rf_nsin = bond_rank_complex(mlt_lb, mlt_ub, mdg_lb, mdg_ub, 'AND', rdate );
% mlt_lb = 'BBB-';
% mlt_ub = 'AAA';
% mdg_lb = 'Baa3';
% mdg_ub = 'Aaa';
% mr_nsin = bond_rank_complex(mlt_lb, mlt_ub, mdg_lb, mdg_ub, 'AND', rdate );
% intersect(rf_nsin, mr_nsin)=======
toc

