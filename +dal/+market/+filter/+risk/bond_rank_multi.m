function [ nsin_struct ] = bond_rank_multi(rdate, ds_rank, nsin_list )
%BOND_RANK_MULTI return list of bonds that are satisfied the input conditions.
%
%    [ struct_nsin ] = bond_rank_multi(rdate, ds, nsin_list )
%   receives user defined bounds for required bond ranks (of one or of both ranking companies) and
%   return struct of list of nsins of all bonds that are satisfied to given conditions.
%
%   Input:
%      ds - is a dataset that each line in it is a different filter parameters
%       
%       rdate - is a scalar value specifying the required date.
% 
%   Optional Input:
%       nsin_list - is an NBONDSx1 vector soecifying the list of bonds. 
%
%   Output:
%       struct_nsin - is an NBONDSx1 vector specifying the nsins of bonds that satisfied given conditions. 
%
% Yaakov Rechtman
% 02/07/2012

    %% Import external packages and declaration of global variables:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import dal.mysql.data_access.*;
    import utility.*;
    import utility.dataset.*;  
    
    %% Input validation:
    error(nargchk(2,3, nargin));
        
    % Date checking:
    if isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    %% Step #2 (SQL query for this filter):
    % Sql query for bulding the temporary table of the inputs
    sqlquery_rank_const_groups = 'CREATE TEMPORARY TABLE ilmarket.tt_prop(prim_key INT PRIMARY KEY AUTO_INCREMENT,mlt_lb TEXT,mlt_ub TEXT,mdg_lb TEXT,mdg_ub TEXT,log_opr TEXT);';
    sqlquery = ['CALL sp_fa_rank_multi(''' rdate ''', ''asset'');'];
    
    %% Step #3 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','cellarray');      
    conn = mysql_conn();
        
    % The case of given nsin list for additional filtering:
     if(nargin == 3)
       create_nsin_table(nsin_list,conn);
     end
     
    % Creating temporary table of grouped constraints:
    exec(conn, sqlquery_rank_const_groups);

    % Inserting values in the table above:
    fastinsert(conn,'tt_prop',dsnames(ds_rank) , ds_rank);
      
    % Executing procedure
    curs = exec(conn, sqlquery);
    return_values = fetchmulti(curs);
    
    %% Step #4 (Close opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');      
 
    %% Step #7 (Convert cell-array to numeric vector):
     for i = 1 : length(return_values.data)
        if ~isempty(return_values) && all(~strcmp(return_values.data{i},'No Data'))
            nsin_struct.(['filter_' num2str(i)]) = cell2mat(return_values.data{i});
        else
          nsin_struct.(['filter_' num2str(i)])  = fints();
        end
     end
    
end
