function [ nsin ] = bond_rank_partial(rating_scale, lower_bound, upper_bound, rdate )
%BOND_RANK_PARTIAL return list of bonds that satisfy the input conditions.
%
%   [ nsin ] = bond_rank_maalot(rating_scale, lower_bound, upper_bound, rdate )
%   receives user defined bounds for required rating scale and return list
%   of nsins of bonds that are satisfied to the given conditions.  
%
%   Input:
%       rating_scale - it is a string value specifying the name of the rating company. Possible
%                           values are: 'maalot', 'midroog'; 
%
%       lower_bound - it is a scalar value specifying the lower bond of the rank.
%
%       upper_bound - it is a scalar value specifying the upper bound of the rank. 
%
%       rdate - it is a scalar value specifying the required date.
%
%   Output:
%       nsin - it is an NBONDSx1 vector specifying the nsins of bonds that are satisfied to the
%                           given rating conditions. 
%
% Yigal Ben Tal
% Copyright 2011.
  
    %% Import external packages:
    import dal.mysql.connection.*;
        
    %% Input validation:
    error(nargchk(3,4,nargin));
    
    % Date checking:
    if (nargin < 4) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    % Filter name checking:
    if isempty(rating_scale) || ~ischar(rating_scale)
        error('dal_filter_risk:bond_rank_partial:wrongInput', ...
            'Empty or not valid data type of required rating scale (company).');
    end
    
    % Bounds checking:
    if isempty(lower_bound)
        lower_bound = 'NULL';
    elseif ~ischar(lower_bound)
        error('dal_filter_risk:bond_rank_partial:wrongInput', ...
            'Empty or not valid data type of the rating''s lower bound.');
    end
    
    if isempty(upper_bound)
        upper_bound = 'NULL';
    elseif ~ischar(lower_bound)
        error('dal_filter_risk:bond_rank_partial:wrongInput', ...
            'Empty or not valid data type of the rating''s upper bound.');
    end
    
    %% Step #1 (Check the empty input):
    if ~strcmpi(lower_bound, 'NULL')
        lower_bound = ['''', lower_bound ,''''];
    end
    if ~strcmpi(upper_bound, 'NULL')
        upper_bound = ['''', upper_bound ,''''];
    end
    
    %% Step #2 (Choosing needed filterring interface):
    switch lower(rating_scale)
        case 'maalot'
            sqlquery = ['CALL sp_fa_rank_mlt(''' rdate ''', ''asset'', ' lower_bound ', ' upper_bound ');'];
        case 'midroog'
            sqlquery = ['CALL sp_fa_rank_mdg(''' rdate ''', ''asset'', ' lower_bound ', ' upper_bound ');'];
        otherwise
                    error('dal_filter_risk:bond_risk_partial:wrongInput', 'Not valid rating scale');
    end

    %% Step #3 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','cellarray');
    conn = mysql_conn();
    nsin = fetch(conn, sqlquery);
       
    %% Step #4 (Close opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');  
    
    %% Step #5 (Convert cell-array to numeric vector):
    nsin = cell2mat(nsin);
    
end

