clear; clc;

import dal.market.filter.total.*;

nsin = [1087915,1093343,1094101,1320100,1460070,2300051,2860088,4570024,6080170,6120067,6390157,7150204,9230137,9230236,9230335,9230434,9230533,9266735,9542531,9590134];

stat_param.class_name = {'Government Bond'};
% stat_param.coupon_type = {'Fixed Interest'};
% stat_param.linkage_index = {'CPI'};
% stat_param.sector = {};
% stat_param.bond_type = {'Corporate Bond'};

% stat_param.class_name = {};
% stat_param.coupon_type = {'Variable Interest', 'Fixed Interest'};
% stat_param.linkage = {'CPI', 'NON LINKED'};
% stat_param.sector = {'COMMUNICATIONS AND MEDIA'};
% stat_param.bond_type = {'Corporate Bond'};
% stat_param.class_name = {};
% stat_param.coupon_type = {'Fixed Interest'};
% stat_param.linkage_index = {'NON LINKED'};
% stat_param.sector = {};
% stat_param.bond_type = {};

% dyn_param.history_length = {60 100000};
% dyn_param.ytm = {0 0.5};
% dyn_param.mod_dur = {2 7};
%  dyn_param.turnover = {100000 1.0000e+009};
dyn_param.ttm = {NaN NaN};

rdate = '2006-01-01';
% dyn_param.history_length = {NaN NaN};
% dyn_param.ytm = {NaN NaN};
% dyn_param.mod_dur = {NaN NaN};
% dyn_param.turnover = {NaN NaN};

tic
  [ nsin ] =  bond_ttl_nsin( rdate,stat_param, dyn_param,nsin );
toc
