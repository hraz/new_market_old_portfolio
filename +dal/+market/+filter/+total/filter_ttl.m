function [ nsin ] = filter_ttl( rdate,  stat_param, dyn_param,conn ,nsin_list)
%FILTER_TTL return list of bonds that satisfy the input conditions.
%
%   [ nsin ] = bond_ttl( type, sector, linkage, coupon_type, rdate, varargin ) receives user defined
%   conditions like bond type, sector of industry, linkage and/or coupon_type and list of dynamic
%   data conditions, and  return list of nsin numbers of all bonds that satisfy to these conditions.
%
%   Input:
%       rdate - is a scalar value specifying the required date.
%
%   Optional static input as fileds of struct:
%       class_name - is an 1x1 cell-array of string specifying the possible bond class_name like:
%                       'GOVERNMENT BOND' or 'NON-GOVERNMENT BOND'
%
%       coupon_type - is an 1xNCOUPONTYPES cell array of strings specifying the list of bond
%                   coupon types. Possible values are:  
%                   'Unknown', 'Fixed Interest', 'Variable Interest' or 'Special' and its combinations.
%
%       linkage - is an 1xNLINKAGES cell array of strings specifying list of required linkage
%                   variations, based on exist in database linkage types. Possible values are:
%                   'CPI', 'USD', 'UKP', 'EURO' and its combinations.
%
%       sector - is an 1xNSECTORS cell array of strings specifying list of required bond sectors
%                   based on exist in database sectors. 
%
%       bond_type - is an 1xNTYPES cell array of strings specifying list of required bond types based on
%                   exist in database types. 
% 
%       redemption_error -  is an 1xNRedemption_type cell array of strings specifying list of bonds
%                                             redemption sum error.
%                                             possible values
%                                             are:'no_error', 'error'
%
%   Optional dynamic input as fileds of struct:
%       'history_length' - is an 1x2 vector specifying lower and upper bounds of history length.
%
%       'yield' - is an 1x2 vector specifying lower and upper bounds of ytm bruto.
%
%       'mod_dur' - is an 1x2 vector specifying lower and upper bounds of modified duration.
%
%       'turnover' - is an 1x2 vector specifying lower and upper bounds of turnover.
%
%       'ttm' - is an 1x2 cell array of strings specifying lower and upper bounds of time 2 maturity.
%
%   Output:
%       nsin - is an NBONDSx1 cell array specifying the  nsin numbers of bonds that are satisfied to
%                   all filter conditions . 
%
% Yigal Ben Tal, Yaakov Rechtman
% Copyright 2011.
    
    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.market.filter.total.*
    import utility.dal.*;
       
    %% Input validation:
    error(nargchk(4, 5, nargin));
  
    if (nargin == 4)
        nsin_list = ',null,';
    else
        nsin_list = [ ',' num2str4sql(nsin_list) , ','];
    end
     %% Step #1 (SQL query for this filter):
    sqlquery = ['CALL fltr_bond_ttl(''', rdate, '''' , nsin_list , ' CONCAT(', stat_param.class_name, ...
                                                            '), CONCAT(', stat_param.coupon_type, ...
                                                            '), CONCAT(', stat_param.linkage_index, ...
                                                            '), CONCAT(', stat_param.sector, ...
                                                            '),CONCAT(', stat_param.bond_type, ...
                                                             '),CONCAT(', stat_param.redemption_error, '),'...
                                                        dyn_param.history_length{1} ', ' dyn_param.history_length{2} ', ' ...
                                                        dyn_param.mod_dur{1} ', ' dyn_param.mod_dur{2} ', ' ...
                                                        dyn_param.turnover{1} ', ' dyn_param.turnover{2} ', ' ...
                                                        dyn_param.yield{1} ', ' dyn_param.yield{2} ', ' ...
                                                        dyn_param.ttm{1} ', ' dyn_param.ttm{2} ');'];
    
    %% Step # 2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','cellarray');
%     conn = mysql_conn();
    nsin = fetch(conn, sqlquery);                
    
    %% Step #3 (Close the opened connection):
%     close(conn);
    setdbprefs ('DataReturnFormat','dataset');  
    
    %% Step #4 (Convert cell-array to numeric vector):
    nsin = cell2mat(nsin);
    
end

