function [ nsin ] = bond_ttl( rdate,  stat_param, dyn_param )
%BOND_TTL return list of bonds that satisfy the input conditions.
%
%   [ nsin ] = bond_ttl( type, sector, linkage, coupon_type, rdate, varargin ) receives user defined
%   conditions like bond type, sector of industry, linkage and/or coupon_type and list of dynamic
%   data conditions, and  return list of nsin numbers of all bonds that satisfy to these conditions.
%
%   Input:
%       rdate - is a scalar value specifying the required date.
%
%   Optional static input as fileds of struct:
%       class_name - is an 1x1 cell-array of string specifying the possible bond class_name like:
%                       'GOVERNMENT BOND' or 'NON-GOVERNMENT BOND'
%
%       coupon_type - is an 1xNCOUPONTYPES cell array of strings specifying the list of bond
%                   coupon types. Possible values are:  
%                   'Unknown', 'Fixed Interest', 'Variable Interest' or 'Special' and its combinations.
%
%       linkage - is an 1xNLINKAGES cell array of strings specifying list of required linkage
%                   variations, based on exist in database linkage types. Possible values are:
%                   'CPI', 'USD', 'UKP', 'EURO' and its combinations.
%
%       sector - is an 1xNSECTORS cell array of strings specifying list of required bond sectors
%                   based on exist in database sectors. 
%
%       bond_type - is an 1xNTYPES cell array of strings specifying list of required bond types based on
%                   exist in database types. 
% 
%       redemption_error - 1x1 cell array of staring specifying good or bad
%       bonds.
%
%   Optional dynamic input as fileds of struct:
%       'history_length' - is an 1x2 vector specifying lower and upper bounds of history length.
%
%       'yield' - is an 1x2 vector specifying lower and upper bounds of ytm bruto.
%
%       'mod_dur' - is an 1x2 vector specifying lower and upper bounds of modified duration.
%
%       'turnover' - is an 1x2 vector specifying lower and upper bounds of turnover.
%
%       'ttm' - is an 1x2 cell array of strings specifying lower and upper bounds of time 2 maturity.
%
%   Output:
%       nsin - is an NBONDSx1 cell array specifying the  nsin numbers of bonds that are satisfied to
%                   all filter conditions . 
%
% Yigal Ben Tal
% Copyright 2011.
    
    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.market.filter.total.*;
    import utility.dal.*;
    import utility.*;
       
    %% Input validation:
    error(nargchk(0, 3, nargin));
    
    
%     preparing arguments
    input_stat = set_fltr_ttl_stat_args(stat_param);   
    input_dyn = set_fltr_ttl_dyn_args(dyn_param);
    
%     Connecting to the databse
    conn = mysql_conn();
    nsin = filter_ttl(rdate,input_stat,input_dyn,conn);
    close(conn);

    
end

