function [ nsin_struct ] = bond_ttl_multi( rdate,ds_static, dyn_param, nsin_list)
%BOND_TTL_MULTI return list of bonds that satisfy the input conditions.
%
%    [ nsin_struct ] = bond_ttl_multi( rdate,ds_static, dyn_param, nsin_list) receives user defined
%   conditions like bond type, sector of industry, linkage and/or coupon_type and list of dynamic
%   data conditions, and  return struct of  list of nsin numbers of all bonds that satisfy to these conditions.
%
%   Input:
%       rdate - is a scalar value specifying the required date.
%
%   Optional static input as fileds of dataset:
%       class_name - is an 1x1 cell-array of string specifying the possible bond class_name like:
%                       'GOVERNMENT BOND' or 'NON-GOVERNMENT BOND'
%
%       coupon_type - is an 1xNCOUPONTYPES cell array of strings specifying the list of bond
%                   coupon types. Possible values are:  
%                   'Unknown', 'Fixed Interest', 'Variable Interest' or 'Special' and its combinations.
%
%       linkage - is an 1xNLINKAGES cell array of strings specifying list of required linkage
%                   variations, based on exist in database linkage types. Possible values are:
%                   'CPI', 'USD', 'UKP', 'EURO' and its combinations.
%
%       sector - is an 1xNSECTORS cell array of strings specifying list of required bond sectors
%                   based on exist in database sectors. 
%
%       type - is an 1xNTYPES cell array of strings specifying list of required bond types based on
%                   exist in database types. 
% 
%       redemption_error -  is an 1xNRedemption_type cell array of strings specifying list of bonds
%                                             redemption sum error.
%                                             possible values are:'no_error', 'greater' forgreater than 100%, 'smaller' for smaller than 100%                  
%
%   Optional dynamic input as fileds of struct:
%       'history_length' - is an 1x2 vector specifying lower and upper bounds of history length.
%
%       'yield' - is an 1x2 vector specifying lower and upper bounds of ytm bruto.
%
%       'mod_dur' - is an 1x2 vector specifying lower and upper bounds of modified duration.
%
%       'turnover' - is an 1x2 vector specifying lower and upper bounds of turnover.
%
%       'ttm' - is an 1x2 cell array of strings specifying lower and upper bounds of time 2 maturity.
%   
%        nsin_list - is an NBONDSx1 cell array specifying the  nsin numbers of bonds on which to filter by, this parameter is optional.
%                   if empty, the filter iterates on all nsin in the database.
%   Output:
%       nsin_struct - is an struct of NBONDSxMFILERPARAM cell arrays specifying the  nsin numbers of bonds that are satisfied to
%                   all filter conditions . 
%
% Yaakov Rechtman
% 30/06/2012
     
    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.market.filter.total.*;
     import utility.dal.*;
    import utility.*;
       
    %% Input validation:
    error(nargchk(0, 4, nargin));
    
     if (nargin == 0) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
     end
%     Connecting to the databse
    conn = mysql_conn();
    
%     Preparing dynamic arguments
    dyn_param = set_fltr_ttl_dyn_args(dyn_param);
    
%     starting to iterate over static arguments
    for i = 1:size(ds_static,2)
        if(nargin == 4)
            nsin = filter_ttl(rdate,ds_static(i), dyn_param,conn,nsin_list);
        else
            nsin = filter_ttl(rdate,ds_static(i), dyn_param,conn);
        end
         if ~isempty(nsin) && ~strcmp(nsin,'No Data')
                nsin_struct.(['filter_' num2str(i)]) = cell2mat( {double(nsin)});
         else
                nsin_struct.(['filter_' num2str(i)])  = []; 
         end
  
    end
     
      %%  (Close the opened connection):
    close(conn);
    
end

