function [ cmp ] = get_market_indx_components( indx_nsin, rdate )
%GET_MARKET_INDX_COMPONENTS returns index components at required date.
%
%   [ cmp ] = get_market_indx_components( indx_nsin, rdate ) receives index
%   nsin and interested date, and returns all components.
%
%   Input:
%       indx_nsin - is a scalar value (INT) specifying the nsin of some market index.
%
%       rdate - is a scalar value specifying the required date.
%
%   Output:
%       cmp - is a NASSETSx2 dataset of index components' nsins and its weight at given date.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(2,2,nargin));
          
    if (nargin == 1) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end

    if ~isnumeric(indx_nsin)
        error('dal_filter_market_indx:get_market_indx_components:wrongInput', ...
            'Wrong type of indx_nsin.');
    end
    
        %% Step #1 (SQL query for this filter):
    sqlquery = ['CALL get_market_indx_cmp(''' rdate ''', ' num2str4sql(indx_nsin) ');'];
    
    %% Step #2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn();
    cmp = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
        
end

