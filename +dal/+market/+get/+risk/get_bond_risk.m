function [ ds ] = get_bond_risk( nsin_list, rdate )
%GET_BOND_RISK returns last known at required date risk ratings.
%
%   [ ds ] = get_bond_risk( nsin_list, rdate ) receives list of nsin and interested date, and returns
%   the last known at the date risk ratings of Maalot and Midroog companies.
%
%   Input:
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsin numbers of needed bonds. 
%
%       rdate - is a time value ('yyyy-mm-dd') specifying the required date.
%
%   Output:
%       ds - is a NASSETSx5 dataset specifying the known rating values of Maalot and Midroog
%                       companies includes is last update dates for each asset.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(1, 2, nargin));
        
    if (nargin == 1) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    if ~isnumeric(nsin_list)
        error('dal_market_get_risk:get_bond_risk:wrongInputType', ...
            'Wrong type of nsin_list.');
    end
    if ~isvector(nsin_list)
        error('dal_market_get_risk:get_bond_risk:wrongInputDim', ...
            'Wrong dimentions of nsin_list.');
    end
    
    %     Temporary test until the end of simulations debugging
    if length(nsin_list) ~= length(unique(nsin_list))
         error('dal_market_get_dynamic:get_dynamic_data:wrongInputType', ...
            'Duplicate id in nsin_list.');
    end
    
     %% Step #1 (SQL sqlquery for this filter):
    sqlquery = ['CALL get_bond_risk( ''' rdate ''', ' num2str4sql(nsin_list) ');'];
   
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);

end

