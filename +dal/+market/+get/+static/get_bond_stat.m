function [ ds ] = get_bond_stat( nsin_list, rdate, data_name )
%GET_BOND_STAT return static data about required bonds at given date.
%
%   [ ds ] = get_bond_stat( nsin_list, rdate, data_list ) receives nsins of needed bonds and return static data
%   at given date. 
%
%   Input:
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsin numbers of needed bonds. 
%
%       rdate - is a date value specifying the required date.
%       
%       data_name - is a string value specifying which static data to
%       extract: isin, symbol, name, bond_class, bond_type, sector, issuer,
%       coupon_type, guarantee, seniority, linkage_index, notional_amount,
%       linkage_base_date, linkage_base_value, linkage_ratio_floor, issue_date,
%       first_trading_date,last_ex_date, maturity_date.
%       null value for all static data.
%
%   Output:
%       ds - is an NBONDSxMDATA dataset specifying the static data about required bonds at given
%               date. 
%
% Yigal Ben Tal
% Copyright 2011

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*
    
    %% Input validation:
    error(nargchk(1, 3, nargin));
    
    if (nargin == 1) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    if (nargin < 3) || (isempty(data_name))
        data_name = {'NULL'};
    end
    if (nargin == 3) && (ischar(data_name))
        data_name = {data_name};
    end
    %     Temporary test until the end of simulations debugging
    if length(nsin_list) ~= length(unique(nsin_list))
         error('dal_market_get_dynamic:get_dynamic_data:wrongInputType', ...
            'Duplicate id in nsin_list.');
    end
    
    %% Step #1 (SQL query for this filter):
    sqlquery = ['CALL get_bond_static_data(''' rdate ''', ' num2str4sql(nsin_list) ',' cell2str4sqlbracket(data_name) ');'];
    
    %% Step #2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end

