function [ ds ] = get_market_indx_stat( nsin_list, rdate )
%GET_MARKET_INDX_STAT return static data about required market index at given date.
%
%   [ ds ] = get_market_indx_stat( nsin_list, rdate ) receives nsins of required indices, date and
%   return static data at given date. 
%
%   Input:
%       nsin_list - is an NINDICESx1 numeric vector specifying the nsins of required indices. 
%
%       rdate - is a date value specifying the required date.
%
%   Output:
%       ds - is an NINDICESxMDATA dataset specifying the static data about required indices at
%                   given date. 
%
% Yigal Ben Tal
% Copyright 2011

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*
    
    %% Input validation:
    error(nargchk(1, 2, nargin));
    
    if (nargin == 1) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    %% Step #1 (SQL query for this filter):
    sqlquery = ['CALL get_market_indx_static(''' rdate ''', ' num2str4sql(nsin_list) ');'];
    
    %% Step #2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end


