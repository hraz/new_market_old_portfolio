function [ bench_holding_ret,bench_cumulative_ret ] = get_bench_holding_return( benchmark_id, first_date, last_date )
%GET_BENCH_HOLDING_RETURN returns benchmark holding return in given period.
%
%   [ bench_holding_ret ] = get_bench_holding_return( benchmark_id, first_date, last_date )
%   receives benchmark id number, first and last dates in required period, and return holding return
%   in given period.
%
%   Input:
%       benchmark_id - is a scalar value specifying the benchmark index id number.
%
%       first_date - is a scalar value specifying the first date of the required period.
%
%       last_date - is a scalar value specifying the last date of the required period.
%
%   Output:
%       bench_holding_ret - is an NOBSERVATIONx2 matrix of the benchmark holding return (the first
%                       column is  observation dates).
%
%       bench_cumulative_ret - is an NOBSERVATIONx2 matrix of the benchmark cumulative return (the first
%                       column is  observation dates).
%
% Yigal Bet Tal
% Copyright 2012,  BondIT Ltd.

    %% Import external packages:
    import dal.market.get.dynamic.*;
    
    %% Input validation:
    error(nargchk(3,3,nargin));
    if (~isnumeric(first_date))
        first_date = datenum(first_date, 'yyyy-mm-dd');
    end
    if (~isnumeric(last_date))
        last_date = datenum(last_date, 'yyyy-mm-dd');
    end
    
    %% Step #1 (Get benchmark values in required period):
    tmp = get_multi_dyn_between_dates(benchmark_id, {'value'}, 'index', first_date, last_date);
    bench_value = fts2mat(tmp.value,1);
    
    %% Step #2 (Build holding return):
    bench_holding_ret = zeros(size(bench_value,1), size(bench_value,2));
    bench_holding_ret(:,1) = bench_value(:, 1);
    init_bench_value = bench_value(1,2);
    bench_holding_ret(:,2) = (bench_value(:,2) ./ init_bench_value(ones(size(bench_value,1), 1), :)) - 1;
    
    %% Step #3 (Change unknown values to zero):
    bench_holding_ret(isnan(bench_holding_ret)) = 0;
    if nargout > 1
        bench_cumulative_ret = bench_holding_ret;
        bench_cumulative_ret(:,2) = bench_cumulative_ret(:,2) + 1;
    end
    
end

