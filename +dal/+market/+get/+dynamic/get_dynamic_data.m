function [ struct_of_fts ] = get_dynamic_data( nsin_list, data_name, tbl_name, start_date, end_date, obs_count )
%GET_DYNAMIC_DATA return dynamic multiple data about required input at given date.
%
%   [ struct_of_fts ] = dget_dynamic_data( nsin_list, data_name, tbl_name, start_date, end_date, obs_count ) receives  nsin, multiple interested data
%   name and required date interval, and return all daily dynamic  multiple data about required input untill given 
%   date. 
%
%   Input:
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsin numbers of needed input. 
%       
%       data_name - is an 1xNPROPS cellarray each value specifying the needed property. 
%                   Possible values for bonds are:
%                       - 'price_dirty' is a bond dirty price
%                       - 'price_clean' is a bond clean price without accured interest
%                       - 'acc_interest' is the accrued  interest embodied in the dirty_price
%                       - 'price_change' is a daily bond price change
%                       - 'price_change_clean' is a clean price daily change
%                       - 'yield' is yield to maturity bruto
%                       - 'duration' is a bond Macaulay duration
%                       - 'time_2_maturity' is a time to maturity
%                       - 'modified_duration' is a modified duration
%                       - 'convexity' is a convexity
%                       - 'volume' is a volume
%                       - 'turnover_monetary' is a turnover
%                       - 'market_capital' is a market capital
%                       - 'transaction_number' is a number of daily transactions
%                       - 'coupon_pmt' is a discounted near coupon payment if required date is
%                          between ex-day and payment-day and we were holders of the asset before
%                          ex-day
%                 Possible values for index are:
%                       - 'value' is a value of the index
%                       - 'value_change' is a daily change of the index value
%                       - 'yield' is an average of index components ytm
%                       - 'duration' is an average of index components duration
%                       - 'time_2_maturity' is an average of index components time to maturity
%                       - 'modified_duration' is an average of index components modified duration
%                       - 'convexity' is an average of index components convexity
%                       - 'volume' is an average of index components volume
%                       - 'turnover' is an average of index components turnover
%                       - 'capital_issued' is an average of index components issued capital
%                       - 'capital_registered' is an average of index components registerred to trading capital
%                       - 'market_capital' is an average of index components market capital
%                   Possible values for linkage index are:
%                           - 'index_value' is  a value of the linkage index
%       
%       tbl_name -  is a string value specifying the needed table. Possible values are:
%                           - bond - which checks the bonds history
%                           - index - which checks indices history
%                           - linkage - which checks linkage indices history
% 
%       start_date - is a time value ('yyyy-mm-dd') specifying the required start date.
% 
%       end_date - is a time value ('yyyy-mm-dd') specifying the required end date.
% 
%       obs_count - is a int value specifying the count of required
%                            observation, null if all observation are needed.
%
%   Output:
%       struct_of_fts - is a struct containing (NOBSERVATIONSx(MASSETS+1)) financial time series specifying the daily
%                   dynamic data untill given date, where the names of field in the struct is the same as
%                   the input.
%
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    % Input validation:
    error(nargchk(5,6,nargin)); 
    
    if isempty(start_date)
        start_date = '2005-01-02';
    elseif isnumeric(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd');
    end
    
    if isempty(end_date)
        end_date = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(end_date)
        end_date = datestr(end_date, 'yyyy-mm-dd');
    end
    
     if (nargin == 6) && ~isnumeric(obs_count)
         error('dal_market_get_dynamic:get_dynamic_data:wrongInputType', ...
            'Wrong type of obs_count.');
     end
    
    if datenum(start_date) > datenum(end_date)
        error('dal_market_get_dynamic:get_dynamic_data:wrongDateOrder', ...
            'Wrong order of period bounds.');
    end
    
    if ~isnumeric(nsin_list)
        error('dal_market_get_dynamic:get_dynamic_data:wrongInputType', ...
            'Wrong type of nsin_list.');
    end
    
%     Temporary test until the end of simulations debugging
    if length(nsin_list) ~= length(unique(nsin_list))
         error('dal_market_get_dynamic:get_dynamic_data:wrongInputType', ...
            'Duplicate id in nsin_list.');
    end
   
    if ~isvector(nsin_list)
        error('dal_market_get_dynamic:get_dynamic_data:wrongInputDim', ...
            'Wrong dimentions of nsin_list.');
    end
    
   bond ={  'price_dirty','price_dirty';...
                    'price_clean','price_clean';...
                   'price_clean_yld', 'price_change_clean';...
                   'acc_interest','acc_interest';...
                   'price_yld','price_change' ;...
                   'ytm','yield';...
                    'yield','yield';...
                   'mod_dur','modified_duration';...
                   'duration','duration';...
                   'ttm','time_2_maturity';...
                   'conv', 'convexity' ;...
                   'volume' ,'volume';...
                   'turnover','turnover_monetary';...
                   'market_cap','market_capital';...
                   'transaction','transaction_number';...
                   'coupon_pmt','coupon_pmt'};
    index = {'value','value_close';...
                    'value_yld','value_change';...
                    'ytm','yield';...
                    'yield','yield';...
                    'duration','duration';...
                    'ttm','time_2_maturity';...
                   'mod_dur','modified_duration';...
                   'conv', 'convexity' ;...
                  'volume' ,'volume';...
                  'turnover','turnover_monetary';...
                  'issued_cap','capital_issued' ;...
                  'reg_cap','capital_registered';...
                  'market_cap','market_capital' };
      linkage={'index_value','index_value'};
    
    
%     data_list = textscan(data_name, '%s', 'delimiter', ',');
%     data_list = data_list{:};
    
    if(strcmp('bond',tbl_name))
             if ~all(ismember(data_name,bond(:,1)))
                    error(' Incorrect property name');
             else
                 idx = ismember(bond(:,1), data_name);
                 dbDataName = bond(idx,2);
                 data_name = bond(idx,1);
            end; 
    elseif(strcmp(tbl_name,'index'))
         if ~all(ismember(data_name,index(:,1)))
                    error(' Incorrect property name');
         else
             idx = ismember(index(:,1), data_name);
                 dbDataName = index(idx,2);
                 data_name = index(idx,1);
            end; 
   elseif(strcmp(tbl_name,'linkage'))
         if ~all(ismember(data_name,linkage(:,1)))
                    error(' Incorrect property name');
         else
                 dbDataName = linkage(ismember(linkage(:,1), data_name),2);
            end; 
    else
        error(' Incorrect table name');
    end;
    data_list=cell2mat(strcat(dbDataName(:)',','));
%     data_list=cell2mat(strcat(data_name(:)',','));
     data_list=data_list(1:end-1);
    
    %% Step #1 (SQL sqlquery for this filter):
    if  (nargin == 6 && ~isempty(obs_count))
         sqlquery = ['CALL get_dynamic_data( ''' start_date ''',''' end_date ''', ''' data_list ''', ''' tbl_name ''', ' num2str4sql(nsin_list) ',' num2str(obs_count) ');'];
     else
        sqlquery = ['CALL get_dynamic_data( ''' start_date ''',''' end_date ''', ''' data_list ''', ''' tbl_name ''', ' num2str4sql(nsin_list) ',null);'];
     end
    
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn();
    curs = exec(conn, sqlquery);    
    ds = fetchmulti(curs);
    %% Step #3 (Close the opened connection):
    close(conn);
    
    %% Step #4 (Transform dataset to fts):
     for i = 1 : length(ds.data)
        if ~isempty(ds)  && all(all(~strcmp(ds.data{i},'No Data')))
            struct_of_fts.(data_name{i}) = dataset2fts( ds.data{i}, data_name(i) );
        else
            struct_of_fts.(data_name{i}) = fints();
        end
     end

end
