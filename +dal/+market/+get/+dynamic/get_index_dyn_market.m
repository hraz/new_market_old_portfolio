function [ res_fts ] = get_index_dyn_market( nsin_list, data_name, rdate )
%GET_INDEX_DYN_MARKET return dynamic data about required market index at given date.
%
%   [ res_fts ] = get_index_dyn_market( nsin_list, data_name, rdate ) receives nsins of indices,
%   required dynamic data name and required date, and return all known daily dynamic data untill
%   given date. 
%
%   Input:
%       nsin_list - is an NINDICESx1 numeric vector specifying
%                       the nsin numbers of needed indices. 
%
%       data_name - is a string value specifying the needed data value. Possible values are:
%                       - 'value' is a value of the index
%                       - 'value_change' is a daily change of the index value
%                       - 'yield' is an average of index components ytm
%                       - 'duration' is an average of index components duration
%                       - 'time_2_maturity' is an average of index components time to maturity
%                       - 'modified_duration' is an average of index components modified duration
%                       - 'convexity' is an average of index components convexity
%                       - 'volume' is an average of index components volume
%                       - 'turnover_monetary' is an average of index components turnover
%                       - 'capital_issued' is an average of index components issued capital
%                       - 'capital_registered' is an average of index components registerred to trading capital
%                       - 'market_capital' is an average of index components market capital
%
%       rdate - is a time value ('yyyy-mm-dd') specifying the required date.
%
%   Output:
%       res_fts - is an NOBSERVATIONSx(MINDICES+1) financial time series specifying the daily
%                       dynamic data about required indices untill given date.
%
% Yigal Ben Tal
% Copyright 2011

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(2, 3, nargin));
       
    if (nargin == 2) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    if ~isnumeric(nsin_list)
        error('dal_market_get_dynamic:da_market_indx_dyn:wrongInputType', ...
            'Wrong type of nsin_list.');
    end
    if ~isvector(nsin_list)
        error('dal_market_get_dynamic:da_market_indx_dyn:wrongInputDim', ...
            'Wrong dimentions of nsin_list.');
    end
    possible_prop =  { 'value','value_change', 'yield', 'duration' , 'time_2_maturity','modified_duration', 'convexity', 'volume', 'turnover_monetary', 'capital_issued' ,'capital_registered', 'market_capital'};
    if ~all(ismember(data_name,possible_prop))
        error(' Incorrect property name');
     end 
    
     %% Step #1 (SQL query for this filter):
    sqlquery = ['CALL get_market_indx_dyn(''' rdate ''', ''' data_name ''', ' num2str4sql(nsin_list) ');'];
    
    %% Step #2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','dataset');
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);

    %% Step #3 (Close the opened connection):
    close(conn);
    
    %% Step #4 (Rebuild data to fts):
    if ~isempty(ds)
        [ res_fts ] = dataset2fts( ds, data_name );
    else
        res_fts = fints();
    end

end

