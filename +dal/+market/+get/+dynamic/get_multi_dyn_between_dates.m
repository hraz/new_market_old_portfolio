function [ struct_of_fts ] = get_multi_dyn_between_dates( nsin_list, data_name, tbl_name, start_date, end_date )
%GET_MULTI_DYN_BETWEEN_DATES return dynamic multiple data about required input at given date.
%
%   [ struct_of_fts ] = get_multi_dyn_between_dates( nsin_list, data_name, tbl_name, start_date, end_date ) receives  nsin, multiple interested data
%   name and required date interval, and return all daily dynamic  multiple data about required input untill given 
%   date. 
%
%   Input:
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsin numbers of needed input. 
%       
%       data_name - is an 1xNPROPS cellarray each value specifying the needed property. 
%                   Possible values for bonds are:
%                       - 'price_dirty' is a bond dirty price
%                       - 'price_clean' is a bond clean price without accured interest
%                       - 'acc_interest' is the accrued  interest embodied in the dirty_price
%                       - 'price_change' is a daily bond price change
%                       - 'price_change_clean' is a clean price daily change
%                       - 'yield' is yield to maturity bruto
%                       - 'duration' is a bond Macaulay duration
%                       - 'time_2_maturity' is a time to maturity
%                       - 'modified_duration' is a modified duration
%                       - 'convexity' is a convexity
%                       - 'volume' is a volume
%                       - 'turnover_monetary' is a turnover
%                       - 'market_capital' is a market capital
%                       - 'transaction_number' is a number of daily transactions
%                       - 'coupon_pmt' is a discounted near coupon payment if required date is
%                          between ex-day and payment-day and we were holders of the asset before
%                          ex-day
%                 Possible values for index are:
%                       - 'value' is a value of the index
%                       - 'value_change' is a daily change of the index value
%                       - 'yield' is an average of index components ytm
%                       - 'duration' is an average of index components duration
%                       - 'time_2_maturity' is an average of index components time to maturity
%                       - 'modified_duration' is an average of index components modified duration
%                       - 'convexity' is an average of index components convexity
%                       - 'volume' is an average of index components volume
%                       - 'turnover' is an average of index components turnover
%                       - 'capital_issued' is an average of index components issued capital
%                       - 'capital_registered' is an average of index components registerred to trading capital
%                       - 'market_capital' is an average of index components market capital
%                   Possible values for linkage index are:
%                           - 'index_value' is  a value of the linkage index
%       
%       tbl_name -  is a string value specifying the needed table. Possible values are:
%                           - bond - which checks the bonds history
%                           - index - which checks indices history
%                           - linkage - which checks linkage indices history
% 
%       start_date - is a time value ('yyyy-mm-dd') specifying the required start date.
% 
%       end_date - is a time value ('yyyy-mm-dd') specifying the required end date.
% 
%       obs_count - is a int value specifying the count of required
%                            observation, null if all observation are needed.
%
%   Output:
%       struct_of_fts - is a struct containing (NOBSERVATIONSx(MASSETS+1)) financial time series specifying the daily
%                   dynamic data untill given date, where the names of field in the struct is the same as
%                   the input.
%
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import dal.market.get.dynamic.*;

    %% Input validation:
    error(nargchk(5,5,nargin)); 
    
%     if isempty(start_date)
%         start_date = '2005-01-02';
%     elseif isnumeric(start_date)
%         start_date = datestr(start_date, 'yyyy-mm-dd');
%     end
%     
%     if isempty(end_date)
%         end_date = datestr(today, 'yyyy-mm-dd');
%     elseif isnumeric(end_date)
%         end_date = datestr(end_date, 'yyyy-mm-dd');
%     end
%     
%     if datenum(start_date) > datenum(end_date)
%         error('dal_market_get_dynamic:get_multi_dyn_between_dates:wrongDateOrder', ...
%             'Wrong order of period bounds.');
%     end
%     
%     if ~isnumeric(nsin_list)
%         error('dal_market_get_dynamic:get_multi_dyn_between_dates:wrongInputType', ...
%             'Wrong type of nsin_list.');
%     end
%    
%     if ~isvector(nsin_list)
%         error('dal_market_get_dynamic:get_multi_dyn_between_dates:wrongInputDim', ...
%             'Wrong dimentions of nsin_list.');
%     end
%     
% %     data_list = textscan(data_name, '%s', 'delimiter', ',');
% %     data_list = data_list{:};
%     
%     if(strcmp('bond',tbl_name))
%              possible_prop =  { 'price_dirty', 'price_clean','price_change_clean', 'acc_interest', 'price_change' , 'yield', 'modified_duration','duration','time_2_maturity' ,'convexity' , 'volume' , 'turnover_monetary','market_capital', 'transaction_number','coupon_pmt'};
%              if ~all(ismember(data_name,possible_prop))
%                     error(' Incorrect property name');
%             end; 
%     elseif(strcmp(tbl_name,'index'))
%         possible_prop =  { 'value_close','value_change', 'yield', 'duration' , 'time_2_maturity','modified_duration', 'convexity', 'volume', 'turnover_monetary', 'capital_issued' ,'capital_registered', 'market_capital'};
%          if ~all(ismember(data_name,possible_prop))
%             error(' Incorrect property name');
%         end; 
%     elseif (strcmp(tbl_name,'linkage'))
%         possible_prop =  {'index_value'};
%          if ~all(ismember(data_name, possible_prop))
%             error(' Incorrect property name');
%          end;
%     else
%         error(' Incorrect table name');
%     end;
    
    struct_of_fts = get_dynamic_data(nsin_list, data_name, tbl_name, start_date, end_date); 

end
