clear; clc;

%% Import external packages:
import dal.market.get.dynamic.*;

%% Definition the input parameters:
first_date = 732687; %datenum('2006-01-02');
last_date = first_date + 4*365; % datenum('2009-01-16');
compounding_interest_rate_lower_bound = -0.2;
compounding_interest_rate_upper_bound = 0.8;
nsin = sort([
    1092600
     1940360
     4570024
     7410111
     7410129
     ]);
 %nsin = sort([1081223;1081546;1082197;1087915;1091164;1091388;1092220;1092600;1093186;1094051;1094101;1095058;1095066;1210087;1260165;1260272;1260306;1980119;2260040;2260073;2260081;2260099;2260115;2260131;3230067;3900099;3900149;4110060;4160057;5730064;6000020;6080170;6080188;6120059;6390140;6390157;6620207;6910079;7190051;7230139;7230279;7360043;7480015;7480023;7770142;7980055;7980097;7980121;7980139]);

weight = ones(size(nsin, 1),1)/size(nsin, 1); 
count = round(weight * 1000000);
allocation = sortrows(dataset(nsin, weight, count), 1);
sim_freq = []; % six times per each year in the given time period

%% Call the function:
tic
[ cumulative_holding_ret, holding_ret, ~, stop_point  ] = get_bond_holding_return(allocation, first_date, last_date, ...
                                                                compounding_interest_rate_lower_bound, ...
                                                                compounding_interest_rate_upper_bound, sim_freq); 
toc

%% Porfolio sample (equal weighted):
wt = 1/(size(cumulative_holding_ret,2)-1);
dates = cumulative_holding_ret(:,1,end);
cumulative_portfolio_ret = [dates squeeze(nansum(cumulative_holding_ret(:,2:end,:) .* wt(ones(size(cumulative_holding_ret,1), size(cumulative_holding_ret,2)-1, size(cumulative_holding_ret,3))),2))];
portfolio_rets = [dates squeeze(nansum(holding_ret(:,2:end,:) .* wt(ones(size(holding_ret,1), size(holding_ret,2)-1, size(holding_ret,3))),2))];

% Full period case:
subplot(2,1,1)
plot(fints(cumulative_portfolio_ret(1:end,[1,end])));
subplot(2,1,2)
plot(fints(portfolio_rets(1:end,[1,end])));
