clear; clc;

%% Import external packages:
import dal.market.get.dynamic.*;

% %% Definition the input parameters:
% first_date = 732687; %datenum('2006-01-02');
% calc_date = first_date + 4*365; 
% last_date = first_date + ceil(4.5*365); %datenum('2009-01-16');
% compounding_interest_rate_lower_bound = -0.2;
% compounding_interest_rate_upper_bound = 0.8;
% nsin = sort([
%     1092600
%      1940360
%      4570024
%      7410111
%      7410129
%      ]);
% weight = ones(size(nsin, 1),1)/size(nsin, 1); 
% allocation = sortrows(dataset(nsin, weight), 1);
% sim_freq = [];

%% Call the function:
tic

first_date = 734432;
calc_date = 735164;
last_date = 735529;
ir_lb =-0.2000;
ir_ub =0.6000;
sim_freq = [];
benchmark_id = 601;
% rf_in_period_time(early_last_trading_date_place + 1:end,  early_last_trading_date_holder_place + 1)

load 'D:/Core/+dal/+data_access/+dynamic/+test/cumulative_income_input.mat'
[ income, rf, bench ] = get_bond_cumulative_income(allocation, first_date, calc_date, last_date, ir_lb, ir_ub, sim_freq, benchmark_id); 
toc

