function [ res_fts ] = get_bond_dyn_single_date( nsin_list, data_name, rdate )
%GET_BOND_DYN_SINGLE_DATE return dynamic data about required bonds at given date only.
%
%   [ res_fts ] = get_bond_dyn_single_date( nsin_list, data_name, rdate ) receives bond nsin, interested data
%   name and required date, and return all daily dynamic data about required bonds untill given 
%   date. 
%
%   Input:
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsin numbers of needed bonds. 
%
%       data_name - is a string value specifying the needed bond property. Possible values are:
%                        - 'price_dirty' is a bond dirty price
%                       - 'price_clean' is a bond clean price without accured interest
%                       - 'acc_interest' is the accrued  interest embodied
%                         in the dirty_price
%                       - 'price_change' is a daily bond price change
%                       - 'yield' is yield to maturity bruto
%                       - 'duration' is a bond Macaulay duration
%                       - 'time_2_maturity' is a time to maturity
%                       - 'modified_duration' is a modified duration
%                       - 'convexity' is a convexity
%                       - 'volume' is a volume
%                       - 'turnover_monetary' is a turnover
%                       - 'market_capital' is a market capital
%                       - 'transaction_number' is a number of daily transactions
%
%       rdate - is a time value ('yyyy-mm-dd') specifying the required date.
%
%   Output:
%       res_fts - is an NOBSERVATIONSx(MASSETS+1) financial time series specifying the daily
%                   dynamic data about required bonds untill given date.
%
% Yigal Ben Tal
% Copyright 2011

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import dal.market.get.dynamic.*;

    %% Input validation:
    error(nargchk(2, 3, nargin));
        
    if (nargin == 2) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
%     
%       if isempty(nsin_list)
%         error('dal_market_get_dynamic:da_bond_dyn:wrongInputDim', ...
%             'nsin_list can not be empty.');
%       end
%     
%     if ~isnumeric(nsin_list)
%         error('dal_market_get_dynamic:da_bond_dyn:wrongInputType', ...
%             'Wrong type of nsin_list.');
%     end
%     if ~isvector(nsin_list)
%         error('dal_market_get_dynamic:da_bond_dyn:wrongInputDim', ...
%             'Wrong dimentions of nsin_list.');
%     end
    
   
    res_fts = get_dynamic_data(nsin_list, {data_name}, 'bond', rdate, rdate);
    res_fts = res_fts.(data_name);

    
   
    
end

