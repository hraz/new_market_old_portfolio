function [ res_fts ] =get_bond_cf_dates( start_date, finish_date, nsin_list )
%GET_BOND_CF_DATES return ex and pay date of given assets from required period.
%
%   [ res_fts ] = get_bond_cf_dates( start_date, finish_date, nsin_list )( start_date, finish_date, nsin_list ) receives bounds of required
%   period and list of assets, and returns ex and pay date from given period.
%
%   Input:
%       start_date - is a scalar time value specifying the start of required period.
%
%       finish_date - is a scalar time value specifying the end date of required period.
%
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsins of needed assets.
%
%   Output:
%       res_fts - is an (NEXDAYSxNASSETS) fts specifying the ex-dates of each given asset for all
%                   its payments in required period. The count of observations depends from dates
%                   pay matching. 

% Yaakov Rechtman
% Copyright 2011-2012

    
    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(3,3,nargin));
    
    if isempty(start_date)
        start_date = '2005-01-01';
    elseif isnumeric(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd');
    end
    
    if isempty(finish_date)
        finish_date = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(finish_date)
        finish_date = datestr(finish_date, 'yyyy-mm-dd');
    end
    
    if datenum(start_date) > datenum(finish_date)
        error('dal_market_get_cash_flow:get_bond_cf_dates:wrongDateOrder', ...
            'Wrong order of period bounds.');
    end

    if ~isnumeric(nsin_list)
        error('dal_market_get_cash_flow:get_bond_cf_dates:wrongInputType', ...
            'Wrong type of nsin_list.');
    end
    if ~isvector(nsin_list)
        error('dal_market_get_cash_flow:get_bond_cf_dates:wrongInputDim', ...
            'Wrong dimentions of nsin_list.');
    end
    
    %     Temporary test until the end of simulations debugging
    if length(nsin_list) ~= length(unique(nsin_list))
         error('dal_market_get_dynamic:get_dynamic_data:wrongInputType', ...
            'Duplicate id in nsin_list.');
    end
    
    %% Step #1 (SQL sqlquery for this filter):
    sqlquery = ['CALL get_bond_ex_dates( ''' start_date ''', ''' finish_date ''', ' num2str4sql(nsin_list) ');'];
   
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);

    %% Step #4 (Transform dataset to fts):
    if ~isempty(ds)
        [ res_fts ] = dataset2fts( ds, 'ex_day' );
    else
        res_fts = fints();
    end 
end

