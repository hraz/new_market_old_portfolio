function [ struct_fts ] = get_nominal_yield_with_param( interest_period, start_date, end_date )
%GET_NOMINAL_YIELD_BETWEEN_DATES returns periodical yield in the given historical period.
%
%   [ yield ] = get_nominal_yield_with_param( interest_period, start_date, end_date )
% 	returns nominal periodical yield and time-to-maturity of the given yield during given time period.
% 	
% 	Input:
% 		interest_period - it is a scalar value (DOUBLE) specifying the required yield horizon in years. 
%               Minimal value is a month (1/12). 
% 				Possible values are:
% 				1,2,3,4,5,6,7,8,9,10,11 months in year representation
% 				and 1,2,3,4, 5,6, 7,8,9,10,11,12 years.
% 
% 		start_date - it is a scalar value (DATETIME) specifying the start date of the required period.
% 					
% 		end_date - it is a scalar value (DATETIME) specifying the end date of the required period.
% 					
% 	Output:
% 		res_fts - it is a NOBSx2 fts specifying the required periodical yield for during the given period. 
%                           
% 	Yigal Ben Tal
% 	Copyright 2012.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import utility.dataset.*;
    import utility.dataset.*;

    %% Input validation:
    error(nargchk(3,3,nargin));
    
     if isempty(start_date)
        start_date = '2005-01-01';
    elseif isnumeric(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd');
    end
    
    if isempty(end_date)
        end_date = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(end_date)
        end_date = datestr(end_date, 'yyyy-mm-dd');
    end
    
    if datenum(start_date) > datenum(end_date)
        error('dal_market_get_base_data:da_nominal_yield_between_dates:wrongDateOrder', ...
            'Wrong order of period bounds.');
    end
    
    possible_periods = round([(1:1:11)'/12; 1; 2; 3;4; 5;6; 7;8;9; 10;11;12] * 10000) / 10000;
    if ~isfloat(interest_period)
        error('dal_market_get_base_data:da_nominal_yield_between_dates:wrongTypeInterestPeriod', ...
            'Wrong datatype of the interest period value.');
    elseif ~any(ismember(interest_period, possible_periods))
        error('dal_market_get_base_data:da_nominal_yield_between_dates:wrongInterestPeriod', ...
            'Wrong interest period value.');
    end
    
     %% Step #1 (SQL sqlquery for this filter):
    sqlquery = ['CALL get_multi_nominal_yield_with_param( ' num2str4sqlbracket(round(10000*interest_period) / 10000) ',''' start_date ''', ''' end_date ''')'];
    
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    conn = mysql_conn();
    curs = exec(conn, sqlquery);    
    ds = fetchmulti(curs);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    if ~isempty(ds)
        if all(all(~strcmp(ds.data{1},'No Data')))
            struct_fts.('yield') = dataset2fts( ds.data{1}, 'yield' );
        end
        if all(all(~strcmp(ds.data{2},'No Data')))
            struct_fts.('id') = dataset2fts( ds.data{2}, 'id' );
        end
        if all(all(~strcmp(ds.data{3},'No Data')))
            struct_fts.('ttm') = dataset2fts( ds.data{3}, 'ttm' );
        end
         if all(all(~strcmp(ds.data{4},'No Data')))
            struct_fts.('price_dirty') = dataset2fts( ds.data{4}, 'price_dirty' );
        end
    end
    
    
        
%     %% Step #4 (Transform dataset to fts):
%      for i = 1 : length(ds.data)
%         if ~isempty(ds)  && all(all(~strcmp(ds.data{i},'No Data')))
%             struct_of_fts.(data_name{i}) = dataset2fts( ds.data{i}, data_name(i) );
%         else
%             struct_of_fts.(data_name{i}) = fints();
%         end
%      end
    
end

