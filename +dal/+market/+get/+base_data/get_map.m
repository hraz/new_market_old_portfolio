function [ ds ] = get_map(  data_name )
%GET_MAP return mappings of the input data type
%
%   [ data ] = get_map( data_type, rdate ) return all values of required data type at given date.
%
%   Input:
%       Input: 
%                 data_name - is a STRING value specifying the one of the follow possible values:
% 						~ class
% 						~ type
% 						~ coupon_type
% 						~ issuer
% 						~ linkage_index
% 						~ sector
% 						~ class_linkage_combination
% 
% 						~ maalot_rating_scale
% 						~ midroog_rating_scale
% 
%   Output:
%      [ ds ] - It is a dataset containing all mapping of the dataset.
%
%   Yaakov Rechtman BondIT
%   04/12/2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(1,1, nargin));
    
  if isempty(data_name)
        error('dal_market_get_base_Data:get_mapping_list_asset:wrongInputType', ...
            'data_name can not be empty.');
  end
    
  possible_prop =  {'class','type','coupon_type','issuer','linkage','sector','class_linkage_combination','maalot_rating_scale' ,'midroog_rating_scale'};
    if ~all(ismember(data_name,possible_prop))
        error(' Incorrect data_name');
    end;
    
    
    %% Step #1 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    conn = mysql_conn();
    sqlquery = ['call mapping_list_asset(''',data_name,''');' ];
    ds = fetch(conn,sqlquery);
    close(conn);
    
    
end
