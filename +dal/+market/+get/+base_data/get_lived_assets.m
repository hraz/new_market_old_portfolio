function [ nsin ] = get_lived_assets( rdate, nsin_list )
%GET_LIVED_ASSETS returns list of the lived assets at given date.
%
%   [ nsin ] = get_lived_assets( rdate, nsin_list ) receives required date and returns a list of lived assets at
%   the date.
%
%   Input:
%       rdate - is a scalar value specifying the required date.
%
%       nsin_list - is a list of some asserts' NSIN numbers.
%
%   Output:
%       nsin - is an NBONDSx1 vector specifying the nsins of bonds that satisfied given conditions. 
%
% Yigal Ben Tal
% Copyright 2011.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(1,2, nargin));
    
    % Date checking:
    if (nargin < 1) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    %% Step #1 (SQL query for this filter):
    %     checking if the input consists of nsin to filter
     if(nargin == 2)
        sqlquery_create = ' CREATE TEMPORARY TABLE tt_nsin_list(security_id INT PRIMARY KEY); ';
        sqlquery_insert = [' INSERT INTO tt_nsin_list(security_id) VALUES ' num2sqlbracket(nsin_list) ';' ];
     end
    sqlquery = ['CALL get_lived_assets(''', rdate , ''');'];
    
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','cellarray');
    conn = mysql_conn();
    if (nargin == 2)
        exec(conn, sqlquery_create);
        exec(conn, sqlquery_insert);
    end
    
    nsin = fetch(conn, sqlquery);
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');
    
    %% Step #3 (Return parameters initialization):
    nsin = cell2mat(nsin);

end

