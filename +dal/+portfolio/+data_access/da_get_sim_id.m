function [ sim_id ] = da_get_sim_id(min_sim_id,max_sim_id  )
%DA_GET_SIM_ID get simulation id which has not done yet
%   
%  [ sim_id ] = da_get_sim_id(min_sim_id,max_sim_id  ) - returning simulation id which has not been done yet between max and min sim_id
% 
%    Input:
%           min_sim_id - is a NUMERIC value specifying the minimum simulation id on which to get the sim id
% 
%           max_sim_id - is a NUMERIC value specifying the maximum simulation id on which to get the sim id
%     
%   Output:
%              sim_id - is an INTEGER specifying the sim_id which has not been done yet.    
%
% Yaakov Rechtman
%  10/09/2012
% Copyright 2012 BondIT

    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.mysql.data_access.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(2, 2, nargin));   
  
   
    %% Step #1 (Execution of sql function):
    setdbprefs ('DataReturnFormat','dataset');                                                     
    conn = mysql_conn('portfolio');
    sim_id = get_func(conn,'sf_get_free_sim_id',{ min_sim_id max_sim_id }, java.sql.Types.INTEGER  );
       
    %% Step #2 (Close the opened connection):
    close(conn);
    
end

