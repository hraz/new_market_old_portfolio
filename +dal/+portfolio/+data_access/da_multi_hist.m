function [ struct_fts ] = da_multi_hist( account_id, start_date, end_date, data_name )
%DA_MULTI_HIST returns required group of some histrorical portfolio characteristics given period.
% 
%   [struct_fts] = da_multi_hist( account_id, start_date, end_date, data_name )
%   receives account id, required period bounds and asked portfolio characteristic group, and
%   returns the values using struct of fts.
%
%   Input:
%       account_id - is an  number  specifying the account_id needed for
%                            input.
%       start_date - is a time value ('yyyy-mm-dd') specifying the required start date.
% 
%       end_date - is a time value ('yyyy-mm-dd') specifying the required end date.
% 
%       data_name - is a STRING value specifying the table to select from
% 
%   Output:
%     [  struct_fts ]- is a struct of the all historic characteristics according to the given group
%                           name that are represented by NOBSERVATIONSx(MASSETS+1) 
%                           fts.
%
% 26/03/2012
% Yaakov Rechtman
% Copyright 2012

%% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    
    
 %% Input validation:
    error(nargchk(4,4,nargin));

    if isempty(account_id)
         error('dal_portfolio_data_access:da_multi_hist:wrongInput', ...
        'account_id can not be empty.');
    end
     
    if isempty(start_date)
         error('dal_portfolio_data_access:da_multi_hist:wrongInput', ...
        'start_date can not be empty.');
    end
     
    if isempty(end_date)
         error('dal_portfolio_data_access:da_multi_hist:wrongInput', ...
        'end_date can not be empty.');
    end
   
    if isnumeric(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd');
    end  
    
    if isnumeric(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd');
    end    
   
    if isnumeric(end_date)
        end_date = datestr(end_date, 'yyyy-mm-dd');
    end
    
    if ~isnumeric(account_id)
         error('dal_portfolio_data_access:da_portfolio:wrongInput', ...
            'wrong input for account_id.');
    end
     possible_prop =  { 'allocation' , 'performance' , 'risk' , 'port_deltas', 'money_breakdown', 'cost'};
     if ~all(ismember(data_name,possible_prop))
                    error(' Incorrect property name');
     end   
    
    %% Step #1 (SQL Connect to the database):
    if(strcmp(data_name,'money_breakdown'))
        prop_name = {'asset_income','coupon_income','iirr','interest_paid','tbill_interest','nav'};
        sqlquery = ['CALL sp_da_multi_hist(' num2str(account_id) ',''' start_date ''',''' end_date ''',' cell2str4sqlbracket(data_name) ',' cell2str4sqlbracket(prop_name) ');'];
        data_name = prop_name;
    else
        sqlquery = ['CALL sp_da_multi_hist(' num2str(account_id) ',''' start_date ''',''' end_date ''',' cell2str4sqlbracket(data_name) ',NULL );'];
    end
   
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    curs = exec(conn, sqlquery);
    ds = fetchmulti(curs);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    %% Step #4 (Transform dataset to fts):
    if all(strcmpi('No Data', ds.Data{:}))
      struct_fts.no_data = fints();
      return
    end   
 
    for i = 1 : length(ds.data)
        if ~isempty(ds)
            struct_fts.(data_name{i}) = dataset2fts( ds.data{i}, data_name(i) );
        else
            struct_fts.(data_name{i}) = fints();
        end
    end
    
end
