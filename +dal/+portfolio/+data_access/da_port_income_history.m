function [ ds ] = da_port_income_history( account_id, start_date, end_date )
%DA_PORT_INCOME_HISTORY returns income history between dates.
% 
%   [ds] = da_port_income_history( account_id, start_date, end_date )
%   receives account id, required period bounds and asked portfolio income history
%
%   Input:
%       account_id - is an  number  specifying the account_id needed for
%                            input.
%       start_date - is a time value ('yyyy-mm-dd') specifying the required start date.
% 
%       end_date - is a time value ('yyyy-mm-dd') specifying the required end date.
% 
%   Output:
%     [  ds ]- is a dataset Nx2 containing all portfolio income history
%
% 26/03/2012
% Yaakov Rechtman
% Copyright 2012

%% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    
    
 %% Input validation:
    error(nargchk(3,3,nargin));

    if isempty(account_id)
         error('dal_portfolio_data_access:da_port_income_history:wrongInput', ...
        'account_id can not be empty.');
    end
     
    if isempty(start_date)
         error('dal_portfolio_data_access:da_port_income_history:wrongInput', ...
        'start_date can not be empty.');
    end
     
    if isempty(end_date)
         error('dal_portfolio_data_access:da_port_income_history:wrongInput', ...
        'end_date can not be empty.');
    end
   
    if isnumeric(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd');
    end  
   
    if isnumeric(end_date)
        end_date = datestr(end_date, 'yyyy-mm-dd');
    end
    
    if ~isnumeric(account_id)
         error('dal_portfolio_data_access:da_port_income_history:wrongInput', ...
            'wrong input for account_id.');
    end
    
    sqlquery = ['CALL sp_da_port_income_history(' num2str(account_id) ',''' start_date ''',''' end_date ''');'];
   
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
  
end
