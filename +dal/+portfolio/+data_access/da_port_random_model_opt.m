function [ model,opt_type ] = da_port_random_model_opt()
%DA_PORT_RANDOM_MODEL_OPT return a random pair of model and optimization type
%
%  [ model,opt_type ] = da_port_random_model_opt() returns random model and optimization type
%
%   Input:
%
%   Output:
%       model - random model.
%       optimization_type - random optimization type
%
%
% 16/08/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.mysql.data_access.*;
    import utility.dal.*;

    %% Input validation:
    error(nargchk(0, 0, nargin));

    %% Step #1 (Execution of sql function):
    setdbprefs ('DataReturnFormat','dataset');
    
    conn = mysql_conn('portfolio');
    sql_query = [ 'call sp_utility_rand_model_opt();'];
    ds = fetch(conn,sql_query);
    model = ds.model{:};
    opt_type = ds.optimization_type{:};
    %% Step #2 (Close the opened connection):
    close(conn);
    
end
