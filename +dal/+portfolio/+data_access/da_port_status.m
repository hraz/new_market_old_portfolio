function [ status ] = da_port_status( id_list, acc_or_owner )
%DA_PORT_STATUS return owners_id for each acount id OR all accoutn id for
%                                       each owners_is
%
%   [ ds ] = da_port_status( id_list, acc_or_owner) receives  id_list ,
%                        if  acc_or_owner = owner, it returns all account
%                            for the owner's id's.
%                        if  acc_or_owner = account, it returns all owners
%                            for the account id.
%   Input:
%      id_list - is an NACCOUNTx1 numeric vector specifying the id numbers of needed account.
%                    OR NOWNERSx1 numeric vector specifying the id numbers of needed owners.
%       
%   Output:
%               if  acc_or_owner = owner - status is a struct containing
%                                                           (NOWNERSx(MACCOUNTS+1))  dataset containing all account for
%                                                           all input owers.
%              if  acc_or_owner = account - status  contains (MACCOUNTS+1)
%                                                               dataset series containing all account_id from
%                                                               account_id_list and its owners.
%               
%
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(2, 2, nargin));
     
    if ~ismember(acc_or_owner, {'account','owner'})
         error('dal_portfolio_data_access:da_port_status:wrongInput', ...
            'wrong input for acc_or_owner.');
    end
    if(isempty(id_list))
          error('dal_portfolio_data_access:da_port_status:wrongInput', ...
            'id_list can not be empty');
    end
  
    %% Step #1 (SQL Connect to the database):
     if strcmp(acc_or_owner,'account')
    	sqlquery = ['CALL sp_da_port_status(' cell2str4sql(id_list) ', null );'];
     else
         sqlquery = ['CALL sp_da_port_status(null , ' cell2str4sqlbracket(id_list) ');'];
     end
     
     %% Step #2 (Execution of built SQL sqlquery):
      setdbprefs ('DataReturnFormat','dataset');
    
        conn = mysql_conn('portfolio');
        curs = exec(conn, sqlquery);
        ds = fetchmulti(curs);

     if ~isempty(ds.data)
          if strcmp(acc_or_owner,'account')
              status = cell2struct( ds.data, strcat('account_',id_list));
          else
            status = cell2struct( ds.data, strcat('owner_',id_list), 2);
          end
    else
       status = [];
    end
       
    %% Step #3 (Close the opened connection):
    close(conn);
    
    
 
end
