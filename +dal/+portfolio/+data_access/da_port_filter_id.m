function [ filter_id ] = da_port_filter_id( account_id, obs_date)
%DA_PORT_FILTER_ID a function for retreiving filter id according to account and date
%  
% input:
%        account_id - is an INTEGER specifying the account id.
%       rdate - is a time value ('yyyy-mm-dd') specifying the required date.
%
% 
% output: 
%          filter_id - is an INTEGER specifying the required filter id.
%           
% Yaakov Rechtman
%  02/082012
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.mysql.data_access.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(2, 2, nargin));   
    
    if(isempty(account_id))
          error('dal_portfolio_data_access:da_port_filter_id:wrongInput', ...
            'account_id can not be empty');
    end
     if(isempty(obs_date))
          error('dal_portfolio_data_access:da_port_filter_id:wrongInput', ...
            'obs_date can not be empty');
     elseif isnumeric (obs_date)
         obs_date = datestr(obs_date, 'yyyy-mm-dd');
    end
       
    %% Step #1 (Execution of sql function):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    filter_id = get_func(conn,'sf_get_filter_id',{ account_id, obs_date }, java.sql.Types.INTEGER  );
       
    %% Step #2 (Close the opened connection):
    close(conn);
    
end
