function [ obj ] = da_portfolio( account, up_date, manager_id, checked_holding_period, ~ )
%DA_PORTFOLIO returns required portfolio object with parameters according to the given date.
% 
%   [ obj ] = da_portfolio( account_name, up_date, manager_id, rf, calc_flag, checked_holding_period )
%
%   Input:
%       account - is a string value specifying the required account name or a scalar value specifying the account_id.
%
%       up_date - is a date value ('yyyy-mm-dd') specifying the required date.
%
%       manager_id - is a scalar spceifying the account manager inner id number.
%
%   Optional input:
%       checked_holding_period - is an optional scalar parameter specifying the required holding period in years (1 year = 365 days).
%
%   Output:
%     obj - is a dml\portfolio class object specifying some portfolio.
%
% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import dal.portfolio.data_access.*;
    import dml.*;
        
    %% Input validation:
    error(nargchk(3,5,nargin));
    
    if (isempty(account) )
        error('portfolio:da_portfolio:mismatchInput', ...
            'Account identificator is empty.');
    elseif isnumeric(account)
        account_id = account;
    elseif ischar(account)
        account_id = da_account_id(account);
        if isempty(account_id)
            error('portfolio:da_portfolio:mismatchInput', ...
                'There is no such account.');
        end   
    end
    
    if isempty(manager_id)
        error('portfolio:set:mismatchInput', ...
            'The manager_id value cann''t be an empty.');
    elseif ~isnumeric(manager_id)
        error('portfolio:set:wrongInput', ...
            'Wrong input for manager_id.');
     end
       
    %% Step #1 (Get static data about the portfolio):
    stat_ds = da_port_static(account_id);
    if isempty(stat_ds)
        error('dal_portfolio:da_portfolio:wrongInput', ...
            'Account_id is not valid.');
    end
    
    %% Step #2 (Define update date):
    if nargin == 4 && ~isempty(checked_holding_period)
        if (datenum(stat_ds.creation_date ) > 734138) && (checked_holding_period > 1) % 31.12.2009 < creation_date
            checked_holding_period = 1;
        elseif (datenum(stat_ds.creation_date ) > 733773) && (checked_holding_period > 2)  % 31.12.2008 < creation_date < 31.12.2009
            checked_holding_period = 2;
        elseif (datenum(stat_ds.creation_date ) > 733407) && (checked_holding_period > 3)  % 31.12.2007 < creation_date < 31.12.2008
            checked_holding_period = 3;
        end
        up_date = datenum(stat_ds.creation_date ) + ceil(checked_holding_period * 365);
    elseif isempty(up_date)
        up_date = datestr(today, 'yyyy-mm-dd');
    end
   
    %% Step #3 (Get portfolio last known filter id number):
    filter_id = []; %da_port_filter_id(account_id, up_date);
    
    %% Step #4 (Get last known portfolio allocation):
    alloc_ds = da_port_dynamic(account_id, up_date, 'allocation');
    if isempty(alloc_ds)
        error('portfolio:set:mismatchData', 'The allocation is empty.');
    end
    
    %% Step #5 (Get last known portfolio model definition):
    port_model_ds = da_port_model(account_id, up_date);
    
    %% Step #6  (Get performance and last_update portfolio property):
    perf_ds = da_port_dynamic(account_id, up_date, 'performance');
    
    %% Step #7 (Get portfolio cumulative holding return):
    if (datenum(stat_ds.creation_date) < datenum(perf_ds.obs_date(end)))
        port_holding_ret = da_port_income_history(account_id, datenum(stat_ds.creation_date), up_date);
    end

     %% Step #6 (Create portfolio object):
    if (datenum(stat_ds.creation_date) < datenum(perf_ds.obs_date(end)))
        [ obj ] = portfolio( stat_ds.account_id, stat_ds.account_name{:}, stat_ds.owner_id, stat_ds.manager_id, datenum(stat_ds.creation_date), ...
                                 up_date, stat_ds.initial_amount, [], filter_id, alloc_ds(:, 3:end), port_model_ds(:, 3:end), perf_ds, port_holding_ret);
    else
        [ obj ] = portfolio( stat_ds.account_id, stat_ds.account_name{:}, stat_ds.owner_id, stat_ds.manager_id, datenum(stat_ds.creation_date), ...
                                 up_date, stat_ds.initial_amount, [], filter_id, alloc_ds(:, 3:end), port_model_ds(:, 3:end), perf_ds);
    end
    
end
