function [ res ] = get_menu_lists(choosen_value, sim_from, sim_to, first_date, last_date)
%GET_MENU_LISTS returns all kinds of models,optimization type and portfolio duration
%
%    [ ds ] = da_menu_options(choosen_value, sim_from, sim_to, first_date, last_date)
%
%   Input:
%       choosen_value - is a struct, each field of which has a name of some menu header and
%                       the value must be a string of chosen menu values in the follow format:
%                         '"'VALUE1','VALUE2'"', '"1,2,4"'
%       sim_from - is a scalar value specifying (now) Hillel's simulation code (lower bound)
%
%       sim_to - is a scalar value specifying (now) Hillel's simulation code (upper bound)
%
%       first_date - is a datetime value in the follow format: "yyyy-mm-dd HH:MM:SS"
%
%       last_date - is a datetime value in the follow format: "yyyy-mm-dd HH:MM:SS"
%
%   Output:
%       res - a struct with 4 fields, each of them is an Nx1 dataset containg all possible values of follow menus:
%               models, optimization type, portfolio holding period or assets' ttm depends on given
%               input.
%
% NOTE: THIS IS A TEMPORARY FUNCTION!!!
%
% Developed byYaakov Rechtman
% Updated by Yigal Ben Tal
% Copyright 2012,  bondIT Ltd.

%% Import external packages:
import dal.mysql.connection.*;
import utility.dal.*;
import utility.dataset.*;

%% Input validation:
error(nargchk(5, 5, nargin));

%% Step #1 (Check filter names):
filter_name = fieldnames(choosen_value);
if isempty(sim_from)
    sim_from = 'NULL';
elseif isnumeric(sim_from)
    sim_from = num2str(sim_from);
end
if isempty(sim_to)
    sim_to = 'NULL';
elseif isnumeric(sim_to)
    sim_to = num2str(sim_to);
end


if isempty(first_date)
    first_date = 'NULL';
end
if isempty(last_date)
    last_date = 'NULL';
end


%% Step #2 (Build SQL query):
sqlquery = ['CALL get_menu_lists(', sim_from, ', ', sim_to, ',', first_date, ',', last_date, ','];
for i=1:length(filter_name)
    sqlquery = [sqlquery, choosen_value.(filter_name{i}), ','];
end
sqlquery = [sqlquery(1:end-1), ');'];


%% Step #3 (SQL Connect to the database):
setdbprefs ('DataReturnFormat','dataset')
conn = mysql_conn('portfolio');


%% Step #4 (Execution of built SQL sqlquery):
curs = exec(conn, sqlquery);
ds = fetchmulti(curs);


%% Step #5 (Close the opened connection):
close(conn);

%% Step #6 (Get data from cursor):
for i = 1:length(ds.data)
    if ~strcmpi(ds.data{i}, 'No Data')
        data_name = dsnames(ds.data{i});
        res.(filter_name{i}) = ds.data{i}.(data_name{1});
        if ~iscell(res.(filter_name{i})) && isnumeric(res.(filter_name{i}))
            nan_ind = isnan( res.(filter_name{i}));
            res.(filter_name{i})(nan_ind) = [];
        end
    else
        res.(filter_name{i}) = [];
    end
end

end