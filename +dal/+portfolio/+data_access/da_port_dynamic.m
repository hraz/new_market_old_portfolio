function [ ds ] = da_port_dynamic( account_id,obs_date,data_name )
%DA_PORT_DYNAMIC return dynamic data about required account at given date.
%
%   [ ds ] = da_port_dynamic( account_id,obs_date,data_name ) receives  account_id, table name
%                         and required date, and return all daily dynamic data about required input untill given 
%                           date. 
%
%   Input:
%       account_id - avalue specifyning the account id.
%       obs_date -  is a time value ('yyyy-mm-dd') value specifyning the observation date.
%       data_name - is an STRING value specifying on which table to get the
%                               data.
%                              possible values are:
%                              allocation,costs,performance,risk,money_breakdown, port_deltas,port_income_history.
%      
%   Output:
%       ds - is a DATASET containing the daily dynamic data on given date.
%              for costs - DATASET 1OBSERVATIONx6
%              for performance - DATASET 1OBSERVATIONx9
%              for risk - DATASET 1OBSERVATIONx25
%              for allocation - DATASET MASSETSx4
%              for money_breakdown - DATASET MASSETSx7.
%              for port_delta - DATASET MASSETSx5.
%              for port_income_history -  DATASET MASSETSx3.
%               
%               
%
% 23/04/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(3, 3, nargin));
        
    if isempty(account_id)
        error('dal_portfolio_dat_access:da_port_dynamic:wrongInputType', ...
            'account_id can not be empty.');
    end
    
    if isempty(obs_date)
        error('dal_portfolio_dat_access:da_port_dynamic:wrongInputType', ...
            'obs_date can not be empty.');
    elseif isnumeric(obs_date)
        obs_date = datestr(obs_date, 'yyyy-mm-dd');
    end
    
     if isempty(data_name)
        error('dal_portfolio_dat_access:da_port_dynamic:wrongInputType', ...
            'data_name can not be empty.');
     end
    
    if ~isnumeric(account_id)
        error('dal_portfolio_dat_access:da_port_dynamic:wrongInputType', ...
            'Wrong type of account_id.');
    end
    possible_prop =  { 'allocation' , 'cost' , 'performance' , 'risk' , 'money_breakdown', 'benchmark_perf','port_deltas','port_income_history'};
     if ~all(ismember(data_name,possible_prop))
                    error(' Incorrect property name');
     end   
    
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['CALL sp_da_port_dynamic(' num2str(account_id) ',  ''' obs_date ''',''' data_name ''');'];
     
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    ds = fetch(conn,sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end
