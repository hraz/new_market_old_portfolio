function [ res ] = da_menu_options(varargin)
%DA_MENU_OPTIONS returns all kinds of models,optimization type and portfolio duration
% 
%    [ ds ] = da_menu_options(varargin)
%
%   Input:
%       model_filter_name - is a string value specifying the name of the required filter.
%
%   Optional input (pair/value):
%       model - is a string value specifying the model on which optimization type  to return.
%
%       optimization type - is a string value specifying which portfolio duration to return.
%
%       port_dur - is a string value specifying wich portfolio holding return is required.
% 
%   Output:
%       res - a struct with 4 fields, each of them is an Nx1 dataset containg all possible values of follow menus:
%               models, optimization type, portfolio holding period or assets' ttm depends on given
%               input.
%    
% Developed byYaakov Rechtman
% Updated by Yigal Ben Tal
% Copyright 2012,  bondIT Ltd.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;  
    
    %% Input validation:
    error(nargchk(8, 8, nargin));
    
    %% Step #1 (Input parsing):
    p = inputParser;    % Create an instance of the class_name:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
     p.addParamValue('model', '', @(x)(ischar(x)));
     p.addParamValue('opt_type', '', @(x)(ischar(x)));
     p.addParamValue('holding_period', '', @(x)(ischar(x)));
     p.addParamValue('asset_ttm', '', @(x)(ischar(x)));
        
    % Parse method to parse and validate the inputs:
    p.StructExpand = true; % for input as a structure
    p.KeepUnmatched = true;

    % Getting results:
    p.parse(varargin{:});
    input = p.Results;

    %% Step #2 (Build SQL query):
    if isempty(input.model) | any(strcmpi(input.model, {'All', 'Model'}))
        model_value = 'NULL';
    else
        model_value = ['''', input.model, ''''];
    end
     
    if isempty(input.opt_type) | any(strcmpi(input.opt_type, {'All', 'Optimization'}))
        opt_type_value = 'NULL';
    else
        opt_type_value = ['''', input.opt_type, ''''];
    end
    
    if isempty(input.holding_period) | any(strcmpi(input.holding_period, {'All', 'Holding Horizon'}))
        portfolio_holding_period_value = 'NULL';
    else
        portfolio_holding_period_value = input.holding_period;
    end

    if isempty(input.asset_ttm) | any(strcmpi(input.asset_ttm, {'All', 'Asset ttm'}))
        asset_ttm_value = 'NULL';
    else
        asset_ttm_value = ['''', input.asset_ttm, ''''];
    end
        
    %% Step #3 (Build SQL query):
    sqlquery = ['call sp_da_menu_options(',  model_value, ', ', opt_type_value , ', ', portfolio_holding_period_value, ', ', asset_ttm_value ,');'];
    
    %% Step #3 (SQL Connect to the database):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
     
    %% Step #4 (Execution of built SQL sqlquery):
    curs = exec(conn, sqlquery);
    ds = fetchmulti(curs);
    
    %% Step #5 (Close the opened connection):
    close(conn);

    %% Step #6 (Get data from cursor):
    res.model = ds.data{1};
    res.opt_type = ds.data{2};
    res.holding_period = dataset(num2cell(ds.data{3}.req_holding_horizon), 'VarNames', {'req_holding_horizon'}); 
    res.asset_ttm = dataset(ds.data{4}.asset_ttm_range, 'VarNames', {'asset_ttm'});
    
end

