function [ ds ] = da_port_deltas( model, opt_type, portfolio_holding_period, bond_ttm, first_date, last_date )
%DA_PORT_DELTA return all deltas of account according to their model, optimization type and the holding time
%
%   [ ds ] = da_port_deltas( model,opt_type,holding_period ) 
%   receives  model, optimiztion type and holding period of account and returning the deltas.
%
%   Input:
%       model - is an STRING value specifying on which model to take the account.
%       opt_type -  is an STRING value specifying on which optimization type to take the account.
%       portfolio_holding_period - is a numeric scalar specifying the planned by user holding period.
%       asset_ttm - is numeric value specifying the weighted average of assets' ttm.
%      
%   Output:
%       ds - is a DATASET Nx3 containing all deltas of account according to model
%              and optimization type.
%               
%
% 15/08/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;

    %% Input validation:
    error(nargchk(6, 6, nargin));
        
     %Check input data;
    if isempty(model) || any(strcmpi({'Model', 'All'},model))
        model = 'NULL';
    else
        model = ['''' model ''''];
    end
    
    if isempty(opt_type) || any(strcmpi({'Optimization','All'},opt_type))
        opt_type = 'NULL';
    else
        opt_type = ['''' opt_type ''''];
    end
    
    if isempty(portfolio_holding_period) || any(strcmpi({'Holding Horizon','All'},portfolio_holding_period))
        portfolio_holding_period = 'NULL';
    elseif (iscellstr(portfolio_holding_period))
        portfolio_holding_period = portfolio_holding_period{:};
    end
    
    if isempty(bond_ttm) || any(strcmpi({'Asset TtM','All'},bond_ttm))
        bond_ttm = 'NULL';
    else
        bond_ttm = ['''' bond_ttm ''''];
    end
    
    if isempty(first_date) || strcmpi('All', first_date)
        first_date = 'NULL';
    else
        first_date = ['''' first_date ''''];
    end
    
    if isempty(last_date) || strcmpi('All', last_date)
        last_date = 'NULL';
    else
        last_date = ['''' last_date ''''];
    end
       
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['CALL sp_da_port_deltas(' model ',' opt_type ',' portfolio_holding_period ',' bond_ttm ', ' first_date ', ' last_date ');'];
     
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    ds = fetch(conn,sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end
