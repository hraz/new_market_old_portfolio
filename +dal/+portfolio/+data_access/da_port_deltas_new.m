function [ delta, portfolio_last_update ] = da_port_deltas_new( current_value, first_date, last_date, from_sim, to_sim )
%DA_PORT_DELTA return all deltas of account according to their model, optimization type and the holding time
%
%   [ delta ] = da_port_deltas( model,opt_type,holding_period ) 
%   receives  model, optimiztion type and holding period of account and returning the deltas.
%
%   Input:
%       model - is an STRING value specifying on which model to take the account.
%       opt_type -  is an STRING value specifying on which optimization type to take the account.
%       portfolio_holding_period - is a numeric scalar specifying the planned by user holding period.
%       asset_ttm - is numeric value specifying the weighted average of assets' ttm.
%      
%   Output:
%       delta - is a DATASET Nx3 containing all deltas of account according to model
%              and optimization type.
%               
%
% 15/08/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;

    %% Input validation:
    error(nargchk(5, 5, nargin));
  
     %% Step #1 (SQL sqlquery for this filter):
     value_name = fieldnames(current_value);
    
    sqlquery = ['CALL sp_da_port_deltas_1(', from_sim,', ', to_sim, ', ', first_date, ', ', last_date, ', '];
    for i=1:length(value_name)
        sqlquery = [sqlquery, current_value.(value_name{i}), ','];
    end
    sqlquery = [sqlquery(1:end-1), ');'];
          
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    delta = fetch(conn,sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    %% Step #4 (Format input):
    if isempty(delta)
        portfolio_last_update = dataset([], [], 'VarNames', {'portfolio_id', 'obs_date', 'sim_id'});
        delta = dataset([], [], [], [], 'VarNames', {'delta1', 'delta2', 'delta3', 'delta4'});
    else
        portfolio_last_update = delta(:,1:3);
        portfolio_last_update.Properties.VarNames = {'portfolio_id', 'obs_date', 'sim_id'};
        delta = delta(:,4:end);
    end
end
