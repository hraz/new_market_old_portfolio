function  [ ds ] =   da_port_list_perm_model_opt()
%DA_PORT_LIST_PERM_MODEL_OPTI returns all possible pair of model and optimization type
%
%   [ ds ] =   da_port_list_perm_model_opti() - return all permutation of model and optimization type.
%
%   Input:
%       none.
%     
%   Output:
%       ds - is a dataset Nx2 containing all possible pairs of model and optimizaiotn type   
%
% Yaakov Rechtman
%  15/08/2012
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.mysql.data_access.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(0, 0, nargin));   
 
    %% Step #1 (Execution of sql function):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    ds = fetch(conn,'call sp_utility_list_perm_model_opt()');
       
    %% Step #2 (Close the opened connection):
    close(conn);
    
end





