function [ delta, portfolio_last_update ] = get_deltas( chosenValue, firstDate, lastDate, fromSim, toSim )
%DA_PORT_DELTA return all deltas according to given filters.

% THERE IS A TEMPORARY FUNCTION !!!
%%%%%%%%%%%%%%%%%%%%%%%

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;

    %% Input validation:
    error(nargchk(5, 5, nargin));
 
    %% Step #1 (Check filter names):
    valueName = fieldnames(chosenValue);
    if isempty(fromSim)
        fromSim = 'NULL';
    elseif isnumeric(fromSim)
        fromSim = num2str(fromSim);
    end
    if isempty(toSim)
        toSim = 'NULL';
    elseif isnumeric(toSim)
        toSim = num2str(toSim);
    end
    
    if isempty(firstDate)
        firstDate = 'NULL';
    end
    if isempty(lastDate)
        lastDate = 'NULL';
    end
    
    %% Step #2 (Build SQL query):
    sqlquery = ['CALL get_deltas(', fromSim,', ', toSim, ', ', firstDate, ', ', lastDate, ', '];
    for i=1:length(valueName)
        sqlquery = [sqlquery, chosenValue.(valueName{i}), ','];
    end
    sqlquery = [sqlquery(1:end-1), ');'];
          
    %% Step #3 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    delta = fetch(conn,sqlquery);
    
    %% Step #4 (Close the opened connection):
    close(conn);
    
    %% Step #5 (Format input):
    if isempty(delta)
        portfolio_last_update = dataset([], [], 'VarNames', {'portfolio_id', 'obs_date', 'sim_id'});
        delta = dataset([], [], [], [], 'VarNames', {'delta1', 'delta2', 'delta3', 'delta4'});
    else
        portfolio_last_update = delta(:,1:3);
        portfolio_last_update.Properties.VarNames = {'portfolio_id', 'obs_date', 'sim_id'};
        delta = delta(:,4:end);
    end
end
