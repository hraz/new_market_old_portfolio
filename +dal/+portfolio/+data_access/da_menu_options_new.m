function [ res ] = da_menu_options_new(chosen_value, sim_from, sim_to, first_date, last_date)
%DA_MENU_OPTIONS_NEW returns all kinds of models,optimization type and portfolio duration
% 
%    [ res ] = da_menu_options_new(chosen_value, sim_from, sim_to, first_date, last_date)
%
%   Input:
%
% 
%   Output:
%       res - a struct with 4 fields, each of them is an Nx1 dataset containg all possible values of follow menus:
%               models, optimization type, portfolio holding period or assets' ttm depends on given
%               input.
%    
% Developed by Yaakov Rechtman
% Updated by Yigal Ben Tal
% Copyright 2012,  bondIT Ltd.
%
% Modified by Eleanor Millman, July 22, 2013 (updated comments)
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;  
    import utility.dataset.*;
    
    %% Input validation:
    error(nargchk(5, 5, nargin));
    
    %% Step #1 (Check filter names):
    filter_name = fieldnames(chosen_value);
    
    %% Step #2 (Build SQL query):
    sqlquery = ['CALL sp_da_menu_options_new(', sim_from, ', ', sim_to, ',', first_date, ',', last_date, ','];
    for i=1:length(filter_name)
        sqlquery = [sqlquery, chosen_value.(filter_name{i}), ','];
    end
    sqlquery = [sqlquery(1:end-1), ');'];
    
    %% Step #3 (SQL Connect to the database):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
     
    %% Step #4 (Execution of built SQL sqlquery):
    curs = exec(conn, sqlquery);
    ds = fetchmulti(curs);
    
    %% Step #5 (Close the opened connection):
    close(conn);

    %% Step #6 (Get data from cursor):
    for i = 1:length(ds.data)
        if ~strcmpi(ds.data{i}, 'No Data')
                data_name = dsnames(ds.data{i});
                res.(filter_name{i}) = ds.data{i}.(data_name{1});
                if ~iscell(res.(filter_name{i})) && isnumeric(res.(filter_name{i}))
                    nan_ind = isnan( res.(filter_name{i}));
                    res.(filter_name{i})(nan_ind) = [];
                end
        else
                res.(filter_name{i}) = [];
        end
    end
    
end

