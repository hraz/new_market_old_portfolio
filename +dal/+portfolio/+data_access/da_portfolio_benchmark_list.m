function [ benchmark_id ] = da_portfolio_benchmark_list( portfolio_id, first_date, last_date )
%DA_PORTFOLIO_BENCHMARK_LIST Summary of this function goes here
%   Detailed explanation goes here

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;

    %% Input validation:
    error(nargchk(3, 3, nargin));
  
     %% Step #1 (Build sql query):
     sqlquery = ['CALL sp_get_portfolio_benchmak_list(', num2str(portfolio_id),', ''', first_date, ''', ''', last_date, '''); '];
          
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    
    conn = mysql_conn('portfolio');
    data = fetch(conn,sqlquery);
    benchmark_id = data.benchmark_id;
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end

