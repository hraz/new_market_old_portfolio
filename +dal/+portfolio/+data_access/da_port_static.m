function [ ds ] = da_port_static( account_id )
%DA_PORT_STATIC  returns static data of the portfolio according to
%                                       account_id
%   [ ds ] = da_port_static( account_id ) - receives  account_id, and
%                                                                return all daily staic data according to account_id
%
%   Input:
%       account_id - is an INTEGER value  specifying the account id number.
%
%   Output:
%       ds - is a dataset 1X6 containg all static data according to
%       account_id.
%
% 22/04/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(1, 1, nargin));
   
     if isempty(account_id)
        error('dal_portfolio_data_access:da_port_static:wrongInputType', ...
            'account_id can not be empty.');
    end
    if ~isnumeric(account_id)
        error('dal_portfolio_data_access:da_port_static:wrongInputType', ...
            'Wrong type of account_id.');
    end
    
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['CALL sp_da_port_static(' num2str(account_id) ');'];

    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    
    conn = mysql_conn('portfolio');
    ds = fetch(conn,sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end