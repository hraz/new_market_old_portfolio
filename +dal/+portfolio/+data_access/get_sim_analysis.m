function [ ds ] = get_sim_analysis( err_msg )
%GET_SIM_ANALYSIS  returns solver data according to err_msg
%   [ ds ] = get_sim_analysis( err_msg )
%
%   Input:
%       err_msg - is an STRING value  specifying the error msg on whch to
%       get the data
%       if empty it returns all the table
%
%   Output:
%       ds - is a dataset nX10 containg simulations solver data
%       account_id.
%
% 20/06/2013
% Yaakov Rechtman BondIT LTD
% Copyright 2013

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    narginchk(0, 1);
    str = '';
     if ~isempty(err_msg)
        str = [ ' WHERE message = ' err_msg  ];
    end
    
    
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['SELECT * FROM sim_analysis', str ,' ;' ];

    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    
    conn = mysql_conn('portfolio');
    ds = fetch(conn,sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end