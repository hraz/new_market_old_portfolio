function [outID, outStatus, outException] = set_specific_def(inID, inGroupName, inName, inDesc)
%SET_SPECIFIC_DEF save definitions of some specific properties.
%
%   [outID, outStatus, outException] = set_specific_def(inID, inGroupName, inName, inDesc)
%
%	Input:
%       inID - is a scalar specifying the unique ID of the defined name.
%
%       inGroupName - is a string value specifying model constraint�s group name like �model�, ...
%                                       �optimization�, �reinvestment�, �tax�.
%
%       inName - is a string value specifying the new name for the some case of the property.
%
%       inDesc - is a string value specifying the new decryption for the case above of the property.
% 
%	Output:
%       outID - is a scalar specifying the unique ID of the defined name.
%
%       outStatus - is a boolean value specifying the success of the operation.
%
%       outException - is a string value specifying the error message. If operation successed 
%                                       exception will be an empty string.
%
%	Sample:
%       [outID, resStatus, errMessage] = set_specific_def([], �model�, �SingleIndex�, �Optimization model.�)
%	Result set: outID = 1, resStatus = 1, errMessage = ��;

% Created by Yigal Ben Tal.
% Date:	02.05.2013	
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
      
        error(nargchk(3, 4, nargin));
        if (isempty(inID) | isnan(inID))
            inID = 'NULL';
        elseif ~isnumeric(inID)
            error('dal_portfolio_set:set_specific_def:wrongInput', 'inID must be a numeric value.');
        else
            inID = num2str(inID);
        end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL set_specific_def(', inID, ', ''', inGroupName, ''', ''', inName, ''', ''', inDesc,''' );'];

         %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','numeric');
        conn = mysql_conn('portfolio');
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery): 
        outID = fetch(conn,sqlquery);
        if (nargout > 0) 
            outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Given definition is saved.'];
            end
        end
        
        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end
        setdbprefs ('DataReturnFormat','dataset');

    catch ME
    
        if (nargout > 0)
            outID = inID;
            if (nargout > 1)
                outStatus = false;
                if (nargout > 2)
                    outException = [outException, ME.message];
                end
            end
        end
    
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end
        setdbprefs ('DataReturnFormat','dataset');

    end

end
