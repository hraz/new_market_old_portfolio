function [outPortfolioID, outStatus, outException] = set_summary(inPortfolioID, inName, inDescription, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag) 
%SET_SUMMARY initializes and updates common properties of the portfolio.
% 
%	[outPortfolioID, outStatus, outException] = set_summary(inPortfolioID, 
%                                                                                          inName, 
%                                                                                          inDescription,
%                                                                                          inCreationDate,
%                                                                                          inMaturityDate,
%                                                                                          inPublicFlag,
%                                                                                          inVirtualFlag,
%                                                                                          inBasedOnFlag) 
% 
% 	Input:
%       inPortfolioID - is a scalar value specifying the given portfolio ID number. In case of the
%                               portfolio creation the first input parameter must be an empty array ([]), in other case it
%                               must have some valid portfolio ID number.   
%   Variable input:
%       inName - is a string value specifying the given portfolio name that may contain until 255
%                               symbols. It cannot be an empty! 
%
%       inDescription - is a string value specifying some portfolio description limited up to 255
%                               symbols. It may be an empty string (��)! 
%
%       inCreationDate - is a scalar value specifying given portfolio creation date. This value
%                               cannot be an empty ([]). 
%
%       inMaturityDate - is a scalar value specifying given portfolio maturity date. This value
%                               cannot be an empty ([]). 
%
%       inPublicFlag - is a boolean value specifying if given portfolio is public, default value is
%                               0 (false).  
%
%       inVirtualFlag - is a boolean value specifying if given portfolio is virtual, default value
%                               is 0 (false). 
%
%       inBasedOnFlag - is a scalar value specifying the some portfolio sample ID that  the current
%                               portfolio based on. More detailed definition must be done in the near future. 
%
% 	Output:
%       outPortfolioID - is a scalar value specifying used portfolio ID number.
%
%       outStatus - is a boolean value specifying the success of the operation.
%
%       outException - is a string value specifying the error message. If operation successed
%                               exception will be an empty string. 
%
% Caution: 
% 	The parameters like inName, inDescription, inMaturityDate, inPublicFlag, inVirtualFlag and
% 	inBasedOnFlag may be updated if the inputted value and stored are different. Otherwise,
% 	parameters like inPortfolioID and inCreationDate are constants and all different values that may
% 	be passed will be ignored.  Maturity date must be greater than creation date!
%
% Note:
%	Creation case:
%       �	If operation successes, the procedure returns new portfolio unique ID, success true and
%           exception notice will be an empty string. 
%       �	If it failure returns NULL value as a portfolio ID, false as a status value and the
%           relevant exception notice. 
%	Update case:
%       �	If operation successes, the procedure returns inputted portfolio ID, true success value
%           and an empty exception message. 
%       �	If it failure it returns the inputted portfolio ID, false value of success and the
%           relevant exception notification. 
% Sample:
%	�	PORTFOLIO CREATION CASE:
%       [newPortfolioID, resStatus, errMessage] = set_summary ([], �Cohen�, �Middle term low risk
%                                                                                               investment
%                                                                                               portfolio.�,
%                                                                                               756132,
%                                                                                               756543,
%                                                                                               0,
%                                                                                               1,
%                                                                                               3);     
%
% Result set: 
%                   newPortfolioID = 1, resStatus = 1, errMessage = ��.
%	�	PORTFOLIO UPDATE CASE (BOLD TEXT INDICATES CHANGES/UPDATES):
%       [newPortfolioID, resStatus, errMessage] = set_summary (1, �Israeli�, ...
%                                                                                               �Long term low risk investment portfolio.�,
%                                                                                               756132,
%                                                                                               757543,
%                                                                                               1,
%                                                                                               0,
%                                                                                               2);       
% Result set: 
%                   newPortfolioID = 1, resStatus = 1, errMessage = ��.
% But, if with updated fields will be passed wrong creation date result will be else: 
%                   newPortfolioID = 1, resStatus = 0, errMessage = �Creation date cannot be changed.�;  

% Created by Yigal Ben Tal.
% Date:		02.05.2013.
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    %% Input validation:
        if (nargout > 1)
        outException = '';
    end
    try
        
        error(nargchk(8, 8, nargin));
        if (isempty(inPortfolioID) || isnan(inPortfolioID))
            inPortfolioID = 'NULL';
        elseif ~isnumeric(inPortfolioID)
            error('dal_portfolio_set:set_summary:wrongInput', 'inPortfolioID must be a numeric value.');
        else
            inPortfolioID = num2str(inPortfolioID);
        end

        if (isempty(inDescription))
            inDescription = 'NULL';
        end

        if ~(isempty(inCreationDate)) && ~isnan(inCreationDate) 
            if isnumeric(inCreationDate)
                inCreationDate = datestr(inCreationDate, 'yyyy-mm-dd HH:MM:SS');
            end
        else
            inCreationDate = 'NULL';
        end

        if ~(isempty(inMaturityDate)) && ~isnan(inMaturityDate) && isnumeric(inMaturityDate)
            inMaturityDate = datestr(inMaturityDate, 'yyyy-mm-dd HH:MM:SS');
        else 
            inMaturityDate = 'NULL';
        end

        if ~isempty(inPublicFlag) && ~isnan(inPublicFlag)
            if isnumeric(inPublicFlag)
                inPublicFlag = num2str(inPublicFlag);
            end
        else
            inPublicFlag = 'NULL';
        end

         if ~isempty(inVirtualFlag) && ~isnan(inVirtualFlag)
             if isnumeric(inVirtualFlag)
                inVirtualFlag = num2str(inVirtualFlag);
             end
         else
             inVirtualFlag = 'NULL';
         end

         if ~isempty(inBasedOnFlag) && ~isnan(inBasedOnFlag) 
             if isnumeric(inBasedOnFlag)
                 inBasedOnFlag = num2str(inBasedOnFlag);
             end
         else
             inBasedOnFlag = 'NULL';
         end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL set_summary(', inPortfolioID, ', ''', inName, ''', ''' ...
                                    inDescription, ''', ''', inCreationDate, ''', ''', inMaturityDate, ''', ', ...
                                    inPublicFlag, ', ', inVirtualFlag, ', ', inBasedOnFlag, ') ;'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','numeric');
        conn = mysql_conn('portfolio');
        set(conn,'AutoCommit','off');
    
        %% Step #3(Execution of built SQL sqlquery):
        outPortfolioID = fetch(conn,sqlquery);
        if (nargout > 0) 
            outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Portfolio summary is saved.'];
            end
        end
        
        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end
        setdbprefs ('DataReturnFormat','dataset');
        
    catch ME
        
         if (nargout > 0)
            outPortfolioID = inPortfolioID;
            if (nargout > 1)
                outStatus = false;
                if (nargout > 2)
                    outException = [outException, ME.message];
                end
            end
        end
    
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end
        setdbprefs ('DataReturnFormat','dataset');

    end

end
