%% Import external packages:
import dal.portfolio.test.t12_get_summary.*;

%% Test description:
funName = 'get_summary';
testDesc = 'Negative value for some portfolio ID in the given list. -> error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID list must include just positive numbers.';
expectedResult = dataset();

%% Input definition:
 inPortfolioIDList = [-1,1];
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList );
