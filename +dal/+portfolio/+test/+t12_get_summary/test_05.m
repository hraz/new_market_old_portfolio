%% Import external packages:
import dal.portfolio.test.t12_get_summary.*;

%% Test description:
funName = 'get_summary';
testDesc = 'Right portfolio id list. -> result set:';

expectedStatus = 1;
expectedError = '';
expectedResult = [dataset({1,'portfolioID'}), ...
                            dataset({{'Updated name','Updated', '2012-01-01 12:12:12.0',  '2012-12-31 12:12:12.0'},...
                                            'name', 'description', 'creationDate' 'maturityDate'}), ...
                            dataset({{true, false}, 'publicFlag', 'virtualFlag'}), dataset({2, 'basedOn'}), ...
                            dataset({{ '2012-04-02 12:12:12.0' }, 'lastUpdate'})];

%% Input definition:
 inPortfolioIDList = [1;2];
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList );
