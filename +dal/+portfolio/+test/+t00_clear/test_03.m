%% Import tested directory:
import dal.portfolio.test.t00_clear.*;

%% Test description:
funName = 'clear';
testDesc = 'Clear all portfolio db -> expected success:';

%% Expected results:
expectedStatus = 1;
expectedError = 'Portfolio database is clean.';

%% Input definition:
portoflioID = [];

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portoflioID);
