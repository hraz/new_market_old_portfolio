%% Import tested directory:
import dal.portfolio.test.t00_clear.*;

%% Test description:
funName = 'clear';
testDesc = 'Clear existed specific portfolio -> expected success:';

%% Expected results:
expectedStatus = 1;
expectedError = 'Given portfolio was removed from database.';

%% Input definition:
portoflioID = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portoflioID);
