%% Import external packages:
import dal.portfolio.test.t08_set_income_status.*;

%% Test description:
funName = 'set_income_status';
testDesc = 'Negative value as portfolio ID. -> error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID must be a positive number.';

%% Input definition:
inID = -1;
inData = dataset({[datenum('2012-03-01 12:12:12'),111111,111], 'inDate','holdingValue', 'cashAmount'});
    
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inData);
