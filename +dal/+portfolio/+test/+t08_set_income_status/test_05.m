%% Import external packages:
import dal.portfolio.test.t08_set_income_status.*;

%% Test description:
funName = 'set_income_status';
testDesc = 'Duplicate entry for income status data. -> error message:';

expectedStatus = 0;
expectedError = 'Duplicate entry ''1-2013-05-01 11:07:32'' for key ''UK_income_status''';

%% Input definition:
inID = 1;
inData = dataset({[datenum({'2013-05-01 11:07:32';'2013-05-02 11:08:58';'2013-05-03 12:09:25'; ...
                                           '2013-05-03 12:10:32';'2013-05-07 11:09:59';'2013-05-08 18:10:31'; ...
                                           '2013-05-09 18:10:54';'2013-05-10 11:11:20';'2013-05-13 11:11:38'}), ...
                                           [300,0;310,5;305,6;300,4;312,5;310,6;312,7;313,7;315,10]], 'inDate','holdingValue', 'cashAmount'});
    
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inData);
