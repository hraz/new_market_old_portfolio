%% Import external packages:
import dal.portfolio.test.t15_get_excess_statistics.*;

%% Test description:
funName = 'get_excess_statistics';
testDesc = 'NULL first date-time and not NULL a second one. -> result set:';

expectedStatus = 1;
expectedError = '';

calcDate = {'2012-03-01 12:12:12.0'};
data = dataset({[0.25,0.21,0.15,0.43], 'delta_1','delta_2','delta_3','delta_4'});
expectedResult = [dataset(calcDate),data];

%% Input definition:
 inPortfolioIDList = 1;
 inFirstDate = []; 
 inLastDate = datenum('2012-03-15 12:12:12');

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList, inFirstDate, inLastDate );
