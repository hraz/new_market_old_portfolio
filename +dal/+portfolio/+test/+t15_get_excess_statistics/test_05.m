%% Import external packages:
import dal.portfolio.test.t15_get_excess_statistics.*;

%% Test description:
funName = 'get_excess_statistics';
testDesc = 'NULL as a first date-time and a second date-time. -> result set:';

expectedStatus = 1;
expectedError = '';

calcDate = {'2012-03-01 12:12:12.0'; '2012-04-02 12:12:12.0'};
data = dataset({[0.25,0.21,0.15,0.43;0.24,0.22,0.16,0.44], 'delta_1','delta_2','delta_3','delta_4'});
expectedResult = [dataset(calcDate),data];

%% Input definition:
 inPortfolioIDList = 1;
 inFirstDate = []; 
 inLastDate = [];

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList, inFirstDate, inLastDate );
