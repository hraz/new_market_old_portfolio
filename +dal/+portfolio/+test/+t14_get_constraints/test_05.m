%% Import external packages:
import dal.portfolio.test.t14_get_constraints.*;

%% Test description:
funName = 'get_constraints';
testDesc = 'Wrong date-time string. -> error message:';

expectedStatus = 0;
expectedError = 'Data truncation: Incorrect datetime value: ''2012-22-01 12:12:12'' for column ''in_required_date'' at row 198';
expectedResult = dataset();

%% Input definition:
 inPortfolioID = 1;
 inDate = '2012-22-01 12:12:12'; 

expectedResult.marketConstraint = struct();
expectedResult.specificConstraint = struct();
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate );
