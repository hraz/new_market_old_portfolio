function [] = test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.get.*;
    import utility.dataset.*;
    import dal.portfolio.test.*;
    
    %% Test execution:
    isIdentical = [true, true; true, true; true, true; true, true];
    
    errMsg = '';
    try
         [outMarketConstraints, outSpecificConstraints] = get_constraints(inPortfolioID, inDate);
         marketConstraintGroup = fieldnames(outMarketConstraints);
         nMarketConstraintGroups = length(marketConstraintGroup);
         
         specificConstraintGroup = fieldnames(outSpecificConstraints);
         nSpecificConstraintGroup = length(specificConstraintGroup);
         
         isIdentical = true(nMarketConstraintGroups + nSpecificConstraintGroup, 1);
         
         i = 1;
         while i  <= nMarketConstraintGroups
             isIdentical(i) = isidentic(expectedResult.marketConstraint.(marketConstraintGroup{i}), ...
                                                 outMarketConstraints.(marketConstraintGroup{i}));
             i = i+1;
         end
         
         while i <= nMarketConstraintGroups + nSpecificConstraintGroup
             isIdentical(i) = isidentic(expectedResult.specificConstraint.(specificConstraintGroup{i-nMarketConstraintGroups}), ...
                                                 outSpecificConstraints.(specificConstraintGroup{i-nMarketConstraintGroups}));
             i = i+1;
         end
         
         status = true;
         
    catch ME
        errMsg = ME.message;
        status = false;
    end
    
    %% Result analysis:
    if (status == expectedStatus) & (strcmpi(errMsg, expectedError)) & all(isIdentical)
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
