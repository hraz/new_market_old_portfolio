%% Import external packages:
import dal.portfolio.test.t16_get_constraints_map.*;

%% Test description:
funName = 'get_constraints_map';
testDesc = 'Wrong inConstraintGroup value. -> error message:';

expectedStatus = 0;
expectedError = 'You enter unrecognized constraint name.';
expectedResult = [];

%% Input definition:
inGroupName = 'tax';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName);
