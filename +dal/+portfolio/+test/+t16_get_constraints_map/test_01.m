%% Import external packages:
import dal.portfolio.test.t16_get_constraints_map.*;

%% Test description:
funName = 'get_constraints_map';
testDesc = 'NULL as inConstraintGroup value. -> error message:';

expectedStatus = 0;
expectedError = 'Required input cannot be NULL or an empty string.';
expectedResult = [];

%% Input definition:
inGroupName = [];

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName);
