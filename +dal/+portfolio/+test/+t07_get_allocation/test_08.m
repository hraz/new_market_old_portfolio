%% Import external packages:
import dal.portfolio.test.t07_get_allocation.*;

%% Test description:
funName = 'get_allocation';
testDesc = 'Not NULL first date-time and NULL as a second one. -> result set:';

expectedStatus = 1;
expectedError = '';

securityID = [1234567;1234568;1234569;2345670;2345678;2345679;3456780;3456788;3456789];
lastUpdate = {  '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0',...
                        '2013-02-13 10:38:30.0'}';
unitNum = [2;5;8;1;3;6;7;2;4];
expectedResult.allocationTbl  = dataset(lastUpdate,securityID,unitNum);

securityID = [3456780;1234569;2345670;3456788];
transactionDate = {'2013-02-08 17:37:18.0';'2013-02-09 17:37:38.0';...
                             '2013-02-09 17:50:01.0';'2013-02-13 10:38:30.0'};
unitNum = [7;8;1;2];
transactionCost = [8;9;2;4];
actionAmount = [-7;60;20;23];
expectedResult.transactionList = dataset(securityID,transactionDate,unitNum,transactionCost,actionAmount);

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = datenum('2013-02-06 12:12:12');
 inLastDate = [];

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
