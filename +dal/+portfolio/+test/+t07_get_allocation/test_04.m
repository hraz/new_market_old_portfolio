%% Import external packages:
import dal.portfolio.test.t07_get_allocation.*;

%% Test description:
funName = 'get_allocation';
testDesc = 'Wrong order of the first and second date-times. -> error message:';

expectedStatus = 0;
expectedError = 'Wrong time range!';
expectedResult = struct('allocationTbl', [], 'transactionList', []);

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = datenum('2013-12-12 18:00:00');
 inLastDate = datenum('2013-11-12 19:00:00');

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
