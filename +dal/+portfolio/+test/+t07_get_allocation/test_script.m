function [] = test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.get.*;
    import utility.dataset.*;
    import dal.portfolio.test.*;
    
    %% Test execution:
    isIdentical = [true, true];
    errMsg = '';
    try
        
         [outAllocationTbl, outTransactionList] = get_allocation(inPortfolioID, inFirstDate, inLastDate);       
         
         [isIdentical(1)] = isidentic(expectedResult.allocationTbl, outAllocationTbl);
         [isIdentical(2)] = isidentic(expectedResult.transactionList, outTransactionList);
            
         status = true;
    catch ME
        errMsg = ME.message;
        status = false;
    end
    
    %% Result analysis:
    if (status == expectedStatus) & (strcmpi(errMsg, expectedError)) & all(isIdentical)
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
