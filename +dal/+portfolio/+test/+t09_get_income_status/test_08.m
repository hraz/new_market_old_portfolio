%% Import external packages:
import dal.portfolio.test.t09_get_income_status.*;

%% Test description:
funName = 'get_income_status';
testDesc = 'Right date-times. -> result set:';

expectedStatus = 1;
expectedError = '';
date = {'2013-05-03 12:09:25.0'; '2013-05-03 12:10:32.0';...
            '2013-05-07 11:09:59.0';'2013-05-08 18:10:31.0'};
expectedResult = [dataset(date), dataset({[305,6,805;300,4,805;312,5, 805;310,6,805], ...
                            'holdingValue', 'cashAmount', 'investmentAmount'})];

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = datenum('2013-05-02 12:12:12'); 
 inLastDate = datenum('2013-05-09 12:12:12');
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
