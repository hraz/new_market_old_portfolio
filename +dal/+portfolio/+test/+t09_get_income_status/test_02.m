%% Import external packages:
import dal.portfolio.test.t09_get_income_status.*;

%% Test description:
funName = 'get_income_status';
testDesc = 'Negative value for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID must be a positive number.';
expectedResult = struct('portStatistics', [], 'perBondVaRStatistics', [], 'benchStatistics', []);

%% Input definition:
 inPortfolioID = -1;
 inFirstDate = datenum('2010-01-01 12:12:12'); 
 inLastDate = datenum('2013-03-01 12:12:12');
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
