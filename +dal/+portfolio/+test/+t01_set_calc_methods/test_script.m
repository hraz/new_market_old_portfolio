function [] = test_script( funName, testDesc, expectedStatus, expectedError, inID, inStatisticName, inMethod, inDesc )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.set.*;
    import dal.portfolio.test.*;
    
    %% Define globals;
    global calcMethodID
    
     %% Test execution:
    [calcMethodID, status, errMsg] = set_calc_methods(inID, inStatisticName, inMethod, inDesc);

    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
