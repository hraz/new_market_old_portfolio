%% Import external packages:
import dal.portfolio.test.t01_set_calc_methods.*;

%% Test description:
funName = 'set_calc_methods';
testDesc = 'Update description for existed calculation method to empty value -> expected error:';

expectedStatus = 0;
expectedError = 'Description cannot be NULL or an empty string.';

%% Input definition:
global inCalcID
inID = inCalcID;
inGroupName = {'exp_return', 'exp_volatility'};
inName = 'Test';
inDesc = '';

%% Test execution:
for i = 1: length(inGroupName)
    test_script( funName, [inGroupName{i}, ' :: ', testDesc], expectedStatus, expectedError, inID(i), inGroupName{i}, inName, inDesc);
end
