%% Import external packages:
import dal.portfolio.test.t01_set_calc_methods.*;

%% Test description:
funName = 'set_calc_methods';
testDesc = 'Add new name -> expected success:';

expectedStatus = 1;
expectedError = 'Given definition is saved.';

%% Input definition:

inID = [];
inGroupName = {'exp_return', 'exp_volatility'};
inName = 'Tst';
inDesc = 'Test desc.';

%% Test execution:
for i = 1: length(inGroupName)
    test_script( funName, [inGroupName{i}, ' :: ', testDesc], expectedStatus, expectedError, inID, inGroupName{i}, inName, inDesc);
end
