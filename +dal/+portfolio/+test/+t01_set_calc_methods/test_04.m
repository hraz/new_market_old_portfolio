%% Import external packages:
import dal.portfolio.test.t01_set_calc_methods.*;

%% Test description:
funName = 'set_calc_methods';
testDesc = 'Update existed properties -> expected success:';

expectedStatus = 1;
expectedError = 'Given definition is saved.';

%% Input definition:
global calcMethodID
global inCalcID
inCalcID = NaN(2,1);
inGroupName = {'exp_return', 'exp_volatility'};
inName = 'Test';
inDesc = 'Test description.';

%% Test execution:
for i = 1: length(inGroupName)
    test_script( funName, [inGroupName{i}, ' :: ', testDesc], expectedStatus, expectedError, inCalcID(i), inGroupName{i}, inName, inDesc);
    inCalcID(i) = calcMethodID;
end
