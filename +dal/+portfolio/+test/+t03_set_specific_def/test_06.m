%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Negative ID -> expected error:';

expectedStatus = 0;
expectedError = 'Data truncation: Out of range value for column ''in_id'' at row 198';

%% Input definition:
global inSpecID
inID = -inSpecID;
inGroupName = {'model', 'optimization', 'reinvestment', 'tax'};
inName = 'Test';
inDesc =  'Test description.';

%% Test execution:
for i = 1:length(inGroupName)
    test_script( funName, testDesc, expectedStatus, expectedError, inID(i), inGroupName{i}, inName, inDesc);
end
