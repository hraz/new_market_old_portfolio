function [] = test_script( funName, testDesc, expectedStatus, expectedError, inID, inGroupName, inMethod, inDesc )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.set.*;
    import dal.portfolio.test.*;
    
    %% Define globals;
    global specificDefID
    
     %% Test execution:
     [specificDefID, status, errMsg] = set_specific_def(inID, inGroupName, inMethod, inDesc);
    
    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
