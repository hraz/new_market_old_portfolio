%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Add new name -> expected success:';

expectedStatus = 1;
expectedError = 'Given definition is saved.';

%% Input definition:

inID = [];
inGroupName = {'model', 'optimization', 'reinvestment', 'tax'};
inName = 'Tst';
inDesc =  'Test desc.';

%% Test execution:
for i = 1:length(inGroupName)
    test_script( funName, testDesc, expectedStatus, expectedError, inID, inGroupName{i}, inName, inDesc);
end
