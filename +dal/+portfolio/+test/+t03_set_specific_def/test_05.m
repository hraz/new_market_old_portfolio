%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Update non existed properties -> expected error:';

expectedStatus = 0;
expectedError = 'Not found such ID.';

%% Input definition:
global inSpecID
inID = inSpecID + 1;
inGroupName = {'model', 'optimization', 'reinvestment', 'tax'};
inName = 'Test';
inDesc =  'Test description.';

%% Test execution:
for i = 1:length(inGroupName)
    test_script( funName, testDesc, expectedStatus, expectedError, inID(i), inGroupName{i}, inName, inDesc);
end
