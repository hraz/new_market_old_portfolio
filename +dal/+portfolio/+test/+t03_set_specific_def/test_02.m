%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Wrong group name -> expected error:';

expectedStatus = 0;
expectedError = 'Undefined group name! It may be one of: model, optimization, reinvestment or tax.';

%% Input definition:
inID = [];
inGroupName = 'fee';
inName = 'Tmp';
inDesc =  'Tmp';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inGroupName, inName, inDesc);
