%% Import external packages:
import dal.portfolio.test.t11_get_statistics.*;

%% Test description:
funName = 'get_statistics';
testDesc = 'Negative value for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Data truncation: Out of range value for column ''in_portfolio_id'' at row 198';
expectedResult = dataset();

%% Input definition:
 inPortfolioID = -1;
 inFirstDate = datenum('2010-01-01 12:12:12'); 
 inLastDate = datenum('2013-03-01 12:12:12');
 inBenchmarkID = 601;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate, inBenchmarkID );
