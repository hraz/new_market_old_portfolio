%% Import external packages:
import dal.portfolio.test.t06_set_allocation.*;

%% Test description:
funName = 'set_allocation';
testDesc = 'Negative value for portfolio ID value: -> error message.';

expectedStatus = 0;
expectedError = ' Data truncation: Out of range value for column ''in_portfolio_id'' at row 198';

%% Input definition:
inID = -1;
securityID = [1234567,1534568,7534568]';
unitNum = [12,15,15]';
dates = datenum({'2012-12-12 14:25:36', '2012-12-12 14:25:39', '2012-12-12 18:29:39'});
transactionCost = [0.25, 1.20, 0.20]';
actionAmount = [345, 195,205]';

inDate =  max(dates);

inAllocationTbl = dataset(securityID, unitNum);
inTransactionData = dataset(dates, securityID, unitNum, transactionCost, actionAmount);

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inAllocationTbl, inTransactionData);
