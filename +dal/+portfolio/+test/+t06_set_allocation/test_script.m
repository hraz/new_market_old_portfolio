function [] = test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inAllocationTbl, inTransactionData)
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.set.*;
    import dal.portfolio.test.*;
       
     %% Test execution:
     [status, errMsg] = set_allocation(inID ,inDate, inAllocationTbl, inTransactionData);
         
    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);


end
