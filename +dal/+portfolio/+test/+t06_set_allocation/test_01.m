%% Import external packages:
import dal.portfolio.test.t06_set_allocation.*;

%% Test description:
funName = 'set_allocation';
testDesc = 'NULL as portfolio ID value: -> error message.';

expectedStatus = 0;
expectedError = ' Portfolio ID cannot be NULL.';

%% Input definition:
inID = [];
inDate =  datenum('2012-03-01 12:12:12');
inAllocationTbl = dataset();
inTransactionData = dataset();

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inAllocationTbl, inTransactionData);
