%% Import external packages:
import dal.portfolio.test.t13_set_constraints.*;

%% Test description:
funName = 'set_constraints';
testDesc = 'Wrong number of input arguments - absent static. -> error message:';

expectedStatus = 0;
expectedError = 'inMarketConstraints must have strictly 4 constraint groups.';

%% Input definition:
inID = 1;
inDate =  datenum('2012-03-09 12:12:12');
inMarketConstraints = struct(...
                                            'dynamicConstraint', dataset({[6,0.1,1.0;9,0.2,0.98], 'constraintID', 'minValue', 'maxValue'}), ...
                                            'sectorConstraint', dataset({[601,0.0,1.0], 'sectorID', 'minValue', 'maxValue'}), ...
                                            'ratingConstraint', [dataset({[1,20,1,20], 'maalotMinID', 'maalotMaxID', 'midroogMinID', 'midroogMaxID'}), dataset({{'AND'}, 'logicOperand'})]);
inSpecificConstraints = struct('basicConstraint', dataset({[1,0.05;2,0.10], 'constraintID', 'value'}), ...
                                             'targetConstraint', dataset({[3,0.1;4,0.12;5,0.20], 'constraintID', 'value'}), ...
                                             'modelConstraint', dataset({[1,1,1], 'modelID', 'optimizationTypeID', 'reinvestmentStrategyID'}), ...
                                             'taxConstraint', dataset({[1,0.2], 'taxID','feeValue'}));

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inMarketConstraints, inSpecificConstraints);
