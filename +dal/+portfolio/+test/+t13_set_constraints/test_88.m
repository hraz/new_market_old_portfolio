%% Import external packages:
import dal.portfolio.test.t13_set_constraints.*;

%% Test description:
funName = 'set_constraints';
testDesc = 'Duplicate input for rating constraint. -> error message:';

expectedStatus = 0;
expectedError = 'Duplicate entry ''1-2012-03-01 12:12:12'' for key ''UK_rating_constraints''';

%% Input definition:
inID = 1;
inDate =  datenum('2012-03-01 12:12:12');
inMarketConstraints = struct('staticConstraint', dataset({[13,1,0.1,1.0;14,1,0.2,0.99], 'constraintID', 'constraintValueID', 'minValue', 'maxValue'}), ...
                                            'dynamicConstraint', dataset({[8,0.1,1.0], 'constraintID', 'minValue', 'maxValue'}), ...
                                            'sectorConstraint', dataset({[602,0.0,1.0], 'sectorID', 'minValue', 'maxValue'}), ...
                                            'ratingConstraint', [dataset({[1,20,1,20], 'maalotMinID', 'maalotMaxID', 'midroogMinID', 'midroogMaxID'}), dataset({{'AND'}, 'logicOperand'})]);
inSpecificConstraints = struct('basicConstraint', dataset({[1,0.05;2,0.10], 'constraintID', 'value'}), ...
                                             'targetConstraint', dataset(), ...
                                             'modelConstraint', dataset(), ...
                                             'taxConstraint', dataset());
                                         
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inMarketConstraints, inSpecificConstraints);
