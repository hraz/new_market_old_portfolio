%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'New portfolio creation: wrong positive virtual flag -> expected error:';

expectedStatus = 0;
expectedError = 'Virtual flag may get just boolean values - 0 or 1!';

%% Input definition:
inID = [];
inName = 'New name';
inDesc =  'New';
inCreationDate = datenum('2012-01-01 12:12:12');
inMaturityDate = inCreationDate + 365;
inPublicFlag = 0;
inVirtualFlag = 2;
inBasedOnFlag = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
