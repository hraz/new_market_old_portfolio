function [] = test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDescription, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag)
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.set.*;
    import dal.portfolio.test.*;
    
    %% Define globals;
    global portfolioID
    
     %% Test execution:
     [portfolioID, status, errMsg]  = set_summary(inID, inName, inDescription, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
         
    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
