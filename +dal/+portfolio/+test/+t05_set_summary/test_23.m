%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'Update properties of existed portfolio: update of  based_on value -> expected success:';

expectedStatus = 1;
expectedError = 'Portfolio summary is saved.';

%% Input definition:
global CREATION;

inID = 1;
inName = 'Updated name';
inDesc =  'Updated';
inCreationDate = CREATION;
inMaturityDate = inCreationDate + 365;
inPublicFlag = 1;
inVirtualFlag = 0;
inBasedOnFlag = 2;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
