%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'New portfolio creation: empty portfolio name -> expected error:';

expectedStatus = 0;
expectedError = 'The name cannot be an empty or NULL!';

%% Input definition:
inID = [];
inName = '';
inDesc =  'New';
inCreationDate = datenum('2012-01-01 12:12:12');
inMaturityDate = inCreationDate + 365;
inPublicFlag = 0;
inVirtualFlag = 1;
inBasedOnFlag = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
