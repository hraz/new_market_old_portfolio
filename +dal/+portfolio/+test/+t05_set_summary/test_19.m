%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'Update properties of existed portfolio: update of creation date -> expected error:';

expectedStatus = 0;
expectedError = 'Possible you tried to change creation date? It is a constant value.';

%% Input definition:
global CREATION;

inID = 1;
inName = 'Updated name';
inDesc =  'Updated';
inCreationDate = CREATION + 1;
inMaturityDate = inCreationDate + 365;
inPublicFlag = 1;
inVirtualFlag = 0;
inBasedOnFlag = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
