%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'New portfolio creation: wrong negative based_on value -> expected error:';

expectedStatus = 0;
expectedError = 'Data truncation: Out of range value for column ''in_based_on_flag'' at row 198';

%% Input definition:
inID = [];
inName = 'New name';
inDesc =  'New';
inCreationDate = datenum('2012-01-01 12:12:12');
inMaturityDate = inCreationDate + 365;
inPublicFlag = 0;
inVirtualFlag = 1;
inBasedOnFlag = -1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
