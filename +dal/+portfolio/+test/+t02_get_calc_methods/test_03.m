%% Import external packages:
import dal.portfolio.test.t02_get_calc_methods.*;

%% Test description:
funName = 'get_calc_methods';
testDesc = 'Wrong inStatisticsName value. -> error message:';

expectedStatus = 0;
expectedError = 'Wrong statistic''s name. It may one of ''exp_return'', ''exp_volatility''.';
expectedResult = [];

%% Input definition:
inGroupName = 'target';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName);
