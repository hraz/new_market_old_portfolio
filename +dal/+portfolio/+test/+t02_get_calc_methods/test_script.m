function [] = test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.get.*;
    import utility.dataset.*;
    import dal.portfolio.test.*;
    
    %% Test execution:
    isIdentical = true;
    try
         [resultSet] = get_calc_methods( inGroupName );
         status = true;
         errMsg = '';
         if ~isempty(resultSet) & ~isempty(expectedResult)
            expDataName = dsnames(expectedResult);
            givenDataName = dsnames(resultSet);
            isIdentical = all(ismember(givenDataName, expDataName));
            for i = 1 : length(expDataName)
                if iscellstr(resultSet.(givenDataName{i})) & iscellstr(expectedResult.(expDataName{i}))
                    isIdentical = isIdentical & all(strcmpi(resultSet.(givenDataName{i}),expectedResult.(expDataName{i})));
                else
                    isIdentical = isIdentical & all(resultSet.(givenDataName{i}) == expectedResult.(expDataName{i}));
                end
            end
         elseif isempty(resultSet) & isempty(expectedResult)
             isIdentical = true;
         else
             isIdentical = false;
        end
    
    catch ME
        errMsg = ME.message;
        status = false;
    end
    
    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError)) & isIdentical
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
