%% Import external packages:
import dal.portfolio.test.t02_get_calc_methods.*;

%% Test description:
funName = 'get_calc_methods';
testDesc = 'Right inGroupName value -> result set:';

id = 1:4;

expectedStatus = 1;
expectedError = '';
expectedResult  = {[dataset([1;3], 'VarNames', {'id'}),dataset({{'Tst','Test desc.';'Test','Test description.'},'methodName','desc'})];...
                              [dataset([2;4], 'VarNames', {'id'}),dataset({{'Tst','Test desc.';'Test','Test description.'},'methodName','desc'})]};

%% Input definition:
inGroupName = {'exp_return','exp_volatility'}';

%% Test execution:
for i = 1:length(inGroupName)
    test_script( funName, [inGroupName{i}, ' :: ', testDesc], expectedStatus, expectedError, expectedResult{i}, inGroupName{i});
end
