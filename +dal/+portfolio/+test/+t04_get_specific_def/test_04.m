%% Import external packages:
import dal.portfolio.test.t04_get_specific_def.*;

%% Test description:
funName = 'get_specific_def';
testDesc = 'Right inGroupName value -> result set:';

expectedStatus = 1;
expectedError = '';
expectedResult  = {[dataset(1, 'VarNames', {'id'}),dataset({{'Test','Test description.'},'name','desc'})];...
                              [dataset(1, 'VarNames', {'id'}),dataset({{'Test','Test description.'},'name','desc'})];...
                              [dataset(1, 'VarNames', {'id'}),dataset({{'Test','Test description.'},'name','desc'})];...
                              [dataset(1, 'VarNames', {'id'}),dataset({{'Test','Test description.'},'name','desc'})]};

%% Input definition:
inGroupName = {'model','optimization','reinvestment','tax'}';

%% Test execution:
for i = 1:length(inGroupName)
    test_script( funName, [inGroupName{i}, ' :: ', testDesc], expectedStatus, expectedError, expectedResult{i}, inGroupName{i});
end
