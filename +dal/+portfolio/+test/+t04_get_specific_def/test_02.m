%% Import external packages:
import dal.portfolio.test.t04_get_specific_def.*;

%% Test description:
funName = 'get_specific_def';
testDesc = 'Empty string as inGroupName value. -> error message:';

expectedStatus = 0;
expectedError = 'Required input cannot be NULL or an empty string.';
expectedResult = [];

%% Input definition:
inGroupName = '';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName);
