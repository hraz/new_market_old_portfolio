function [outIncomeStatus] = get_income_status(inPortfolioID, inFirstDate, inLastDate)
%GET_INCOME_STATUS returns history of portfolio value and free cash changes during given time period.
%
%   [outIncomeStatus] = get_income_status(inPortfolioID, inFirstDate, inLastDate)
%
%	Input:
%       inPortfolioID - is a scalar value specifying the given portfolio ID number. 
%
%       inFirstDate - is a scalar value specifying the first date in required time period.
%
%       inLastDate - is a scalar value specifying the last date in required time period.
%
%	Output:
%       outIncomeStatus - is a dataset specifying all changes in portfolio value status and in the cash status. This
%                           dataset have follow fields: 
%                           - date - is a column vector specifying the date of the transaction.
%                           - holdingValue - is a column vector specifying the portfolio market values per date (in
%                                                     agorot). 
%                           - cashAmount - is a column  vector specifying the rest of free cash amount per  date (in
%                                                     agorot). 
% 
% Sample:
%	[outIncomeStatus] = get_income_status(1, 756321, 756843);
% Result set:
%       date | holdingValue | cashAmount
%       756326 | 5065056 | 1100022
%       756326 | 5100689 | 5058
%       756390 | 6500050 | 50622
%       756500 | 6346657 | 33330

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    %% Input validation:
    error(nargchk(3,3,nargin));
    
    if isempty(inPortfolioID)
        error('dal_portfolio_get:get_income_status:mismatchInput', 'Portfolio ID cannot be an empty.');
    elseif ~isnumeric(inPortfolioID)
        error('dal_portfolio_get:get_income_status:mismatchInput', 'Portfolio ID must be numeric.');
    elseif (inPortfolioID < 0)
        error('dal_portfolio_get:get_income_status:mismatchInput', 'Portfolio ID must be a positive number.');
    else
        inPortfolioID = num2str(inPortfolioID);
    end
    
    if isempty(inFirstDate)
        inFirstDate = 'NULL';
    elseif isnumeric(inFirstDate)
        inFirstDate = ['"' datestr(inFirstDate, 'yyyy-mm-dd HH:MM:SS') '"'];
    end
    
    if isempty(inLastDate)
        inLastDate = 'NULL';
    elseif isnumeric(inLastDate)
        inLastDate = ['"' datestr(inLastDate, 'yyyy-mm-dd HH:MM:SS') '"'];
    end
    
    %% Create SQL query:
    sqlquery = ['CALL get_income_status(', inPortfolioID, ',', inFirstDate, ',', inLastDate ');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try
        outIncomeStatus = fetch(conn, sqlquery);
        if ~isempty(outIncomeStatus)
            outIncomeStatus = set(outIncomeStatus, 'VarNames', {'date', 'holdingValue', 'cashAmount', 'investmentAmount'});
        else
            outIncomeStatus = dataset({cell(1,4), 'date', 'holdingValue', 'cashAmount', 'investmentAmount'});
        end
    catch ME
        error('dal_portfolio_get:get_income_status:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);

end

