function [outDefinitionList] = get_specific_def(inGroupName)
%GET_SPECIFIC_DEF returns a list of specific definitions by given group name.
%
%   [outDefinitionList] = get_specific_def(inGroupName)
%   
%   Input:
%       inGroupName - is a string value specifying some model constraint�s parameter name 
%                                       like �model�, �optimization�, �reinvestment�, �tax�.
%
%	Output:
%       outDefinitionList - is a result dataset that contains follow fields:
%               id - is a column vector specifying the property ID number.
%               name - is a column vector of strings describes the property name.
%               desc - is a column vector of strings specifying the some property description.
% 
%	Sample:	
%           [definitionList] = get_specific_def(�optimization�)
%	Result set:
%               id | name | desc
%               1 | Optimal Sharpe.
%               2 | Target Return.
%               3 | Target Risk.	

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(1,1,nargin));
    
    if ischar(inGroupName)
        groupName = ['''' inGroupName ''''];
    elseif ~isempty(inGroupName) 
        error('dal_portfolio_get:get_specific_def:wrongInput', 'inGroupName must be a string.');
    else
        groupName = 'NULL';
    end
    
    %% Create SQL query:
    sqlquery = ['CALL get_specific_def(' groupName ');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try
        outDefinitionList = fetch(conn, sqlquery);
        if ~isempty(outDefinitionList)
            outDefinitionList = set(outDefinitionList, 'VarNames', {'id','name','desc'});
        else
            outDefinitionList = dataset({cell(1,2), 'id','name','desc'});
        end
    catch ME
        outDefinitionList = dataset(); % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
        error('dal_portfolio_get:get_specific_def:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);

end
