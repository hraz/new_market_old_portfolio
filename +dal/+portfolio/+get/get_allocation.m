function [outAllocationTbl, outTransactionList] = get_allocation(inPortfolioID, inFirstDate, inLastDate)
%GET_ALLOCATION returns allocation table changes during some predefined period
%
% [outAllocationTbl, outTransactionList, errMsg] = get_allocation(inPortfolioID, inFirstDate, inLastDate)
% The procedure returns last saved allocation table for given portfolio ID and full list of portfolio transaction in the required period.
%
%	Input:
%       inPortfolioID - is a scalar value specifying the given portfolio ID number. It cannot be an empty! 
%
%	Output:
%       outAllocationTbl - is a dataset specifying the last known allocation table. This table has follow fields:
%               - lastUpdate - is a scalar value specifying last known update.
%               - securityID - is a scalar value specifying ID of the some allocated asset.
%               - unitNum - is a scalar value specifying the number of security units in the portfolio.
% 
%       outTransactionList - is a dataset specifying a list of transactions that were done in the required period. The
%                               dataset have follow fields: 
%               - securityID - is a column vector specifying some asset that was purchased or sold at given transaction.
%               
%               - transactionDate - is a column vector specifying the date of the transaction. 
%
%               - unitNum - is a column vector specifying the number of asset units changed during given transaction
%                               (�+� means buy operation, �-�means sale operation). 
%
%               - transactionCost - is a column vector specifying the tax on the given action in cash of the domestic
%                               currency (agorot). 
%
%               - actionAmount - is a column vector specifying the gross (before tax)money amount (in agorot) that was
%                               used for given operation (�+� means buy operation, �-�means sale operation). 
%
%       errMsg - is a string value specifying the error message.
% 
% Sample:
%   [outAllocationTbl, outTransactionList] = get_allocation(1, 756321, 756843);
% Result set is:
%   �	outAllocationTbl:
%           lastUpdate | securityID | unitNum
%               756850 | 1092618 | 11
%               756850 | 1093319 | 8
%               756850 | 1940345 | 22
%               756850 | 2600195 | -3
%               756850 | 2860088 | 6
%               756850 | 4110060 | 10
%               756850 | 4570024 | 8
%               756850 | 6040075 | 2
%               756850 | 7240054 | 5
%               756850 | 7410103 | 4
%	�	outTransactionList:
%           securityID | transactionDate | unitNum | transactionCost | actionAmount
%               2600195 | 756326 | 11 | 26001 | 110002
%               2860088 | 756326 | -8 | 1400 | -80020
%               4110060 | 756390 | -22 | 41324 | -222222
%               4570024 | 756500 | 3 | 4616 | 33330

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
        
    %% Input validation:
    error(nargchk(3,3,nargin));

    if isempty(inPortfolioID)
        error('dal_portfolio_get:get_allocation:mismatchInput', 'Portfolio ID cannot be an empty.');
    elseif isnumeric(inPortfolioID) & inPortfolioID < 0
        error('dal_portfolio_get:get_allocation:mismatchInput', 'Portfolio ID must be positive value.');
    else
        inPortfolioID = num2str(inPortfolioID);
    end

    if isempty(inFirstDate)
        strFirstDate = 'NULL';
    elseif isnumeric(inFirstDate)
        strFirstDate = ['''' datestr(inFirstDate, 'yyyy-mm-dd HH:MM:SS') ''''];
    end

    if isempty(inLastDate)
        strLastDate = 'NULL';
    elseif isnumeric(inLastDate)
        strLastDate = ['''' datestr(inLastDate, 'yyyy-mm-dd HH:MM:SS') ''''];
    end
    
    if inLastDate < inFirstDate
        error('dal_portfolio_get:get_allocation:mismatchInput', 'Wrong time range!');
    end

    %% Create SQL query:
    try

    allocationTblSqlQuery = ['CALL get_allocation(', inPortfolioID, ');'];
    transactionSqlQuery = ['CALL get_transaction(', inPortfolioID, ',', strFirstDate, ',', strLastDate ');'];

    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');

    %% Get required data:
    outAllocationTbl = fetch(conn, allocationTblSqlQuery);
    outTransactionList = fetch(conn, transactionSqlQuery);

    if ~isempty(outAllocationTbl)
        outAllocationTbl = set(outAllocationTbl, 'VarNames', {'lastUpdate', 'securityID', 'unitNum'});
    else
        outAllocationTbl = dataset({cell(1,3), 'lastUpdate', 'securityID', 'unitNum'});
    end
    
    if ~isempty(outTransactionList)
        outTransactionList = set(outTransactionList, 'VarNames', {'securityID', 'transactionDate', 'unitNum', 'transactionCost', 'actionAmount'});
    else
        outTransactionList = dataset({cell(1,5),'securityID','transactionDate','unitNum','transactionCost','actionAmount'});
    end

   %% Close opened connection:
    if exist('conn', 'var')
        close(conn)
    end
        
    catch ME
        %% Close opened connection:
        if exist('conn', 'var')
            close(conn)
        end 
%         outAllocationTbl = dataset(); % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
%         outTransactionList = dataset(); % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
       error('dal_portfolio_get:get_allocation:mismatchInput',  ME.message);
              
    end
    
end

