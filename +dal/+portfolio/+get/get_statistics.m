function [outPortStatistic, outPerBondVaRStatistics, outBenchStatistic] = get_statistics(inPortfolioID, inFirstDate, inLastDate, inBenchmarkID)
%GET_STATISTICS returns the history of portfolio risk/performance parameters during some time period.
%
%   [outPortStatistic, outPerBondVaRStatistics, outBenchStatistic] = get_statistics(inPortfolioID, inFirstDate, inLastDate, inBenchmarkID)
%
%	Input:
%        inPortfolioID - is a scalar value specifying the given portfolio ID number. 
%
%        inFirstDate - is a scalar value specifying the first date in required time period.
%
%        inLastDate - is a scalar value specifying the last date in required time period.
%
%       inBenchmarkID - is a scalar value specifying the some market index ID.
%
% Output:
%        outPortStatistic - is a dataset specifying the portfolio risk and performance that includes follow fields:
%            - calcDate - is a DATETIME value specifying the date of the of statistics measuring.
%
%            - expectedReturn - is a DOUBLE value specifying portfolio expected return.
%
%            - expectedVolatility - is an UNSIGNED DOUBLE  value specifying portfolio expected volatility.
%
%            - expReturnCalcMethod - is a scalar value specifying the expected return calculation method ID. The
%                                       description of all possible calculation methods may be gotten using
%                                       get_calc_methods('exp_return') procedure.  
%
%            - expVolatilityCalcMethod - is a scalar value specifying the expected volatility calculation method ID. The
%                                       description of all possible calculation methods may be gotten using
%                                       get_calc_methods(�exp_volatility�) procedure. 
%
%            - valueAtRisk - is a scalar value represented Value-at-Risk of the given portfolio.
%
%            - cVaR - is a scalar value represented Conditional Value-at-Risk of the given portfolio.
%
%            - holdingReturn - is a scalar value specifying the percent of the holding income vs. investment amount at
%                                       the given time point. This value must take to account all investment amount that
%                                       were done from portfolio creation date until given date and current market
%                                       portfolio value together with free cash amount the portfolio has.   
%
%            - moneyWeightedReturn - is a scalar value specifying the money weighted portfolio return. 
%
%            - timeWeightedReturn - is a scalar value specifying the time weighted portfolio return.
%
%            - volatility - is a scalar  value specifying the portfolio volatility of holding return.
%
%            - downsideVolatility - is a scalar  value specifying the downside part of the portfolio holding return volatility.
%
%            - skewness - is a scalar value specifying the third moment of the portfolio holding return at given date.
%
%            - kurtosis - is a scalar value specifying the forth moment of the portfolio holding return at given date.
%
%            - alpha - is a scalar value specifying the portfolio alpha vs. current risk free market rate (See CAPM model).
%
%            - beta - is a scalar value specifying the portfolio beta vs. some given benchmark (See CAPM model).
%
%            - uniqueVisk - is a scalar value specifying undiversified (specific) risk of the portfolio (See CAPM model).
%
%            - sharpe - is a scalar value represented portfolio Sharpe ratio value.
%
%            - jensen - is a scalar value represented portfolio Jensen ratio value.
%
%            - omega - is a scalar value represented portfolio Omega ratio value.
%
%            - infRatio - is a scalar value represented Information ratio value
%
%            - rsquareVsBench - is a scalar value represented R-Square ratio value of the portfolio vs. some portfolio
%                                       benchmark. 
%
%            - rmseVsBench - is a scalar  value specifying the Route-Mean-Square_Error value of the portfolio vs. some
%                                       portfolio benchmark. 
%        
%        outPerBondVaRStatistics - is a dataset specifying the contribution of each asset portfolio Value-at-Risk and
%                                       must have follow fields: 
%            - securityID - is a column vector specifying the some security ID number.
%
%            - marginalVaR - is a column vector specifying the marginal Value-at-Risk value of the given asset in the
%                                       portfolio at given time point. 
%
%            - componentVaR - is a column vector specifying the component Value-at-Risk value of the given asset in the
%                                       portfolio at given time point. 
%        
%        outBenchStatistic - is a dataset specifying benchmark performance indicators in the same with given portfolio
%                                       period, and must have follow fields: 
%            - benchmarkID - is a scalar value specifying the given benchmark ID number. 
%
%            - holdingReturn - is a scalar value specifying the benchmark�s holding return.
%
%            - volatility - is a scalar  value specifying the benchmark�s volatility of holding return.
%
%            - skewness - is a scalar value specifying the third moment of the benchmark�s holding return at a given
%                                       date. 
%
%            - kurtosis - is a scalar value specifying the forth moment of the benchmark�s holding return at a given
%                                       date. 
%
%            - sharpe - is a scalar value represented benchmark�s Sharpe ratio value.
% 
%	Note:
%       At the creation date performance indicators could not be calculated because it was not any history data. So, in
%       this case all of them will have NULL values. There are may be some partial cases when not all risk parameters
%       will be calculated, so in this case you could see NULL values in result table.  
%
% Sample:
%	[portStatistic, perBondVaRStatistics, benchStatistic] = get_statistics(1, 756321, 756843);
% Result set is:
%   �	portStatistic:
%       calcDate | expectedReturn | expectedVolatility | expReturnCalcMethod | expVolatilityCalcMethod | valueAtRisk | cVaR
%       756322 | 0.12 | 0.21 | 1 | 1 | 0.213 | 0.2464
%       756332 | 0.12 | 0.21 | 1 | 2 | 0.5462 | 0.2165
% 
%       holdingReturn | moneyWeigthedReturn | timeWeigthedReturn | volatility | downsideVolatility | skewness | kurtosis
%       0.1156 | 0.1256 | 0.1056 | 0.26 | 0.14 | 0.25 | 2.8
% 
%       alpha | beta | uniqueRisk | sharpe | jensen | omega | infRatio | rsquare_vs_bench | rmse_vs_bench
%       0.05 | 0.66 | 0.00 | -3.98 | 0.00 | 0.00 | -1.71 | 0.26 | 0.01
% 
%   �	perBondVaRStatistics:
%       securityID | calcDate | marginalVaR | componentVaR
%       1234567 | 756322 | 0.213 | 0.2464
%       2345678 | 756322 | 0.5462 | 0.2165
%       3456789 | 756342 | 0.3564 | 0.4654
% 
%   �	benchStatistic:
%       benchmarkID | calcDate | holdingReturn | volatility | skewness | kurtosis | sharpe
%       601 | 756322 | 0.0126 | 0.021 | 0.05 | 0.5 | -0.032
%       707 | 756332 | 0.0135 | 0.21 | 1 | 2 | 0.213

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import utility.dal.*;
    import dal.mysql.connection.*;
    
    %% Input validation:
    error(nargchk(4,4,nargin));
    
    if isempty(inPortfolioID)
        error('dal_portfolio_get:get_statistics:mismatchInput', 'Portfolio ID cannot be an empty.');
    else
        inPortfolioID = num2str4sql(inPortfolioID);
    end
    
    if isempty(inFirstDate)
        inFirstDate = 'NULL';
    elseif isnumeric(inFirstDate)
        inFirstDate = ['"' datestr(inFirstDate, 'yyyy-mm-dd HH:MM:SS') '"'];
    end
    
    if isempty(inLastDate)
        inLastDate = 'NULL';
    elseif isnumeric(inLastDate)
        inLastDate = ['"' datestr(inLastDate, 'yyyy-mm-dd HH:MM:SS') '"'];
    end
    
    if isempty(inBenchmarkID)
        inBenchmarkID = 'NULL';
%         error('dal_portfolio_get:get_statistics:mismatchInput', 'Benchmark ID cannot be an empty.');
    elseif isnumeric(inBenchmarkID)
        if inBenchmarkID < 0 
            error('dal_portfolio_get:get_statistics:mismatchInput', 'Benchmark ID cannot be negative.');
        else
            inBenchmarkID = num2str4sql(inBenchmarkID);
        end
    end
      
    %% Create SQL query:
    portStatisticSqlQuery = ['CALL get_statistics(', inPortfolioID, ',', inFirstDate, ',', inLastDate ');'];
    perBondVaRStatisticSqlQuery = ['CALL get_partial_var_statistics(', inPortfolioID, ',', inFirstDate, ',', inLastDate ');'];
    benchStatisticSqlQuery = ['CALL get_benchmark_statistics(', inPortfolioID, ',', inFirstDate, ',', inLastDate, ',', inBenchmarkID ');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try
        outPortStatistic = fetch(conn, portStatisticSqlQuery);
        if ~isempty(outPortStatistic)
            outPortStatistic = set(outPortStatistic, 'VarNames', {'calcDate', 'expectedReturn', 'expectedVolatility', ...
                                     'expReturnCalcMethod', 'expVolatilityCalcMethod', ...
                                     'valueAtRisk', 'cVaR',  ...
                                     'holdingReturn', 'moneyWeightedReturn', 'timeWeightedReturn', ...
                                     'volatility', 'downsideVolatility', 'skewness', 'kurtosis', ...
                                     'alpha', 'beta', 'uniqueRisk', 'sharpe', 'jensen', 'omega', 'infRatio', ...
                                     'rsquareVsBench', 'rmseVsBench'});
        else
            outPortStatistic = dataset({cell(1,23), 'calcDate', 'expectedReturn', 'expectedVolatility', ...
                                     'expReturnCalcMethod', 'expVolatilityCalcMethod', ...
                                     'valueAtRisk', 'cVaR',  ...
                                     'holdingReturn', 'moneyWeightedReturn', 'timeWeightedReturn', ...
                                     'volatility', 'downsideVolatility', 'skewness', 'kurtosis', ...
                                     'alpha', 'beta', 'uniqueRisk', 'sharpe', 'jensen', 'omega', 'infRatio', ...
                                     'rsquareVsBench', 'rmseVsBench'});
        end
        
        outPerBondVaRStatistics = fetch(conn, perBondVaRStatisticSqlQuery);
        if ~isempty(outPerBondVaRStatistics)
            outPerBondVaRStatistics = set(outPerBondVaRStatistics, 'VarNames', {'securityID', 'calcDate', 'marginalVaR', 'componentVaR'});
        else
            outPerBondVaRStatistics = dataset({cell(1,4), 'securityID', 'calcDate', 'marginalVaR', 'componentVaR'});
        end
        
        outBenchStatistic = fetch(conn, benchStatisticSqlQuery);
        if ~isempty(outBenchStatistic)
            outBenchStatistic = set(outBenchStatistic, 'VarNames', {'calcDate', 'benchmarkID', 'holdingReturn', 'volatility', 'skewness', 'kurtosis', 'sharpe'});
        else
            outBenchStatistic = dataset({cell(1,7), 'calcDate', 'benchmarkID', 'holdingReturn', 'volatility', 'skewness', 'kurtosis', 'sharpe'});
        end
    catch ME
        error('dal_portfolio_get:get_statistics:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);
    
end
