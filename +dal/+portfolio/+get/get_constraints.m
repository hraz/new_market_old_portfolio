function [outMarketConstraints, outSpecificConstraints] = get_constraints(inPortfolioID, inDate)
%GET_CONSTRAINTS returns full list of portfolio constraints at given date.
%
%   [outMarketConstraints, outSpecificConstraints] = get_constraints(inPortfolioID, inDate)
%
%   Input:
%       inPortfolioID - is a scalar value specifying some portfolio ID number.
%       inDate - is a scalar value specifying required date.
%
% | Output:
%       outMarketConstraints - is a struct of datasets that are market constraints set. This set 
%                                                               constraints includes follow more detailed constraint groups:
%           staticConstraint - is a dataset value represented the list of all static constraints. 
%                                                               The dataset has fields like: �constraintID�,�constraintValueID�, ...
%                                                               �minValue�, �maxValue�.
%           dynamicConstraint - is a dataset value represented the list of all relevant dynamic 
%                                                               constraints. The dataset has fields like: �constraintID�, �minValue�, �maxValue�.
%           sectorConstraint - is a dataset value represented the list of all relevant sector constraints. 
%                                                               The dataset has fields like:�sectorID�,�minValue�,�maxValue�.
%           ratingConstraint - is a dataset value represented the list all relevant rating constraints. 
%                                                               The dataset has fields like: 
%                                                               �maalotMinID�, �maalotMaxID�, �midroogMinID�, �midroogMaxID�, �logicOperand�.
% 
%       outSpecificConstraints - is a struct of datasets that are specific constraints set. This set 
%                                                               constraints includes follow more detailed constraint groups, like:
%           basicConstraint - is a dataset value represented the list of all relevant basic portfolio 
%                                                               constraints. The dataset has fields like: �constraintID�,�value�.
%           targetConstraint - is a dataset value represented the list of all relevant target portfolio 
%                                                               constraints. The dataset has fields like:�constraintID�,�value�.
%           modelConstraint - is a dataset value represented the list of all relevant specific portfolio 
%                                                               constraints. The dataset has fields like:...
%                                                               �modelID�, �optimizationTypeID�,�reinvestmentStrategyID�.
%           taxConstraint - is a dataset value represented the list of all relevant for this portfolio fees. 
%                                                               The dataset has fields like: �taxID�, �feeValue�.
% 
%	Sample:
%       [structMarketConstraints, structSpecificConstraints] = get_constraints(1, 723400);
%	Result set:
%       staticConstraint
%           constraintID | constraintValueID | minValue | maxValue
%               2 | 1 | 0.02 | 0.08
%               3 | 2 | 0.08 | 0.17
%               16 | 8086065 | 0.2 | 0.6
%       dynamicConstraint
%           constraintID | minValue | maxValue
%               2 | 0.2 | 0.27
%               7 | 0.5 | 0.12
%       sectorConstraint
%           sectorID | minValue | maxValue
%               2 | 0.05 | 0.1
%               3 | 0.08 | 0.15
%       ratingConstraint
%           maalotRatingMinID | maalotRatingMaxID | midroogRatingMinID | midroogRatingMaxID | logicOperand
%               18 | 23 |  | 22 | �AND�
% 
%       basicConstraint
%           constraintID | value
%               2 | 2000
%               3 | 0.12
%       targetConstraint
%           constraintID | value
%               1 | 0.2
%               2 | 0.08
%       modelConstraint
%           modelID | optimizationTypeID | reinvestmentStrategyID
%               2 | 3 | 1
%       taxConstraint
%           taxID | feeValue
%               5 | 0.25

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;
    
    %% Input validation:
    error(nargchk(2,2,nargin));
    
    if isempty(inPortfolioID)
        error('dal_portfolio_get:get_constraints:mismatchInput', 'Portfolio ID cannot be an empty.');
    elseif isnumeric(inPortfolioID) & inPortfolioID < 0
        error('dal_portfolio_get:get_constraints:mismatchInput', 'Portfolio ID must be positive value.');
    else
        inPortfolioID = num2str(inPortfolioID);
    end
    
    if isempty(inDate)
        error('dal_portfolio_get:get_constraints:mismatchInput', 'Required date cannot be an empty.');
    elseif isnumeric(inDate)
        inDate = datestr(inDate, 'yyyy-mm-dd HH:MM:SS');
    end
    
    %% Constant definition:
    marketConstraint.staticConstraint = dataset({NaN(1,4), 'constraintID', 'constraintValueID', 'minValue', 'maxValue'});
    marketConstraint.dynamicConstraint = dataset({NaN(1,3), 'constraintID', 'minValue', 'maxValue'});
    marketConstraint.sectorConstraint = dataset({NaN(1,3), 'sectorID', 'minValue', 'maxValue'});
    marketConstraint.ratingConstraint = dataset({NaN(1,5), 'maalotRatingMinID', 'maalotRatingMaxID', 'midroogRatingMinID', 'midroogRatingMaxID', 'logicOperand'});
    
    specificConstraint.basicConstraint = dataset({NaN(1,2), 'constraintID', 'value'});
    specificConstraint.targetConstraint = dataset({NaN(1,2), 'constraintID', 'value'});
    specificConstraint.modelConstraint = dataset({NaN(1,3), 'modelID', 'optimizationTypeID', 'reinvestmentStrategyID'});
    specificConstraint.taxConstraint = dataset({NaN(1,2), 'taxID', 'feeValue'});
    
    marketConstraintName = fieldnames(marketConstraint);
    specificConstraintName = fieldnames(specificConstraint);
    
    marketConstraintsNum = numel(marketConstraintName);
    specificConstraintsNum = numel(specificConstraintName);
    
    %% Create SQL query:
    sqlquery = ['CALL get_constraints(', inPortfolioID, ',''', inDate ''');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try

        dbCurs = exec(conn, sqlquery);    
        if isempty(dbCurs.Message)
            curs = fetchmulti(dbCurs);
        else
            error('dal_portfolio_get:get_constraints:errMessage', dbCurs.Message);
        end
        
        % Check if all constraints groups are gotten:
        if (length(curs.data) ~= (marketConstraintsNum + specificConstraintsNum))
            error('dal_portfolio_get:get_constraints:wrongOutput', 'Wrong number of constraint groups is given.');
        end
        
        % Collect market constraints:
        for i = 1 : marketConstraintsNum
            if ~isempty(curs)  && all(all(~strcmp(curs.data{i},'No Data')))
                outMarketConstraints.(marketConstraintName{i}) = set(curs.data{i}, 'VarNames', dsnames(marketConstraint.(marketConstraintName{i})));
            else
                outMarketConstraints.(marketConstraintName{i}) = dataset();
            end
        end
        
        % Collect specific constraints:
        for i = 1 : specificConstraintsNum
            if ~isempty(curs)  && all(all(~strcmp(curs.data{i+marketConstraintsNum},'No Data')))
                outSpecificConstraints.(specificConstraintName{i}) = set(curs.data{i+marketConstraintsNum}, 'VarNames', dsnames(specificConstraint.(specificConstraintName{i})));
            else
                outSpecificConstraints.(specificConstraintName{i}) = dataset();
            end
        end
    catch ME
        outMarketConstraints = []; % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
        outSpecificConstraints = []; % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
        error('dal_portfolio_get:get_constraints:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);
    
end
