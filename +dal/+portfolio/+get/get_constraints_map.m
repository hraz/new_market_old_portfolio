function [outConstraintList] = get_constraints_map(inConstraintGroup)
%GET_CONSTRAINTS_MAP returns constraints list per given group name.
%
%   [outConstraintList] = get_constraints_map(inConstraintGroup)
%	The procedure returns table specifying the mapping of required data 
%   possible values with its ID. 
%
%	Input: 
%       inConstraintGroup - is a string value specifying the one of the possible values. 
%                                      Possible values are: �basic�, �static�, �dynamic�, �target�. 
%                                      It cannot be an empty! 
%
% 	Output:
%       outConstraintList - is a list of possible constraints according to given 
%                                      group mapped with its own IDs:
%           - id - is a  scalar value specifying the unique ID of the constraint.
%           - dataName - is a scalar value specifying some possible constraint name.
% 
%	Note: 
%       In case of input that not included in the possible values above, it returns message 
%       �You enter unrecognized constraints' name�.

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    %% Input validation:
    error(nargchk(1,1,nargin));
    
    if isempty(inConstraintGroup)
        error('dal_portfolio_get:get_constraints_map:mismatchInput', 'Required input cannot be NULL or an empty string.');
    elseif ~ischar(inConstraintGroup)
        error('dal_portfolio_get:get_constraints_map:wrongInput', 'inConstraintGroup must be a string.');
    end
    
    %% Create SQL query:
    sqlquery = ['CALL get_constraints_map(''' inConstraintGroup ''');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try
        outConstraintList = fetch(conn, sqlquery);
        if ~isempty(outConstraintList)
            outConstraintList = set(outConstraintList, 'VarNames', {'id','dataName'});
        else
            outConstraintList = dataset({cell(1,2), 'id', 'dataName'});
        end
    catch ME
        outConstraintList = dataset(); % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
        error('dal_portfolio_get:get_constraints_map:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);
    
end

