function [ result ] = update_creation_date(account_id,owner_id , manager_id, date2change)
%UPDATE_CREATION_DATE update creation date of an account 
%
%   [ result ] = update_creation_date(account_id , manager_nsin, date2change) - 
%                     update the creation date of an account.
%                    
%                      
%   Input:
%       account_id - a value specifying the account id.
%       owner_id - a value specifying the owner id which the owner of the
%       account.
%       manger_id - is an integer specifying the manager id who is in
%                               charge of the account
% 
%       date2change - it is a datetime string specifying the date to change
%                               the creation date to.
%       
%       
%   Output:
%             result - 1 for success or 2 for failed.
%               
%16/04/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

   %% Input validation:
    error(nargchk(4, 4, nargin));
    
      
     if(isempty(manager_id))
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'manager_nsin can not be empty');
     end
     if(isempty(account_id))
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'account_id can not be empty');
     end
      if(isempty(owner_id))
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'owner_nsin can not be empty');
     end
     if ~isnumeric(manager_id)
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'wrong input type for manager nsin');
     end
      if ~isnumeric(account_id)
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'wrong input type for account_id nsin');
      end
     if ~isnumeric(owner_id)
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'wrong input type for owner_nsin nsin');
     end

     %% Step #1 (SQL sqlquery for this filter):
    sqlquery = ['CALL sp_port_update_creation_date( ' num2str(account_id) ',' num2str(owner_id) ',' num2str(manager_id) ',''' date2change ''');'];

   
    %% Step #2 (Execution of built SQL sqlquery):
     setdbprefs ('DataReturnFormat','dataset');                                                     
   
    conn = mysql_conn('portfolio');
    result = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    
end


