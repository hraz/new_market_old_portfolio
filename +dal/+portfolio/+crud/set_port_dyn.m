function [result,err] = set_port_dyn( obs_date, account_id , dyn_param )
%SET_PORT_DYN update portfolio on account_id with the input datasets which
%                                       contains dynamic data.
%
%   [result,err]  = set_port_dyn( obs_date, account_id , dyn_param ) - update
%                       portfolio dynamic data with different datasets.
%                      
%   Input:
%       obs_date - is a DATETIME specifying the date of the obsevation.
% 
%       account_id - a value specifying the account to update.
% 
%      dyn_param - is a struct containing datasets that each dataset is to be inserted to a table.
%                           struct fields names hast to be exactly the same as the table names.
%                           possible values are:allocation,model,performance,risk,port_filter_history.
% 
%   Output:
%            result - is a boolean value specifying if the insert was
%                           successful
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%               
% 29/04/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    
    import utility.dataset.*;    

    %% Input validation:
    error(nargchk(3, 3, nargin));

    if(isempty(obs_date))
      error('dal_portfolio_crud:set_port_dyn:wrongInput', ...
        'obs_date can not be empty');
    end

    if isempty(dyn_param)
      error('dal_portfolio_crud:set_port_dyn:wrongInput', ...
        'dyn_param can not be empty');
    end

    if(isempty(account_id))
      error('dal_portfolio_crud:set_port_dyn:wrongInput', ...
        'account_id can not be empty');
    end
    
    if ~isnumeric(account_id)
      error('dal_portfolio_crud:set_port_dyn:wrongInput', ...
        'wrong input type for account');
    end
   
    possible_prop =  { 'allocation','model','performance','risk', 'port_filter_history', 'benchmark_perf' ,'port_deltas'};
    names = fieldnames(dyn_param)';
    
    if isnumeric(obs_date)
        obs_date = {datestr(obs_date, 'yyyy-mm-dd')};
    else
        obs_date = {obs_date};
    end
    
    % %       connecting to database
     setdbprefs ('DataReturnFormat','dataset');                                                     
   
    conn = mysql_conn('portfolio'); 
    set(conn,'AutoCommit','off');
    try
    % %       iterating through datasets in the struct
        for i = 1: length(names)
        % %             names validation
            if ~all(ismember(names(i),possible_prop))
                 error(' Incorrect table name');
            end
        % %             preparing dataset for insert
            obs_date = obs_date(ones(size(dyn_param.(names{i}),1),1));
            account_id = account_id(ones(size(dyn_param.(names{i}),1),1));
            additional_cols = dataset(account_id,obs_date);
            dyn_param.(names{i}) =  [additional_cols , dyn_param.(names{i}) ];
            fastinsert(conn,names{i}, dsnames( dyn_param.(names{i})),  dyn_param.(names{i}));
        end
% %      inserting all datasets to the database
        commit(conn);
        result = true;
        err = '';
    catch ME
        rollback(conn);
        result = false;
        err = ME.message;
    end
    
    %% Step #3 (Close the opened connection):
    close(conn);
        
end
