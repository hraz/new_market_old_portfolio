function [ out  ] = update_port_tables( account_id, tbl_name, parameters, rdate )
%UPDATE_PORT_TABLES - insert account id, rdate and parameters into required
%                                               table.
%   Input:
%       rdate - is a scalar value specifying required date for account.
%       
%       account_id - is an integer specifying the account id to insert.
%
%   Optional Input (possible argument names as fields of the structure and its possible format values):
% 
%       for table account_movements:
%            'transaction_cost' - is a value vector specifying transaction costs.
%            'holding_cost' - is a value specifying holding cost for the account
%            'tax_private' - is a value specifying private tax for the account
%            'tax_corporate' -a value specifying corporate tax for the account
% 
%       for table operation_costs:
%            'transaction_cost' - is a value vector specifying transaction costs.
%            'holding_cost' - is a value specifying holding cost for the account
%            'tax_private' - is a value specifying private tax for the account
%            'tax_corporate' -a value specifying corporate tax for the account
% 
%       for table risk:
%            'return' -
%            'beta' -
%            'non_systematic_risk' -
%            'sharpe' -
%            'volatility' -
%            'kurtosis' -
%            'skewness' -
%            'alpha' -
%            'jensen' -
%            'inf_ratio' -
%            'omega' -
%            'hurst' -
%            'var' -
%            'cond_var' -
%            'margin_var' -
%            'coherent_var' -
%            'alt_inf_ratio' -
%            'debt_statistics' -
% 
%   Output:
%       none.
%
% Yaakov Rechtman
% 18/04/2012

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    error(nargchk(3 ,4, nargin));
    
    if (nargin == 3) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
     if(isempty(account_id))
          error('dal_portfolio_crud:update_operation_costs:wrongInput', ...
            'account_id can not be empty');
    end
  
     if ~isnumeric(account_id)
        error('dal_portfolio_crud:update_operation_costs:wrongInputType', ...
            'Wrong type of account_id.');
     end
     
      possible_tbl =  { 'risk', 'price_clean','account_movements', 'operation_costs'};
     if ~all(ismember(tbl_name,possible_tbl))
          error(' Incorrect table name');
     end
        %% Step #1 (Input parsing):
    p = inputParser;    % Create an instance of the class:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
    p.addParamValue('transaction_cost', NaN, @(x)(~isempty(x) || isnumeric(x) || (size(x) == 1)));
    p.addParamValue('holding_cost', NaN, @(x)(~isempty(x) || isnumeric(x) || (size(x) == 1)));
    p.addParamValue('tax_private', NaN, @(x)(~isempty(x) || isnumeric(x) || (size(x) == 1)));
    p.addParamValue('tax_corporate', NaN, @(x)(~isempty(x) || isnumeric(x) || (size(x) == 1)));
    

    % Parse method to parse and validate the inputs:
    p.StructExpand = true; % for input as a structure
    p.KeepUnmatched = true;

    try
         p.parse(parameters);
    catch ME
        newME = MException('dal_portfolio_crud:update_operation_costs:optionalInputError',...
                'Error in input arguments');
        newME = addCause(newME,ME);
        throw(newME)
    end

   
    
    %% Step #4 (SQL query for this filter):
    
    sqlquery = ['CALL sp_port_update_' tbl_name '(' num2str(account_id) ',''' rdate ''', ' ...
                                                         num2str(p.Results.transaction_cost) ', '  ...
                                                         num2str(p.Results.holding_cost) ', ' ...
                                                         num2str(p.Results.tax_private) ', ' ...
                                                         num2str(p.Results.tax_corporate) ');'];
    
    %% Step # 5 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','cellarray');    
    conn = mysql_conn('portfolio');
    
    try
    fetch(conn, sqlquery);
    catch ME
        newME = MException('dal_portfolio_crud:update_operation_costs:optionalInputError',...
                'Error in inserting to database');
        newME = addCause(newME,ME);
        throw(newME)
    end
    
    %% Step #6 (Close the opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');                                                     
      
end
