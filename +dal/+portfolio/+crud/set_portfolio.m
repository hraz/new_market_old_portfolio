function [result, err_msg, account_id] = set_portfolio( obs_date, account_id , dyn_param, is_first, varargin )
%SET_PORTFOLIO inserting portfolio dynamic and static data to the database
% 
%   [result,err_msg,account_id_new] = set_portfolio(  obs_date, account_id_1 , dyn_param,  is_first, varargin ) - update
%                       portfolio dynamic data with different datasets.
%                      
%   Input:
%       obs_date - is a DATETIME specifying the date of the obsevation.
% 
%       account_id - a value specifying the account to update.
% 
%      dyn_param - is a struct containing datasets that each dataset is to be inserted to a table.
%                           struct fields names hast to be exactly the same as the table names.
%                           possible values are:allocation,model,performance,risk,port_filter_history.
% 
%       is_first - is a boolean value specifying if it is the first insert for account id of
%                           cumulative income history.
% 
%	Optional Input:
%       static - is a dataset containg static data
%
%       income - is a dataset containing cumulative income history.
% 
%   Output:
%       result - is a boolean value specifying if the insert was
%                           successful
%       err_msg- is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%       account_id - a value specifying the last inserted
%                                   account id.
%               
% 04/09/2012
% Yaakov Rechtman
% Copyright 2012 BondIT

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    
    import utility.dataset.*;    

    %% Input validation:
    error(nargchk(4, 6, nargin)); %Changed first number to 4, H
    
    if(isempty(obs_date))
        error('dal_portfolio_crud:set_portfolio:wrongInput', 'obs_date can not be empty');
    end

    if isempty(dyn_param)
        error('dal_portfolio_crud:set_portfolio:wrongInput', 'dyn_param can not be empty');
    end

%     if(isempty(account_id_1))
%         error('dal_portfolio_crud:set_portfolio:wrongInput', 'account_id_1 can not be empty');
%     end

%     if ~isnumeric(account_id_1)
%         error('dal_portfolio_crud:set_portfolio:wrongInput', 'wrong input type for account_id_1');
%     end

    possible_prop =  { 'allocation','model','performance','risk', 'port_filter_history', 'benchmark_perf' ,'port_deltas'};
    names = fieldnames(dyn_param)';

    if isnumeric(obs_date)
        obs_date = datestr(obs_date, 'yyyy-mm-dd HH:MM:SS');
    end
    obs_date = {obs_date};
    
    %% Step #1 (Input parsing):
    p = inputParser;    % Create an instance of the class_name:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
    p.addParamValue('static', dataset(), @(x)( isa(x,'dataset')));
    p.addParamValue('income', dataset(), @(x)( isa(x,'dataset')));
   
    % Parse method to parse and validate the inputs:
    p.StructExpand = false; %true; % for input as a structure
    p.KeepUnmatched = true;

    %% Step #2 (MATLAB puts the results of the parse into a property named Results):
    p.parse(varargin{:});
    input = p.Results;
    var = p.Parameters;
    
    %% Step #3 (Connecting to database):
     setdbprefs ('DataReturnFormat','dataset');                                                     
   
    conn = mysql_conn('portfolio'); 
%     set(conn,'AutoCommit','off');
    try
         % insert static dataset if exists
        if ismember('static', var) && ~isempty(input.static)
            fastinsert(conn,'port_list' , dsnames(input.static), input.static)
            sqlquery = 'SELECT LAST_INSERT_ID() AS account_id';
            result = fetch(conn, sqlquery);
            account_id = result.account_id;
        end
        
        % Iterating through datasets in the struct
        for l = 1: length(names)
            % Names validation
            if ~all(ismember(names(l), possible_prop))
                 error(' Incorrect table name');
            end
            
            % Preparing dataset for insert
            dyn_param.(names{l}) =  [dataset(account_id(ones(size(dyn_param.(names{l}),1),1)), obs_date(ones(size(dyn_param.(names{l}),1),1)), ...
                                                    'VarNames', {'account_id', 'obs_date'}), dyn_param.(names{l}) ];
            fastinsert(conn, names{l}, dsnames( dyn_param.(names{l})),  dyn_param.(names{l}));
        end
       
        % Insert cumulative income history dataset if exists
        if ismember('income',var) && ~isempty(input.income)
            if is_first
                input.income =  [dataset(account_id(ones(size(input.income,1),1),1), 'VarNames', {'account_id'}) , input.income ];
                fastinsert(conn,'port_income_history', dsnames( input.income),  input.income);
            else
                % If not the first insert, update the first row
                first_obs_date = input.income{1,1};
                value =  input.income{1,2};
                where_query = ['WHERE obs_date = ''',first_obs_date , ''' AND account_id = ', num2str(account_id) ];
                update(conn, 'port_income_history', {'value'}, {value}, where_query);
        
                % Insert the rest of the rows
                 if~isempty(account_id)
                    input.income =  input.income(2:end, :); % removing data at last saved observation because extra payment (if the observation between ex-date and payment date, and we are entitled to get it)
                    input.income =  [dataset(account_id(ones(size(input.income,1),1), :), 'VarNames', {'account_id'}) , input.income ];
                    fastinsert(conn,'port_income_history', dsnames( input.income),  input.income);
                 end 
            end
        end
        
        % Inserting all datasets to the database
%         commit(conn);
        result = true;
        err_msg= '';
    catch ME
        rollback(conn);
        result = false;
        err_msg= ME.message;
    end
    
    %% Step #4 (Close the opened connection):
    close(conn);

end

