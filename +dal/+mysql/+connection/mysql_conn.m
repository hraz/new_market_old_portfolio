function [ conn, err_msg ] = mysql_conn( dbname )
%MYSQL_CONN create connection to mysql database.
%
%   [ conn, err_msg ] = mysql_conn( dbname )
%   receives database name as an optional input and return connection if it was 
%   created or empty array if it wasn't. The default database is market (not 
%   required input).
%
%   Input:
%       dbname - is a string specifying the database name like
%                'test_db'.
%
%   Output:
%       conn - is a database connection object.
%
%       err_msg - is a string specifying the error message in case of
%                      connection failed.

% Author: Yigal Ben Tal
% Copyright 2011-2013
% Updated by: Yigal Ben Tal, 31-01-2013
% Reason: Was added a possibility to change access port; were updated comments and
%               function documentation.
% Updated by: Yigal Ben Tal, 18-06-2013
% Reason: Were removed new not needed and not described code line.


    %% Import external packages:
    import dal.txt.import.*;
    
    %% Input validation:
    error(nargchk(0, 1, nargin));
        
    %% Default values declaration:
    Drv = 'com.mysql.jdbc.Driver';
    
    %% CONNECTION PROPERTIES ARE READED FROM DBCONN.CNF FILE %%
    err_msg = '';
    if exist('dbconn.cnf', 'file') 
        
        conn_prop = read2cell( 'dbconn.cnf' , 'r',  'n', '', '%s%s%s%s%s', '\t' );
        
        if (nargin == 0) % Default - market database
            Host = conn_prop.host{1};
            Port = conn_prop.port{1};
            DbName = conn_prop.database{1};
            Usr = conn_prop.user{1};
            Pwd = conn_prop.password{1};
        elseif (nargin == 1) && strcmpi(dbname, 'portfolio')% Portfolio database
            Host = conn_prop.host{2};
            Port = conn_prop.port{2};
            DbName = conn_prop.database{2};
            Usr = conn_prop.user{2};
            Pwd = conn_prop.password{2};
        end
                    
    else
        error('dal_mysql_connection:mysql_conn:mismatchInput', 'There is no dbconn.cnf file in the working directory.');
    end
  
    URL = ['jdbc:mysql://' Host ':' Port '/' DbName];
    
    %% Step #1 (Set time allowed to establish database connection):
    logintimeout('com.mysql.jdbc.Driver', 5); %max allowed time = 5sec
    conn = database(DbName, Usr, Pwd, Drv, URL);

    %% Step #2 (Check that the database connection status is successful):
    if (~isconnection(conn))
        err_msg = ['Connection failed. ', err_msg];
    else
        err_msg = 'Connection successful';
    end
      
end
