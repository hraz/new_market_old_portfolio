function [ result ]= get_func(c,spcall,inarg,typeout)
%GET_FUNC executes stored functions
%   [ result ] = GET_FUNC(C,SPCALL,INARG,TYPEOUT) calls a stored function given
%   input parameters and returns output parameters. 
% 
%   Input:
%       c -connection object.
%       spcall - a string specifying th function's name.
%       inarg - a cell array with the with string argument to pass the
%                    function.
%       typeout - the type of output parameter. like :
%                        java.sql.Types.INTEGER for integer
% 
%   Output:
%              result - the return value from the database function.      
% 
% 16/04/2012
% Yaakov Rechtman

    import dal.mysql.connection.*;

    %% Input validation:
    error(nargchk(4, 4, nargin));   
    if(isempty(c))
      error('dal_mysql_data_access:get_func:wrongInput', ...
        'c can not be empty');
    end

    if(isempty(spcall))
      error('dal_mysql_data_access:get_func:wrongInput', ...
        'spcall can not be empty');
    end
    if(isempty(inarg))
        error('dal_mysql_data_access:get_func:wrongInput', ...
            'inarg can not be empty');
    end

    if(isempty(typeout))
        error('dal_mysql_data_access:get_func:wrongInput', ...
            'typeout can not be empty');
    end

    %% Get JDBC connection Handle
    h = c.Handle;

    %Get Database name
    dMetaData = dmd(c);
    sDbName = get(dMetaData,'DatabaseProductName');

    %Build stored function call
    spcall = ['{? = call ' spcall '(?'];
    for i = 1:numel(inarg) -1
        spcall = [spcall ',?'];
    end
    spcall = [spcall ')}'];

    %Create callable statement
    csmt = h.prepareCall(spcall);
    %Register output parameters
    csmt.registerOutParameter(1,typeout);

    %   set input parameters
    for i = 2:length(inarg)+1
      if isnumeric(inarg{i-1})
          tmp =  inarg{i-1} ; 
      else
          tmp = ['' inarg{i-1} '']; 
      end
      csmt.setObject(i, tmp);
    end

    try  
        csmt.execute;
        result = csmt.getObject(1);
    catch exception 
        error(message('database:runstoredprocedure:returnedResultSet', exception.message));
    end

    %Close callable statement
    close(csmt)
end

