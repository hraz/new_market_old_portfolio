function [ ds ] = dsrename( ds, newheader )
%DSRENAME rename headers in given dataset.
%
%   [ ds ] = dsrename( ds, newheader ) receive dataset object and new headers,
%   and returns dataset with renamed header.
%
%   Inputs:
%       ds - it is a dataset object. See description about DATASET function in Statistic Toolbox. 
%
%       newheader - is an 1xNHEADERS cell array specifying the new dataset
%                              headers.
%
%   Outputs:
%       ds - it is a dataset object. See description about DATASET function in Statistic Toolbox. 
%
% Yigal Ben Tal
% Copyright 2011.
    
    %% Input validation:
    error(nargchk(2, 2, nargin));
    
    if (~isa(ds, 'dataset'))
        error('dal_txt_tools:dsrename:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~iscellstr(newheader))
        error('dal_txt_tools:dsrename:wrongInput', 'The second input argument is not a cell array of strings');
    end
    
    if (length(newheader) ~= size(ds, 2))
        error('dal_txt_tools:dsrename:wrongInput', 'The number of new headers is not equal to number of dataset variables.');
    end
    
    %% Step #1 (Rename headers of given dataset variables):
    [ ds ] = set(ds, 'VarNames', newheader);
    
end

