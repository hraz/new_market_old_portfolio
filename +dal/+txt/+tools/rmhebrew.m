function [ ds ] = rmhebrew( ds )
%RMHEBREW remove data in hebrew from given dataset.
%
%   [ ds ] = rmhebrew( ds ) recieve dataset and return the same dataset without
%   data in hebrew.
%
%   Inputs:
%       ds - it is a dataset object. See description about DATASET function in Statistic Toolbox.
%
%   Outputs:
%       ds - it is a dataset object. See description about DATASET function in Statistic Toolbox.
%
% Yigal Ben Tal
% Copyright 2011.
    
    %% Input validation:
    error(nargchk(1, 1, nargin));
    if (~isa(ds, 'dataset'))
        error('dal_txt_tools:rmhebrew:wrongInput', 'The input argument must has a dataset type');
    end
    
    %% Step #1 (Collect headers from dataset):
    [ header ] = get(ds, 'VarNames');
    
    %% Step #2 (Looking for non-Hebrew data column indices):
    [ rmdata ] = strfind(header, 'Hebrew');
    rmdataL = length(rmdata);
    nonHebDataIndex = zeros(1, rmdataL);
    for i = 1 : rmdataL
        nonHebDataIndex(i) = i * ( isempty( rmdata{i} ) );
    end
    nonHebDataIndex = nonHebDataIndex( nonHebDataIndex ~= 0 );
    
    %% Step #3 (Remove Hebrew data from given dataset):
    ds = ds(:, header(nonHebDataIndex));
    
end

