import dal.txt.import.*;

file = 'dbconn.cnf';
permission = 'r';
machine_format = 'n';
encoding = '';
data_format = '%s%s%s%s';
delimiter = '\t';

tic
[ data ] = read2cell( file, permission, machine_format, encoding, data_format, delimiter )
toc
