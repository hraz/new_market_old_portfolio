function [ bond_rank_ds, maalot_ds, midroog_ds ] = bond_rank( source_ds, all_ind, source_path )
%BOND_RANK create dataset for tables bond_rank, bond_rank_maalot, bond_rank_midroog.
%    
%   [ bond_rank_ds, maalot_ds, midroog_ds ] = bond_rank( source_ds, all_ind ) 
%   receives source dataset and vector secifying the rows with unique
%   bond_uuid, and return required datasets.
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       all_ind - it is an NOBSERVATIONSx1 vector specifying indices of unique
%                           values with given field name in source dataset.
%
%   Outputs:
%       bond_rank_ds - it is a dataset object specifying the bond_rank table.
%
%       maalot_ds - it is a dataset object specifying the bond_rank_maalot table.
%
%       midroog_ds -  - it is a dataset object specifying the bond_rank_midroog table.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(3, 3, nargin));
    
    if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bond_rank:wrongInput', 'Input argument has not a dataset type');
    end
    
    if (~isnumeric(all_ind))
        error('dal_txt_rebuild:bond_rank:wrongInput', 'All_ind argument is not a numerical vector.');
    end
    
    if (size(source_ds, 1) ~= size(all_ind, 1))
        error('dal_txt_rebuild:bond_rank:wrongInput', 'The dimenion of all_ind argument is not equal to source_db one.');    
    end
    
    if (~ischar(source_path))
        error('dal_txt_rebuild:bond_rank:wrongInput', 'The last argument is not a string.');    
    end
    
    %% Step #1 (Create dataset for bond_rank_maalot table):
    [ maalot_ds ] = bond_ranks( fullfile(source_path, 'rank_maalot.txt') );
    
    %% Step #2 (Create dataset for bond_rank_midroog table):
    [ midroog_ds ] = bond_ranks( fullfile(source_path, 'rank_midroog.txt' ) );
    
    %% Step #3 (Create dataset for bond_rank table):
    % Simulation AUTO_INCREMENT key:
    rank_id = zeros(length(all_ind), 2);
    
    % Replacing empty ranks by 'NR' - Non-Ranked:
    source_ds.rank_maalot(cellfun('isempty', source_ds.rank_maalot) == true) = {'NR'};
    source_ds.rank_midroog(cellfun('isempty', source_ds.rank_midroog) == true) = {'NR'};
        
    % Simulation foreign key in table bond_rank:
    for i = 1 : length(maalot_ds)
        rank_id(~cellfun('isempty',strfind( source_ds.rank_maalot, maalot_ds.local_rank{i})), 1) = i;
    end
    for i = 1 : length(midroog_ds)
        rank_id(~cellfun('isempty',strfind( source_ds.rank_midroog, midroog_ds.local_rank{i})), 2) = i;
    end
    
    % Filtration of source ranks by its change values:
    ranks = [uint32(all_ind), rank_id];
    row_rank_change = ones(size(ranks, 1),1);
    for i = 2:size(ranks, 1)
        row_rank_change(i) = all(ranks(i,:) ~= ranks(i-1,:));
    end
    row_rank_change = find(row_rank_change ~=0);
    ranks = ranks(row_rank_change, :);
    
    % Formatting dates to numeric format:
    date = uint32(datenum(source_ds.date(row_rank_change), 'yyyy-mm-dd'));
    
    % Bond_rank dataset creation:
    bond_id = ranks(:,1);
    maalot_rank_id = ranks(:,2);
    midroog_rank_id = ranks(:,3);
    bond_rank_ds = dataset( bond_id, date, maalot_rank_id, midroog_rank_id);  
    
end

function [ target_ds ] = bond_ranks( source_file )
%BOND_RANKS create dataset according to needed rank.
%
%   [ target_ds, old_ind ] = bond_ranks( source_ds, old_field_name )
%   receive source dataset and field name and return needed dataset
%   according to given field name. 
%
%   Inputs:
%       source_ds - it is a dataset object. See description about DATASET
%                            function in Statistic Toolbox. 
%
%       old_field_name - it is a string specifying the needed unique filter.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
%       old_ind - it is an NOBSERVATIONSx1 vector specifying indices of unique
%                           values with given field name in source dataset
%                           SOURCE_DS = TARGET(old_ind).
%
% Yigal Ben Tal
% Copyright 2011.   
    
    %% Import external packages:
    import dal.txt.import.*;
    
    %% Input validation:
    error(nargchk(1, 1, nargin));
    
    if (~ischar(source_file))
        error('dal_txt_rebuild:tools:bond_ranks:wrongInput', 'The input argument is not a string.');
    end
    
    %% Step #1 (New dataset builder):
    [ target_ds ] = read2dataset( source_file, '%s', '\t', true);
    
end