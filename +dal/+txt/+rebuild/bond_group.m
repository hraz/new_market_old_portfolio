function [ target_ds, old_ind ] = bond_group( source_ds, field_name )
%BOND_GROUP create dataset for table bond_group.
%
%   [ target_ds, old_ind ] = bond_group( source_ds, field_name )
%   receive source dataset and field names and return dataset according to
%   given field name. 
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       field_name - it is a string specifying the needed unique filter.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
%       old_ind - it is an NOBSERVATIONSx1 vector specifying indices of unique
%                           values with given field name in source dataset
%                           SOURCE_DS = TARGET(old_ind).
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(2, 2, nargin));
    
    if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bond_group:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~ischar(field_name))
        error('dal_txt_rebuild:bond_group:wrongInput', 'The second input argument is not a string.');
    end
    
    if (~isempty(source_ds)) && (isempty(strfind(fieldnames(source_ds), field_name)))
        error('dal_txt_rebuild:bond_group:wrongInput', 'There is no such field in given dataset.');
    end
    
    %% Step #1 (New dataset builder):
    if (~isempty(source_ds))
        [ target_ds, ~, old_ind ] = unique( source_ds(:, field_name) );
    else
        target_ds = source_ds;
        old_ind = [];
    end
    
end
