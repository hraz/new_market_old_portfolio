clear; clc;

tic
%%
file = 'D:\Database\raw_source\Bond.txt';
Permission = 'r';
mashine_format = 'n';
encoding = 'UTF-8';
bond_data_format = '%d%s%s%s%s%s%s%s%s%s%s%f%f%d%f%f%f%s%s%f%f%d';
delimiter = '\t';
raw_dataset = dataset('file', file, 'format', bond_data_format, 'Delimiter', delimiter, 'ReadVarNames', true);
header = get(raw_dataset, 'VarNames');

%%
[ remData ] = strfind(header, 'Hebrew'); remDataL = length(remData);
notHebDataCol = zeros(1, remDataL);
for i = 1:remDataL
    notHebDataCol(i) = i * (isempty(remData{i}));
end
notHebDataCol = notHebDataCol(notHebDataCol ~= 0);
raw_dataset =raw_dataset(:, header(notHebDataCol));
header = get(raw_dataset, 'VarNames');
new_header = {'tase_uuid', 'bond_name', 'sector_name', 'group_name', 'linkage', 'coupon_type', 'issue_date', 'date', 'value', 'value_change', 'turnover', 'ytm_bruto', 'ytm_neto', 'duration', 'rank_maalot', 'rank_midroog', 'market_cap', 'convexity', 'issuer_id'};
raw_dataset = set(raw_dataset, 'VarNames', new_header);

%%
[tbl.bond_type, ~, group_ind] = unique(raw_dataset(:, 'group_name'));
[tbl.bond_sector, ~, sector_ind] = unique(raw_dataset(:, 'sector_name'));

%%
cmn_header = {'tase_uuid', 'bond_name', 'linkage', 'coupon_type', 'issue_date', 'issuer_id'};
[tbl.bond_cmn, last_ind, all_ind] = unique(raw_dataset(:, cmn_header), 'tase_uuid');
Leng = length(last_ind);
tbl.bond_cmn = replacedata(tbl.bond_cmn, strcat({'IL'}, num2str(tbl.bond_cmn.tase_uuid)), 'tase_uuid'); 
tbl.bond_cmn = replacedata(tbl.bond_cmn, datenum(tbl.bond_cmn.issue_date, 'yyyy-mm-dd'), 'issue_date'); 

cmn_bond_id = uint16([1 : Leng]');
symbol = strcat({'S'}, tbl.bond_cmn.tase_uuid);
sector_id = uint8(sector_ind(last_ind));
group_id = uint8(group_ind(last_ind));
for i = 1 : Leng
    if ~isempty(tbl.bond_cmn.linkage{i})
        base_cpi_curr(i, 1) = rand / 10;
        base_cpi_curr_date(i, 1) = tbl.bond_cmn.issue_date(i);
    else
        tbl.bond_cmn.base_cpi_curr(i, 1) = NaN;
        tbl.bond_cmn.base_cpi_curr_date(i, 1) = NaN;
    end
end
notional_amount = 100*ones(Leng, 1);
redemption_value =  99 * ones(Leng, 1);
for i = 1:Leng
    first_coupon_date(i, 1) = uint32(addtodate(double(tbl.bond_cmn.issue_date(i)), 1, 'year'));
    last_coupon_date(i, 1) = uint32(addtodate(double(tbl.bond_cmn.issue_date(i)), round(rand)+1, 'year'));
end
 last_coupon_rate = rand([Leng, 1])/10;
 coupon_frequency = ones(Leng, 1);
 compounding_frequency = ones(Leng, 1);
 discount_basis = 0.5*ones(Leng, 1);
for i = 1:Leng
    start_date(i, 1) = uint32(addtodate(double(tbl.bond_cmn.issue_date(i)), 1, 'day'));
end
maturity_date = last_coupon_date;
end_month_rule = uint8(ones(Leng, 1));

bond_cmn1 = dataset(symbol, sector_id, group_id, base_cpi_curr, base_cpi_curr_date, notional_amount, redemption_value, first_coupon_date, last_coupon_date, last_coupon_rate, coupon_frequency, compounding_frequency, discount_basis, start_date, maturity_date, end_month_rule);
tbl.bond_cmn = horzcat(tbl.bond_cmn, bond_cmn1); clear tbl.bond_cmn1;

cmn_header = { 'tase_uuid','bond_name', 'symbol', 'sector_id', 'group_id', 'issuer_id', 'issue_date', 'linkage', 'base_cpi_curr', ...
                            'base_cpi_curr_date', 'notional_amount', 'redemption_value', 'coupon_type', 'first_coupon_date', 'last_coupon_date',...
                            'last_coupon_rate', 'coupon_frequency', 'compounding_frequency', 'discount_basis', 'start_date', 'maturity_date',...
                            'end_month_rule'};
tbl.bond_cmn = tbl.bond_cmn(:, cmn_header);

%%
daily_bond_id = uint16(all_ind);
mod_duration =raw_dataset.duration;
eff_duration = raw_dataset.duration * 0.9;
cap_listed_4_trade = raw_dataset.market_cap *0.9;
daily_header = {'date', 'value', 'value_change', 'turnover', 'ytm_bruto', 'ytm_neto', 'market_cap', 'convexity'};
tbl.bond_daily = horzcat( set(dataset(daily_bond_id, mod_duration, eff_duration, cap_listed_4_trade), 'VarNames', {'bond_id', 'mod_duration', 'eff_duration', 'cap_listed_4_trade'}), raw_dataset(:, daily_header) );
daily_header = {'bond_id', 'date', 'value', 'value_change', 'ytm_bruto', 'ytm_neto', 'mod_duration', 'eff_duration', 'convexity','market_cap', 'turnover', 'cap_listed_4_trade'};
tbl.bond_daily = tbl.bond_daily(:, daily_header);
tbl.bond_daily.date = datenum(tbl.bond_daily.date, 'yyyy-mm-dd');
tbl.bond_daily.value_change = tbl.bond_daily.value_change / 100;
tbl.bond_daily.ytm_bruto = tbl.bond_daily.ytm_bruto / 100;
tbl.bond_daily.ytm_neto = tbl.bond_daily.ytm_neto / 100;

%%
[coupon_type, ~, coupon_ind] = unique(raw_dataset(:, 'coupon_type'));

fixed_bond_id = cmn_bond_id( coupon_ind(last_ind) == find(~cellfun('isempty',strfind(coupon_type.coupon_type, 'FIX'))));
coupon_rate = rand(length(fixed_bond_id), 1) / 10;
tbl.bond_fixed_coupon = set(dataset(fixed_bond_id, coupon_rate), 'VarNames', {'bond_id', 'coupon_rate'});

float_bond_id = cmn_bond_id( coupon_ind(last_ind) == find(~cellfun('isempty',strfind(coupon_type.coupon_type, 'VAR'))));
floating_index = rand(length(float_bond_id), 1) / 10;
floating_spread = rand(length(float_bond_id), 1) / 10;
tbl.bond_float_coupon = set(dataset(float_bond_id, floating_index, floating_spread), 'VarNames', {'bond_id', 'floating_index', 'floating_spread'});
    
%%
[bond_rank_maalot, ~, maalot_ind] = unique(raw_dataset(:, 'rank_maalot'));
maalot_rank_id = uint32([1:length(bond_rank_maalot)]);

[bond_rank_midroog, ~, midroog_ind] = unique(raw_dataset(:, 'rank_midroog'));
midroog_rank_id = uint32([1:length(bond_rank_midroog)]);

%%
[tbl.bond_rank, m] = unique([uint32(all_ind), maalot_rank_id(maalot_ind)', midroog_rank_id(midroog_ind)'], 'rows');
rank_bond_id = tbl.bond_rank(:,1);
date = uint32(datenum(raw_dataset.date(m), 'yyyy-mm-dd'));
maalot_rank_id = tbl.bond_rank(:,2);
midroog_rank_id = tbl.bond_rank(:,3);
tbl.bond_rank = set(dataset(rank_bond_id, date, maalot_rank_id, midroog_rank_id), 'VarNames', {'bond_id', 'date', 'maalot_rank_id', 'midroog_rank_id'});    

%%
tbl_name = fieldnames(tbl);
tbl_num = length(tbl_name);
for i =1:tbl_num
    export(tbl.(tbl_name{i}), 'file', fullfile('D:\Database\db_source\', [tbl_name{i} '.txt']), 'WriteVarNames', true);
end
toc