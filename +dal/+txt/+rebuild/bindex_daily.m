function [ target_ds ] = bindex_daily( source_ds, old_field_name, new_field_name, all_ind )
%BINDEX_DAILY create a dataset for bond_index_daily table.
%
%   [ target_ds ] = bindex_daily( source_ds, old_field_name, new_field_name, all_ind )
%   receive source dataset, list of old field names, list of needed field
%   names and all old row indices according to common id of bond indices, return
%   the dataset for bond_index_daily table.
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       old_field_name - it is an 1xMFIELDS cell array specifying the
%                            exist fields.
%
%       new_field_name - it is an 1xKFIELDS cell array specifying the
%                           required fields.
%
%       all_ind - it is an NOBSERVATIONSx1 vector specifying the raw data
%                           rows' indices according to common index_uuid.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(4, 4, nargin));
    
     if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bindex_daily:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~iscellstr(old_field_name)) || (~iscellstr(new_field_name))
        error('dal_txt_rebuild:bindex_daily:wrongInput', 'At least one of old or new field names is not a cell of strings.');
    end
    
    if (~isnumeric(all_ind))
        error('dal_txt_rebuild:bindex_daily:wrongInput', 'All_ind argument is not numerical vector.');
    end
    
    if (size(source_ds, 1) ~= size(all_ind, 1))
        error('dal_txt_rebuild:bindex_daily:wrongInput', 'The dimenion of all_ind argument is not equal to source_db one.');    
    end

    %% Step #1 (Absent data simulation):
    index_id = uint32(all_ind);
    convexity = 100 * rand(size(all_ind));
    turnover = round(10000000 * rand(size(all_ind)));
    cap_listed_4_trade = source_ds.market_cap * 0.9;
    
    %% Step #2 (Creation bond_index_daily dataset):
    target_ds = horzcat(source_ds(:, old_field_name), dataset(index_id, convexity, turnover, cap_listed_4_trade));
    target_ds = target_ds(:, new_field_name);
    
    %% Step #3 (Format dates to numeric format):
    target_ds.date = uint32(datenum(target_ds.date, 'yyyy-mm-dd'));
    
    %% Step #4 (Translate percents to decimal fraction):
    target_ds.value_change = target_ds.value_change / 100;
    target_ds.yield = target_ds.yield / 100;
    
end

