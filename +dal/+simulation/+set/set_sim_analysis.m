function [outStatus, outException] = set_sim_analysis( dsErrSummary )
%SET_SIM_ANALYSIS insert solver analisys to the database
%
%   [outStatus, outException]  = set_sim_analysis( dsErrSummary )
%
%   Input:
%       dsErrSummary -  it is a  nX15 dataset specifying data about the solver
%                             portfolioID - simulated portfolio ID
%                             callNum -  the number of call that was made to any of the solver function in the current
%                                               portfolio
%                             errFlag - matlab exit flag (positive - solver was successful, negative - solver faild)
%                             iterations - number of iteration done by the solver
%                             constrViolation - size of constraint violation
%                             algo - name of used algorithm in the current solver 
%                             cgIterations -  total value of PCG iterations
%                             msg - exit message
%                             firstOrderOpt - measure of first order optimality                             
%                             firstExcessDuration - unknown,
%                             secondExcessDuration - unknown
%                             infeasibleNonDurationConstStart - boolean variable which indicates whether the initial
%                                               weights are infeasible in the sense that one of the non-duration inequality constraints
%                                               doesn't hold. 
%                             infeasibleDurationConstStart - same as above, for the duration constraints.
%                             infeasibleNonDurationConstFinish - same as above, for the final weights returned from the solver
%                             infeasibleDurationConstFinish - same as above, for the final weights from the solver
%                           
%	Output:
%       outStatus - is a boolean value specifying if the insert was successful.
%
%       outException - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%
% Created by Yaakov Rechtman
% Date:		20.06.2012
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by:	Yigal Ben Tal, 
% at 16.07.2013. 
% The sense of update: remove saved data to independed simulation database.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import utility.dataset.*;

    %% Input validation: 
    outStatus = true;
    outException = '';
    
    try
        error(nargchk(1, 1, nargin));
        if(isempty(dsErrSummary))
              error('dal_simulation_set:set_sim_analysis:wrongInput', ...
                'dsErrSummary cannot be an empty.');
        end

        %% Step #1 (Execution of built SQL sqlquery):
        setdbprefs ('DataReturnFormat','dataset');                                                     
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_set:set_run_time:connectionFailure', errMsg);
        end

        %% Insert data ino database:
        fastinsert(conn,'summary' , dsnames(dsErrSummary), dsErrSummary);
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
        outException = ME.message;
        outStatus = false;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            rollback(conn);
            close(conn);
        end 
    end
   
end
