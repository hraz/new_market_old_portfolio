function [ outStatus, outException ] = set_run_time( inPortfolioID, inRunTime )
%SET_RUN_TIME insert portfolio run time into the database
%
%   [result,err]  = set_run_time( inPortfolioID, inRunTime )
%   receives portfolio run-time and svae into the database.
% 
%   Input:
%       inPortfolioID - is a INTEGER value specifying the portfolio ID number.
%
%       inRunTime - is a DOUBLE value specifying the portfolio run time in seconds.
%
%   Output:
%       outStatus - is a boolean value specifying if the insert was successful.
% 
%       outException - is a STRING value specifying error message.
%

% Created by Yigal Ben Tal.
% Date:		16.07.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: ______________
%                at: ______________
% The sense of update: _____________________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;

    %% Input validation:
    try
        error(nargchk(2, 2, nargin));

        if (isempty(inPortfolioID) || isnan(inPortfolioID))
            error('dal_simulation_set:set_run_time:wrongInput', 'inPortfolioID cannot be an empty.');
        elseif ~isnumeric(inPortfolioID)
            error('dal_simulation_set:set_run_time:wrongInput', 'inPortfolioID must be a numeric value.');
        end

        if (isempty(inRunTime) || isnan(inRunTime))
            error('dal_simulation_set:set_run_time:wrongInput', 'inRunTime cannot be an empty.');
        elseif ~isnumeric(inRunTime)
            error('dal_simulation_set:set_run_time:wrongInput', 'inRunTime must be a numeric value.');
        end

        %% Step #1 (Data adoption for inserting process):
        ds =  dataset({[inPortfolioID , inRunTime], 'portfolioID', 'runTime'});

        %% Step #2 (Create connection to database):
        setdbprefs ('DataReturnFormat','dataset');                                                     
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_set:set_run_time:connectionFailure', errMsg);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Insert data into database):
 
        fastinsert(conn,'time_running', dsnames( ds),  ds);
        commit(conn);
        
        outStatus = true;
        outException = '';
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
        outStatus = false;
        outException = ME.message;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            rollback(conn);      
            close(conn);
        end

    end

end
