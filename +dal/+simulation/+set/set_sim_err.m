function [outStatus, outException] = set_sim_err( obsDate, portfolioID , dsSimErr )
%SET_SIM_ERR insert simulation outExceptionor to the database
%
%   [outStatus,outException]  = set_sim_err( obsDate, portfolioID , dsSimErr )
%   recives error messages and insert them to the database
%   according to obsDate and portfolioID.
%
%   Input:
%       obsDate - is a DATETIME specifying the date of the obsevation.
%
%       portfolioID - is an INTEGER value specifying the simulated portfolio ID.
%
%       dsSimErr -is a dataset containing error message.
%
%   Output:
%       outStatus - is a boolean value specifying if the insert was successful.
%
%       outException - is a STRING value specifying error message if the
%                        insert was unsuccessful, null otherwise.
%
% Created by Yaakov Rechtman.
% Date:		06.08.2012
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by: ______________
%                at: ______________
% The sense of update: _____________________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;

    %% Input validation:
    try
        error(nargchk(3, 3, nargin));

        if(isempty(obsDate))
            error('dat_simulation_set:set_sim_outException:wrongInput', ...
                'obsDate cannot be an empty.');
        elseif isnumeric(obsDate)
            obsDate = {datestr(obsDate, 'yyyy-mm-dd')};
        else
            obsDate = {obsDate};
        end

        if isempty(dsSimErr)
            error('dat_simulation_set:set_sim_outException:wrongInput', ...
                'dsSimErr cannot be an empty.');
        end

        if(isempty(portfolioID))
            error('dat_simulation_set:set_sim_outException:wrongInput', ...
                'portfolioID cannot be an empty.');
        end
        if ~isnumeric(portfolioID)
            error('dat_simulation_set:set_sim_outException:wrongInput', ...
                'portfolioID must be a numeric value.');
        end

        %% Step #1 (Data adoption for inserting process):
        obsDate = obsDate(ones(size(dsSimErr,1),1));
        portfolioID = portfolioID(ones(size(dsSimErr,1),1));
        ds =  [dataset(portfolioID,obsDate) , dsSimErr];
   
        %% Step #2 (Create connection to database):
        setdbprefs ('DataReturnFormat','dataset');  
       [conn, outExceptionMsg] = mysql_conn('simulation');
        if ~isempty(outExceptionMsg)
            error('dal_simulation_set:set_run_time:connectionFailure', outExceptionMsg);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Insert data into database):
        fastinsert(conn,'sim_errors', dsnames( ds ),  ds);
        commit(conn);
        
        outStatus = true;
        outException = '';
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
    
    catch ME
       %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
             rollback(conn);
             close(conn);
        end
        
        outStatus = false;
        outException = ME.message;
    end
    
end
