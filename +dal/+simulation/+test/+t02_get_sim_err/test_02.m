%% Import tested directory:
import dal.simulation.test.t02_get_sim_err.*;

%% Test description:
funName = 'get_sim_errors';
testDesc = 'An empty error message constraint: -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';
expectedResult = [dataset({[1;2], 'portfolioID'}), dataset({{'2010-07-01 12:12:12.0'; '2010-07-01 14:12:12.0'}, 'obsDate'}), ...
                                dataset({{'Test error', 'Test description of the error.';...
                                                'Test error 2', 'Test description of the error 2.'}, ...
                                'errMsg', 'errDescription'})];

%% Input definition:
errMsg = '';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, errMsg );
