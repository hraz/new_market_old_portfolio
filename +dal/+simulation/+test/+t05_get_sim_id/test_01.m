%% Import tested directory:
import dal.simulation.test.t05_get_sim_id.*;

%% Test description:
funName = 'get_sim_id';
testDesc = 'Get existed free sim ID: -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';
expectedResult = 1;

%% Input definition:
minID = 0;
maxID = 5;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, minID, maxID );
