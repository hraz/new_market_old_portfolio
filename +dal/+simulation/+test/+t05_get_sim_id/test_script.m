function [result] = test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, minID,maxID )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.simulation.get.*
    import dal.simulation.test.*;
    
    %% Test execution:
    isIdentical = true;
    status = true;
    errMsg = '';
    try
        [ resultSet ] = get_sim_id( minID,maxID );
        isIdentical = (expectedResult == resultSet);
    catch ME
        errMsg = ME.message;
        status = false;
    end

    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError)) && isIdentical
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
