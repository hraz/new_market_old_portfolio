function [result] = test_script( funName, testDesc, expectedStatus, expectedError, newSimNum, username )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.simulation.del.*
    import dal.simulation.test.*;
    
    %% Test execution:
    [status, errMsg] = clear(username, newSimNum);

    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
