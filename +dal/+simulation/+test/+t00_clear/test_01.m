%% Import tested directory:
import dal.simulation.test.t00_clear.*;

%% Test description:
funName = 'clear';
testDesc = 'Clear simulation database without definition of the new simulation count -> expected error:';

%% Expected results:
expectedStatus = 0;
expectedError = 'newSimNumber cannot be an empty.';

%% Input definition:
newSimNum = [];
username = 'Hillel';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, newSimNum, username);
