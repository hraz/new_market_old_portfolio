function [outResult] = isidentic(expectedData, checkedData)
%ISIDENTIC checks get functions results vs.expected.

    import utility.dataset.*;
    
    if ~isempty(checkedData) & ~isempty(expectedData)
        expDataName = dsnames(expectedData);
        givenDataName = dsnames(checkedData);
       outResult = all(ismember(givenDataName, expDataName));
        for i = 1 : length(expDataName)
            if iscellstr(checkedData.(givenDataName{i})) & iscellstr(expectedData.(expDataName{i}))
                outResult = outResult & all(strcmpi(checkedData.(givenDataName{i}),expectedData.(expDataName{i})));
            elseif iscell(checkedData.(givenDataName{i})) & iscell(expectedData.(expDataName{i}))
                outResult = outResult & all(checkedData.(givenDataName{i}){:} == expectedData.(expDataName{i}){:});
            elseif isnan(checkedData.(givenDataName{i})) & isnan(expectedData.(expDataName{i}))
                outResult = outResult & true;
            else
                outResult = outResult & all(checkedData.(givenDataName{i}) == expectedData.(expDataName{i}));
            end
        end
     elseif isempty(checkedData) & isempty(expectedData)
         outResult = true;
     else
         outResult = false;
     end

end
