%% Import tested directory:
import dal.simulation.test.t06_set_run_time.*;

%% Test description:
funName = 'set_run_time';
testDesc = 'Empty time: -> error message:';

%% Expected results:
expectedStatus = 0;
expectedError = 'inRunTime cannot be an empty.';

%% Input definition:
portfolioID = 1;
runTime = [];

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portfolioID, runTime );
