%% Import tested directory:
import dal.simulation.test.t06_set_run_time.*;

%% Test description:
funName = 'set_run_time';
testDesc = 'Not numerical portfolio ID: -> error message:';

%% Expected results:
expectedStatus = 0;
expectedError = 'inPortfolioID must be a numeric value.';

%% Input definition:
portfolioID = num2str(1);
runTime = 0.2346546;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portfolioID, runTime );
