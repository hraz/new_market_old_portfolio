clear; clc;

%% Import external packages:
import dal.simulation.test.*;

%% Global variables definition:
global FILETYPE
FILETYPE = 'tsv';

%% Collect all folder names of all tested functions:
extraDir = {'.', '..', '+log'}';
extraFile = 'test_script.m';
projDir = fullfile('+dal', '+simulation', '+test');
currDir = fullfile(pwd, projDir);

listing = dir(currDir);
totalFilesDir = {listing.name}';
testDir = totalFilesDir([listing.isdir]);
testDir = testDir(~ismember(testDir, extraDir));

%% Remove old log file:
logFile = fullfile(pwd,projDir, '+log', 'test_log.tsv');
if (exist(logFile, 'file') == 2)
    delete(logFile);
end

%% Collect all tests for each function:
tic
for i = 1: length(testDir)
    testCollection = fullfile(currDir, testDir{i});
    testList = dir(testCollection);
    fileList = {testList.name}';
    testList = fileList(~[testList.isdir]);
    testList = testList(~ismember(testList, extraFile));
    testList = strrep(testList, '.m', '');
    
    for j = 1:length(testList)  
        importDir = [strrep(strrep([projDir, '.',  testDir{i}], '+', ''), '\', '.'), '.', testList{j}];
        eval(importDir)
    end
        
end
toc
