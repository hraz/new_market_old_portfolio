%% Import tested directory:
import dal.simulation.test.t01_set_sim_err.*;

%% Test description:
funName = 'set_sim_err';
testDesc = 'Right input of the error message with its description: -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';

%% Input definition:
obsDate = '2010-07-01 14:12:12';
portfolioID = 2;
dsSimErr = dataset({{'Test error 2', 'Test description of the error 2.'}, 'errMsg', 'errDescription'});

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, obsDate, portfolioID , dsSimErr );
