%% Import tested directory:
import dal.simulation.test.t01_set_sim_err.*;

%% Test description:
funName = 'set_sim_err';
testDesc = 'Empty observation date: -> expected error:';

%% Expected results:
expectedStatus = 0;
expectedError = 'obsDate cannot be an empty.';

%% Input definition:
obsDate = [];
portfolioID = 1;
dsSimErr = dataset({{'Test error', 'Test description of the error.'}, 'errMsg', 'errDescription'});

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, obsDate, portfolioID , dsSimErr );
