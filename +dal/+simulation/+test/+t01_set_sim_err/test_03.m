%% Import tested directory:
import dal.simulation.test.t01_set_sim_err.*;

%% Test description:
funName = 'set_sim_err';
testDesc = 'Empty simID: -> expected error:';

%% Expected results:
expectedStatus = 0;
expectedError = 'portfolioID cannot be an empty.';

%% Input definition:
obsDate = '2010-07-01 12:12:12';
portfolioID = [];
dsSimErr = dataset({{'Test error', 'Test description of the error.'}, 'errMsg', 'errDescription'});

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, obsDate, portfolioID , dsSimErr );
