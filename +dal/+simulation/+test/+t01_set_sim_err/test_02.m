%% Import tested directory:
import dal.simulation.test.t01_set_sim_err.*;

%% Test description:
funName = 'set_sim_err';
testDesc = 'Empty error information: -> expected error:';

%% Expected results:
expectedStatus = 0;
expectedError = 'dsSimErr cannot be an empty.';

%% Input definition:
obsDate = '2010-07-01 12:12:12';
portfolioID = 1;
dsSimErr = dataset();

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, obsDate, portfolioID , dsSimErr );
