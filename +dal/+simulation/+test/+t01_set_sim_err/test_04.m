%% Import tested directory:
import dal.simulation.test.t01_set_sim_err.*;

%% Test description:
funName = 'set_sim_err';
testDesc = 'simID as non numeric value.: -> expected error:';

%% Expected results:
expectedStatus = 0;
expectedError = 'portfolioID must be a numeric value.';

%% Input definition:
obsDate = '2010-07-01 12:12:12';
portfolioID = num2str(1);
dsSimErr = dataset({{'Test error', 'Test description of the error.'}, 'errMsg', 'errDescription'});

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, obsDate, portfolioID , dsSimErr );
