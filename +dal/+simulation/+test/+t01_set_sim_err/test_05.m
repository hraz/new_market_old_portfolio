%% Import tested directory:
import dal.simulation.test.t01_set_sim_err.*;

%% Test description:
funName = 'set_sim_err';
testDesc = 'Right input of the error message with its description: -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';

%% Input definition:
obsDate = '2010-07-01 12:12:12';
portfolioID = 1;
dsSimErr = dataset({{'Test error', 'Test description of the error.'}, 'errMsg', 'errDescription'});

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, obsDate, portfolioID , dsSimErr );
