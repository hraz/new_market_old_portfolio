function [outStatus, outException] = clear( userName, newSimNumber )
%CLEAR_SIM_MONITOR clears all monitoring in the simulation database.
%
%   [result,err]  = clear( newSimNumber ) 
%   Clears all data in the simulation database and update number of simulations.
%
%   Input:
%      newSimNumber - is an INTEGER scalar value specifying the 
%                       required number of simulations that will be done.
%
%       userName - is a string value specifying the cleaner user name.
%
%   Output:
%           outStatus - is a boolean value specifying the success of the operation.
%
%           outException - is a string value specifying the error message. If operation 
%                                            successed exception will be an empty string. 
%
% Created by 	Yigal Ben Tal
% Date:		14.07.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, 
% at ___________. 
% The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
        error(nargchk(1,2, nargin));

        if (nargin == 1) || isempty(newSimNumber)
            error('dal_simulation_del:clear:mismatchInput', 'newSimNumber cannot be an empty.');
        elseif ~isnumeric(newSimNumber)
            error('dal_simulation_del:clear:mismatchInput', 'newSimNumber must be numeric.');
        else
            newSimNumber = num2str(newSimNumber);
        end
        
        if isempty(userName)
            error('dal_simulation_del:clear:mismatchInput', 'userName cannot be an empty.');
        else
            userName = ['"', userName, '"'];
        end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL simulation.clear(',  newSimNumber, ', ', userName ');'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_del:clear:connectionFailure', errMsg);
        end
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):
        fetch(conn, sqlquery);
        if (nargout > 0)
            outStatus = true;
        end

        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end
    
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end    
    end
       
end
