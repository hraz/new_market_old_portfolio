function [ ds ] = get_sim_analysis( errFlag )
%GET_SIM_ANALYSIS  returns solver data according to errFlag
%   [ ds ] = get_sim_analysis( errFlag )
%
%   Input:
%       errFlag - is an INTEGER value specifying some error flag. 
%                       If input is empty the function returns full list of saved data.
%
%   Output:
%       ds - is a dataset with follow fields:
%                           portfolioID - is a simulated portfolio ID
%                           callNum - is a number of call that was made to any of the solver function in the current
%                                               portfolio
%                           errFlag - is a MATLAB exit flag (positive - solver was successful, negative - solver faild)
%                           iterations - is a number of iteration done by the solver
%                           constrViolation - is a size of constraint violation
%                           algo - is a name of used algorithm in the current solver 
%                           cgIterations - is a total value of PCG iterations
%                           msg - is an exit message
%                           firstOrderOpt - is a measure of first order optimality                             
%                           firstExcessDuration - is unknown,
%                           secondExcessDuration - is unknown
%                           infeasibleNonDurationConstStart - is a boolean variable which indicates whether the initial
%                                               weights are infeasible in the sense that one of the non-duration inequality constraints
%                                               doesn't hold. 
%                           infeasibleDurationConstStart - is the same as above, for the duration constraints.
%                           infeasibleNonDurationConstFinish - is the same as above, for the final weights returned from the solver
%                           infeasibleDurationConstFinish - is the same as above, for the final weights from the solver.
%
% Created by Yaakov Rechtman.
% Date:		20.06.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: Yigal Ben Tal
%                at: 16.07.2013. 
% The sense of update: removing data about the simulation into independent database.

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    error(nargchk(0, 1, nargin));
    if (nargin == 0) | (isempty(errFlag))
        errFlag = 'NULL';
    elseif isnumeric(errFlag)
        errFlag = num2str(errFlag);
    end
    
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['CALL simulation.get_summary(', errFlag ,');' ];

    %% Step #2 (Create a connection):
    setdbprefs ('DataReturnFormat','dataset');
    [conn, errFlag] = mysql_conn('simulation');
    if ~isempty(errFlag)
        error('dal_simulation_del:clear:connectionFailure', errFlag);
    end
    
    %% Step #3 (Execution of built SQL sqlquery):
    try
        ds = fetch(conn,sqlquery);
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
        error('dal_simulation_del:clear:connectionFailure', ME.message);
    end
    
end
